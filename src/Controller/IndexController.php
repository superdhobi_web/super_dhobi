<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class IndexController extends AppController {

    public function beforeFilter(Event $event) {
        $this->loadmodel("Clothtypes");
        $this->loadmodel("Pricings");
        $this->loadmodel("Services");
        $this->loadModel('Termsandconditions');
        $this->loadModel('Privacyandpolicies');
        $this->loadModel('Coupons');
        $this->viewBuilder()->layout('indexlayout');
    }

    public function index() {
        $tc = $this->Termsandconditions->find()->first();
        $pv = $this->Privacyandpolicies->find()->first();

        $service_types = $this->Services->find()->where(array('is_deleted' => 0))->all()->toArray();
        $conditions = array('status' => 1, 'is_deleted' => 0, 'cloth_parent' => 0);
        $allcloth = $this->Clothtypes->find()->where($conditions)->all()->toArray();
        foreach ($allcloth as $val) {
            $newcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $val->id);
            $newcloth = $this->Clothtypes->find()->where($newcond)->all()->toArray();
            $i = 0;
            $j = 0;
            foreach ($newcloth as $v) {
                $clothtype[$val->id][$i]['cloth_name'] = $v->cloth_name;
                $clothtype[$val->id][$i]['cloth_parent'] = $v->cloth_parent;
                $clothtype[$val->id][$i]['id'] = $v->id;
                $i++;
                $newnewcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $v->id);
                $newnewcloth = $this->Clothtypes->find()->where($newnewcond)->all()->toArray();
                foreach ($newnewcloth as $vv) {
                    $pricecond = array("cloth_id" => $vv->id, "status" => 1);
                    $priceres = $this->Pricings->find()->where($pricecond)->all()->toArray();
                    $k = 0;
                    foreach ($priceres as $pv) {

                        $servcond = array("id" => $pv->service_id);
                        $servres = $this->Services->find()->where($servcond)->first();
                        $pricear[$vv->id][$k]['wastype'] = $servres->sname;
                        $pricear[$vv->id][$k]['cloth_id'] = $vv->id;
                        $pricear[$vv->id][$k]['price'] = (float) $pv->price;
                        //   $pricear[$vv->id][$k]['price_id'] = $pv->id;
                        //  $pricear[$vv->id][$k]['wash_id'] = $pv->service_id;
                        $pricear[$vv->id][$k]['description'] = $pv->description;
                        $k++;
                    }
                    $endcloths[$v->id][$j]['cloth_name'] = $vv->cloth_name;
                    $endcloths[$v->id][$j]['cloth_parent'] = $vv->cloth_parent;
                    $endcloths[$v->id][$j]['id'] = $vv->id;
                    $j++;
                }
            }
        }
        $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
        $today = date("D", strtotime("today"));
        $couponData = $this->Coupons->find()->where(array("Coupons.end_date >=" => date("Y-m-d"),'status'=>1,'is_delete'=>0))->limit(5)->all()->toArray();
        $coupon=array();
        if (!empty($couponData)) {
            foreach ($couponData as $k => $coupons) {
                $week = (array_flip(array_intersect(array_flip($days), json_decode($coupons['weekday']))));
                $coupon[$k]['coupon_code'] = $coupons['coupon_code'];
                $coupon[$k]['coupon_name'] = $coupons['coupon_name'];
                $coupon[$k]['discount'] = $coupons['discount'];
                $coupon[$k]['img'] = $coupons['image_web'];
            }
        }
        $this->set(compact('tc', 'pv', 'allcloth', 'clothtype', 'endcloths', 'pricear', 'service_types','coupon'));
    }

    public function offers() {
        $this->viewBuilder()->layout('');
        $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
        $today = date("D", strtotime("today"));
        $couponData = $this->Coupons->find()->where(array("Coupons.end_date >=" => date("Y-m-d"),'status'=>1,'is_delete'=>0))->all()->toArray();
        if (!empty($couponData)) {
            foreach ($couponData as $k => $coupons) {
                $week = (array_flip(array_intersect(array_flip($days), json_decode($coupons['weekday']))));
                $coupon[$k]['id'] = $coupons['id'];
                $coupon[$k]['coupon_name'] = $coupons['coupon_name'];
                $coupon[$k]['coupon_code'] = $coupons['coupon_code'];
                $coupon[$k]['coupon_desc'] = $coupons['coupon_desc'];
                $coupon[$k]['discount'] = $coupons['discount'];
                $coupon[$k]['weekdays'] = implode(",", $week);
                $coupon[$k]['start_date'] = date("jS F, Y", strtotime($coupons['start_date']));
                $coupon[$k]['end_date'] = date("jS F, Y", strtotime($coupons['end_date']));
            }
            $this->set(compact("coupon"));
        } 
    }

    public function price() {
        $this->viewBuilder()->layout('');
        $service_types = $this->Services->find()->where(array('is_deleted' => 0))->all()->toArray();
        $conditions = array('status' => 1, 'is_deleted' => 0, 'cloth_parent' => 0);
        $allcloth = $this->Clothtypes->find()->where($conditions)->all()->toArray();
        foreach ($allcloth as $val) {
            $newcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $val->id);
            $newcloth = $this->Clothtypes->find()->where($newcond)->all()->toArray();
            $i = 0;
            $j = 0;
            foreach ($newcloth as $v) {
                $clothtype[$val->id][$i]['cloth_name'] = $v->cloth_name;
                $clothtype[$val->id][$i]['cloth_parent'] = $v->cloth_parent;
                $clothtype[$val->id][$i]['id'] = $v->id;
                $i++;
                $newnewcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $v->id);
                $newnewcloth = $this->Clothtypes->find()->where($newnewcond)->all()->toArray();
                foreach ($newnewcloth as $vv) {
                    $pricecond = array("cloth_id" => $vv->id, "status" => 1);
                    $priceres = $this->Pricings->find()->where($pricecond)->all()->toArray();
                    $k = 0;
                    foreach ($priceres as $pv) {
                        $servcond = array("id" => $pv->service_id);
                        $servres = $this->Services->find()->where($servcond)->first();
                        $pricear[$vv->id][$pv->service_id]['wastype'] = $servres->sname;
                        $pricear[$vv->id][$pv->service_id]['washid'] = $servres->id;
                        $pricear[$vv->id][$pv->service_id]['cloth_id'] = $vv->id;
                        $pricear[$vv->id][$pv->service_id]['price'] = (float) $pv->price;
                        $pricear[$vv->id][$pv->service_id]['description'] = $pv->description;
                        $k++;
                    }
                    $endcloths[$v->id][$j]['cloth_name'] = $vv->cloth_name;
                    $endcloths[$v->id][$j]['cloth_parent'] = $vv->cloth_parent;
                    $endcloths[$v->id][$j]['id'] = $vv->id;
                    $j++;
                }
            }
        }
        $this->set(compact('allcloth', 'clothtype', 'endcloths', 'pricear', 'service_types'));
    }

    public function testimonial() {
        $this->viewBuilder()->layout('');
    }

}

?>