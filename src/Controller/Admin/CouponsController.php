<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 */
class CouponsController extends AppController {

    public function beforeFilter(Event $event) {
        $this->loadmodel("Orders");
        $this->loadmodel("OrderDetails");
        $name = $this->request->session()->read('username');
        if (empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $condition = array();
        if (!empty($this->request->query['customer_type']) || !empty($this->request->query['coupondata'])) {
            $search = $this->request->query("customer_type");
            $coupon = trim($this->request->query("coupondata"));
            if ($coupon) {
                $condition = array("OR" => array("Coupons.coupon_code LIKE" => "%$coupon%", "Coupons.min_apply_amt" => $coupon, "Coupons.discount" => $coupon));
            } else {
                $condition = array("Coupons.customer_type LIKE" => "%$search%");
            }
        }
        if ($this->request->query("expiredcoupon") != '') {
            $coupondate = date('Y-m-d');
            $condition = array("Coupons.end_date <" => $coupondate);
        }
        $this->paginate = array('limit' => 10);
        $query = $this->Coupons->find()->where(array('is_delete' => 0))->order(array('id'=>"DESC"));
        $query->where($condition)->all();
        $coupons = $this->paginate($query);
        //pr($coupons);exit;
        //$coupons = $this->paginate($this->Coupons->find()->where($condition));
        $this->set(compact('coupons'));
        $this->set('_serialize', ['coupons']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addcoupon($id = null) {
        $this->loadModel('Services');
        $coupondata = $this->Coupons->newEntity();
        if ($id != null) {
            $coupondata = $this->Coupons->get($id);
        }
        if ($this->request->is('post')) {
            //pr($this->request->data);exit();
            if (!empty($this->request->data['image_web']['name']) || !empty($this->request->data['image_mob']['name'])) {
                $file = $this->request->data['image_web'];
                $mobimg = $this->request->data['image_mob'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $extension = substr(strtolower(strrchr($mobimg['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                if (in_array($ext, $arr_ext) || in_array($extension, $arr_ext)) {
                    $image_path = WWW_ROOT . 'img/' . $file['name'];
                    $thumb_path = WWW_ROOT . 'thumbimg/' . $file['name'];
                    $mobimg_path = WWW_ROOT . 'img/' . $mobimg['name'];
                    $mobimg_thumb_path = WWW_ROOT . 'thumbimg/' . $mobimg['name'];
                    if (move_uploaded_file($file['tmp_name'], $image_path)) {
                    echo "1";
                        $this->createthumb($image_path, $thumb_path, 100, 100);
                        $this->request->data['image_web'] = $file['name'];
                    }
                    if (move_uploaded_file($mobimg['tmp_name'], $mobimg_path)) {  
                        $this->createthumb($mobimg_path, $mobimg_thumb_path, 100, 100);
                        $this->request->data['image_mob'] = $mobimg['name'];
                    }
                }
            }

            $coupondata = $this->Coupons->patchEntity($coupondata, $this->request->data);
            $jsonday = json_encode($coupondata["day"]);
            $coupondata['weekday'] = $jsonday;
            $coupondata['coupon_code'] = strtoupper($this->request->data['coupon_code']);
            $json_wash = json_encode($coupondata["wtype"]);
            $coupondata['washtype'] = $json_wash;
            $coupondata['start_date'] = date('Y-m-d', strtotime($this->request->data['start_date']));
            $coupondata['end_date'] = date('Y-m-d', strtotime($this->request->data['end_date']));
            // pr($coupondata);exit();
            if ($this->Coupons->save($coupondata)) {
                if ($id != null) {
                 $this->Flash->success(__('The coupon has been Updated successfully.'));
             } else {
                 $this->Flash->success(__('The coupon has been Saved successfully.'));
             }
             return $this->redirect(['action' => 'index']);
         } else {
            $this->Flash->error(__('The coupon could not be saved. Please, try again.'));
        }
    }
    $washtype = TableRegistry::get('Services');
    $cond = array("status" => 1, "is_deleted" => 0);
    $wash = $washtype->find()->where($cond)->all()->toArray();

    $this->set('wash', $wash);
}

public function couponedit($id) {
    if (isset($id) && !empty($id)) {
        if ($id != null) {
            $coupon = $this->Coupons->get($id, [
                'contain' => []
                ]);
        }
        $editcoupon=$this->Coupons->find()->where(['id' => $id])->first();
        $jsonday = json_decode($editcoupon["weekday"]);
        $json_wash = json_decode($editcoupon["washtype"]);
        $this->set(compact('coupon'));
        $this->set('_serialize', ['coupon']);
        $washtype = TableRegistry::get('Services');
        $cond = array("status" => 1, "is_deleted" => 0);
        $wash = $washtype->find()->where($cond)->all()->toArray();

        $this->set('wash', $wash);
    }else{
        $this->Flash->success(__('Invalid URL...'));
        return $this->redirect(['action' => 'index']);
    }
}

public function deletecoupon($id=null) {
    $coupon = $this->Coupons->get($id);
    if ($this->Coupons->updateAll(array('is_delete' => 1), array('id' => $id))) {
        $this->Flash->success(__('The coupon has been deleted.'));
        return $this->redirect(['action' => 'index']);
    } else {
        $this->Flash->error(__('The coupon could not be deleted. Please, try again.'));
    }
    return $this->redirect(['action' => 'index']);
}

public function changestatus() {
    $status = $this->request->data('status');
    $id = $this->request->data('id');
    if ($this->Coupons->updateAll(array('status' => $status), array('id' => $id))) {
        $this->Flash->success(__('The status of coupon changed.'));
        return $this->redirect(['action' => 'index']);
    } else {
        $this->Flash->error(__('The status of coupon has not changed. Please, try again.'));
    }
    return $this->redirect(['action' => 'index']);
    echo $status;
    exit;
}

    /**
     * [check_couponcode description]
     * @return [type] [description]
     */
    public function check_couponcode() {
        if ($this->request->is('post')) {
            $coupon_check = $this->Coupons->find()->where(['coupon_code' => $this->request->data('coupon_code')])->count();
            echo $coupon_check;
            exit;
        }
    }

    public function viewcoupon() {
        $this->loadModel('Services');
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $coupon_id = $data['id'];
            $viewcoupon = $this->Coupons->find()->where(array('id' => $coupon_id))->first();
            //pr($viewcoupon);exit;
            $jsonday = json_decode($viewcoupon["weekday"]);
            $json_wash = array_flip(json_decode($viewcoupon["washtype"]));
            // for ($i=0; $i < count($json_wash); $i++) { 
            //     $serviceName[$i]=$this->Services->find()->where(array('id' => $json_wash[$i]))->first();
            // }
            // echo $serviceName[$i];exit();
            $cond = array("status" => 1, "is_deleted" => 0);
            $washname = $this->Services->find()->where($cond)->all()->toArray();
            foreach ($washname as $key => $value) {
                $service_id = $value['id'];
                $newwash[$service_id] = $value['sname'];
            }
            $wash_id = array_intersect_key($newwash, $json_wash);
            $this->set(compact('viewcoupon'));
            $this->set(compact('wash_id'));
            $this->viewBuilder()->layout('ajax');
        }
    }

    public function ordercoupon($id=null) {
        if (!empty($id)) {
            $condition=array();
            $id = base64_decode($id);
            $OrderData = $this->Orders->find()->where(array('id' => $id))->first();
            $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
            $orderday = date("D", strtotime($OrderData['ordereddate']));
            $day_id = array_keys($days, $orderday);
            $order_id = $OrderData['order_id'];
            $billing_amount = $OrderData['order_price'];
            $orderdate = $OrderData['ordereddate'];
            $condition = array("AND" => array("Coupons.end_date >" => $orderdate, 'Coupons.start_date <' => $orderdate, 'Coupons.min_apply_amt <=' => $billing_amount));
            $couponData = $this->Coupons->find()->where($condition)->all()->toArray();
            if (!empty($couponData)) {
                foreach ($couponData as $k => $coupons) {
                    if (in_array($day_id[0], json_decode($coupons['weekday']))) {
                        //pr($coupons);exit;
                        $washdetail = $this->OrderDetails->find()->where(array('order_id' => $order_id))->all()->toArray();
                        foreach ($washdetail as $wash) {
                            $orderwashtype[] = $wash['service_id'];
                        }
                        if (count(array_intersect(json_decode($coupons['washtype']), $orderwashtype)) == count($orderwashtype)) {
                            $coupon[$k]['id'] = $coupons['id'];
                            $coupon[$k]['coupon_code'] = $coupons['coupon_code'];
                            $coupon[$k]['discount'] = $coupons['discount'];
                            $coupon[$k]['min_discount'] = $coupons['min_discount'];
                            $coupon[$k]['max_discount'] = $coupons['max_discount'];
                        }
                    }
                }
            } 
            $this->set(compact('order_id', 'billing_amount', 'coupon'));
            if ($this->request->is("post")) {
                $data = $this->request->data;
                $this->Orders->updateAll(array("coupon_code" => $data['coupon'], "discount_amount" => $data['discount_amount'], "totalprice" => $data['total_price']), array("order_id" => $order_id));
                $this->redirect(['controller' => 'Orders', 'action' => 'index']);
            }
        }
    }

}

?>