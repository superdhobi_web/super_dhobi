<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Faqlistings Controller
 *
 * @property \App\Model\Table\FaqlistingsTable $Faqlistings
 */
class FaqlistingsController extends AppController {

    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
//        $this->paginate = [
//            'contain' => ['Cats']
//        ];
        $faqcats = TableRegistry::get('Faqcats');
        $faqcategory = $faqcats->find()->all()->toArray();
        $query = $this->Faqlistings->find()->where(array('is_deleted'=>0));
        $faqlistings = $this->paginate($query);
        $this->set(compact('faqlistings', 'faqcategory'));
        $this->set('_serialize', ['faqlistings', 'faqcategory']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id=null) {
        $faqlisting = $this->Faqlistings->newEntity();
        if ($id != null) {
            $faqlisting = $this->Faqlistings->get($id, [
                'contain' => []
                ]);
        }
        if ($this->request->is('post')) {
            $faqlisting = $this->Faqlistings->patchEntity($faqlisting, $this->request->data);
            if ($this->Faqlistings->save($faqlisting)) {
                $this->Flash->success(__('The faqlisting has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The faqlisting could not be saved. Please, try again.'));
            }
        }
        $cats = $this->Faqlistings->find('list', ['limit' => 200]);
        $this->set(compact('faqlisting', 'cats'));
        $this->set('_serialize', ['faqlisting']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Faqlisting id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $faqlisting = $this->Faqlistings->get($id);
        $faqcats = TableRegistry::get('Faqcats');
        $faqcategory = $faqcats->find()->all()->toArray();
        $cats = $this->Faqlistings->find('list', ['limit' => 200]);
        $this->set(compact('faqlisting', 'faqcategory'));
        $this->set('_serialize', ['faqlisting']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Faqlisting id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $faqlisting = $this->Faqlistings->get($id);
        if($this->Faqlistings->updateAll(array('is_deleted'=>1), array('id'=>$id))){
            $this->Flash->success(__('The faqlisting has been deleted.'));
        } else {
            $this->Flash->error(__('The faqlisting could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    public function changestatus(){
     $id = $this->request->data("id");
     $status = $this->request->data("status");
     if($this->Faqlistings->updateAll(array('status' => $status), array('id' => $id))){
        echo $status;
    }
    exit;
}

}
