<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class MembershipController extends AppController {

    public function beforeFilter(Event $event) {
        $this->loadmodel("Clothtypes");
        $this->loadmodel("Services");
        $this->loadmodel("Memberships");
        $this->loadmodel("MembershipDetails");
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index() {
        $query = $this->Memberships->find()->where(array('status' => 1, 'is_deleted' => 0));
        $data = $this->paginate($query);
        $this->set(compact('data'));
    }

    public function add($id=null) {
        if ($this->request->is("post")) {
            $data = $this->request->data;

            $data["discount"] = json_encode($data["discount"]);
            $data["valid_from"] = date("Y-m-d", strtotime($data["valid_from"]));
            $data["valid_to"] = date("Y-m-d", strtotime($data["valid_to"]));
            $membership_data = $this->Memberships->newEntity();
            if ($id != null) {
                $membership_data = $this->Memberships->get($id);
            }
            $membership = $this->Memberships->patchentity($membership_data, $data);
            $membership->valid_from = date("Y-m-d", strtotime($data["valid_from"]));
            $membership->valid_to = date("Y-m-d", strtotime($data["valid_to"]));
            if ($this->Memberships->save($membership)) {
                $i = 0;
                foreach ($data['clothParent'] as $va) {

                    $coltharr = $this->MembershipDetails->newEntity();
                    if ($id != null) {
                        $memberData=$this->MembershipDetails->find()->where(array("id_member"=>$id))->first();
                        $coltharr = $this->MembershipDetails->get($memberData["id"]);
                    }
                    $coltharr->id_member = $membership->id;
                    $coltharr->category = $va;
                    $coltharr->service = $data['washType'][$i];
                    $coltharr->quantity = $data['pcs'][$i];
                    $this->MembershipDetails->save($coltharr, array("id_member" => $id));

                    $i++;
                }
                $this->Flash->success(__('Membership has been created successfully'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Membership could not be created'));
            }
        } else {
            if ($id) {
                $editmembership = $this->Memberships->get($id);
                $membershipData = $this->MembershipDetails->find()->where(array("id_member"=>$id))->first();
                $this->set(compact('editmembership','membershipData'));
            }
            $query = $this->Clothtypes->find()->where(array('is_deleted' => 0, 'status' => 1, 'cloth_parent' => 0));
            $cloth_parent = $query->toArray();
            $service_types = $this->Services->find()->where(array('is_deleted' => 0, 'status' => 1))->all()->toArray();
            $this->set(compact('cloth_parent', 'service_types'));
        }
    }

    public function setplan() {
        $cloth_parent = $this->Clothtypes->find()->where(array('is_deleted' => 0, 'status' => 1, 'cloth_parent' => 0))->all()->toArray();

        $service_types = $this->Services->find()->where(array('is_deleted' => 0, 'status' => 1))->all()->toArray();
        $clothOption = "";
        foreach ($cloth_parent as $key => $clothList) {
            $clothOption.= "<option value='" . $clothList['id'] . "'>" . $clothList['cloth_name'] . "</option>";
        }
        $washOption = "";
        foreach ($service_types as $key => $washList) {
            $washOption.= "<option value='" . $washList['id'] . "'>" . $washList['sname'] . "</option>";
        }
        $str = '<div class="input-field col s4"><select name="clothParent[]" class="filter-select browser-default">' . $clothOption . '</select></div><div class="input-field col s4"><select name="washType[]" class="filter-select browser-default">' . $washOption . '</select></div><div class="input-field col s2"><input id="amount" class="validate" type="number" required="" name="pcs[]" ><label for="amount" data-error="Please enter pcs." data-success="Perfect!" class="active">Amount</label></div><div class="input-field col s2"><br><a class="btn-floating btn-small  green waves-effect waves-light" href="#" onclick="insertnewprice();"><i class="mdi-content-add"></i></a><a class="btn-floating btn-small  red waves-effect waves-light " href="#"><i class="mdi-content-clear"></i></a></div>';
        echo $str;
        exit;
    }

    public function delete($id = null) {
        $service = $this->Memberships->get($id);
        if ($this->Memberships->updateAll(array('is_deleted' => 1), array('id' => $id))) {
            $this->Flash->success(__('The Membership has been deleted.'));
        } else {
            $this->Flash->error(__('The Membership could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}