<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Clothtype Controller
 *
 * @property \App\Model\Table\ClothtypeTable $Clothtype
 */
class ClothtypesController extends AppController {
    
    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $query = $this->Clothtypes->find()->where(array('is_deleted'=>0));
        $cloth_parent = $query->toArray();
        $clothArray = array();
        foreach ($cloth_parent as $key => $cparentList) {
            $clothArray[$key]['id'] = $cparentList->id;
            $clothArray[$key]['cloth'] = $cparentList->cloth_name;
            $clothArray[$key]['level'] = (int)$cparentList->level+1;
            $clothArray[$key]['status'] = $cparentList->status;
            $clothArray[$key]['parent'] = $cparentList->cloth_parent;
        }
        //index elements by id
        $indexedItems=array();
        foreach ($clothArray as $item) {
            $item['subs'] = array();
            $indexedItems[$item['id']] = (object) $item;
        }
        //assign to parent
        $topLevel = array();
        foreach ($indexedItems as $item) {
            if ($item->parent == 0) {
                $topLevel[] = $item;
            } else {
                $indexedItems[$item->parent]->subs[] = $item;
            }
        }
        //pr($topLevel);exit();

        $this->set('clothtype', $topLevel);
    }
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addCloth() {
        $clothtype = $this->Clothtypes->newEntity();
        if ($this->request->is('post')) {
            $clothtype = $this->Clothtypes->patchEntity($clothtype, $this->request->data);
            $result=$this->Clothtypes->save($clothtype);
            echo $result->id;
        }
        exit;
    }

    /**
     * Edit method
     *
     * @param string|null $id Clothtype id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editCloth() {
        $id=$this->request->data('cloth_id');
        if(!empty($id)){
            $clothtype = $this->Clothtypes->get($id, [
                'contain' => []
                ]);}
            if ($this->request->is('post')) {
                $clothname=$this->request->data('cloth_name');
                $this->Clothtypes->updateAll(array('cloth_name'=>"$clothname"),array('id'=>$id));
            }
            exit;
        }

    /**
     * Delete method
     *
     * @param string|null $id Clothtype id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteCloth() {
        $this->request->allowMethod(['post', 'delete']);
        $id = $this->request->data('cloth_id');
        if (!empty($id)) {
            $clothtype = $this->Clothtypes->get($id);
            $this->Clothtypes->updateAll(array('is_deleted'=>1), array('id'=>$id));
            exit;
        }
    }

    function setParent() {
        $result = 0;
        $clothtype = $this->Clothtypes->newEntity();
        if ($this->request->is('post')) {
            $clothtype = $this->Clothtypes->patchEntity($clothtype, $this->request->data);
            $clothtype['level']=1;
            if ($this->Clothtypes->save($clothtype)) {
                $result = 1;
            } else {
                $result = 2;
            }
        }
        echo $result;
        exit();
    }

}
