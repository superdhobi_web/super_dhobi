<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController {

    public $components = array('Paginator');

    public function beforeFilter(Event $event) {
        $this->loadModel('Coupons');
        $name = $this->request->session()->read('username');
        if (empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($page='') {
        $cond = "c.is_deleted=0";
        $this->loadModel('Locations');
        $this->loadModel('Usertransactions');
        if (!empty($this->request->query['userCondition']) && !empty($this->request->query['userData'])) {
            $userData = $this->request->query['userData'];
            $userCondn = $this->request->query['userCondition'];
            if ($userCondn == "name") {
                $cond.=" AND c.name LIKE '%" . $userData . "%'";
            } elseif ($userCondn == "mobile") {
                $cond.=" AND c.mobile LIKE '%" . $userData . "%'";
            } elseif ($userCondn == "email") {
                $cond.=" AND c.email LIKE '%" . $userData . "%'";
            } elseif ($userCondn == "userid") {
                $cond.=" AND a.user_id LIKE '%" . $userData . "%'";
            }
        }
        if (empty($this->request->query['from_date']) && empty($this->request->query['to_date'])) {
            if (!empty($this->request->query['orderdate'])) {
                $cond.=" AND (a.deliverydate = '" . date("Y-m-d", strtotime($this->request->query['orderdate'])) . "' || a.pickupdate = '" . date("Y-m-d", strtotime($this->request->query['orderdate'])) . "')";
            }
            if (!empty($this->request->query['oredrslot'])) {
                $cond.=" AND (a.pickupslot = '" . $this->request->query['oredrslot'] . "' || a.deliveryslot = '" . $this->request->query['oredrslot'] . "')";
            }
        }
        if (!empty($this->request->query['from_date'])) {
            $cond.=" AND (a.ordereddate >= '" . date("Y-m-d", strtotime($this->request->query['from_date'])) . "')";
        }
        if (!empty($this->request->query['to_date'])) {
            $cond.=" AND (a.ordereddate <= '" . date("Y-m-d", strtotime($this->request->query['to_date'])) . "')";
        }
        if (!empty($this->request->query['city'])) {
            $cond.=" AND b.city = '" . $this->request->query['city'] . "'";
            $circlelist=$this->Locations->find()->where(['location_parent' => $this->request->query['city']])->all()->toArray();
            foreach ($circlelist as $val) {
                $child['circle'][$val['id']] = $val['location_name'];
            }
        }
        if (!empty($this->request->query['circle'])) {
            $cond.=" AND b.circle = '" . $this->request->query['circle'] . "'";
            $circlelist=$this->Locations->find()->where(['location_parent' => $this->request->query['circle']])->all()->toArray();
            foreach ($circlelist as $val) {
                $child['location'][$val['id']] = $val['location_name'];
            }
        }
        if (!empty($this->request->query['location'])) {
            $cond.=" AND b.location = '" . $this->request->query['location'] . "'";
            $circlelist=$this->Locations->find()->where(['location_parent' => $this->request->query['location']])->all()->toArray();
            foreach ($circlelist as $val) {
                $child['area'][$val['id']] = $val['location_name'];
            }
        }
        if (!empty($this->request->query['area'])) {
            $cond.=" AND b.area = '" . $this->request->query['area'] . "'";
        }
        if (!empty($this->request->query['orderStatus'])) {
            $cond.=" AND a.order_status = '" . $this->request->query['orderStatus'] . "'";
        }
        $this->loadModel('Timeslots');
        $this->loadModel('UserAddresses');
        $page = ($page != '') ? ($page * 10) : 0;
        $conn = ConnectionManager::get('default');
        //$cond.= 'SELECT * FROM orders a,users c WHERE a.user_id=c.userid AND '.$cond.' GROUP BY a.order_id ORDER BY a.ordereddate DESC LIMIT '.$page.', 20');
        // pr($this->request->query);exit;  
        $data = $conn->execute('SELECT * FROM orders a,user_addresses b,users c WHERE a.user_id=c.userid AND (a.pickupadd=b.id OR a.deliveryadd=b.id) AND ' . $cond . ' GROUP BY a.order_id ORDER BY a.id DESC');
        $rows = $data->fetchAll('assoc');
        $arr = array();
        $i = 0;
        foreach ($rows as $va) {
            $arr[$i]['order_id'] = $va['order_id'];
            $arr[$i]['pickupdate'] = date("jS F, Y", strtotime($va['pickupdate']));
            $pickslot = $this->Timeslots->find()->where(array('id' => $va['pickupslot']))->first();
            if(!empty($pickslot)){
            $arr[$i]['pickupslot'] = date("h:i A", strtotime($pickslot->from_time)) . "-" . date("h:i A", strtotime($pickslot->to_time));
            } else {
                $arr[$i]['pickupslot'] ="No slot";
            }
            $pickadd = $this->UserAddresses->find()->where(array('id' => $va['pickupadd']))->first();
            $pickcity = $this->Locations->find()->where(array('id' => $pickadd->city))->first();
            $pickcircle = $this->Locations->find()->where(array('id' => $pickadd->circle))->first();
            $picklocation = $this->Locations->find()->where(array('id' => $pickadd->location))->first();
            $pickarea = $this->Locations->find()->where(array('id' => $pickadd->area))->first();
            $payment = $this->Usertransactions->find()->where(array('order_id' => $va['order_id']))->all()->toArray();
            $totalpay = 0.00;
            foreach ($payment as $pay) {
                $totalpay = $totalpay + $pay['amount'];
            }
            $arr[$i]['pickcity'] = $pickcity->location_name;
            $arr[$i]['pickcircle'] = $pickcircle->location_name;
            $arr[$i]['picklocation'] = $picklocation->location_name;
            $arr[$i]['pickarea'] = $pickarea->location_name;
            $arr[$i]['pickaddress'] = $pickadd->address;
            $arr[$i]['pickpin'] = $pickadd->pincode;
            $arr[$i]['deliverydate'] = date("jS F, Y", strtotime($va['deliverydate']));
            $delislot = $this->Timeslots->find()->where(array('id' => $va['deliveryslot']))->first();
            if (!empty($delislot)) {
                $arr[$i]['deliveryslot'] = date("h:i A", strtotime($delislot->from_time)) . "-" . date("h:i A", strtotime($delislot->to_time));
            } else {
                $arr[$i]['deliveryslot'] = "No Slot";
            }
            $deliadd = $this->UserAddresses->find()->where(array('id' => $va['pickupadd']))->first();
            $delicity = $this->Locations->find()->where(array('id' => $deliadd->city))->first();
            $delicircle = $this->Locations->find()->where(array('id' => $deliadd->circle))->first();
            $delilocation = $this->Locations->find()->where(array('id' => $deliadd->location))->first();
            $deliarea = $this->Locations->find()->where(array('id' => $deliadd->area))->first();
            $arr[$i]['delicity'] = $delicity->location_name;
            $arr[$i]['delicircle'] = $delicircle->location_name;
            $arr[$i]['delilocation'] = $delilocation->location_name;
            $arr[$i]['delikarea'] = $deliarea->location_name;
            $arr[$i]['deliaddress'] = $deliadd->address;
            $arr[$i]['delipin'] = $deliadd->pincode;
            $arr[$i]['totalprice'] = ($totalpay == 0) ? $va['totalprice'] : (($totalpay > $va['totalprice']) ? 0.00 : ($va['totalprice'] - $totalpay));
            $arr[$i]['order_status'] = $va['order_status'];
            $arr[$i]['user_name'] = $va['name'];
            $arr[$i]['mobile'] = $va['mobile'];
            $arr[$i]['email'] = $va['email'];
            $arr[$i]['user_id'] = $va['user_id'];
            $arr[$i]['order_id'] = $va['order_id'];
            $arr[$i]['wallet'] = $va['wallet'];
            $i++;
        }
        $location = TableRegistry::get('Locations');
        $city=$location->find()->where(['location_parent' => 0,'status'=>1,'is_deleted'=>0])->all()->toArray();
        $this->set('data', $arr);
        $this->set(compact('city'));
        $this->set('_serialize', ['city']);
        $this->set(compact('child'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function insertorderdata() {
        // $this->loadmodel("");
        if ($this->request->is("post")) {
            $data = $this->request->data;
            $val = $this->insertOrder($data);
            if ($val) {
                $this->Flash->success(__('The order has been placed successfully.'));
                return $this->redirect(['controller' => 'Coupons', 'action' => "ordercoupon/$val"]);
            } else {
                $this->Flash->error(__('The order submission has been failed.please try agian'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function addorder() {
        $this->loadModel('Users');
        $this->loadModel('Locations');
        $this->loadModel('UserAddresses');
        $this->loadModel('Clothtypes');
        $this->loadModel('Pricings');
        $this->loadModel('Services');
        $order = $this->Orders->newEntity();
        if (!empty($this->request->query("userCondition")) || !empty($this->request->query("getUser_src"))) {
            $condition = array("status" => 1, "is_deleted" => 0);
            $SearchBy = $this->request->query("userCondition");
            $userData = $this->request->query("getUser_src");
            if ($SearchBy == 'all') {
                $condition = array("OR" => array("Users.userid LIKE" => "%$userData%", "Users.name LIKE" => "%$userData%", "Users.email LIKE" => "%$userData%", "Users.mobile LIKE" => "%$userData%"));
            } else {
                $condition = array("Users.$SearchBy LIKE" => "%$userData%");
            }
            $users = $this->Users->find()->where($condition)->first();
            $userData = array();
            $i = 0;
            //pr($users);//exit;
            if (!empty($users)) {
                $userData['userid'] = $users->userid;
                $userData['name'] = $users->name;
                $userData['email'] = $users->email;
                $userData['mobile'] = $users->mobile;
                $address = $this->UserAddresses->find()->where(array("user_id" => $users->userid, "status" => 1, "is_deleted" => 0))->order(array("set_default" => "DESC"))->all();
                if (!empty($address)) {
                    foreach ($address as $key => $add_heading) {
                        $userData['address_heading'][$i]['add_heading'] = $add_heading['add_heading'];
                        $userData['address_heading'][$i]['id'] = $add_heading['id'];
                        $userData['address_heading'][$i]['set_default'] = $add_heading['set_default'];
                        $i++;
                    }
                }
            } else {
                $this->Flash->error(__('User Not Found'));
                return $this->redirect(['action' => 'addorder']);
            }
        }
        $clothtypes = $this->Clothtypes->find()->where(array('level' => 3, 'status' => 1, 'is_deleted' => 0))->all();
        foreach ($clothtypes as $cloth) {
            $id = $cloth['id'];
            $clothtype[$id]['cloth_name'] = $cloth['cloth_name'];
            $clothtype[$id]['cloth_parent'] = $cloth['cloth_parent'];
        }


        $cond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => 0);
        $allcloth = $this->Clothtypes->find()->where($cond)->all()->toArray();
        $newcloth = array();
        $Childcloth = array();
        $pricearr = array();
        foreach ($allcloth as $val) {
            $newcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $val->id);
            $nwcloth = $this->Clothtypes->find()->where($newcond)->all()->toArray();
            $i = 0;
            $j = 0;
            $k = 0;
            foreach ($nwcloth as $v) {
                $newcloth[$val->id][$i]['cloth_name'] = $v->cloth_name;
                $newcloth[$val->id][$i]['cloth_parent'] = $v->cloth_parent;
                $newcloth[$val->id][$i]['id'] = $v->id;
                $i++;
                $newnewcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $v->id);
                $newnewcloth = $this->Clothtypes->find()->where($newnewcond)->all()->toArray();
                foreach ($newnewcloth as $vv) {
                    $pricecond = array("cloth_id" => $vv->id, "status" => 1);
                    $priceres = $this->Pricings->find()->where($pricecond)->all()->toArray();
                    foreach ($priceres as $pv) {
                        $servcond = array("id" => $pv->service_id);
                        $servres = $this->Services->find()->where($servcond)->first();
                        $pricearr[$vv->id][$k]['wastype'] = $servres->sname;
                        $pricearr[$vv->id][$k]['sicon'] = $servres->sicon;
                        $pricearr[$vv->id][$k]['price'] = $pv->price;
                        $pricearr[$vv->id][$k]['price_id'] = $pv->id;
                        $pricearr[$vv->id][$k]['wash_id'] = $pv->service_id;
                        $pricearr[$vv->id][$k]['cloth_id'] = $pv->cloth_id;
                        $k++;
                    }
                    $Childcloth[$v->id][$j]['cloth_name'] = $vv->cloth_name;
                    $Childcloth[$v->id][$j]['cloth_parent'] = $vv->cloth_parent;
                    $Childcloth[$v->id][$j]['id'] = $vv->id;
                    $j++;
                }
            }
        }
        $cond = array("status" => 1, "is_deleted" => 0);
        $allservices = $this->Services->find()->where($cond)->all()->toArray();
        // pr($pricearr);exit;
        $this->set(compact("allservices", "allcloth", "newcloth", "Childcloth", "pricearr"));
        $this->set(compact('userData', 'cloths', 'services'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $order = $this->Orders->get($id, [
        'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        }
        $cloths = $this->Orders->Clothtypes->find('list', ['limit' => 200])->execute();
        //  pr($cloths);exit;
        $services = $this->Orders->Services->find('list', ['limit' => 200]);
        $this->set(compact('order', 'cloths', 'services'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

//    function orderdata() {
//        if ($this->request->is("post")) {
//            $location = TableRegistry::get('Locations');
//            $userId = $this->request->data("userid");
//            $UserAdd = $location->find()->where(array("user_id" => $userId))->all();
//            pr($UserAdd);
//        }
//        echo "echo";
//        exit;
//    }

    function SelectSlot() {
        $this->loadModel('Timeslots');
        $conditions = array();
        if ($this->request->is("post")) {
            $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
            $date = $this->request->data;
            $timestamp = strtotime($date['date']);
            $day = date('D', $timestamp);
            $day_id = array_keys($days, $day);
            $conditions = array('status' => 1, 'is_deleted' => 0, 'is_leave' => 0, 'day' => $day_id[0]);
            $timeSlot = $this->Timeslots->find()->where($conditions)->all()->toArray();
            if (!empty($timeSlot)) {
                $str = "<option value=''>--Select Slot--</option>";
                foreach ($timeSlot as $day) {
                    $str.= "<option value='" . $day['id'] . "'>" . $day['from_time'] . "-" . $day['to_time'] . "</option>";
                }
            } else {
                $str = 0;
                // $str = "<option value='' selected>No Time Slot Available</option>";
            }
        }
        echo $str;
        exit;
    }

    public function findslot($date) {
        $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
        $timestamp = strtotime($date);
        $day = date('D', $timestamp);
        $day_id = array_keys($days, $day);
        $conditions = array('status' => 1, 'is_deleted' => 0, 'is_leave' => 0, 'day' => $day_id[0]);
        $timeSlot = $this->Timeslots->find()->where($conditions)->all()->toArray();
        return $timeSlot;
        exit;
    }

    public function invoice() {
        
    }

    public function orderdetail($order_id='') {
        $this->loadModel('Timeslots');
        $this->loadModel('Locations');
        $this->loadModel('Orders');
        $this->loadModel('UserAddresses');
        $this->loadModel('OrderDetails');
        $this->loadModel('OrderTrackings');
        $this->loadModel('Services');
        $this->loadModel('Clothtypes');
        $conn = ConnectionManager::get('default');
        $data = $conn->execute('SELECT * FROM orders a,users b WHERE a.user_id=b.userid AND a.order_id="' . $order_id . '" GROUP BY a.order_id');
        $rows = $data->fetchAll('assoc');
        $arr = array();
        foreach ($rows as $va) {
            $arr['order_id'] = $va['order_id'];
            $arr['pickupdate'] = date("jS F, Y", strtotime($va['pickupdate']));
            $pickslot = $this->Timeslots->find()->where(array('id' => $va['pickupslot']))->first();
            $arr['pickupslot'] = date("h:i A", strtotime($pickslot->from_time)) . "-" . date("h:i A", strtotime($pickslot->to_time));
            $pickadd = $this->UserAddresses->find()->where(array('id' => $va['pickupadd']))->first();
            $pickcity = $this->Locations->find()->where(array('id' => $pickadd->city))->first();
            $pickcircle = $this->Locations->find()->where(array('id' => $pickadd->circle))->first();
            $picklocation = $this->Locations->find()->where(array('id' => $pickadd->location))->first();
            $pickarea = $this->Locations->find()->where(array('id' => $pickadd->area))->first();
            $arr['pickcity'] = $pickcity->location_name;
            $arr['pickcircle'] = $pickcircle->location_name;
            $arr['picklocation'] = $picklocation->location_name;
            $arr['pickarea'] = $pickarea->location_name;
            $arr['pickaddress'] = $pickadd->address;
            $arr['pickpin'] = $pickadd->pincode;
            $arr['deliverydate'] = date("jS F, Y", strtotime($va['deliverydate']));
            $delislot = $this->Timeslots->find()->where(array('id' => $va['deliveryslot']))->first();
            $arr['deliveryslot'] = date("h:i A", strtotime($delislot->from_time)) . "-" . date("h:i A", strtotime($delislot->to_time));
            $deliadd = $this->UserAddresses->find()->where(array('id' => $va['pickupadd']))->first();
            $delicity = $this->Locations->find()->where(array('id' => $deliadd->city))->first();
            $delicircle = $this->Locations->find()->where(array('id' => $deliadd->circle))->first();
            $delilocation = $this->Locations->find()->where(array('id' => $deliadd->location))->first();
            $deliarea = $this->Locations->find()->where(array('id' => $deliadd->area))->first();
            $arr['delicity'] = $delicity->location_name;
            $arr['delicircle'] = $delicircle->location_name;
            $arr['delilocation'] = $delilocation->location_name;
            $arr['delikarea'] = $deliarea->location_name;
            $arr['deliaddress'] = $deliadd->address;
            $arr['delipin'] = $deliadd->pincode;
            $arr['totalprice'] = $va['totalprice'];
            $arr['order_status'] = $va['order_status'];
            $arr['user_name'] = $va['name'];
            $arr['mobile'] = $va['mobile'];
            $arr['email'] = $va['email'];
            $arr['user_id'] = $va['user_id'];
            $arr['order_id'] = $va['order_id'];
            $arr['ordereddate'] = $va['ordereddate'];
        }
        $clotharr = $this->OrderDetails->find()->where(array('order_id' => $order_id))->order(array("id" => "asc"))->all()->toArray();
        $statusarr = $this->OrderTrackings->find()->where(array('order_id' => $order_id))->order(array("id" => "asc"))->all()->toArray();
        $clothtypearr = $this->Clothtypes->find()->where(array('level' => 3))->all()->toArray();
        foreach ($clothtypearr as $va) {
            $clothtypear[$va->id] = $va->cloth_name;
        }
        $servarr = $this->Services->find()->all()->toArray();
        foreach ($servarr as $val) {
            $servar[$val->id] = $val->sname;
        }
        $transorder = $conn->execute('SELECT * FROM usertransactions WHERE order_id="' . $order_id . '" AND type=1 ORDER BY id asc');
        $alltransorder = $transorder->fetchAll('assoc');
        $this->set('transorder', $alltransorder);
        $this->set('orderdetail', $arr);
        $this->set('clothdetails', $clotharr);
        $this->set('statusdetails', $statusarr);
        $this->set('clothtypearr', $clothtypear);
        $this->set('servarr', $servar);
    }

    public function getorderdetails($order_id='', $flag='') {
        $this->loadModel('Timeslots');
        $this->loadModel('Locations');
        $this->loadModel('Orders');
        $this->loadModel('UserAddresses');
        $this->loadModel('OrderDetails');
        $this->loadModel('OrderTrackings');
        $this->loadModel('Services');
        $this->loadModel('Clothtypes');
        $this->loadModel('Usertransactions');
        $conn = ConnectionManager::get('default');
        $data = $conn->execute('SELECT * FROM orders a,users b WHERE a.user_id=b.userid AND a.order_id="' . $order_id . '" GROUP BY a.order_id');
        $rows = $data->fetchAll('assoc');
        $arr = array();
        foreach ($rows as $va) {
            $arr['order_id'] = $va['order_id'];
            $arr['pickupdate'] = date("jS F, Y", strtotime($va['pickupdate']));
            $pickslot = $this->Timeslots->find()->where(array('id' => $va['pickupslot']))->first();
            $arr['pickupslot'] = date("h:i A", strtotime($pickslot->from_time)) . "-" . date("h:i A", strtotime($pickslot->to_time));
            $pickadd = $this->UserAddresses->find()->where(array('id' => $va['pickupadd']))->first();
            $pickcity = $this->Locations->find()->where(array('id' => $pickadd->city))->first();
            $pickcircle = $this->Locations->find()->where(array('id' => $pickadd->circle))->first();
            $picklocation = $this->Locations->find()->where(array('id' => $pickadd->location))->first();
            $pickarea = $this->Locations->find()->where(array('id' => $pickadd->area))->first();
            $arr['pickcity'] = $pickcity->location_name;
            $arr['pickcircle'] = $pickcircle->location_name;
            $arr['picklocation'] = $picklocation->location_name;
            $arr['pickarea'] = $pickarea->location_name;
            $arr['pickaddress'] = $pickadd->address;
            $arr['pickpin'] = $pickadd->pincode;
            $arr['deliverydate'] = date("jS F, Y", strtotime($va['deliverydate']));
            $delislot = $this->Timeslots->find()->where(array('id' => $va['deliveryslot']))->first();
            $arr['deliveryslot'] = date("h:i A", strtotime($delislot->from_time)) . "-" . date("h:i A", strtotime($delislot->to_time));
            $deliadd = $this->UserAddresses->find()->where(array('id' => $va['pickupadd']))->first();
            $delicity = $this->Locations->find()->where(array('id' => $deliadd->city))->first();
            $delicircle = $this->Locations->find()->where(array('id' => $deliadd->circle))->first();
            $delilocation = $this->Locations->find()->where(array('id' => $deliadd->location))->first();
            $deliarea = $this->Locations->find()->where(array('id' => $deliadd->area))->first();
            $arr['delicity'] = $delicity->location_name;
            $arr['delicircle'] = $delicircle->location_name;
            $arr['delilocation'] = $delilocation->location_name;
            $arr['delikarea'] = $deliarea->location_name;
            $arr['deliaddress'] = $deliadd->address;
            $arr['delipin'] = $deliadd->pincode;
            $arr['totalprice'] = $va['totalprice'];
            $arr['order_status'] = $va['order_status'];
            $arr['user_name'] = $va['name'];
            $arr['mobile'] = $va['mobile'];
            $arr['email'] = $va['email'];
            $arr['user_id'] = $va['user_id'];
            $arr['order_id'] = $va['order_id'];
            $arr['ordereddate'] = $va['ordereddate'];
        }
        $clotharr = $this->OrderDetails->find()->where(array('order_id' => $order_id))->order(array("id" => "asc"))->all()->toArray();
        $statusarr = $this->OrderTrackings->find()->where(array('order_id' => $order_id))->order(array("id" => "asc"))->all()->toArray();
        $clothtypearr = $this->Clothtypes->find()->where(array('level' => 3))->all()->toArray();
        foreach ($clothtypearr as $va) {
            $clothtypear[$va->id] = $va->cloth_name;
        }
        $servarr = $this->Services->find()->all()->toArray();
        foreach ($servarr as $val) {
            $servar[$val->id] = $val->sname;
        }
        $transorder = $conn->execute('SELECT * FROM usertransactions WHERE order_id="' . $order_id . '" AND type=1 ORDER BY id asc');
        $alltransorder = $transorder->fetchAll('assoc');
        $this->set('orderdetail', $arr);
        $this->set('clothdetails', $clotharr);
        $this->set('statusdetails', $statusarr);
        $this->set('clothtypearr', $clothtypear);
        $this->set('servarr', $servar);
        $this->set('transorder', $alltransorder);
        $this->viewBuilder()->layout('ajax'); //for using ajax include another view page
    }

    public function OrderStatusChange() {
        if ($this->request->is("post")) {
            $orderId = $this->request->data('orderid');
            $orderstatus = $this->request->data('status');
            if ($this->Orders->updateAll(array('order_status' => $orderstatus), array('order_id' => $orderId))) {
                $this->insertintotrack($orderId, $order_status = $orderstatus, $remark = null, $by_employee = 1, $status = null);
                echo 1;
            } else {
                echo 0;
            }
            exit;
        }
    }

    public function printpage() {
        $this->loadModel('Orders');
        $this->loadModel('UserAddresses');
        $this->loadModel('Users');
        $this->loadModel('Locations');
        $this->loadModel('OrderDetails');
        $this->loadModel('Adminusers');
        $this->loadModel('Clothtypes');
        $this->loadModel('Services');
        $da = $this->request->data;
        $emp_id = $da['userid'];
        $order_id = $da['dataall'];
        $orderid = explode(" ", trim(base64_decode($order_id)));
        $data = array();
        $i = 0;
        foreach ($orderid as $id) {
            $cond = array("order_id" => $id);
            $order = $this->Orders->find()->where($cond)->first();
            $data[$i]['order_id'] = $order->order_id;
            $data[$i]['user_id'] = $order->user_id;
            $data[$i]['totalprice'] = $order->totalprice;
            if ($order->order_status == 3) {
                $data[$i]['status'] = "Pickup";
                $ucon = $order->pickupadd;
            } else {
                $ucon = $order->deliveryadd;
                $data[$i]['status'] = "Delivery";
            }
            $addcond = array("id" => $ucon);
            $pickadd = $this->UserAddresses->find()->where($addcond)->first();
            $pickcity = $this->Locations->find()->where(array('id' => $pickadd->city))->first();
            $pickcircle = $this->Locations->find()->where(array('id' => $pickadd->circle))->first();
            $picklocation = $this->Locations->find()->where(array('id' => $pickadd->location))->first();
            $pickarea = $this->Locations->find()->where(array('id' => $pickadd->area))->first();
            $data[$i]['city'] = $pickcity->location_name;
            $data[$i]['circle'] = $pickcircle->location_name;
            $data[$i]['location'] = $picklocation->location_name;
            $data[$i]['area'] = $pickarea->location_name;
            $data[$i]['address'] = $pickadd->address;
            $data[$i]['pin'] = $pickadd->pincode;
            $userdetails = $this->Users->find()->where(array("userid" => $order->user_id))->first();
            $data[$i]['user_name'] = isset($userdetails->name) ? $userdetails->name : "";
            $data[$i]['mobile'] = isset($userdetails->mobile) ? $userdetails->mobile : "";
            $data[$i]['email'] = isset($userdetails->email) ? $userdetails->email : '';
            $orerdetails = $this->OrderDetails->find()->where(array("order_id" => $id))->all()->toArray();
            $tota = 0;
            $arr = array();
            $j = 0;
            foreach ($orerdetails as $ne) {
                $clothnam = $this->Clothtypes->find()->where(array('id' => $ne['cloth_id']))->first();
                $servicenam = $this->Services->find()->where(array('id' => $ne['service_id']))->first();
                $arr[$j]['cloth'] = $clothnam['cloth_name'];
                $arr[$j]['clothparent'] = $this->getclothparent($clothnam['cloth_parent']);
                $arr[$j]['service'] = $servicenam['sname'];
                $arr[$j]['quantity'] = $ne['quantity'];
                $arr[$j]['unitprice'] = $ne['price'];
                $tota = (int) $tota + $ne->quantity;
                $j++;
            }
            $data[$i]['totalcloth'] = $tota;
            $data[$i]['clothdetails'] = $arr;
            $i++;
        }
        $empdetails = $this->Adminusers->find()->where(array("emp_id" => $emp_id))->first();
        $this->set("data", $data);
        $this->set("empdata", $empdetails);
    }

    public function orderedit($order_id=null) {
        $this->loadModel('Users');
        $this->loadModel('Locations');
        $this->loadModel('UserAddresses');
        $this->loadModel('Clothtypes');
        $this->loadModel('Pricings');
        $this->loadModel('Services');
        $this->loadModel('Timeslots');
        $this->loadModel('OrderDetails');
        if ($order_id != null) {
            $conn = ConnectionManager::get('default');
            $data = $conn->execute('SELECT *,a.id as oid FROM orders a,users b WHERE a.user_id=b.userid AND a.order_id="' . $order_id . '" GROUP BY a.order_id');
            $users = $data->fetch('assoc');
            $userData['userid'] = $users['userid'];
            $userData['oid'] = $users['oid'];
            $userData['name'] = $users['name'];
            $userData['email'] = $users['email'];
            $userData['mobile'] = $users['mobile'];
            $userData['totalprice'] = $users['totalprice'];
            $fortotalcloth = $this->OrderDetails->find()->where(array("order_id" => $order_id))->all()->toArray();
            $tota = 0;
            foreach ($fortotalcloth as $ne) {
                $tota = (int) $tota + $ne->quantity;
            }
            $userData['totalcloth'] = $tota;
            $pickupAdd = $users['pickupdate'];
            $deliveryAdd = $users['deliverydate'];

            $pickupdate = $users['pickupadd'];
            $deliverydate = $users['deliveryadd'];
            $pickupslots = $this->findslot($pickupdate);
            $deliveryslots = $this->findslot($pickupdate);
            $address = $this->UserAddresses->find()->where(array("user_id" => $users['userid'], "status" => 1, "is_deleted" => 0))->order(array("set_default" => "DESC"))->all();
            if (!empty($address)) {
                $i = 0;
                foreach ($address as $key => $add_heading) {
                    $userData['address_heading'][$i]['add_heading'] = $add_heading['add_heading'];
                    $userData['address_heading'][$i]['id'] = $add_heading['id'];
                    $userData['address_heading'][$i]['set_default'] = $add_heading['set_default'];
                    $i++;
                }
            }
            if ($this->request->is("post")) {
                $data = $this->request->data;
                $orderTable = TableRegistry::get('Orders');
                $orderen = $orderTable->newEntity();
                $orderen->user_id = $data['user_id'];
                $orderen->id = $data['oid'];
                $orderen->pickupdate = date("Y-m-d", strtotime($data['pickupdate']));
                $orderen->pickupslot = $data['pickupslot'];
                $orderen->pickupadd = $data['pickupadd'];
                $orderen->deliverydate = date("Y-m-d", strtotime($data['deliverydate']));
                $orderen->deliveryadd = $data['deliveryadd'];
                $orderen->deliveryslot = $data['deliveryslot'];
                $orderen->totalprice = isset($data['totalprice']) ? $data['totalprice'] : 100;
                $orderen->remarks = $data['remarknew'];
                $this->OrderDetails->deleteAll(array('order_id' => $order_id), false);
                if ($orderTable->save($orderen)) {
                    $orderdetailsTable = TableRegistry::get('OrderDetails');
                    if (isset($data['cloth_info'])) {
                        foreach ($data['cloth_info'] as $va) {
                            $coltharr = $orderdetailsTable->newEntity();
                            $datanews = explode(":@:", $va);
                            $coltharr->order_id = $order_id;
                            $coltharr->service_id = $datanews[0];
                            $coltharr->cloth_id = $datanews[1];
                            $coltharr->price = $datanews[2];
                            $coltharr->quantity = $datanews[3];
                            $coltharr->remark = $datanews[4];
                            $orderdetailsTable->save($coltharr);
                        }
                    }
                    $this->Flash->success(__('The order has been updated successfully.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The order updation has been failed.please try agian'));
                    return $this->redirect(['action' => 'index']);
                }
            }
        }
        $clothtypes = $this->Clothtypes->find()->where(array('level' => 3, 'status' => 1, 'is_deleted' => 0))->all();
        if (!empty($clothtypes)) {
            foreach ($clothtypes as $cloth) {
                $id = $cloth['id'];
                $clothtype[$id]['cloth_name'] = $cloth['cloth_name'];
                $clothtype[$id]['cloth_parent'] = $cloth['cloth_parent'];
            }
        }

        $cond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => 0);
        $allcloth = $this->Clothtypes->find()->where($cond)->all()->toArray();
        $newcloth = array();
        $Childcloth = array();
        $pricearr = array();
        foreach ($allcloth as $val) {
            $newcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $val->id);
            $nwcloth = $this->Clothtypes->find()->where($newcond)->all()->toArray();
            $i = 0;
            $j = 0;
            $k = 0;
            if (!empty($nwcloth)) {
                foreach ($nwcloth as $v) {
                    $newcloth[$val->id][$i]['cloth_name'] = $v->cloth_name;
                    $newcloth[$val->id][$i]['cloth_parent'] = $v->cloth_parent;
                    $newcloth[$val->id][$i]['id'] = $v->id;
                    $i++;
                    $newnewcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $v->id);
                    $newnewcloth = $this->Clothtypes->find()->where($newnewcond)->all()->toArray();
                    foreach ($newnewcloth as $vv) {
                        $pricecond = array("cloth_id" => $vv->id, "status" => 1);
                        $priceres = $this->Pricings->find()->where($pricecond)->all()->toArray();
                        foreach ($priceres as $pv) {
                            $servcond = array("id" => $pv->service_id);
                            $servres = $this->Services->find()->where($servcond)->first();
                            $oreddcon = array("order_id" => $order_id, "cloth_id" => $vv->id, "service_id" => $pv->service_id);
                            $OrderDetails = $this->OrderDetails->find()->where($oreddcon)->first();
                            $pricearr[$vv->id][$k]['wastype'] = $servres->sname;
                            $pricearr[$vv->id][$k]['sicon'] = $servres->sicon;
                            $pricearr[$vv->id][$k]['price'] = $pv->price;
                            $pricearr[$vv->id][$k]['price_id'] = $pv->id;
                            $pricearr[$vv->id][$k]['wash_id'] = $pv->service_id;
                            $pricearr[$vv->id][$k]['cloth_id'] = $pv->cloth_id;
                            $pricearr[$vv->id][$k]['quantity'] = $OrderDetails['quantity'] ? $OrderDetails['quantity'] : 0;
                            $pricearr[$vv->id][$k]['remark'] = $OrderDetails['remark'] ? $OrderDetails['remark'] : '';
                            $k++;
                        }
                        $Childcloth[$v->id][$j]['cloth_name'] = $vv->cloth_name;
                        $Childcloth[$v->id][$j]['cloth_parent'] = $vv->cloth_parent;
                        $Childcloth[$v->id][$j]['id'] = $vv->id;
                        $j++;
                    }
                }
            }
        }
        $cond = array("status" => 1, "is_deleted" => 0);
        $allservices = $this->Services->find()->where($cond)->all()->toArray();
        $order_details = $this->OrderDetails->find()->where(array('order_id' => $order_id))->all()->toArray();
        $this->set(compact("allservices", "allcloth", "newcloth", "Childcloth", "pricearr"));
        $this->set(compact('userData', 'cloths', 'services', 'order_id', 'users', 'pickupslots', 'deliveryslots', 'order_details', 'pickupAdd', 'deliveryAdd'));
    }

    public function reschdule() {
        $orderId = $this->request->data('orderid');
        $oredrslot = $this->request->data('oredrslot');
        $date = date("Y-m-d", strtotime($this->request->data('orderdate_new')));
        $type = $this->request->data('type');
        $conn = ConnectionManager::get('default');

        if ($type == 6) {
            $sql = "UPDATE orders set pickupslot=$oredrslot,order_status=$type,pickupdate='{$date}' WHERE order_id='{$orderId}'";
            $remark = "pick up reschdule";
        } else {
            $sql = "UPDATE orders set deliveryslot=$oredrslot,order_status=$type,deliverydate='{$date}' WHERE order_id='{$orderId}'";
            $remark = "Delivery  reschdule";
        }
        $conn->execute($sql);
        $this->insertintotrack($orderId, $type, $remark = $remark, $by_employee = 1, $status = null);
        $this->Flash->success(__('The order has been Updated successfully.'));
        return $this->redirect(['action' => 'index']);
    }

    /**
     * [addcoupon Allowed discounts on order]
     * @return [type] [description]
     */
    // public function addcoupon()
    // {
    //     if ($this->request->is("post")) {
    //         $data = $this->request->data;
    //         $getDetails = $this->Coupons->find()->where(['coupon_code'=>strtoupper($data['coupon'])])->first();
    //         if (!empty($getDetails)) {
    //         }
    //         else
    //         {
    //             $ajax_status['status']=0;
    //             $ajax_status['response']="Invalidate Coupon Code.";
    //         }
    //         pr($data);exit();
    //     }
    // }
    // public function finduser()
    // {
    //     $this->loadModel("Users");
    //     if($this->request->is("post")){
    //         pr($this->request->data);exit;
    //         $condition=$this->request->data('condition');
    //         echo $condition;exit;     
    //         $data=$this->request->data('data'); 
    //         $users=$this->Users->find()->where(array("$condition LIKE"=>"%$data%"))->all()->toArray();
    //         pr($users);exit;
    //     }
    //     # code...
    // }
    public function payment() {
        if ($this->request->is("post")) {
            $conn = ConnectionManager::get('default');
            if ($this->request->data('fromwallet') == 1) {
                $res = $conn->execute("UPDATE users SET wallet=wallet - {$this->request->data('amountPaid')} WHERE userid='" . $this->request->data('cusTmerid') . "'");
                $remark = "Rs. {$this->request->data('amountPaid')} has been debited from your wallet against Order ID : {$this->request->data('ORder_ID')}";
                $transaction_id = rand(10000, 99999) . time();
                $this->insertintotransaction($this->request->data('cusTmerid'), $transaction_id, $type = '1', $this->request->data('amountPaid'), $remark, $this->request->data('ORder_ID'), '2');
            } else {
                $remark = "Rs. {$this->request->data('amountPaid')} has been accepted against the Order ID : {$this->request->data('ORder_ID')}";
                $transaction_id = rand(10000, 99999) . time();
                $this->insertintotransaction($this->request->data('cusTmerid'), $transaction_id, $type = '1', $this->request->data('amountPaid'), $remark, $this->request->data('ORder_ID'), '1');
                if ($this->request->data('totalPrice') < $this->request->data('amountPaid')) {
                    $extraordprice = $this->request->data('amountPaid') - $this->request->data('totalPrice');
                    $conn->execute("UPDATE users SET wallet=wallet + {$extraordprice} WHERE userid='" . $this->request->data('cusTmerid') . "'");
                    $walletremark = "Rs. {$extraordprice} has been credited to your wallet for extra payment against Order ID : {$this->request->data('ORder_ID')}";
                    $wallettransaction_id = rand(10000, 99999) . time();
                    $this->insertintotransaction($this->request->data('cusTmerid'), $transaction_id, $type = '2', $extraordprice, $walletremark, $this->request->data('ORder_ID'), '2');
                    $premark = "Rs. {$this->request->data('totalPrice')} has been accepted against the Order ID : {$this->request->data('ORder_ID')}";
                    $ptransaction_id = rand(10000, 99999) . time();
                    $this->insertintotransaction($this->request->data('cusTmerid'), $ptransaction_id, $type = '1', $this->request->data('totalPrice'), $premark, $this->request->data('ORder_ID'), '1');
                }
            }
            $this->Flash->success(__('Payment successfully accecpted.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    function getclothparent($cloth_id='') {
        $this->loadModel('Clothtypes');
        $clothnam = $this->Clothtypes->find()->where(array('id' => $cloth_id))->first()->toArray();
        $clothnamn = $this->Clothtypes->find()->where(array('id' => $clothnam['cloth_parent']))->first()->toArray();
        return $clothnamn['cloth_name'];
    }

}
