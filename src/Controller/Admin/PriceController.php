<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 */
class PriceController extends AppController {

    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $cloth = TableRegistry::get('clothtypes');
        $price = TableRegistry::get('pricings');
        $service = TableRegistry::get('services');
        $cond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => 0);
        $allcloth = $cloth->find()->where($cond)->all()->toArray();
        $arr = array();
        $arrr = array();
        $pricear = array();
        foreach ($allcloth as $val) {
            $newcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $val->id);
            $newcloth = $cloth->find()->where($newcond)->all()->toArray();
            $i = 0;
            $j = 0;
            $k = 0;
            foreach ($newcloth as $v) {
                $arr[$val->id][$i]['cloth_name'] = $v->cloth_name;
                $arr[$val->id][$i]['cloth_parent'] = $v->cloth_parent;
                $arr[$val->id][$i]['id'] = $v->id;
                $i++;
                $newnewcond = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $v->id);
                $newnewcloth = $cloth->find()->where($newnewcond)->all()->toArray();
                foreach ($newnewcloth as $vv) {
                    $pricecond = array("cloth_id" => $vv->id, "status" => 1);
                    $priceres = $price->find()->where($pricecond)->all()->toArray();
                    //pr($priceres);exit;
                    foreach ($priceres as $pv) {
                        $servcond = array("id" => $pv->service_id);
                        $servres = $service->find()->where($servcond)->first();
                        $pricear[$vv->id][$k]['wastype'] = $servres->sname;
                        $pricear[$vv->id][$k]['sicon'] = $servres->sicon;
                        $pricear[$vv->id][$k]['price'] = (float)$pv->price;
                        $pricear[$vv->id][$k]['price_id'] = $pv->id;
                        $pricear[$vv->id][$k]['wash_id'] = $pv->service_id;
                        $pricear[$vv->id][$k]['description'] = $pv->description;
                        $k++;
                    }
                   // pr($pricear);exit;
                    $arrr[$v->id][$j]['cloth_name'] = $vv->cloth_name;
                    $arrr[$v->id][$j]['cloth_parent'] = $vv->cloth_parent;
                    $arrr[$v->id][$j]['id'] = $vv->id;
                    $j++;
                }
            }
        }
        $cond = array("status" => 1, "is_deleted" => 0);
        $allservices = $service->find()->where($cond)->all()->toArray();
        //pr($arrr);exit;
        $this->set(compact("allservices"));
        $this->set('allcloth', $allcloth);
        $this->set('newcloth', $arr);
        $this->set('newnewcloth', $arrr);
        $this->set('pricearr', $pricear);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $pricing = TableRegistry::get('pricings');
        if ($this->request->is("post")) {
            $pric = $pricing->newEntity();
            //pr($this->request->data);
            // exit;
            $id = $this->request->data("id");
            $service_id = $this->request->data("service_id");
            $cloth_id = $this->request->data("cloth_id");
            $price = $this->request->data("price");
            $description = $this->request->data("description");
            $count = $pricing->find()->where(array('service_id' => $service_id, 'cloth_id' => $cloth_id))->count();
            if ($count) {
//                if ($id != null) {
//                    $pric = $pricing->get($id);
//                    $price = $pricing->patchEntity($pric, $this->request->data);
//                    $pricing->save($price);
//                } else {
                $pricing->updateAll(array('price' => $price, 'description' => "$description"), array('service_id' => $service_id, 'cloth_id' => $cloth_id));
//                }
                return $this->redirect(['action' => 'index']);
            } else {
                $price = $pricing->patchEntity($pric, $this->request->data);
                $pricing->save($price);
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if ($id != null) {
            $pricing = TableRegistry::get('pricings');
            $pric = $pricing->get($id);
        }
        if ($pricing->delete($pric)) {
            $this->Flash->success(__('The item has been deleted Successfully.'));
        } else {
            $this->Flash->error(__('The item could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function update_price() {
        $pricing = TableRegistry::get('pricings');
        if ($this->request->is("post")) {
            $desc=$this->request->data("desc");
            $pricing->updateAll(array('price' => $this->request->data("price"),'description'=>"$desc"), array('id' => $this->request->data("id")));
            $update_data['price']="Rs. ".$this->request->data("price")."/-";
            $update_data['desc']=$desc;
            echo json_encode($update_data);
            exit;
        }
    }
    public function addClothimage(){
        $this->loadModel('Clothtypes');
        if (!empty($this->request->data['clothImg']['name'])) {
            $file = $this->request->data['clothImg'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                if (in_array($ext, $arr_ext)) {
                    $imagename=time().$file['name'];
                    $image_path = WWW_ROOT . 'img/' . $imagename;
                    if (move_uploaded_file($file['tmp_name'], $image_path)) {
                      $this->Clothtypes->updateAll(array('cloth_image'=>$imagename),array("id"=>$this->request->data['clothId']));
                      $this->Flash->success(__('Image has been uploaded Successfully.'));
                      return $this->redirect(['action' => 'index']);
                  }
              }else{
                $this->Flash->error(__('Invalid Image Type. Please, try again.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

}