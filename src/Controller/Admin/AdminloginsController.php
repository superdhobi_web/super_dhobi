<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class AdminloginsController extends AppController {
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->loadmodel("Users");
        $this->loadmodel("Orders");
        $conn = ConnectionManager::get('default');
        $UserCount = $this->Users->find()->count();
        $PickupCount=$this->Orders->find()->where(array('order_status'=>3,'order_status'=>6))->count();
        $todaydelivery=$this->Orders->find()->where(array('deliverydate'=>date("Y-m-d")))->count();
        $query=$this->Orders->find();
        $sqldata=$query->select(['count' => $query->func()->sum('totalprice')]);
        $data= $conn->execute("$sqldata");
        $totalsale = $data->fetch();
        $userdata=$this->Orders->find()->order(array("ordereddate"=>"DESC"))->limit("5")->all();
        $this->set("userdata", $userdata);
        $this->set(compact("UserCount","PickupCount","todaydelivery","totalsale"));
        // $this->viewBuilder()->layout('default');
    }

    public function checklogin() {
        $name = $this->request->session()->read('username');
        $password = $this->request->session()->read('password');
        if (!empty($name) && !empty($password)) {
            return $this->redirect(['controller' => 'Adminlogins', 'action' => 'index']);
        } else {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Adminlogins', 'action' => 'login']);
        }
    }

    public function checkpassword() {
        $data=$this->Adminlogins->find()->where(['type' => 1])->all()->toArray();
        $userid = $data[0]['id'];
        $this->set(compact('userid'));
        if ($this->request->is('post')) {
            $password = $this->request->data['password'];
            if (!empty($password)) {
                $query=$this->Adminlogins->find()->where(['password' => $password], ['id' => $userid])->count();
                if ($query == 0) {
                    echo 0;
                } else {
                    echo 1;
                    exit;
                }
            }
        }
    }

    public function changepassword() {
        if ($this->request->is('post')) {
            $userid = $this->request->data['userid'];
            //if (isset($this->request->data['g-recaptcha-response'])) {
            $password = base64_encode($this->request->data['password']);
            if ($this->Adminlogins->updateAll(array("password" => $password), array('id' => $userid))) {
                $this->Flash->success(__('The password has been changed.'));
                return $this->redirect(['action' => 'checkpassword']);
            } else {
                $this->Flash->error(__('The password could not be saved. Please, try again.'));
            }
            //}
        }
    }

    public function login() {
        //$this->viewBuilder()->layout('loginlayout');
        //$this->loadmodel();
        if (!empty($this->request->is("post"))) {
            $username = $this->request->data("username");
            $password = base64_encode($this->request->data("password"));
            $useradata = $this->Adminlogins->find()->where(array('type' => 1))->first();
            if ($username == $useradata['username'] && $password == $useradata['password']) {
                $session = $this->request->session();
                $session->write('username', $username);
                $session->write('userid', $useradata['id']);
                return $this->redirect(['action' => 'index']);
            } else {
                $this->viewBuilder()->layout('loginlayout');
                $this->Flash->error('Please enter your Username and Password Again');
            }
            //  pr($useradata);exit;
//            $user = $this->Auth->identify($this->request->data);
//            if ($user) {
//                $this->Auth->setUser($user);
//                return $this->redirect($this->Auth->redirectUrl());
//            } else {
//                
//            $this->Flash->error(__('Invalid username or password, try again'));
//            }
        } else {
            $this->viewBuilder()->layout('loginlayout');
        }
    }

    public function logout() {
        $this->request->session()->delete("username");
        return $this->redirect(['action' => 'login']);
    }
}
?>
