<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 */
class OffersController extends AppController {

    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($id = null) {
        $this->paginate=array('limit' => 10);
        
        $offerinfo = $this->Offers->newEntity();
        if ($id != null) {
            $offerinfo = $this->Offers->get($id);
        }
        if ($this->request->is('post')) {
            $offerinfo = $this->Offers->patchEntity($offerinfo, $this->request->data);
            if ($this->Offers->save($offerinfo)) {
                $this->Flash->success(__('The Offer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The Offer could not be saved. Please, try again.'));
            }
        } 
        $offers = $this->paginate($this->Offers);
        $this->set(compact('offers'));
        $this->set('_serialize', ['offers']);
        
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function saveprice() {
        $id= $this->request->data("id");
        $price = $this->request->data("price");
        if ($this->Offers->updateAll(array('price' => $price), array('id' => $id))) {
            echo $price;
        }else{
            echo "error";
        }
        exit;
    }

}


?>