<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * TimeSlots Controller
 *
 * @property \App\Model\Table\TimeSlotsTable $TimeSlots
 */
class TimeSlotsController extends AppController {

    public function beforeFilter(Event $event) {
        $name = $this->request->session()->read('username');
        if (empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $query = $this->TimeSlots->find()->where(array('is_deleted' => 0));
        $timeSlots = $this->paginate($query);
        $slotArray = array();
        foreach ($timeSlots as $key => $slotList) {
            switch ($slotList->day) {
                case '1':
                    $slotArray['1'][$key] = array(
                        'id' => $slotList->id,
                        'from' => $slotList->from_time,
                        'to' => $slotList->to_time,
                        'status' => $slotList->status);
                    break;
                case '2':
                    $slotArray['2'][$key] = array(
                        'id' => $slotList->id,
                        'from' => $slotList->from_time,
                        'to' => $slotList->to_time,
                        'status' => $slotList->status);
                    break;
                case '3':
                    $slotArray['3'][$key] = array(
                        'id' => $slotList->id,
                        'from' => $slotList->from_time,
                        'to' => $slotList->to_time,
                        'status' => $slotList->status);
                    break;
                case '4':
                    $slotArray['4'][$key] = array(
                        'id' => $slotList->id,
                        'from' => $slotList->from_time,
                        'to' => $slotList->to_time,
                        'status' => $slotList->status);
                    break;
                case '5':
                    $slotArray['5'][$key] = array(
                        'id' => $slotList->id,
                        'from' => $slotList->from_time,
                        'to' => $slotList->to_time,
                        'status' => $slotList->status);
                    break;
                case '6':
                    $slotArray['6'][$key] = array(
                        'id' => $slotList->id,
                        'from' => $slotList->from_time,
                        'to' => $slotList->to_time,
                        'status' => $slotList->status);
                    break;
                case '7':
                    $slotArray['7'][$key] = array(
                        'id' => $slotList->id,
                        'from' => $slotList->from_time,
                        'to' => $slotList->to_time,
                        'status' => $slotList->status);
                    break;
                default:
                    # code...
                    break;
            }
        }
        $this->set(compact('slotArray'));
        $this->set('_serialize', ['slotArray']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Time Slot id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id=null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!empty($this->request->data("SlotId"))) {
            $id = $this->request->data("SlotId");
        }
        if ($this->TimeSlots->updateAll(array('is_deleted' => 1), array('id' => $id))) {
            $this->Flash->success(__('The time slot has been deleted.'));
        } else {
            $this->Flash->error(__('The time slot could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /*
     * Add and Edit method
     */

    public function addSlot() {
        $timeSlot = $this->TimeSlots->newEntity();
        if ($this->request->is("post")) {
            $id = $this->request->data("SlotId");
            $fromTime = $this->request->data("from_time");
            $toTime = $this->request->data("to_time");
            if ($id != null) {
                $timeSlot = $this->TimeSlots->get($id, [
                'contain' => []
                ]);
            }
            $timeSlot = $this->TimeSlots->patchEntity($timeSlot, $this->request->data);
            if ($this->TimeSlots->save($timeSlot)) {
                if ($id == null) {
                    $slotID = $timeSlot->id;
                    echo "<li class='collection-item'><div><i class='mdi-image-timer'></i><span id='slotLine_" . $slotID . "'> " . $fromTime . " - " . $toTime . " </span><div class='secondary-content'><a href='#modalslot' id='slotclick_" . $slotID . "'  class='btn-floating btn-small  green waves-effect waves-light modal-trigger' onclick=UpdateSlot(" . "'" . $fromTime . "'" . "," . "'" . $toTime . "'" . ",1," . "'" . $slotID . "'" . ")><i class='mdi-content-create'></i></a> <a class='btn-floating btn-small red waves-effect waves-light'><i class='mdi-action-delete' onclick='deleteSlot(" . $slotID . ");'> </i></a></div></div></li>";
                } else {
                    echo "1";
                }
            } else {
                echo '0';
            }
            exit;
        }
    }

    public function delete_slot($id='') {
        if ($this->request->is("post")) {
           $this->loadmodel('TimeSlots');
           $this->TimeSlots->updateAll(array('is_deleted'=>1),array('id'=>$id));
            exit;
       }
    }

}
