<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class AdminusersController extends AppController {

    public function beforeFilter(Event $event) {
        $this->loadmodel("Adminlogins");
        $name = $this->request->session()->read('username');
        if (empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $condition = array();
        if (!empty($this->request->query['userCondition']) || !empty($this->request->query['userData'])) {
            $SearchBy = $this->request->query("userCondition");
            $userData = $this->request->query("userData");
            if ($SearchBy == 'all') {
                $condition = array("OR" => array("Adminusers.emp_id LIKE" => "%$userData%", "Adminusers.username LIKE" => "%$userData%", "Adminusers.department LIKE" => "%$userData%", "Adminusers.email LIKE" => "%$userData%", "Adminusers.mobile LIKE" => "%$userData%"));
            } else {
                $condition = array("Adminusers.$SearchBy LIKE" => "%$userData%");
            }
        }
        $this->paginate = array('limit' => 10);
        $query = $this->Adminusers->find()->where(array('is_delete' => 0));
        $query->where($condition)->all();
        $adminusers = $this->paginate($query);
        $dept = TableRegistry::get('departments');
        $department = $dept->find()->where(array('is_deleted' => 0))->all()->toArray();
        $departmentlist = $dept->find()->where(array('is_deleted' => 0,'status'=>1))->all()->toArray();
        $this->set(compact('adminusers', 'departmentlist','department'));
        $this->set('_serialize', ['adminusers', 'department']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $adminuser = $this->Adminusers->newEntity();
        $emps = $this->Adminlogins->newEntity();
        if ($this->request->is('post')) {
            $id = $this->request->data("id");
            if ($id != null) {
                $adminuser = $this->Adminusers->get($id, [
                'contain' => []
                ]);
            }else{
                $adminuser['emp_id'] = $this->getEmpId();
                $adminuser['password'] = "12345"; //$this->getEmpPassword();
            }
            $adminuser = $this->Adminusers->patchEntity($adminuser, $this->request->data);
            //  pr($adminuser);exit;
            if ($this->Adminusers->save($adminuser)) {
                $emp['username'] = $adminuser['emp_id'];
                $emp['password'] = $adminuser['password'];
                $emp['type'] = 2;
                $emp = $this->Adminlogins->patchEntity($emps, $emp);
                if ($this->Adminlogins->save($emp)) {
                    if ($id != null) {
                        $this->Flash->success(__('The Employee Account has been Updated.'));
                    } else {
                        $this->Flash->success(__('The Employee Account has been created.'));
                    }
                    // return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->Flash->error(__('The Employee could not be saved. Please, try again.'));
            }
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('adminuser'));
        $this->set('_serialize', ['adminuser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Faqcat id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $this->loadModel('Departments');
        $adminuser = $this->Adminusers->get($id, [
        'contain' => []
        ]);
        $department = $this->Departments->find()->where(array('is_deleted' => 0,"status"=>1))->all();
        $this->set(compact('adminuser'));
        $this->set(compact('department'));
        $this->set('_serialize', ['adminuser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Faqcat id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $adminuser = $this->Adminusers->get($id);
        if ($this->Adminusers->updateAll(array('is_delete' => 1), array('id' => $id))) {
            $this->Flash->success(__('The Admin User has been deleted.'));
        } else {
            $this->Flash->error(__('The Admin User could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function changestatus() {
        $id = $this->request->data("id");
        $status = $this->request->data("status");
        if ($this->Adminusers->updateAll(array('status' => $status), array('id' => $id))) {
            echo $status;
        }
        exit;
    }

    public function getEmpId() {
        $query = $this->Adminusers->find()->order(array("id" => "desc"))->first();
        if (empty($query)) {
            $userid = "SDE111".$id;
        } else {
            $id = (int) $query->id + 1;
            $userid = "SDE111".$id;
        }
        return $userid;
    }

    public function getEmpPassword() {
        
    }
    public function checkemail() {
        if ($this->request->is('post')) {
            $checkemail = $this->Adminusers->find()->where(array('email' => $this->request->data['email']))->count();
            echo $checkemail;
            exit;
        }
    }
    
     public function checkunique() {
        $data = $this->request->data("data");
        $type = $this->request->data("type");
        $userid = $this->request->data("edituserid");
        $cond = 1;
        if ($type == 1) {
            $cond.= " AND email = '" . $data . "'";
        } else {
            $cond.= " AND mobile = '" . $data . "'";
        }

        if ($userid != '') {
            $cond.=" AND id !=" . $userid;
        }
        $conn = ConnectionManager::get('default');
        $datacon = $conn->execute("SELECT * FROM adminusers WHERE " . $cond);
        $rows = $datacon->fetchall('assoc');
        if (empty($rows)) {
            echo 1;
        } else {
            echo 2;
        }
        exit;
    }
    public function adduser(){
     //   echo "AddUser";exit;
    }
    public function payroll(){
        
    }
    public function addpayroll(){
        
    }

}
?>
