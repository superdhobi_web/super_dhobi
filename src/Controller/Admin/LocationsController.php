<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Location Controller
 *
 * @property \App\Model\Table\LocationTable $Location
 */
class LocationsController extends AppController {

    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $query = $this->Locations->find()->where(array('is_deleted'=>0));
        $location_parent = $query->toArray();
        $clothArray = array();
        foreach ($location_parent as $key => $cparentList) {
            $clothArray[$key]['id'] = $cparentList->id;
            $clothArray[$key]['location'] = $cparentList->location_name;
            $clothArray[$key]['status'] = $cparentList->status;
            $clothArray[$key]['level'] = (int)$cparentList->level+1;
            $clothArray[$key]['parent'] = $cparentList->location_parent;
        }
        //index elements by id
        $indexedItems=array();
        foreach ($clothArray as $item) {
            $item['subs'] = array();
            $indexedItems[$item['id']] = (object) $item;
        }
        //assign to parent
        $topLevel = array();
        foreach ($indexedItems as $item) {
            if ($item->parent == 0) {
                $topLevel[] = $item;
            } else {
                $indexedItems[$item->parent]->subs[] = $item;
            }
        }
        //pr($topLevel);exit();

        $this->set('location', $topLevel);
    }

    // function makeTree( $rst, $level, &$tree )
    // {
    //     for ( $i=0, $n=count($rst); $i < $n; $i++ )
    //     {
    //         if ( $rst[$i]['parent_id'] == $level )
    //         {
    //             $branch = array(
    //               'id' => $rst[$i]['id'],
    //               'title' => $rst[$i]['title'],
    //               'children' => array()
    //               );
    //             makeTree( $rst, $rst[$i]['id'], $branch['children'] );
    //             $tree[] = $branch;
    //         }
    //     }
    // }
    // function createTreeView($array, 0, $currLevel = 0, $prevLevel = -1) {
    //     foreach ($array as $categoryId => $category) {
    //         if ($currentParent == $category['parent_id']) {                       
    //             if ($currLevel > $prevLevel) echo " <ol class='tree'> "; 
    //             if ($currLevel == $prevLevel) echo " </li> ";
    //             echo '<li> <label for="subfolder2">'.$category['name'].'</label> <input type="checkbox" name="subfolder2"/>';
    //             if ($currLevel > $prevLevel) { $prevLevel = $currLevel; }
    //             $currLevel++; 
    //             createTreeView ($array, $categoryId, $currLevel, $prevLevel);
    //             $currLevel--;               
    //         }   
    //     }
    //     if ($currLevel == $prevLevel) echo " </li>  </ol> ";
    // }
    /**
     * View method
     *
     * @param string|null $id Clothtype id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $clothtype = $this->Locations->get($id, [
            'contain' => []
            ]);

        $this->set('clothtype', $clothtype);
        $this->set('_serialize', ['clothtype']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addLocation() {

        $location = $this->Locations->newEntity();
        if ($this->request->is('post')) {
            $location = $this->Locations->patchEntity($location, $this->request->data);
            $result=$this->Locations->save($location);
            echo $result->id;
        }
        exit;
    } 

    /**
     * Edit method
     *
     * @param string|null $id Clothtype id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editLocation() {
        $id = $this->request->data('location_id');
        if (!empty($id)) {
            $clothtype = $this->Locations->get($id, [
                'contain' => []
                ]);
        }
        if ($this->request->is('post')) {
            $location = $this->request->data('location_name');
            $this->Locations->updateAll(array('location_name' => "$location"), array('id' => $id));
        }
        exit;
    }

    /**
     * Delete method
     *
     * @param string|null $id Clothtype id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteLocation() {
        $this->request->allowMethod(['post', 'delete']);
        $id = $this->request->data('location_id');
        if (!empty($id)) {
            $clothtype = $this->Locations->get($id);
            $this->Locations->updateAll(array('is_deleted'=>1), array('id'=>$id));
            exit;
        }
    }

    function setParent() {
        $result = 0;
        $location = $this->Locations->newEntity();
        if ($this->request->is('post')) {
            $location = $this->Locations->patchEntity($location, $this->request->data);
            $location['level']=1;
            if ($this->Locations->save($location)) {
                $result = 1;
            } else {
                $result = 2;
            }
        }
        echo $result;
        exit();
    }

    function findChild() {
        $id = $this->request->data("location");
        $Child= $this->Locations->find()->where(['location_parent' => $id,'is_deleted'=>0,'status'=>1])->all()->toArray();
        $location_opt = "";
        if ($this->request->data("id") == 1) {
            $location_opt = '<option value="">Circle</option>';
        } else if($this->request->data("id") == 2){
            $location_opt = '<option value="">Location</option>';
        } else{
            $location_opt = '<option value="">Area</option>';
        }
        foreach ($Child as $k => $v) {
            $location_opt .= '<option value="' . $v->id . '">' . $v->location_name . '</option>';
        }
        echo $location_opt;
        exit();
    }

}
