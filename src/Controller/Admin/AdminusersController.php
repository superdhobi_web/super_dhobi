<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

/**
 * Adminusers Controller
 *
 * @property \App\Model\Table\AdminusersTable $Adminusers
 */
class AdminusersController extends AppController {

    public function beforeFilter(Event $event) {
        $this->loadmodel("Adminlogins");
        $this->loadmodel("Departments");
        $this->loadmodel("Runpayrolls");
        $name = $this->request->session()->read('username');
        if (empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $condition = array();
        if (!empty($this->request->query['userCondition']) || !empty($this->request->query['userData'])) {
            $SearchBy = $this->request->query("userCondition");
            $userData = $this->request->query("userData");
            if ($SearchBy == 'all') {
                $condition = array("OR" => array("Adminusers.emp_id LIKE" => "%$userData%", "Adminusers.username LIKE" => "%$userData%", "Adminusers.department LIKE" => "%$userData%", "Adminusers.email LIKE" => "%$userData%", "Adminusers.mobile LIKE" => "%$userData%"));
            } else {
                $condition = array("Adminusers.$SearchBy LIKE" => "%$userData%");
            }
        }
        $this->paginate = array('limit' => 10);
        $query = $this->Adminusers->find()->where(array('is_delete' => 0));
        $query->where($condition)->all();
        $adminusers = $this->paginate($query);
        $dept = TableRegistry::get('departments');
        $department = $dept->find()->where(array('is_deleted' => 0))->all()->toArray();
        $departmentlist = $dept->find()->where(array('is_deleted' => 0, 'status' => 1))->all()->toArray();
        //pr(departmentlist);exit;
        $this->set(compact('adminusers', 'departmentlist', 'department'));
        $this->set('_serialize', ['adminusers', 'department']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function adduser($id=null) {
        $id = base64_decode($id);
        $adminuser = array();
        if ($id) {
            $adminuser = $this->Adminusers->get($id);
        }
        if ($this->request->is('post')) {
            if ($this->request->data['id']) {
                $adminuser = $this->Adminusers->get($id);
            } else {
                $adminuser = $this->Adminusers->newEntity();
            }
            if (isset($this->request->data['pincode'])) {
                $this->request->data['pin'] = implode($this->request->data['pincode'], "");
            }
            $id = $this->request->data['id'];
            $adminusers = $this->Adminusers->patchEntity($adminuser, $this->request->data);
            // pr($adminusers);exit;
            if ($this->Adminusers->save($adminusers, array("id" => $id))) {
                if ($id) {
                    $this->Flash->success(__('The adminuser has been Updated Successfully.'));
                } else {
                    $this->Flash->success(__('The adminuser has been Saved Successfully.'));
                }
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The adminuser could not be saved. Please, try again.'));
            }
        }
        $departmentlist = $this->Departments->find()->where(array('is_deleted' => 0, 'status' => 1))->all()->toArray();
        $this->set(compact('adminuser', 'departmentlist'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Adminuser id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $adminuser = $this->Adminusers->get($id);
        if ($this->Adminusers->updateAll(array('is_delete' => 1), array('id' => $id))) {
            $this->Flash->success(__('The Admin User has been deleted.'));
        } else {
            $this->Flash->error(__('The Admin User could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function payroll() {
        $cond = "";
        $condition = array();
        if (!empty($this->request->query("year"))) {
            $condition = array("year" => $this->request->query("year"));
        }
        if (!empty($this->request->query("month"))) {
            $condition = array("month" => $this->request->query("month"));
        }
        $this->paginate = array('limit' => 5);
        $query = $this->Runpayrolls->find()->where($condition)->order(array("id" => "DESC"));
        $query->where($condition);
        $employee = $this->paginate($query);
        foreach ($employee as $pay) {
            $emp_id = $pay['employee_id'];
            $employ = $this->Adminusers->find()->where(array("id" => $emp_id))->first();
            $pay["employee_name"] = $employ["name"];
            $pay["designation"] = $employ["designation"];
        }
        $this->set(compact("employee", "empl"));
    }

    public function addpayroll() {
        if ($this->request->is("post")) {
            $payroll = $this->Runpayrolls->newEntity();
            $this->request->data["leave_type"]=json_encode($this->request->data["leave_type1"]);
            $employee_payroll = $this->Runpayrolls->patchEntity($payroll, $this->request->data);
            if ($this->Runpayrolls->save($employee_payroll)) {
                $this->Flash->success(__('The Payroll has been Saved Successfully.'));
                return $this->redirect(['action' => 'payroll']);
            } else {
                $this->Flash->error(__('The Payroll could not be saved'));
            }
        } else {
            $adminuser = $this->Adminusers->find()->where(array("status" => 1, "is_delete" => 0))->all()->toArray();
            $department = $this->Departments->find()->where(array("status" => 1, "is_deleted" => 0))->all()->toArray();
            $this->set(compact("adminuser", "department"));
        }
    }

    public function checkunique() {
        $data = $this->request->data("data");
        $type = $this->request->data("type");
        $userid = $this->request->data("edituserid");
        $cond = 1;
        if ($type == 1) {
            $cond.= " AND email = '" . $data . "'";
        } else {
            $cond.= " AND mobile = '" . $data . "'";
        }

        if ($userid != '') {
            $cond.=" AND id !=" . $userid;
        }
        $conn = ConnectionManager::get('default');
        $datacon = $conn->execute("SELECT * FROM adminusers WHERE " . $cond);
        $rows = $datacon->fetchall('assoc');
        if (empty($rows)) {
            echo 1;
        } else {
            echo 2;
        }
        exit;
    }

    public function printpayroll($id=null) {
        $this->viewBuilder()->layout('');
        if ($id != null) {
            $id = base64_decode($id);
            $conn = ConnectionManager::get('default');
            $datacon = $conn->execute("SELECT * FROM adminusers a,runpayrolls b WHERE a.id=b.employee_id AND b.id=$id");
            $employee = $datacon->fetchall('assoc');
            // pr($employee);
            // exit;
            $this->set(compact("employee"));
        }
    }

    public function changestatus() {
        $id = $this->request->data("id");
        $status = $this->request->data("status");
        if ($this->Adminusers->updateAll(array('status' => $status), array('id' => $id))) {
            echo $status;
        }
        exit;
    }

    public function empDetail() {
        if ($this->request->is("post")) {
            $emp_id = $this->request->data("emp_id");
            $year = $this->request->data("year");
            $month = $this->request->data("month");
            $employeeData = $this->Runpayrolls->find()->where(array("status" => 1, "is_deleted" => 0, "employee_id" => $emp_id, "year" => "$year", "month" => "$month"))->first();
            if (!empty($employeeData)) {
                echo 1;
            } else {
                $conn = ConnectionManager::get('default');
                $datacon = $conn->execute("SELECT a.basic,a.branch_name,a.account_no,a.total_TA,a.total_DA,a.gross_salary,b.department_name FROM adminusers a ,departments b WHERE a.department=b.id AND a.id=$emp_id");
                $employee = $datacon->fetchall('assoc');
                echo json_encode($employee[0]);
            }
            exit;
        }
    }

    public function deletepayroll($id="") {
        $run = $this->Runpayrolls->get($id);
        if ($this->Runpayrolls->delete($run)) {
            $this->Flash->success(__('The Payroll has been Deleted Successfully.'));
        } else {
            $this->Flash->error(__('The Payroll could not Deleted Successfully.'));
        }
        return $this->redirect(['action' => 'payroll']);
    }

}
