<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * Printnames Controller
 *
 * @property \App\Model\Table\PrintnamesTable $Printnames
 */
class PrintnamesController extends AppController
{

    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $printnames = $this->paginate($this->Printnames);

        $this->set(compact('printnames'));
        $this->set('_serialize', ['printnames']);
    }

    /**
     * View method
     *
     * @param string|null $id Printname id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $printname = $this->Printnames->get($id, [
            'contain' => []
            ]);

        $this->set('printname', $printname);
        $this->set('_serialize', ['printname']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $printname = $this->Printnames->newEntity();
        if ($this->request->is('post')) {
            $printname = $this->Printnames->patchEntity($printname, $this->request->data);
            if ($this->Printnames->save($printname)) {
                $this->Flash->success(__('The printname has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The printname could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('printname'));
        $this->set('_serialize', ['printname']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Printname id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $printname = $this->Printnames->get($id, [
            'contain' => []
            ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $printname = $this->Printnames->patchEntity($printname, $this->request->data);
            if ($this->Printnames->save($printname)) {
                $this->Flash->success(__('The printname has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The printname could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('printname'));
        $this->set('_serialize', ['printname']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Printname id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $printname = $this->Printnames->get($id);
        if ($this->Printnames->delete($printname)) {
            $this->Flash->success(__('The printname has been deleted.'));
        } else {
            $this->Flash->error(__('The printname could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
