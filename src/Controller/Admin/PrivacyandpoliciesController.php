<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * TimeSlots Controller
 *
 * @property \App\Model\Table\TimeSlotsTable $TimeSlots
 */
class PrivacyandpoliciesController extends AppController {

    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $pc=$this->Privacyandpolicies->find()->first();
        $this->set(compact('pc'));
        $this->set('_serialize', ['pc']);
        if($this->request->is("post")){
            $id=$this->request->data('id');
            $details=$this->request->data('editor1');
            if ($this->Privacyandpolicies->updateAll(array('details'=>$details), array('id'=>$id))) {
                $this->Flash->success(__('The data has been updated.'));
                return $this->redirect(['action' => 'index']);  
            } else {
                $this->Flash->success(__('You have made no changes.'));
            }
        }
    }
}
?>