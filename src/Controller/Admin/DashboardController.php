<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class DashboardController extends AppController {

    public function beforeFilter(Event $event) {
        $this->loadmodel("Users");
        $this->loadmodel("Orders");
        $this->loadmodel("Adminlogins");
        $this->loadmodel("Feedbacks");
    }

    public function index() {
        $name = $this->request->session()->read('username');
        if (empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
        $conn = ConnectionManager::get('default');
        $UserCount = $this->Users->find()->count();
        $PickupCount = $this->Orders->find()->where(array('order_status' => 3, 'order_status' => 6))->count();
        $todaydelivery = $this->Orders->find()->where(array('deliverydate' => date("Y-m-d")))->count();
        $query = $this->Orders->find();
        $sqldata=$query->select(['count' => $query->func()->sum('totalprice')]);
        $data = $conn->execute("$sqldata");
        $totalsale = $data->fetch();
        $userdata = $this->Orders->find()->order(array("ordereddate" => "DESC"))->limit("5")->all();
        $this->set("userdata", $userdata);
        $this->set(compact("UserCount", "PickupCount", "todaydelivery", "totalsale"));

        $orderdata = $this->Feedbacks->find()->where(array("feed_by"=>'user'))->group(array("order_id"))->order(array("id" => "DESC"))->limit("5")->all()->toArray();
        $i = 0;
        foreach ($orderdata as $va) {
            $user = $this->Users->find()->where(array('userid' => $va['userid']))->first();
            $order = $this->Orders->find()->where(array('order_id' => $va['order_id']))->first();
            $dataarr[$i] = $va;
            $dataarr[$i]['user'] = $user['name'];
            $dataarr[$i]['rating'] = $order['rating'];
            $dataarr[$i]['rate_cond'] = $order['rate_cond'];
            $i++;
        }
        //pr($alluserorder);exit;
        $this->set('dataarr', $dataarr);

        // $this->viewBuilder()->layout('default');
    }

    public function checklogin() {
        $name = $this->request->session()->read('username');
        $password = $this->request->session()->read('password');
        if (!empty($name) && !empty($password)) {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        } else {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
    }

    public function checkpassword() {
        $data=$this->Adminlogins->find()->where(['type' => 1])->all()->toArray();
        $userid = $data[0]['id'];
        $this->set(compact('userid'));
        if ($this->request->is('post')) {
            $password = base64_encode($this->request->data['password']);
            if (!empty($password)) {
                $query=$this->Adminlogins->find()->where(['password' => $password], ['id' => $userid])->count();
                if ($query == 0) {
                    echo 0;
                } else {
                    echo 1;
                    exit;
                }
            }
        }
    }

    public function changepassword() {
        if ($this->request->is('post')) {
            $userid = $this->request->data['userid'];
            $password = base64_encode($this->request->data['password']);
            if ($this->Adminlogins->updateAll(array("password" => $password), array('id' => $userid))) {
                $this->Flash->success(__('The password has been changed.'));
            } else {
                $this->Flash->error(__('The password could not be saved. Please, try again.'));
            }
            return $this->redirect(['action' => 'checkpassword']);
        }
    }

    public function login() {
        $this->viewBuilder()->layout('loginlayout');
        $name = $this->request->session()->read('username');
        if (!empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        if (!empty($this->request->is("post"))) {
            $username = $this->request->data("username");
            $password = base64_encode($this->request->data("password"));
            $useradata = $this->Adminlogins->find()->where(array('type' => 1))->first();
            if ($username == $useradata['username'] && $password == $useradata['password']) {
                $session = $this->request->session();
                $session->write('username', $username);
                $session->write('userid', $useradata['id']);
                //$this->Flash->success(__('Successfully login.'));
                return $this->redirect(['action' => 'index']);
            } else {
               //  $this->viewBuilder()->layout('loginlayout');
                $this->Flash->error(__('Invalid User Id and password.'));
            return $this->redirect(['action' => 'login']);
            }
            //  pr($useradata);exit;
//            $user = $this->Auth->identify($this->request->data);
//            if ($user) {
//                $this->Auth->setUser($user);
//                return $this->redirect($this->Auth->redirectUrl());
//            } else {
//                
//            $this->Flash->error(__('Invalid username or password, try again'));
//            }
        } else {
            $this->viewBuilder()->layout('loginlayout');
        }
    }
    public function logout() {
        $this->request->session()->delete("username");
        return $this->redirect(['action' => 'login']);
    }
    public function rolemanagement() {   
    }
}
?>