<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * TimeSlots Controller
 *
 * @property \App\Model\Table\TimeSlotsTable $TimeSlots
 */
class TermsandconditionsController extends AppController {

    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $tc = $this->Termsandconditions->find()->first();
        $this->set(compact('tc'));
        $this->set('_serialize', ['tc']);
        if ($this->request->is("post")) {
            $details = $this->request->data('editor1');
            $id = $this->request->data('id');
            if ($this->Termsandconditions->updateAll(array('details' => $details), array('id' => $id))) {
                $this->Flash->success(__('The T&C has been updated.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->success(__('You have made no change'));
            }
        }
    }
    
}

?>