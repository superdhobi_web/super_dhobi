<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\ORM\ResultSet;

/**
 * Adminusers Controller
 *
 * @property \App\Model\Table\AdminusersTable $Adminusers
 */
class FeedbackController extends AppController {

    public function beforeFilter(Event $event) {
        $this->loadmodel("Adminlogins");
        $this->loadmodel("Departments");
        $this->loadmodel("Orders");
        $this->loadmodel("Feedbacks");
        $this->loadmodel("Users");
        $name = $this->request->session()->read('username');
        if (empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function feedbacklist() {
        $condition = array();
        $this->paginate = array('limit' => 15);
        $query = $this->Feedbacks->find()->where(array())->group(array("order_id"))->order(array("id" => "DESC"));
        $query->where($condition);
        $users = $this->paginate($query);
        $i = 0;
        foreach ($users as $va) {
            $last = $this->Feedbacks->find()->where(array('order_id' => $va['order_id']))->order(array("id" => "DESC"))->first();
            $user = $this->Users->find()->where(array('userid' => $va['userid']))->first();
            $order = $this->Orders->find()->where(array('order_id' => $va['order_id']))->first();
            $data[$i] = $last;
            $data[$i]['user'] = $user['name'];
            $data[$i]['rating'] = $order['rating'];
            $data[$i]['rate_cond'] = $order['rate_cond'];
            $i++;
        }
        $alluserorder = $this->Feedbacks->find()->where(array("order_id" => $data[0]['order_id']))->order(array("id" => "asc"))->all()->toArray();
        //pr($alluserorder);exit;
        $this->set('data', $data);
        $this->set('alldata', $alluserorder);
    }

    public function feedbackreply() {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $feedback = $this->Feedbacks->newEntity();
            $feedback->order_id = $data['order_id'];
            $feedback->userid = $data['userid'];
            $feedback->reason = $data['reason'];
            $feedback->feed_by = "admin";
            $feedback->add_date = date("Y-m-d H:i:s");
            if ($this->Feedbacks->save($feedback)) {
                $this->Flash->success(__('Feedback Saved Successfully.'));
            } else {
                $data = array('status' => 2, 'data' => 'Feedback can not be added!! Try again');
                $this->Flash->error(__('Feedback can not be added!! Try again.'));
            }
            return $this->redirect(['action' => 'feedbacklist']);
        }
    }
public function getfeedback($order_id=''){
       $alluserorder = $this->Feedbacks->find()->where(array("order_id" => $order_id))->order(array("id" => "asc"))->all()->toArray();
       $data = $this->Users->find()->where(array('userid' =>$alluserorder[0]['userid']))->first();
        $this->set('data', $data);
        $this->set('alldata', $alluserorder);
        $this->viewBuilder()->layout('ajax'); //for using ajax include another view page
}
}