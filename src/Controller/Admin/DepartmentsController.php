<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 */
class DepartmentsController extends AppController {

    public function beforeFilter(Event $event) {
        $name = $this->request->session()->read('username');
        if (empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $query = $this->Departments->find()->where(array('is_deleted' => 0));
        $departments = $this->paginate($query);
        $this->set(compact('departments'));
        $this->set('_serialize', ['departments']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        $department = $this->Departments->newEntity();
        if ($id != null) {
            $department = $this->Departments->get($id);
        }
        if ($this->request->is('post')) {
            $department = $this->Departments->patchEntity($department, $this->request->data);
            if ($this->Departments->save($department)) {
                if ($id != null) {
                    $this->Flash->success(__('The Department has been Updated Successfully.'));
                } else {
                    $this->Flash->success(__('The Department has been Saved Successfully'));
                }
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The department could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('department'));
        $this->set('_serialize', ['department']);
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Service id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $department = $this->Departments->get($id, [
        'contain' => []
        ]);
        $this->set(compact('department'));
        $this->set('_serialize', ['department']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Service id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $department = $this->Departments->get($id);
        if ($this->Departments->updateAll(array('is_deleted' => 1), array('id' => $id))) {
            $this->Flash->success(__('The department has been deleted.'));
        } else {
            $this->Flash->error(__('The department could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function changestatus() {
        $status = $this->request->data('status');
        $id = $this->request->data('id');
        if ($this->Departments->updateAll(array('status' => $status), array('id' => $id))) {
            echo $status;
        }
        exit;
    }

}