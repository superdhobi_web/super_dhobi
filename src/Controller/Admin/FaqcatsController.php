<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class FaqcatsController extends AppController {

    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $query = $this->Faqcats->find()->where(array('is_deleted'=>0));
        $faqcats = $this->paginate($query);
        $this->set(compact('faqcats'));
        $this->set('_serialize', ['faqcats']);
    }

    /**
     * View method
     *
     * @param string|null $id Faqcat id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $faqcat = $this->Faqcats->get($id, [
            'contain' => []
            ]);

        $this->set('faqcat', $faqcat);
        $this->set('_serialize', ['faqcat']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id=null) {
        $faqcat = $this->Faqcats->newEntity();
        if ($id != null) {
            $faqcat = $this->Faqcats->get($id, [
                'contain' => []
                ]);
        }
        if ($this->request->is('post')) {
            $faqcat = $this->Faqcats->patchEntity($faqcat, $this->request->data);
            if ($this->Faqcats->save($faqcat)) {
                $this->Flash->success(__('The faqcat has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The faqcat could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('faqcat'));
        $this->set('_serialize', ['faqcat']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Faqcat id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $faqcat = $this->Faqcats->get($id, [
            'contain' => []
            ]);
        $this->set(compact('faqcat'));
        $this->set('_serialize', ['faqcat']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Faqcat id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $faqcat = $this->Faqcats->get($id);
        if($this->Faqcats->updateAll(array('is_deleted'=>1), array('id'=>$id))){

            $this->Flash->success(__('The faqcat has been deleted.'));
        } else {
            $this->Flash->error(__('The faqcat could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function changestatus() {
        $id = $this->request->data("id");
        $status = $this->request->data("status");
        if ($this->Faqcats->updateAll(array('status' => $status), array('id' => $id))) {
            echo $status;
        }
        exit;
    }

}
