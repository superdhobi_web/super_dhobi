<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class TaskmanagesController extends AppController {

    public function beforeFilter(Event $event){
        $name = $this->request->session()->read('username');
        if(empty($name)){
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $cond = "c.is_deleted=0";
        $location = TableRegistry::get('Locations');
        if ($this->request->query) {
            // $condition = array_filter($this->request->query);
            $condition = $this->request->query;
            if (!empty($condition['orderstatus'])) {
                $cond .= " AND a.order_status={$condition['orderstatus']}";
            } else {
                $cond .= " AND (a.order_status=3 OR a.order_status=9) ";
            }
            if (!empty($condition['orderdate'])) {
                $orderdate = date('Y-m-d', strtotime($condition['orderdate']));
                $cond .=" AND (a.pickupdate='{$orderdate}' OR a.deliverydate='{$orderdate}')";
            }
            if (!empty($condition['timeslot'])) {
                $timeslot = $condition['timeslot'];
                $cond .=" AND (a.pickupslot=$timeslot OR a.deliveryslot=$timeslot)";
            }
            if (!empty($condition['city'])) {
                $city = $condition['city'];
                $cond .=" AND b.city=$city";
                $circlelist=$location->find()->where(['location_parent' => $city])->all()->toArray();
                foreach ($circlelist as $val) {
                    $child['circle'][$val['id']] = $val['location_name'];
                }
            }
            if (!empty($condition['circle'])) {
                $circle= $condition['circle'];
                $cond .=" AND b.circle=$circle";
                $circlelist=$location->find()->where(['location_parent' => $circle])->all()->toArray();
                foreach ($circlelist as $val) {
                    $child['location'][$val['id']] = $val['location_name'];
                }
            }
            if (!empty($condition['location'])) {
                $locationdata= $condition['location'];
                $cond .=" AND b.location=$locationdata";
                $circlelist=$location->find()->where(['location_parent' => $locationdata])->all()->toArray();
                foreach ($circlelist as $val) {
                    $child['area'][$val['id']] = $val['location_name'];
                }
            }
            if (!empty($condition['area'])) {
                $area= $condition['area'];
                $cond .=" AND b.area=$area";
            }
            $conn = ConnectionManager::get('default');
            $data = $conn->execute('SELECT * FROM orders a,user_addresses b,users c WHERE a.user_id=c.userid AND (a.pickupadd=b.id OR a.deliveryadd=b.id) AND ' . $cond . ' GROUP BY a.order_id ORDER BY a.ordereddate');
            $rows = $data->fetchAll('assoc');
            $dataarr=array();
            $i=0;
            foreach($rows as $k=>$val ){
                $dataarr[$k]=$val;
                $status=($val['order_status']==3)?1:2;
                $datatask = $conn->execute("SELECT * FROM employee_tasks WHERE orderid='" . $val['order_id'] . "'AND  status=$status GROUP BY orderid ORDER BY id DESC LIMIT 1");
                $rowstask = $datatask->fetch('assoc');
                if(empty($rowstask)){
                    $rowstask=array();
                }
                $dataarr[$k][$val['order_id']]=$rowstask;
            }
            $this->set('rows',$dataarr);
        }
        $cond = array();
        $city=$location->find()->where(['location_parent' => 0])->all()->toArray();
        $empuser = TableRegistry::get('Adminusers');
        $alluser=$empuser->find()->where(['department' => 5, "status" => 1, "is_delete" => 0])->all()->toArray();
        $this->set(compact('alluser'));
        $this->set(compact('city'));
        $this->set(compact('child'));
    }
    public function assigntask() {
        $orderdata = TableRegistry::get('EmployeeTasks');
        $orderen = $orderdata->newEntity();
        $orderen->id_employee = $this->request->data['id_employee'];
        $orderen->orderid = $this->request->data['order_id'];
        $orderen->status = ($this->request->data['status'] == 3) ? 1 : 2;
        $orderen->assign_date = time();
        if ($orderdata->save($orderen)) {
            echo $orderen->id;
            exit;
        } else {
            echo 0;
            exit;
        }
    }
    public function removeassigntask() {
        $orderdata = TableRegistry::get('EmployeeTasks');
        $id = $orderdata->get($this->request->data['id']);
        if ($orderdata->delete($id)) {
            echo 1;
            exit;
        } else {
            echo 2;
            exit;
        }
    }
    

}

?>