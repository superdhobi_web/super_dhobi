<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

//$components = array('Mpdf.Mpdf'); 

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController {

    public function beforeFilter(Event $event) {
        $this->loadModel('Locations');
        $this->loadModel('UserAddresses');
        $name = $this->request->session()->read('username');
        if (empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
        }
        $actionName = $this->request->params['action'];
        if ($actionName == "index") {
            # code...
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        $condition = array('is_deleted' => 0);
        if (!empty($this->request->query['userCondition']) || !empty($this->request->query['userData'])) {
            $SearchBy = $this->request->query("userCondition");
            $userData = trim($this->request->query("userData"));
            if ($SearchBy == 'all') {
                $condition = array("OR" => array("Users.userid LIKE" => "%$userData%", "Users.name LIKE" => "%$userData%", "Users.email LIKE" => "%$userData%", "Users.mobile LIKE" => "%$userData%"));
            } else {
                $condition = array("Users.$SearchBy LIKE" => "%$userData%");
            }
        }
        if ($this->request->query) {
            $cond = array();
            $city_id = $this->request->query('city');
            $circle_id = $this->request->query('circle');
            $location_id = $this->request->query('location');
            $area_id = $this->request->query('area');
            if (!empty($city_id)) {
                $cond = array('city' => $city_id);
                $circlelist=$this->Locations->find()->where(['location_parent' => $city_id])->all()->toArray();
                foreach ($circlelist as $val) {
                    $child['circle'][$val['id']] = $val['location_name'];
                }
            } if (!empty($city_id && $circle_id)) {
                $cond = array('circle' => $circle_id, 'city' => $city_id);
                $circlelist=$this->Locations->find()->where(['location_parent' => $circle_id])->all()->toArray();
                foreach ($circlelist as $val) {
                    $child['location'][$val['id']] = $val['location_name'];
                }
            } if (!empty($city_id && $circle_id && $location_id)) {
                $cond = array('circle' => $circle_id, 'city' => $city_id, 'location' => $location_id);
                $circlelist=$this->Locations->find()->where(['location_parent' => $location_id])->all()->toArray();
                foreach ($circlelist as $val) {
                    $child['area'][$val['id']] = $val['location_name'];
                }
            } if (!empty($city_id && $circle_id && $location_id && $area_id)) {
                $cond = array('city' => $city_id, 'circle' => $circle_id, 'location' => $location_id, 'area' => $area_id);
            }
            if (!empty($cond)) {
                $Address = $this->UserAddresses->find()->where($cond)->all()->toArray();
                $user_id_Arr = array();
                foreach ($Address as $k => $v) {
                    $user_id_Arr[] = $v['user_id'];
                }
                $condition = array("id" => implode(",", $user_id_Arr));
            }
        }
        $city=$this->Locations->find()->where(['location_parent' => 0])->all()->toArray();
        $this->paginate = array('limit' => 5);
        $query = $this->Users->find()->where(array('is_deleted' => 0))->order(array("id" => "DESC"));
        $query->where($condition)->all();
        $users = $this->paginate($query);
        if (!empty($users)) {
            foreach ($users as $k => $v) {
                $user_id = $v['userid'];
                $address=$this->UserAddresses->find()->where(['set_default' => 1, 'user_id' => $user_id])->first();
                $v['address'] = $address['address'];
                $city_Id = $address['city'];
                $citylist=$this->Locations->find()->where(['id' => $city_Id])->first();
                $v["city"] = $citylist['location_name'];
                $circle_Id = $address['circle'];
                $citylist=$this->Locations->find()->where(['id' => $circle_Id])->first();
                $v["circle"] = $citylist['location_name'];
                $locaton_Id = $address['location'];
                $citylist=$this->Locations->find()->where(['id' => $locaton_Id])->first();
                $v["location"] = $citylist['location_name'];
                $area_Id = $address['area'];
                $citylist=$this->Locations->find()->where(['id' => $area_Id])->first();
                $v["area"] = $citylist['location_name'];
            }
        }
        $this->set(compact('users', 'city', 'child'));
        $this->set('_serialize', ['users', 'city']);
    }

    /**
     * Add and Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        $user = $this->Users->newEntity();
        $this->loadModel('Offers');
        if ($id != null) {
            $user = $this->Users->get($id, [
                'contain' => []
                ]);
        }
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $val = $this->userSave($user, $this->request->data);
            if ($val) {
                if ($id != null) {
                    $this->Flash->success(__('The user has been updated.'));
                } else {
                    $this->Flash->success(__('The user has been saved.'));
                }
            } else {
                $this->Flash->error(__('The user could not be saved. Please try again.'));
            }
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * [getusername description]
     * @param  [type] $userName [description]
     * @return [type]           [description]
     */
    public function getusername($name) {
        $username = str_replace(' ', '', $name);
        $countUsername = $this->Users->find()->where(array('username' => $username))->count();
        // echo $countUsername;
        if ($countUsername > 0) {
            $username = $username . $countUsername;
        }
        $username = preg_replace('/[^A-Za-z0-9\-\']/', '', $username);
        //echo $username;exit;
        return $username;
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete', 'get']);
        $user = $this->Users->get($id);
        if ($this->Users->updateAll(array('is_deleted' => 1), array('id' => $id))) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getsearch() {
        if ($this->request->is("post")) {
            $SearchBy = $this->request->data("userCondition");
            $userData = $this->request->data("userData");
            if ($SearchBy == 'all') {
                $condition = array("OR" => array("Users.userid LIKE" => "%$userData%", "Users.name LIKE" => "%$userData%", "Users.email LIKE" => "%$userData%", "Users.mobile LIKE" => "%$userData%"));
            } else {
                $condition = array("Users.$SearchBy LIKE" => "%$userData%");
            }
            $query = $this->Users->find();
            $query->where($condition)->all();
            $result = $query->toArray();
            pr($result);
            exit;
        }
    }

    public function SearchSugg() {
        echo "Exit";
        exit;
    }

    public function address($id=null) {
        $citylist=$this->Locations->find()->where(['location_parent' => 0, 'status' => 1, 'is_deleted' => 0])->all()->toArray();
        if ($id) {
            $userId = base64_decode($id);
            if ($this->request->is("post")) {
                $this->request->data['user_id'] = $userId;
                $userAdd = $this->UserAddresses->newEntity();
                if ($this->request->data("id") != null) {
                    $userAdd = $this->UserAddresses->get($this->request->data("id"));
                }
                if ($this->UserAddresses->find()->where(array('user_id' => $userId, 'is_deleted' => 0, 'status' => 1))->count() == 0) {
                    $this->request->data["set_default"] = 1;
                }
                $userAdd = $this->UserAddresses->patchEntity($userAdd, $this->request->data);
                //pr($userAdd);exit;
                if ($this->UserAddresses->save($userAdd)) {
                    $this->Flash->success(__('The user address has been saved.'));
                } else {
                    // $this->Flash->success(__('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><strong></strong>The user address could not be saved. Please try again.</div>'));
                    $this->Flash->error(__('The user address could not be saved. Please try again.'));
                }
            }
            $address=$this->UserAddresses->find()->where(['user_id' => $userId, 'is_deleted' => 0, 'status' => 1])->all()->toArray();
            foreach ($address as $k => $v) {
                // pr($v);exit;
                $Location1 = $this->Locations->find()->where(array('id' => $v['city']))->first()->toArray();
                $v['city'] = $Location1['location_name'];
                $Location2 = $this->Locations->find()->where(array('id' => $v['circle']))->first()->toArray();
                $v['circle'] = $Location2['location_name'];
                $Location3 = $this->Locations->find()->where(array('id' => $v['location']))->first()->toArray();
                $v['location'] = $Location3['location_name'];
                $Location4 = $this->Locations->find()->where(array('id' => $v['area']))->first();
                $v['area'] = $Location4['location_name'];
            }
        }

        $this->set(compact('citylist', 'address'));
        $this->set('_serialize', ['citylist', 'address']);
    }

    public function setAddress() {
        $id = $this->request->data("id");
        $AddressTable = TableRegistry::get('UserAddresses');
        $address=$AddressTable->find()->where(['id' => $id])->first()->toArray();
// $location = TableRegistry::get('Locations');
// $citylist=$location->find()->where(['location_parent' => $address['city']])->all()->toArray();
        echo json_encode($address);
        exit;
    }

    public function setDefault() {
        $id = $this->request->data("id");
        $user_id = $this->request->data("user_id");
        $address = TableRegistry::get('UserAddresses');
        $address->updateAll(array("set_default" => 0), array("user_id" => $user_id));
        $address->updateAll(array("set_default" => 1), array("user_id" => $user_id, "id" => $id));
        exit;
    }

    public function defaultAdd() {
        if ($this->request->is("post")) {
            $user_id = $this->request->data("user_id");
            $AddressTable = TableRegistry::get('UserAddresses');
            $address=$AddressTable->find()->where(['user_id' => $user_id, 'set_default' => 1])->first()->toArray();
            if (!empty($address["id"])) {
                echo $address["id"];
            }
        }
        exit;
    }

    public function changestatus() {
        $id = $this->request->data("id");
        $status = $this->request->data("status");
        if ($this->Users->updateAll(array('status' => $status), array('id' => $id))) {
            echo $status;
        }
        exit;
    }

    public function exportdata($data= '') {
        $this->loadModel('Locations');
        $this->loadModel('UserAddresses');
        $userdata=$this->Users->find()->where(['status' => 1])->all()->toArray();
        $userinfo = array();
        $file = fopen('userdata.csv', 'w');
        fputcsv($file, array('Username', 'Name', 'Mobile', 'Email', 'wallet', 'address', 'city','circle', 'location', 'area'));
        if (!empty($userdata)) {
            foreach ($userdata as $key => $value) {
                $userid = $value['userid'];
                $userinfo['username'] = $value['username'];
                $userinfo['name'] = $value['name'];
                $userinfo['mobile'] = $value['mobile'];
                $userinfo['email'] = $value['email'];
                $userinfo['wallet'] = $value['wallet'];
                $useraddress=$this->UserAddresses->find()->where(['user_id' => $userid], ['status' => 1])->first();
                $userinfo['address'] = $useraddress['address'];
                $cityid = $useraddress['city'];
                $citylist=$this->Locations->find()->where(['id' => $cityid, 'status' => 1])->first();
                $userinfo[$cityid] = $citylist['location_name'];
                $circleid = $useraddress['circle'];
                $circlelist=$this->Locations->find()->where(['id' => $circleid, 'status' => 1])->first();
                $userinfo[$circleid] = $circlelist['location_name'];
                $locationid = $useraddress['location'];
                $locationlist=$this->Locations->find()->where(['id' => $locationid, 'status' => 1])->first();
                $userinfo[$locationid] = $locationlist['location_name'];
                $areaid = $useraddress['area'];
                $arealist=$this->Locations->find()->where(['id' => $areaid, 'status' => 1])->first();
                $userinfo[$areaid] = $arealist['location_name'];
                fputcsv($file, $userinfo);
            }
        }

        fclose($file);

// Download file
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=userdata.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        readfile('userdata.csv');

        exit();
    }

    public function savewallet() {
        $userid = $this->request->data("userid");
        $amount = $this->request->data("amount");
        $preamount = $this->request->data("prevamount");
        $totalamount = (float) $amount + $preamount;
        if ($this->Users->updateAll(array('wallet' => $totalamount), array('userid' => $userid))) {
            $transaction_id = rand(10000, 99999) . time();
            $this->insertintotransaction($user_id = $userid, $transaction_id = $transaction_id, $type = '2', $amount = $amount, $remark = "Add To Wallet By admin", $order_id = NULL,$for_whom=2);
            echo $totalamount;
        } else {
            echo "error";
        }
        exit;
    }

    public function checkreferby() {
        $ref_by = $this->request->data("ref_by");
        $query = $this->Users->find()->where(array('ref_code' => $ref_by, "status" => 1, "is_deleted" => 0))->first();
        if (!empty($query)) {
            echo 1;
        } else {
            echo 2;
        }
        exit;
    }

    public function checkunique() {
        $data = $this->request->data("data");
        $type = $this->request->data("type");
        $userid = $this->request->data("edituserid");
        $cond = 1;
        if ($type == 1) {
            $cond.= " AND email = '" . $data . "'";
        } else {
            $cond.= " AND mobile = '" . $data . "'";
        }

        if ($userid != '') {
            $cond.=" AND id !=" . $userid;
        }
        $conn = ConnectionManager::get('default');
        $datacon = $conn->execute("SELECT * FROM users WHERE " . $cond);
        $rows = $datacon->fetchall('assoc');
        if (empty($rows)) {
            echo 1;
        } else {
            echo 2;
        }
        exit;
    }

    public function getuserorderdetails($user_id='') {
        $this->loadModel('Timeslots');
        $this->loadModel('Locations');
        $this->loadModel('Orders');
        $this->loadModel('UserAddresses');
        $this->loadModel('OrderDetails');
        $conn = ConnectionManager::get('default');
        $data = $conn->execute('SELECT * FROM orders WHERE user_id="' . $user_id . '" GROUP BY order_id order by ordereddate desc');
        $rows = $data->fetchAll('assoc');
        $arr = array();
        $i = 0;
        foreach ($rows as $va) {
            $arr[$i]['order_id'] = $va['order_id'];
            $arr[$i]['pickupdate'] = date("jS F, Y", strtotime($va['pickupdate']));
            $pickslot = $this->Timeslots->find()->where(array('id' => $va['pickupslot']))->first();
            $arr[$i]['pickupslot'] = date("h:i A", strtotime($pickslot->from_time)) . "-" . date("h:i A", strtotime($pickslot->to_time));
            $pickadd = $this->UserAddresses->find()->where(array('id' => $va['pickupadd']))->first();
            $pickcity = $this->Locations->find()->where(array('id' => $pickadd->city))->first();
            $pickcircle = $this->Locations->find()->where(array('id' => $pickadd->circle))->first();
            $picklocation = $this->Locations->find()->where(array('id' => $pickadd->location))->first();
            $pickarea = $this->Locations->find()->where(array('id' => $pickadd->area))->first();
            $arr[$i]['pickcity'] = $pickcity->location_name;
            $arr[$i]['pickcircle'] = $pickcircle->location_name;
            $arr[$i]['picklocation'] = $picklocation->location_name;
            $arr[$i]['pickarea'] = $pickarea->location_name;
            $arr[$i]['pickaddress'] = $pickadd->address;
            $arr[$i]['pickpin'] = $pickadd->pincode;
            $arr[$i]['deliverydate'] = date("jS F, Y", strtotime($va['deliverydate']));
            $delislot = $this->Timeslots->find()->where(array('id' => $va['deliveryslot']))->first();
            $arr[$i]['deliveryslot'] = date("h:i A", strtotime($delislot->from_time)) . "-" . date("h:i A", strtotime($delislot->to_time));
            $deliadd = $this->UserAddresses->find()->where(array('id' => $va['pickupadd']))->first();
            $delicity = $this->Locations->find()->where(array('id' => $deliadd->city))->first();
            $delicircle = $this->Locations->find()->where(array('id' => $deliadd->circle))->first();
            $delilocation = $this->Locations->find()->where(array('id' => $deliadd->location))->first();
            $deliarea = $this->Locations->find()->where(array('id' => $deliadd->area))->first();
            $arr[$i]['delicity'] = $delicity->location_name;
            $arr[$i]['delicircle'] = $delicircle->location_name;
            $arr[$i]['delilocation'] = $delilocation->location_name;
            $arr[$i]['delikarea'] = $deliarea->location_name;
            $arr[$i]['deliaddress'] = $deliadd->address;
            $arr[$i]['delipin'] = $deliadd->pincode;
            $arr[$i]['totalprice'] = $va['totalprice'];
            $arr[$i]['order_status'] = $va['order_status'];
            $arr[$i]['user_id'] = $va['user_id'];
            $arr[$i]['order_id'] = $va['order_id'];
            $arr[$i]['ordereddate'] = date("jS F, Y", strtotime($va['ordereddate']));
            $arr[$i]['status'] = $va['order_status'];
            $clotharr = $this->OrderDetails->find()->where(array('order_id' => $va['order_id']))->order(array("id" => "asc"))->all()->toArray();
            $tota = 0;
            foreach ($clotharr as $ne) {
                $tota = (int) $tota + $ne->quantity;
            }
            $arr[$i]['totalcloth'] = $tota;
            $i++;
        }
        $this->set('orderdetail', $arr);
        $this->viewBuilder()->layout('ajax'); //for using ajax include another view page
    }

    public function getwalletdetails($user_id) {
        $conn = ConnectionManager::get('default');
        $transorder = $conn->execute('SELECT * FROM usertransactions WHERE user_id="' . $user_id . '" AND for_whom=2 ORDER BY id asc');
        $alltransorder = $transorder->fetchAll('assoc');
        $this->set('transorder', $alltransorder);
        $this->viewBuilder()->layout('ajax'); //for using ajax include another view page
    }

    public function address_delete() {
        if ($this->request->is("post")) {
            $address_id = $this->request->data("address_id");
            //echo $address_id;exit;
            if ($this->UserAddresses->updateAll(array("is_deleted" => 1), array('id' => $address_id))) {
                echo "1";
            } else {
                echo "0";
            }
            exit;
        }
    }

}

?>
