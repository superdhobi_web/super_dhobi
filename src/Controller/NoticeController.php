<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Notice Controller
 *
 * @property \App\Model\Table\NoticeTable $Notice
 */
class NoticeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $notice = $this->paginate($this->Notice);

        $this->set(compact('notice'));
        $this->set('_serialize', ['notice']);
    }

    /**
     * View method
     *
     * @param string|null $id Notice id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $notice = $this->Notice->get($id, [
            'contain' => []
        ]);

        $this->set('notice', $notice);
        $this->set('_serialize', ['notice']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $notice = $this->Notice->newEntity();
        if ($this->request->is('post')) {
            $notice = $this->Notice->patchEntity($notice, $this->request->data);
            if ($this->Notice->save($notice)) {
                $this->Flash->success(__('The notice has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The notice could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('notice'));
        $this->set('_serialize', ['notice']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Notice id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $notice = $this->Notice->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $notice = $this->Notice->patchEntity($notice, $this->request->data);
            if ($this->Notice->save($notice)) {
                $this->Flash->success(__('The notice has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The notice could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('notice'));
        $this->set('_serialize', ['notice']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Notice id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $notice = $this->Notice->get($id);
        if ($this->Notice->delete($notice)) {
            $this->Flash->success(__('The notice has been deleted.'));
        } else {
            $this->Flash->error(__('The notice could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
