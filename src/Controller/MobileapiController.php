<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class MobileapiController extends AppController {

    public function beforeFilter(Event $event) {
        $this->loadmodel("Users");
        $this->loadmodel("Pricings");
        $this->loadmodel("Services");
        $this->loadModel('Termsandconditions');
        $this->loadModel('Privacyandpolicies');
        $this->loadModel('Clothtypes');
        $this->loadModel('Coupons');
        $this->loadModel('Locations');
        $this->loadModel('UserAddresses');
        $this->loadModel('Timeslots');
        $this->loadModel('Orders');
        $this->loadModel('OrderDetails');
        $this->loadModel('Feedbacks');
        // $this->viewBuilder()->layout('indexlayout');
    }

    /**
     * [login description : Manual Login by Android Users.....]
     * @return [JSON] [Status with users details......]
     * @author Dhiraj Mohapatra <[dhiraj@pcinfosolutions.com]>
     * @data("28-04-2016")
     */
    public function login() {
        if ($this->request->is("post")) {
            if (($this->request->data['username'] != "" && $this->request->data['username'] != "0") && ($this->request->data['password'] != "" && $this->request->data['password'] != "0")) {
                $userid = $this->request->data['username'];
                $password = $this->request->data['password'];
                if (is_numeric($userid)) {
                    $usernameCond = "mobile";
                } elseif (filter_var($userid, FILTER_VALIDATE_EMAIL)) {
                    $usernameCond = "email";
                } else {
                    $usernameCond = "userid";
                }
                $getLogin = $this->Users->find()->where(array($usernameCond => $userid, 'password' => $password))->first();
                if (!empty($getLogin)) {
                    $getLogin['mobile_varified'] = $getLogin['mobile_varified'] ? $getLogin['mobile_varified'] : 0;
                    $arr['status'] = 1;
                    $arr['data'] = $getLogin;
                } else {
                    $arr['status'] = 0;
                    $arr['data'] = "Invalid Credential, Please try again.";
                }
            } else {
                $arr['status'] = 0;
                $arr['data'] = "Plz check post request";
            }
        }
        echo json_encode($arr);
        exit;
    }

    public function fblogin() {
        // if ($this->request->is("post")) {
        // }
    }

    public function googlelogin() {
        // if ($this->request->is("post")) {
        // }
    }

    /**
     * [signup description]
     * @return [JSON] [Save and return Status with users details......]
     * @author Dhiraj Mohapatra <[dhiraj@pcinfosolutions.com]>
     * @data("28-04-2016")
     */
    public function signup() {
        if ($this->request->is("post") && !empty($this->request->data)) {
            if ($this->request->data['email'] != "" && ($this->request->data['password'] != "" && $this->request->data['mobile'] != "" && $this->request->data['name'] != "")) {
                $email = trim($this->request->data['email']);
                $password = trim($this->request->data['password']);
                $fullname = trim($this->request->data['name']);
                $mobile = trim($this->request->data['mobile']);
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    if (is_numeric($mobile)) {
                        if (strlen($mobile) == 10) {
                            //================= Save users and wallet with transaction ============//
                            $saveStatus = $this->saveuser($user, $id = null, "mob");
                            if ($saveStatus == "1") {

                                $arr['status'] = 1;
                                $arr['data'] = "Mobile Number muyst be 10 digits..";
                                $this->Flash->success(__('The user has been saved.'));
                            } else {
                                $this->Flash->error(__('The user could not be saved. Please, try again.'));
                            }
                            //=====================================================================//
                        } else {
                            $arr['status'] = 0;
                            $arr['data'] = "Mobile Number muyst be 10 digits..";
                        }
                    } else {
                        $arr['status'] = 0;
                        $arr['data'] = "Invalid Mobile Number..";
                    }
                } else {
                    $arr['status'] = 0;
                    $arr['data'] = "Invalid Email-ID..";
                }
            }
        }
    }

    public function parentclothlist() {
        $parent_cloth = $this->Clothtypes->find()->where(array('status' => 1, 'is_deleted' => 0, 'cloth_parent' => 0))->all()->toArray();

        if (!empty($parent_cloth)) {
            $parentlist['status'] = 1;
            foreach ($parent_cloth as $key => $parent) {
                $cloth_id = $parent['id'];
                $parentlist['data'][$key]['Parent'] = $parent->cloth_name;
                $parentlist['data'][$key]['parent_id'] = $cloth_id;
                $cloth_child = array();
                $childlist = $this->Clothtypes->find()->where(array('cloth_parent' => $cloth_id, 'status' => 1, 'is_deleted' => 0))->all()->toArray();
                if (!empty($childlist)) {
                    foreach ($childlist as $k => $child) {
                        $parentlist['data'][$key]['clothes'][$k]['cloth'] = $child->cloth_name;
                        $parentlist['data'][$key]['clothes'][$k]['id'] = $child->id;
                        $parentlist['data'][$key]['clothes'][$k]['description'] = '';
                        $parentlist['data'][$key]['clothes'][$k]['img'] = $child->cloth_image ? BASE_URL . "img/" . $child->cloth_image : "";
                        ;
                    }
                    //$parentval['data'][$key]['child'] = $cloth_child;
                } else {
                    $parentlist['data'][$key]['clothes'] = array();
                }
            }
        } else {
            $parentlist['status'] = 0;
            $parentlist['data'] = "No Cloth Found";
        }
        //pr($parentlist);//exit;
        echo json_encode($parentlist);
        exit;
    }

    public function servicedetails() {
        if ($this->request->is("post")) {
            if (!empty($this->request->data("parent_cloth"))) {
                $condition = array("status" => 1, "is_deleted" => 0, "cloth_parent" => $this->request->data("parent_cloth"));
                $data = $this->Clothtypes->find()->where($condition)->all()->toArray();
                if (!empty($data)) {
                    $postData['status'] = 1;
                    foreach ($data as $ke => $clothtype) {
                        $cond = array("status" => 1, "cloth_id" => $clothtype->id);
                        $prices = $this->Pricings->find()->where($cond)->all()->toArray();
                        if (!empty($prices)) {
                            $service_val = array();
                            foreach ($prices as $k => $price) {
                                $cloth_id = $price['cloth_id'];
                                $service_id = $price['service_id'];
                                $cloth = $this->Clothtypes->find()->where(array('id' => $cloth_id, "status" => 1, "is_deleted" => 0))->first();
                                if (isset($cloth) && !empty($cloth)) {
                                    $postData['data'][$ke]["cloth"] = $cloth['cloth_name'];
                                    $postData['data'][$ke]["id"] = $cloth['id'];
                                    $postData['data'][$ke]['image'] = $clothtype['cloth_image'] ? BASE_URL . "img/" . $clothtype['cloth_image'] : "";
                                    $service_details = $this->Services->find()->where(array('id' => $service_id, 'status' => 1, 'is_deleted' => 0))->first();
                                    if (!empty($service_details)) {
                                        $postData['data'][$ke]["services"][$k]['wash'] = $service_details['sname'];
                                        $postData['data'][$ke]["services"][$k]['wash_id'] = $service_id;
                                        $postData['data'][$ke]["services"][$k]['price'] = $price['price'];
                                    }
                                }
                            }
                        } else {
                            $postData['data'][$ke]["cloth"] = $clothtype->cloth_name;
                            $postData['data'][$ke]["id"] = $clothtype->id;
                            $postData['data'][$ke]["services"] = array();
                        }
                    }
                } else {
                    $postData = array("status" => 0, "data" => "No cloth Found for this clothtypes");
                }
            } else {
                $postData = array("status" => 0, "data" => "Data not sent properly");
            }
            echo json_encode($postData);
            exit;
        }
    }

    public function coupondata() {
        $coupondate = date('Y-m-d');
        $condition = array("AND" => array("Coupons.end_date >" => $coupondate, 'Coupons.status' => 1, 'Coupons.is_delete' => 0));
        $couponData = $this->Coupons->find()->where($condition)->all()->toArray();
        if (!empty($couponData)) {
            $couponVal['status'] = 1;
            foreach ($couponData as $totcpn => $coupon) {
                $couponVal['data'][$totcpn]['id'] = $coupon['id'];
                $couponVal['data'][$totcpn]['coupon_code'] = $coupon['coupon_code'];
                $couponVal['data'][$totcpn]['coupon_name'] = $coupon['coupon_name'];
                $couponVal['data'][$totcpn]['exp_date'] = date("jS F, Y", strtotime($coupon['end_date']));
                $couponVal['data'][$totcpn]['image'] = null; //$coupon['image_mob'];
            }
        } else {
            $couponVal = array("status" => 0,
                "data" => "No Coupons Found");
        }
        echo json_encode($couponVal);
        exit;
    }

    public function locationlist() {
        if (!empty($this->request->data('location_id'))) {
            $locationData = $this->Locations->find()->where(array('location_parent' => $this->request->data('location_id'), 'status' => 1, 'is_deleted' => 0))->all()->toArray();
            if (!empty($locationData)) {
                foreach ($locationData as $k => $location) {
                    $data[$k]['id'] = $location['id'];
                    $data[$k]['location_name'] = $location['location_name'];
                }
                $location_list = array('status' => 1,
                    'data' => $data
                );
            } else {
                $location_list = array('status' => 0,
                    'data' => "No location found"
                );
            }
        } else {
            $cityData = $this->Locations->find()->where(array('location_parent' => 0, 'status' => 1, 'is_deleted' => 0))->all();
            if (!empty($cityData)) {
                foreach ($cityData as $k => $city) {
                    $data[$k]['id'] = $city['id'];
                    $data[$k]['city'] = $city['location_name'];
                }
                $location_list = array('status' => 1,
                    'data' => $data
                );
            } else {
                $location_list = array('status' => 0,
                    'data' => "No Cities available"
                );
            }
        }
        echo json_encode($location_list);
        exit;
    }

    public function couponDetail() {
        if ($this->request->is("post")) {
            if (!empty($this->request->data("coupon_id"))) {
                $coupon_id = $this->request->data("coupon_id");
                $couponData = $this->Coupons->find()->where(array('id' => $coupon_id, 'is_delete' => 0, 'status' => 1))->first();
                $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
                if (!empty($couponData)) {
                    $coupon_detail = array('status' => 1,
                        'data' => array()
                    );
                    $data['coupon_code'] = $couponData['coupon_code'];
                    $data['coupon_name'] = $couponData['coupon_name'];
                    $data['description'] = $couponData['coupon_desc'];
                    $day = json_decode($couponData['weekday']);
                    $d = array_keys(array_intersect(array_flip($days), $day));
                    $da = implode($d, ",");
                    $data['days'] = "This Coupon is valid on following Days: " . $da;
                    $washtype = json_decode($couponData['washtype']);
                    $washt = array();
                    foreach ($washtype as $wash) {
                        $washName = $this->Services->find()->where(array('id' => $wash))->first();
                        array_push($washt, $washName['sname']);
                    }
                    $washval = implode($washt, ",");
                    $data['wash'] = "This Coupon is valid for the following washtypes: " . $washval;
                    $data['coupon_code'] = $couponData['coupon_code'];
                    $data['min_apply_amt'] = "Minimum purchase amount should be more than Rs" . $couponData['min_apply_amt'] . "/-";
                    $data['min_amt'] = "Rs" . $couponData['min_apply_amt'] . "/-";
                    $data['discount'] = $couponData['discount'] . "% of billing amount will be deducted from Total Price";
                    $data['min_discount'] = "Discount amount should be atleast Rs." . $couponData['min_discount'] . "/-";
                    $data['max_discount'] = "Discount amount should not be more than Rs." . $couponData['max_discount'] . "/-";
                    $data['customer_type'] = ($couponData['customer_type'] == 1) ? "This Coupon code is Applicable only for New Users" : (($couponData['customer_type'] == 2) ? "This Coupon code is Applicable only for Existing Users" : "This Coupon code is Applicable for both New and Existing Users");
                    $coupon_detail = array('status' => 1,
                        'data' => $data
                    );
                } else {
                    $coupon_detail = array('status' => 2,
                        'data' => 'No Coupon Found'
                    );
                }
            } else {
                $coupon_detail = array('status' => 0,
                    'data' => 'Insufficient Data'
                );
            }
            echo json_encode($coupon_detail);
            exit;
        }
    }

    public function getUserAddress() {
        if ($this->request->is("post")) {
            $user_id = $this->request->data("user_id");
            if (!empty($this->request->data("user_id"))) {
                $address = $this->UserAddresses->find()->where(array('user_id' => $user_id, "is_deleted" => 0))->all()->toArray();
                if (!empty($address)) {
                    foreach ($address as $key => $address) {
                        $data[$key]['address_id'] = $address['id'];
                        $data[$key]['address_heading'] = $address['add_heading'];
                    }
                    $User_address = array('status' => 1, 'data' => $data);
                } else {
                    $User_address = array('status' => 2, 'data' => "No address found for this User");
                }
            } else {
                $User_address = array('status' => 0, 'data' => "Insufficient Data");
            }
            echo json_encode($User_address);
            exit;
        }
    }

    public function getTimeslot() {
        if ($this->request->is('post')) {
            $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
            if (!empty($this->request->data('date'))) {
                $date = str_replace('/', '-', $this->request->data('date'));
                $day = date("D", strtotime($date));
                $day_id = array_keys($days, $day);
                $flag = $this->request->data('flag');
                $slots = $this->Timeslots->find()->where(array('day' => $day_id[0], 'status' => 1, 'is_deleted' => 0, 'is_leave' => 0))->all()->toArray();
                if (!empty($slots)) {
                    foreach ($slots as $k => $slot) {
                        $data[$k]['slot_id'] = $slot['id'];
                        $data[$k]['from_to'] = date('g:i a', strtotime($slot['from_time'])) . "-" . date('g:i a', strtotime($slot['to_time']));
                        //  $data[$k]['from_to'] = $slot['from_time'];
                        //  $data[$k]['to_time'] = $slot['to_time'];
                    }
                    $slot_Data = array('status' => 1, 'data' => $data);
                } else {
                    $slot_Data = array('status' => 0, 'data' => "Timeslot not assigned for this date");
                }
            } else {
                $slot_Data = array('status' => 0, 'data' => "Insufficient Data");
            }
            echo json_encode($slot_Data);
            exit;
        }
    }

    public function getAddressDetail() {
        if ($this->request->is("post")) {
            if (!empty($this->request->data("address_id"))) {
                $address_id = $this->request->data("address_id");
                $address = $this->UserAddresses->find()->where(array('id' => $address_id))->first();
                if (!empty($address)) {
                    $data['address_heading'] = $address['add_heading'];
                    $city_id = $address['city'];
                    $city_location = $this->Locations->find()->where(array('id' => $city_id))->first();
                    $data['city'] = $city_location['location_name'];
                    $circle_id = $address['circle'];
                    $city_location = $this->Locations->find()->where(array('id' => $circle_id))->first();
                    $data['circle'] = $city_location['location_name'];
                    $location_id = $address['location'];
                    $city_location = $this->Locations->find()->where(array('id' => $location_id))->first();
                    $data['location'] = $city_location['location_name'];
                    $area_id = $address['area'];
                    $city_location = $this->Locations->find()->where(array('id' => $area_id))->first();
                    $data['area'] = $city_location['location_name'];
                    $data['address'] = $address['address'];
                    $data['landmark'] = $address['landmark'];
                    $data['pincode'] = $address['pincode'];
                    $User_address = array('status' => 1, 'data' => $data);
                } else {
                    $User_address = array('status' => 2, 'data' => "No address found for this User");
                }
            } else {
                $User_address = array('status' => 0, 'data' => "Insufficient Data");
            }
            echo json_encode($User_address);
            exit;
        }
    }

    public function getUserProfile() {
        if ($this->request->is('post')) {
            if (!empty($this->request->data('user_id'))) {
                $user_id = $this->request->data('user_id');
                $User_detail = $this->Users->find()->where(array('userid' => $user_id))->first();
                $address = $this->UserAddresses->find()->where(array('user_id' => $user_id, 'is_deleted' => 0))->all()->toArray();
                if (!empty($User_detail)) {
                    $data['username'] = $user_id;
                    $data['name'] = $User_detail['name'];
                    $data['email'] = $User_detail['email'];
                    $data['mobile'] = $User_detail['mobile'];
                    $data['ref_code'] = $User_detail['ref_code'];
                    $i = 0;
                    $arr = array();
                    foreach ($address as $addr) {
                        $arr[$i]['add_heading'] = $addr['add_heading'];
                        $arr[$i]['addr_id'] = $addr['id'];
                        $arr[$i]['city'] = $addr['city'];
                        $arr[$i]['circle'] = $addr['circle'];
                        $arr[$i]['location'] = $addr['location'];
                        $arr[$i]['area'] = $addr['area'];
                        $city_location = $this->Locations->find()->where(array('id' => $addr['city']))->first();
                        $arr[$i]['city_name'] = $city_location['location_name'];
                        $city_location = $this->Locations->find()->where(array('id' => $addr['circle']))->first();
                        $arr[$i]['circle_name'] = $city_location['location_name'];
                        $city_location = $this->Locations->find()->where(array('id' => $addr['location']))->first();
                        $arr[$i]['location_name'] = $city_location['location_name'];
                        $city_location = $this->Locations->find()->where(array('id' => $addr['area']))->first();
                        $arr[$i]['area_name'] = $city_location['location_name'];
                        $arr[$i]['address'] = $addr['address'];
                        $arr[$i]['landmark'] = $addr['landmark'];
                        $arr[$i]['pincode'] = $addr['pincode'];
                        $i++;
                    }
                    $data['address'] = $arr;
                    $userDetail = array('status' => 1, 'data' => $data);
                } else {
                    $userDetail = array('status' => 2, 'data' => "User Detail not found");
                }
            } else {
                $userDetail = array('status' => 0, 'data' => "Insufficient Data");
            }
            echo json_encode($userDetail);
            exit;
        }
    }

    public function getOrderData() {
        if ($this->request->is('post')) {
            if (!empty($this->request->data('user_id'))) {
                $user_id = $this->request->data('user_id');
                $Order_Data = $this->Orders->find()->where(array('user_id' => $user_id))->all()->toArray();
                if (!empty($Order_Data)) {
                    foreach ($Order_Data as $k => $orders) {
                        $data[$k]['order_id'] = $orders['order_id'];
                        if ($orders['pickupadd'] == $orders['deliveryadd']) {
                            $address = $this->UserAddresses->find()->where(array('id' => $orders['pickupadd']))->first();
                            $data[$k]['address'] = $address['add_heading'];
                            $data[$k]['orderstatus'] = $orders['order_status'];
                        } else {
                            $pickadd_id = $orders['pickupadd'];
                            $address1 = $this->UserAddresses->find()->where(array('id' => $orders['pickupadd']))->first();
                            $deladd_id = $orders['deliveryadd'];
                            $address2 = $this->UserAddresses->find()->where(array('id' => $orders['pickupadd']))->first();
                            $data[$k]['address'] = $address1['add_heading'] . "," . $address1['add_heading'];
                            $data[$k]['orderstatus'] = $orders['order_status'];
                        }
                    }

                    $order_data = array('status' => 1,
                        'data' => $data
                    );
                } else {
                    $order_data = array('status' => 2,
                        'data' => "Order information not found"
                    );
                }
            } else {
                $order_data = array('status' => 0,
                    'data' => "Insufficient Data"
                );
            }
            echo json_encode($order_data);
            exit;
        }
    }

    public function getOrderDetail() {
        if ($this->request->is('post')) {
            if (!empty($this->request->data('order_id'))) {
                $order_id = $this->request->data('order_id');
                $orderData = $this->Orders->find()->where(array('order_id' => $order_id))->first();
                $clothData = $this->OrderDetails->find()->where(array('order_id' => $order_id))->all()->toArray();
                if (!empty($orderData)) {
                    $data['order_date'] = date("jS F, Y", strtotime($orderData['ordereddate']));
                    $data['pickup_date'] = date("jS F, Y", strtotime($orderData['pickupdate']));
                    $data['delivery_date'] = date("jS F, Y", strtotime($orderData['deliverydate']));
                    $data['paid_amount'] = $orderData['totalprice'];
                    $data['discount_amount'] = $orderData['discount_amount'];
                    $data['order_price'] = $orderData['order_price'];
                    $data['coupon_code'] = ($orderData['coupon_code'])?($orderData['coupon_code']):"";
                    $address1 = $this->UserAddresses->find()->where(array('id' => $orderData['pickupadd']))->first();
                    $addressDetail = $address1['address'] . ",";
                    $addressDetail.=$address1['landmark'] . ",";
                    $area_location = $this->Locations->find()->where(array('id' => $address1['area']))->first();
                    $addressDetail.=$area_location['location_name'] . ",";
                    $location = $this->Locations->find()->where(array('id' => $address1['location']))->first();
                    $addressDetail.=$location['location_name'] . ",";
                    $circle_location = $this->Locations->find()->where(array('id' => $address1['circle']))->first();
                    $addressDetail.=$circle_location['location_name'] . ",";
                    $city_location = $this->Locations->find()->where(array('id' => $address1['city']))->first();
                    $addressDetail.=$city_location['location_name'] . ",";
                    $addressDetail.=$address1['pincode'];

                    $data['pickupadd'] = $addressDetail;

                    $address1 = $this->UserAddresses->find()->where(array('id' => $orderData['deliveryadd']))->first();
                    $addressDetail = $address1['address'] . ",";
                    $addressDetail.=$address1['landmark'] . ",";
                    $area_location = $this->Locations->find()->where(array('id' => $address1['area']))->first();
                    $addressDetail.=$area_location['location_name'] . ",";
                    $location = $this->Locations->find()->where(array('id' => $address1['location']))->first();
                    $addressDetail.=$location['location_name'] . ",";
                    $circle_location = $this->Locations->find()->where(array('id' => $address1['circle']))->first();
                    $addressDetail.=$circle_location['location_name'] . ",";
                    $city_location = $this->Locations->find()->where(array('id' => $address1['city']))->first();
                    $addressDetail.=$city_location['location_name'] . ",";
                    $addressDetail.=$address1['pincode'];
                    $data['deliveryadd'] = $addressDetail;
                    $status_id = $orderData['status'];
                    $status = array(
                        '1' => 'Order Placed',
                        '2' => 'Order Confirmed',
                        '3' => 'Ready To Pickup',
                        '4' => 'Out For Pickup',
                        '5' => 'Picked up',
                        '6' => 'Pickup Reschedule',
                        '7' => 'Intransit To Warehouse',
                        '8' => 'Order Processing',
                        '9' => 'Ready To deliver',
                        '10' => 'Out for Deliver',
                        '11' => 'Delivered',
                        '12' => 'Deliver Reschedule',
                        '13' => 'Cancel');
                    $data['status'] = $status[$status_id];
                    $arr = array();
                    if (!empty($clothData)) {
                        $i = 0;
                        foreach ($clothData as $d) {
                            $clothnam = $this->Clothtypes->find()->where(array('id' => $d['cloth_id']))->first();
                            $servicenam = $this->Services->find()->where(array('id' => $d['service_id']))->first();
                            $arr[$i]['cloth'] = $clothnam['cloth_name'];
                            $arr[$i]['service'] = $servicenam['sname'];
                            $arr[$i]['quantity'] = $d['quantity'];
                            $arr[$i]['unitprice'] = $d['price'];
                            $i++;
                        }
                    }
                    $data['clothdetails'] = $arr;
                    $OrderDetail = array('status' => 1, 'data' => $data);
                } else {
                    $OrderDetail = array('status' => 2, 'data' => 'Order Details Not available');
                }
            } else {
                $OrderDetail = array('status' => 0, 'data' => "Insufficient Data");
            }
            echo json_encode($OrderDetail);
            exit;
        }
    }

    public function saveOrder() {
        if ($this->request->is("post")) {
            if (!empty($this->request->data)) {
                $data = $this->request->data;
                $newdata['user_id'] = $data['user_id'];
                $newdata['pickupdate'] = $data['pickup_date'];
                $newdata['pickupslot'] = $data['pickup_slotid'];
                $newdata['pickupadd'] = $data['pickup_addrid'];
                $newdata['deliverydate'] = $data['deliv_date'];
                $newdata['deliveryadd'] = $data['deliv_addrid'];
                $newdata['deliveryslot'] = $data['deliv_slotid'];
                $newdata['totalprice'] = $data['total_price'];
                $newdata['order_price'] = $data['order_price'];
                $newdata['coupon_code'] = $data['coupon_code'];
                $newdata['remarknew'] = '';
                $newdata['cloth_info'] = array();
                $i = 0;
                if (!empty($data['clothlist'])) {
                    foreach ($data['clothlist'] as $vv) {
                        $pricee = $this->Pricings->find()->where(array('cloth_id' => $vv['clothId'], 'service_id' => $vv['washId']))->first();
                        $newdata['cloth_info'][$i] = $vv['washId'] . ":@:" . $vv['clothId'] . ":@:" . $pricee['price'] . ":@:" . $vv['total'] . ":@:" . '';
                        $i++;
                    }
                }

                $file = fopen(WWW_ROOT . "test.txt", "w");
                fwrite($file, json_encode($data['clothlist']) . "---<br>---1st" . json_encode($newdata) . "---<br>---2nd");
                fclose($file);
                $val = $this->insertOrder($newdata);
                if ($val) {
                    $datafor = array('status' => 1, 'msg' => 'Order Placed Successfully');
                } else {
                    $datafor = array('status' => 2, 'msg' => 'Order not Placed Successfully');
                }
            } else {
                $datafor = array('status' => 0, 'msg' => 'Insufficient Data');
            }
            echo json_encode($datafor);
            exit;
        }
    }

    public function saveAddress() {
        if ($this->request->is("post")) {
            if (!empty($this->request->data) && !empty($this->request->data('user_id'))) {
                $data = $this->request->data;
                $userAdd = $this->UserAddresses->newEntity();
                if ($this->request->data("id") != null) {
                    $userAdd = $this->UserAddresses->get($this->request->data("id"));
                }
                $userAdd = $this->UserAddresses->patchEntity($userAdd, $this->request->data);
                if ($this->UserAddresses->save($userAdd)) {
                    $data = array('status' => 1, 'data' => 'Address Saved Successfully');
                } else {
                    $data = array('status' => 2, 'data' => 'Address can not be added!! Try again');
                }
            } else {
                $data = array('status' => 0, 'data' => "Insufficient Data");
            }
            echo json_encode($data);
            exit;
        }
    }

    public function saveUser() {
        $user = $this->Users->newEntity();
        if ($this->request->is("post")) {
            if (!empty($this->request->data)) {
                if (!empty(trim($this->request->data("ref_by")))) {
                    $ref_by = $this->request->data("ref_by");
                    $query = $this->Users->find()->where(array('ref_code' => $ref_by, "is_deleted" => 0))->first();
                    if (!empty($query)) {
                        $userData = array('status' => 3, 'data' => "Invalid Reference code");
                        echo json_encode($userData);
                        exit;
                    }
                }
                if (!empty(trim($this->request->data("mobile")))) {
                    $mobile = $this->request->data("mobile");
                    $query = $this->Users->find()->where(array('mobile' => $mobile, "is_deleted" => 0))->first();
                    if (!empty($query)) {
                        $userData = array('status' => 4, 'data' => "Mobile Already Registered");
                        echo json_encode($userData);
                        exit;
                    }
                }
                if (!empty(trim($this->request->data("email")))) {
                    $email = $this->request->data("email");
                    $query = $this->Users->find()->where(array('email' => $email, "is_deleted" => 0))->first();
                    if (!empty($query)) {
                        $userData = array('status' => 5, 'data' => "Email ID Already Registered");
                        echo json_encode($userData);
                        exit;
                    }
                }
                $user = $this->Users->patchEntity($user, $this->request->data);
                $val = $this->userSave($user, $this->request->data);
                if ($val) {
                    $getUserdetail = $this->Users->find()->where(array('id' => $val))->first();
                    $getUserdetail['mobile_varified'] = $getUserdetail['mobile_varified'] ? $getUserdetail['mobile_varified'] : 0;
                    $userData = array('status' => 1, 'data' => $getUserdetail);
                } else {
                    $userData = array('status' => 2, 'data' => "You have already registered with us.Contact Admin!!!");
                }
            } else {
                $userData = array('status' => 0, 'data' => "Insufficient Data");
            }
            echo json_encode($userData);
            exit;
        }
    }

    public function editProfile() {
        if ($this->request->is("post")) {
            if (!empty($this->request->data['user_id']) && !empty($this->request->data['email']) && !empty($this->request->data['mobile'])) {
                $data = $this->request->data;
                $condition1 = array('email' => $data['email'], 'userid !=' => $data['user_id']);
                if ($this->Users->find()->where($condition1)->count() == 0) {
                    $condition2 = array('mobile' => $data['mobile'], 'userid !=' => $data['user_id']);
                    if ($this->Users->find()->where($condition2)->count() == 0) {
                        if ($this->Users->updateAll(array('name' => $data['name'], 'email' => $data['email'], 'mobile' => $data['mobile']), array('userid' => $data['user_id']))) {
                            $data = array('status' => 1, 'data' => $this->request->data);
                        } else {
                            $data = array('status' => 1, 'data' => $this->request->data);
                        }
                    } else {
                        $data = array('status' => 3, 'data' => "Mobile No. already registered");
                    }
                } else {
                    $data = array('status' => 2, 'data' => "Email Id already registered");
                }
            } else {
                $data = array('status' => 0, 'data' => "Data not sent properly");
            }
            echo json_encode($data);
            exit;
        }
    }

    public function getlastOrder() {
        if ($this->request->is("post")) {
            if (!empty($this->request->data('user_id'))) {
                $user_id = $this->request->data('user_id');
                $orderDetail = $this->Orders->find()->where(array('user_id' => $user_id))->order(array("ordereddate" => "DESC"))->first();
                if (!empty($orderDetail)) {
                    $orderStatus = $orderDetail['status'];
                    $lastOrder = array('status' => 1, 'data' => array('order_id' => $orderDetail['order_id'], 'orderStatus' => $orderStatus));
                } else {
                    $lastOrder = array('status' => 2, 'data' => "Order soon to get many more exciting offers for New User");
                }
            } else {
                $lastOrder = array('status' => 0, 'data' => "Data not sent properly");
            }
            echo json_encode($lastOrder);
            exit;
        }
    }

    public function walletdetails() {
        if ($this->request->is("post")) {
            if (!empty($this->request->data('user_id'))) {
                $conn = ConnectionManager::get('default');
                $transorder = $conn->execute('SELECT * FROM usertransactions WHERE user_id="' . $this->request->data('user_id') . '" AND for_whom=2 ORDER BY id asc');
                $alltransorder = $transorder->fetchAll('assoc');
                $userwallet = $this->Users->find()->where(array('userid' => $this->request->data('user_id')))->first();
                $data['staus'] = 1;
                $data['msg'] = 'Successfull';
                $data['data'] = $alltransorder;
                $data['walletamount'] = (float) $userwallet['wallet'];
            } else {
                $data['staus'] = 0;
                $data['msg'] = 'Insufficient Data';
            }
            echo json_encode($data);
            exit;
        }
    }

    public function couponapply() {
        if ($this->request->data()) {
            
        }
    }

    public function tearmscondition() {
        $pc = $this->Privacyandpolicies->find()->first();
        $tc = $this->Termsandconditions->find()->first();
        $data['data']['termscond'] = $tc['details'];
        $data['data']['privacy'] = $pc['details'];
        $data['status'] = 1;
        $data['msg'] = 'Successfull';
        echo json_encode($data);
        exit;
    }

    public function sendotp() {
        if (!empty($this->request->data['user_id'])) {
            $getUserdetail = $this->Users->find()->where(array('userid' => $this->request->data['user_id']))->first();
            if (!empty($getUserdetail)) {
                $otp = mt_rand(100000, 999999);
                $otp_time = date("Y-m-d H:i:s");
                $msg = "Dear Superdhobi user, OTP to authenticate your mobile number is " . $otp . " . Regards Superdhobi Team.";
                $res_sms = $this->sendSms($getUserdetail['mobile'], $msg);
                $this->Users->updateAll(array('otp' => $otp, 'otp_time' => $otp_time), array('id' => $getUserdetail['id']));
                $userData = array('status' => 1, 'data' => "OTP Send Successfully");
            } else {
                $userData = array('status' => 2, 'data' => "Invalid User Id");
            }
        } else {
            $userData = array('status' => 0, 'data' => "Insufficient Data");
        }
        echo json_encode($userData);
        exit;
    }

    public function checkotp() {
        if (!empty($this->request->data['user_id']) && !empty($this->request->data['otp'])) {
            $getUserdetail = $this->Users->find()->where(array('userid' => $this->request->data['user_id']))->first();
            if (!empty($getUserdetail)) {
                if ($getUserdetail['otp'] != '') {
                    $otp_gentime = strtotime('+5 minutes', strtotime($getUserdetail['otp_time']));
                    if ($getUserdetail['otp'] == $this->request->data['otp']) {
                        $this->Users->updateAll(array('mobile_varified' => 1), array('id' => $getUserdetail['id']));
                        $userData = array('status' => 1, 'data' => "OTP Verified Successfully");
                    } else {
                        $userData = array('status' => 1, 'data' => "Invalid OTP, Please Resend");
                    }
                } else {
                    $userData = array('status' => 3, 'data' => "You have not generate any otp");
                }
            } else {
                $userData = array('status' => 2, 'data' => "Invalid User Id");
            }
        } else {
            $userData = array('status' => 0, 'data' => "Insufficient Data");
        }
        echo json_encode($userData);
        exit;
    }

    public function delete_address() {
        if (!empty($this->request->data["addr_id"])) {
            if ($this->UserAddresses->updateAll(array("is_deleted" => 1), array("id" => $this->request->data["addr_id"]))) {
                $data = array("status" => 1, 'data' => "Address deleted successfully");
            } else {
                $data = array("status" => 0, 'data' => "Invalid Address");
            }
        } else {
            $data = array("status" => 0, 'data' => "Data not sent properly");
        }
        echo json_encode($data);
        exit;
    }

    public function couponValidate() {
        if ($this->request->is("post")) {
            //  pr($this->request->data);exit;
            if ($this->request->data['coupon_code'] != "") {
                $couponcode = $this->request->data['coupon_code'];
                $couponDetail = $this->Coupons->find()->where(array("coupon_code" => $couponcode, "is_delete" => 0, "status" => 1))->all()->toArray();
                if (!empty($couponDetail)) {
                    foreach ($couponDetail as $couponData) {
                        $orderdate = date("Y-m-d");
                        if ((date("Y-m-d", strtotime($couponData['start_date'])) < $orderdate) && (date("Y-m-d", strtotime($couponData['end_date'])) > $orderdate)) {
                            $totalprice = $this->request->data['total_price'];
                            if ($couponData['min_apply_amt'] <= $totalprice) {
                                $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
                                $orderday = date("D");
                                $day_id = array_keys($days, $orderday);
                                if (in_array($day_id[0], json_decode($couponData['weekday']))) {
                                    $clothDetail = $this->request->data['clothlist'];
                                    foreach ($clothDetail as $cloth) {
                                        $washId[] = $cloth['washId'];
                                    }
                                    if (count(array_intersect(json_decode($couponData['washtype']), $washId)) == count($washId)) {
                                        $discountAmt = $totalprice * ($couponData['discount'] / 100);
                                        $billingAmt = $totalprice - $discountAmt;
                                        $data = array("status" => 1, "data" => array("discount_amount" => $discountAmt, "billing_amount" => $billingAmt, "total_price" => $totalprice));
                                    } else {
                                        $data = array("status" => 6, "data" => "This coupon code is not Valid for these Washtypes");
                                    }
                                } else {
                                    $data = array("status" => 5, "data" => "This coupon is not Valid for Today");
                                }
                            } else {
                                $data = array("status" => 4, "data" => "Total Price is too low to apply this coupon");
                            }
                        } else {
                            $data = array("status" => 3, "data" => "This Coupon code has been Exired!!!");
                        }
                    }
                } else {
                    $data = array("status" => 2, "data" => "Please Enter valid Coupon Code");
                }
            } else {
                $data = array("status" => 0, "data" => "Data not sent properly");
            }
        }
        echo json_encode($data);
        exit;
    }

    public function checkfeedback() {
        if ($this->request->is("post")) {
            $data = $this->request->data;
            if ($data['order_id']) {
                $orderdetail = $this->Orders->find()->where(array('order_id' => $this->request->data['order_id']))->first();
                if (!empty($orderdetail)) {
                    if ($orderdetail['rating']) {
                        $rating = (int) $orderdetail['rating'];
                        $feeddetail = $this->Feedbacks->find()->where(array('order_id' => $this->request->data['order_id']))->all()->toArray();
                        $i = 0;
                        foreach ($feeddetail as $va) {
                            $newarray[$i] = $va;
                            $newarray[$i]['add_date'] = date("jS M,Y", strtotime($va['add_date']));
                            $i++;
                        }
                        $jsondata = array("status" => 1, "data" => $feeddetail, "rating" => $rating, 'condition' => $orderdetail['rate_cond']);
                    } else {
                        $jsondata = array("status" => 3, "data" => "No rating Set Yet");
                    }
                } else {
                    $jsondata = array("status" => 2, "data" => "Invalid order id");
                }
            } else {
                $jsondata = array("status" => 0, "data" => "Data not sent properly");
            }
            echo json_encode($jsondata);
            exit;
        }
    }

    public function getfeedbackorder() {
        if ($this->request->is('post')) {
            if (!empty($this->request->data('user_id'))) {
                $user_id = $this->request->data('user_id');
                $Order_Data = $this->Orders->find()->where(array('user_id' => $user_id, 'order_status IN' => array(11, 13)))->all()->toArray();
                if (!empty($Order_Data)) {
                    foreach ($Order_Data as $k => $orders) {
                        $data[$k]['order_id'] = $orders['order_id'];
                        $data[$k]['orderdate'] = date("jS M,Y", strtotime($orders['ordereddate']));
                        if ($orders['pickupadd'] == $orders['deliveryadd']) {
                            $address = $this->UserAddresses->find()->where(array('id' => $orders['pickupadd']))->first();
                            $data[$k]['address'] = $address['add_heading'];
                            $data[$k]['orderstatus'] = $orders['order_status'];
                        } else {
                            $pickadd_id = $orders['pickupadd'];
                            $address1 = $this->UserAddresses->find()->where(array('id' => $orders['pickupadd']))->first();
                            $deladd_id = $orders['deliveryadd'];
                            $address2 = $this->UserAddresses->find()->where(array('id' => $orders['pickupadd']))->first();
                            $data[$k]['address'] = $address1['add_heading'] . "," . $address1['add_heading'];
                            $data[$k]['orderstatus'] = $orders['order_status'];
                        }
                    }

                    $order_data = array('status' => 1,
                        'data' => $data
                    );
                } else {
                    $order_data = array('status' => 2,
                        'data' => "Order information not found"
                    );
                }
            } else {
                $order_data = array('status' => 0,
                    'data' => "Insufficient Data"
                );
            }
            echo json_encode($order_data);
            exit;
        }
    }

    public function savefeedback() {
        if ($this->request->is('post')) {
            $dtaa = $this->request->data;
            if (!empty($this->request->data('user_id')) && !empty($this->request->data('order_id'))) {
                $feedback = $this->Feedbacks->newEntity();
                $feedback->order_id = $dtaa['order_id'];
                $feedback->userid = $dtaa['user_id'];
                $feedback->reason = $dtaa['reason'];
                $feedback->feed_by = "user";
                $feedback->add_date = date("Y-m-d H:i:s");
                if ($dtaa['rating'] != '') {
                    $this->Orders->updateAll(array('rating' => $dtaa['rating'], 'rate_cond' => $dtaa['conditions']), array('order_id' => $dtaa['order_id']));
                    $data = array('status' => 1, 'data' => 'Feedback Saved Successfully');
                }
                if ($dtaa['reason'] != '') {
                    if ($this->Feedbacks->save($feedback)) {
                        $data = array('status' => 1, 'data' => 'Feedback Saved Successfully');
                    } else {
                        $data = array('status' => 2, 'data' => 'Feedback can not be added!! Try again');
                    }
                }
            } else {
                $data = array('status' => 0, 'data' => 'Insufficient Data');
            }
            echo json_encode($data);
            exit;
        }
    }

}

?>