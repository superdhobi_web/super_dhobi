<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 */
class ServicesController extends AppController {

  public function beforeFilter(Event $event){
      $this->loadmodel("Pricings");
    $name = $this->request->session()->read('username');
    if(empty($name)){
        $this->viewBuilder()->layout('loginlayout');
        return $this->redirect(['controller'=>'Dashboard','action'=>'login']);
    }
}  

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $query = $this->Services->find()->where(array('is_deleted'=>0));
        $services = $this->paginate($query);
        $this->set(compact('services'));
        $this->set('_serialize', ['services']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        $service = $this->Services->newEntity();
        if ($id != null) {
            $service = $this->Services->get($id);
        }
        if ($this->request->is('post')) {
            if (!empty($this->request->data['sicon']['name'])) {
                $file = $this->request->data['sicon'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                if (in_array($ext, $arr_ext)) {
                    $image_path = WWW_ROOT . 'img/' . $file['name'];
                    $thumb_path = WWW_ROOT . 'thumbimg/' . $file['name'];
                    if (move_uploaded_file($file['tmp_name'], $image_path)) {
                        $this->createthumb($image_path, $thumb_path, 100, 100);
                        $this->request->data['sicon'] = $file['name'];
                    }
                }
                unlink($this->request->webroot."/img/".$this->request->data['prev_sicon']);
                unlink($this->request->webroot."/thumbimg/".$this->request->data['prev_sicon']);
            } else {
                $this->request->data['sicon']=$this->request->data['prev_sicon'];
            }
            $service = $this->Services->patchEntity($service, $this->request->data);
            if ($this->Services->save($service)) {
                $this->Flash->success(__('The service has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The service could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('service'));
        $this->set('_serialize', ['service']);
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Service id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $service = $this->Services->get($id, [
            'contain' => []
            ]);
        $this->set(compact('service'));
        $this->set('_serialize', ['service']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Service id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $service = $this->Services->get($id);
        if($this->Services->updateAll(array('is_deleted'=>1), array('id'=>$id))){
            unlink(BASE_URL . "thumbimg/" . $service->sicon);
            $this->Flash->success(__('The service has been deleted.'));
        } else {
            $this->Flash->error(__('The service could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function changestatus() {
        $status = $this->request->data('status');
        $id = $this->request->data('id');
        if ($this->Services->updateAll(array('status' => $status), array('id' => $id))) {
            echo $status;
        }
        exit;
    }

}