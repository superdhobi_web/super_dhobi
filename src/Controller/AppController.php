<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use MyLibraryBaseClass;
use Cake\Controller\Component\AuthComponent;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function createthumb($pathToImages, $pathToThumbs, $thumbWidth, $thumbheight) {
        $fexten = array('jpg', 'jpeg', 'gif', 'png');
        $info = pathinfo($pathToImages);
        if (strtolower($info['extension']) == 'jpg' || strtolower($info['extension']) == 'jpeg') {
            $fn = "imagecreatefromjpeg";
        } else if (strtolower($info['extension']) == 'gif') {
            $fn = "imageCreateFromGIF";
        } else if (strtolower($info['extension']) == 'png') {
            $fn = "ImageCreateFromPNG";
        }
        if (in_array(strtolower($info['extension']), $fexten)) {
            $img = $fn("{$pathToImages}");
            $width = imagesx($img);
            $height = imagesy($img);
            $new_width = $thumbWidth;
            $new_height = floor($height * ( $thumbWidth / $width ));
            $tmp_img = imagecreatetruecolor($new_width, $new_height);
            imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagejpeg($tmp_img, "{$pathToThumbs}");
        }
        return 1;
    }

    /**
     * [insertintotrack description]
     * @param  string $order_id     [description]
     * @param  string $order_status [description]
     * @param  [type] $remark       [description]
     * @param  string $by_employee  [description]
     * @param  [type] $status       [description]
     * @return [type]               [description]
     */
    public function insertintotrack($order_id='', $order_status='', $remark=null, $by_employee='', $status=null) {
        $ordertrackTable = TableRegistry::get('OrderTrackings');
        $dat = $ordertrackTable->newEntity();
        $dat->order_id = $order_id;
        $dat->order_status = $order_status;
        $dat->remark = $remark;
        $dat->timestamp = time();
        $dat->by_employee = $by_employee;
        $dat->status = $status;
        $ordertrackTable->save($dat);
        $orderTable = TableRegistry::get('Orders');
        $users = TableRegistry::get('Users');
        $timeslot = TableRegistry::get('timeslots');
        $order = $orderTable->find()->where(array('order_id' => $order_id))->first();
        $userData = $users->find()->where(array('userid' => $order['user_id']))->first();
        $mobile = $userData['mobile'];
        $picslotData = $this->getSlotname($order['pickupslot']);
        $delslotData = $this->getSlotname($order['deliveryslot']);
        $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
        switch ($order_status) {
            case '1':
                $msg = "Order Id " . $order_id . " has been successfully Placed";
                break;
            case '2':
                $msg = "Order Id " . $order_id . " has been confirmed";
                // Not required ..........
                break;
            case '3':
                $pickupdate = date("jS F, Y", strtotime($order['pickupdate']));
                $msg = "Order Id" . $order_id . " will be picked up on " . $pickupdate . "(" . $picslotData . ")";
                break;
            case '4':
                $pickupdate = date("jS F, Y", strtotime($order['pickupdate']));
                $msg = "Please keep your items ready.It  will be pickup between " . $picslotData;
                break;
            case '5':
                $msg = "Your Ordered Items has been picked up";
                break;
            case '6':
                $pickupdate = date("jS F, Y", strtotime($order['pickupdate']));
                $msg = "Order Id" . $order_id . " has been rescheduled to " . $pickupdate . "(" . $picslotData . ")";
                break;
            case '7':
                $msg = "Your Ordered items is in transit to warehouse against OrderId : " . $order_id;
                break;
            case '8':
                $msg = "Your orderID" . $order_id . "is processing, we will keep informing you for further process.";
                break;
            case '9':

                $deliverydate = date("jS F, Y", strtotime($order['deliverydate']));
                $msg = "Order Id" . $order_id . " will be delivered on " . $deliverydate . "(" . $delslotData . ")";
                break;
            case '10':

                $deliverydate = date("jS F, Y", strtotime($order['deliverydate']));
                $msg = "Your items has been out for delivery. It  will be delivered between " . $delslotData . " for order : " . $order_id;
                break;
            case '11':
                $msg = "your items having order : " . $order_id . " has been delivered successfully ";
                break;
            case '12':

                $deliverydate = date("jS F, Y", strtotime($order['deliverydate']));
                $msg = "Order Id" . $order_id . " has been rescheduled for delivery to " . $deliverydate . "(" . $delslotData . ")";
                break;
            case '13':
                $msg = "Your Order : " . $order_id . " has been cancelled. Please wash your clothes in your home";
                break;

            default:
                # code...
                break;
        }
        $res = $this->sendSms($mobile, $msg);
        return $res;
    }

    /**
     * [insertintotransaction description]
     * @param  string $user_id        [description]
     * @param  string $transaction_id [description]
     * @param  string $type           [description]
     * @param  string $amount         [description]
     * @param  [type] $remark         [description]
     * @param  [type] $order_id       [description]
     * @param  [type] $for_whom       [description]
     * @return [type]                 [description]
     */
    public function insertintotransaction($user_id='', $transaction_id='', $type='', $amount='', $remark=NULL, $order_id=NULL, $for_whom=NULL) {
        $ordertrackTable = TableRegistry::get('Usertransactions');
        $dat = $ordertrackTable->newEntity();
        $dat->transaction_id = $transaction_id;
        $dat->user_id = $user_id;
        $dat->order_id = $order_id;
        $dat->timestamp = time();
        $dat->type = $type;
        $dat->amount = $amount;
        $dat->remark = $remark;
        $dat->for_whom = $for_whom;
        if ($amount != 0 && $amount != '0.00' && $amount != '') {
            $ordertrackTable->save($dat);
        }
        return true;
    }

    /**
     * [sendSms description]
     * @param  string $phone [description]
     * @param  string $msg   [description]
     * @return [type]        [description]
     */
    public function sendSms($phone='', $msg='') {
        $msg = urlencode($msg);
        $res = "http://digitalrajasthan.com/api/mt/SendSMS?user=sdhobi&password=X7sds324&senderid=SDHOBI&channel=trans&text=$msg&DCS=0&flashsms=0&number=$phone&route=4";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $res);
        curl_setopt($ch, CURLOPT_USERAGENT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($output);
        if ($res->ErrorMessage == 'Done') {
            return 1;
        } else {
            return 2;
        }
    }

    public function beforeFilter(Event $event) {
        $this->loadmodel("Offers");
        $this->loadmodel("Users");
    }

    /**
     * [getorderslot description]
     * @param  [type] $date [description]
     * @return [type]       [description]
     */
    public function getorderslot($date) {
        $days = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', '7' => 'Sun');
        $day = date('D', strtotime($date));
        echo $day;
        $day_id = array_keys($days, $day);
        pr($day_id);
        exit;
        $conditions = array('status' => 1, 'is_deleted' => 0, 'is_leave' => 0, 'day' => $day_id[0]);
        $timeSlot = $timeslot->find()->where($conditions)->all()->toArray();
        $slottime = $timeSlot['from_time'] . "-" . $timeSlot['to_time'];
        return $slottime;
        exit;
    }

    /**
     * [insertOrder description]
     * @param  string $data [description]
     * @return [type]       [description]
     */
    public function insertOrder($data='') {
        $orderTable = TableRegistry::get('Orders');
        $orderen = $orderTable->newEntity();
        $order_id = $this->getorderid();
        $orderen->order_id = $order_id;
        $orderen->user_id = $data['user_id'];
        $orderen->pickupdate = date("Y-m-d", strtotime($data['pickupdate']));
        $orderen->pickupslot = $data['pickupslot'];
        $orderen->pickupadd = $data['pickupadd'];
        $orderen->deliverydate = date("Y-m-d", strtotime($data['deliverydate']));
        $orderen->deliveryadd = $data['deliveryadd'];
        $orderen->deliveryslot = $data['deliveryslot'];
        $orderen->order_price = isset($data['order_price']) ? $data['order_price'] : 100;
        $orderen->totalprice = $orderen->order_price;
        $orderen->ordereddate = date("Y-m-d");
        $orderen->order_status = 1;
        $orderen->coupon_code = isset($data['coupon_code']) ? $data['coupon_code'] : '';
        $orderen->status = 1;
        $orderen->ordered_by = 1;
        $orderen->remarks = $data['remarknew'];
        if ($orderTable->save($orderen)) {
            $lastinsertId = base64_encode($orderen->id);
            $orderdetailsTable = TableRegistry::get('OrderDetails');
            if (isset($data['cloth_info'])) {
                foreach ($data['cloth_info'] as $va) {
                    $coltharr = $orderdetailsTable->newEntity();
                    $datanews = explode(":@:", $va);
                    $coltharr->order_id = $order_id;
                    $coltharr->service_id = $datanews[0];
                    $coltharr->cloth_id = $datanews[1];
                    $coltharr->price = $datanews[2];
                    $coltharr->quantity = $datanews[3];
                    $coltharr->remark = $datanews[4];
                    $orderdetailsTable->save($coltharr);
                }
            }
            $this->insertintotrack($order_id, $order_status = 1, $remark = null, $by_employee = 1, $status = null);
            return $lastinsertId;
        } else {
            return 0;
        }
    }

    public function getorderid($userid='') {
        $this->loadModel('Orders');
        $com = "SD" . date("Y") . "OOO";
        $lastCreated = $this->Orders->find()->order(array("id" => "DESC"))->first();
        if (empty($lastCreated)) {
            $id = 1;
        } else {
            $id = (int) $lastCreated['id'] + 1;
        }
        return $com . $id;
    }

    public function userSave($user, $requestData) {
        $this->loadmodel("Offers");
        $this->loadmodel("Users");
        //================= Save users and wallet with transaction ============//
        $useroffer = $this->Offers->find()->where(array("offer_type" => 1))->toArray();
        if (empty($user['user_id'])) {
            $user['userid'] = $this->getuserid();
            $user['password'] = isset($user['password']) ? $user['password'] : "12345";
            $user['wallet'] = $useroffer[0]['price'] ? $useroffer[0]['price'] : 0;
        }
        if ($this->Users->save($user)) {
            if (empty($user['user_id'])) {
                if (isset($this->request->data['ref_by']) && $this->request->data['ref_by'] != '') {
                    $refoffer = $this->Offers->find()->where(array("offer_type" => 2))->toArray();
                    $ref_by = trim($this->request->data['ref_by']);
                    $conn = ConnectionManager::get('default');
                    $sum = $refoffer[0]['price'] ? $refoffer[0]['price'] : 0;
                    $res = $conn->execute("UPDATE users SET total_ref=total_ref + 1,wallet=wallet + $sum WHERE ref_code='" . $ref_by . "'");
                    $refuser = $this->Users->find()->where(array("ref_code" => $ref_by))->first()->toArray();
                    $transaction_id = rand(10000, 99999) . time();
                    $this->insertintotransaction($user_id = $refuser['userid'], $transaction_id = $transaction_id, $type = '2', $amount = $sum, $remark = "Get For Reference " . $user['userid'], $order_id = NULL, "2");
                }
                $transaction_id = rand(10000, 99999) . time();
                $this->insertintotransaction($user_id = $user['userid'], $transaction_id = $transaction_id, $type = '2', $amount = $user['wallet'], $remark = "New User Creation", $order_id = NULL, "2");
                $lastID = $user->id;
                $userid = "SDU00" . $lastID;
                $refCode = $this->genrefcode($userid);
                $this->Users->updateAll(array('ref_code' => $refCode), array('id' => $lastID));
            } else {
                $lastID = $user->id;
            }
            $res = $lastID;
        } else {
            $res = 0;
        }
        return $res;
    }

    public function getSlotname($slotid) {
        $timeslot = TableRegistry::get('timeslots');
        $slot = $timeslot->find()->where(array('id' => $slotid))->first();
        return $slot['from_time'] . "-" . $slot['to_time'];
    }

    public function getuserid() {
        $this->loadmodel("Users");
        $lastCreated = $this->Users->find()->order(array("id" => "DESC"))->first();
        if (empty($lastCreated)) {
            $id = 1;
        } else {
            $id = (int) $lastCreated['id'] + 1;
        }
        return "SDU00" . $id;
    }

    public function genrefcode($usercode) {
        $new = explode("SDU00", $usercode);
        $refid = "REF" . date("Y") . $new[1];
        return $refid;
    }

}

