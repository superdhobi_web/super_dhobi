<!-- Scripts -->
    
    <!-- jQuery Library -->
    <?= $this->Html->script('admin/plugins/jquery-1.11.2.min.js');?>
    <!-- <?= $this->Html->script('admin/plugins/MultiNestedList.js');?> -->
    <!--materialize js-->
    <?= $this->Html->script('admin/materialize.js');?>
     <!--timer js-->
    <?= $this->Html->script('admin/jquery.timepicker.js');?>
    <!--prism-->
    <?= $this->Html->script('admin/plugins/prism/prism.js');?>
    <!--scrollbar-->
    <?= $this->Html->script('admin/plugins/perfect-scrollbar/perfect-scrollbar.min.js');?>
    <!-- chartist -->
    <?= $this->Html->script('admin/plugins/chartist-js/chartist.min.js');?>
    <!-- masonry -->
    <?= $this->Html->script('admin/plugins/masonry.pkgd.min.js');?>
    <!-- imagesloaded -->
    <?= $this->Html->script('admin/plugins/imagesloaded.pkgd.min.js');?>
    <!-- chartjs -->
    <?= $this->Html->script('admin/plugins/chartjs/chart.min.js');?>
    <?= $this->Html->script('admin/plugins/jquery-validation/jquery.validate.min.js');?>
    <?= $this->Html->script('admin/plugins/jquery-validation/additional-methods.min.js');?>
    <?= $this->Html->script('admin/plugins/chartjs/chart-script.js');?>

    <!-- sparkline -->
    <?= $this->Html->script('admin/plugins/sparkline/jquery.sparkline.min.js');?>
    <?= $this->Html->script('admin/plugins/sparkline/sparkline-script.js');?>
    
    <!-- google map api -->
    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAZnaZBXLqNBRXjd-82km_NO7GUItyKek"></script>-->

    <!--jvectormap-->
    <?= $this->Html->script('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>
    <?= $this->Html->script('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>
    <?= $this->Html->script('admin/plugins/jvectormap/vectormap-script.js');?>
    
    <!--google map-->
    <!--<script type="text/javascript" src="js/plugins/google-map/google-map-script.js"></script>-->

    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <?= $this->Html->script('admin/plugins.js');?>
    <!--custom-script.js - Add your own theme custom JS-->
    <?= $this->Html->script('admin/custom-script.js');?>
    <?= $this->Html->script('admin/form_validation.js');?>
    <?php if($this->request->params['controller']=="Users" || $this->request->params['controller']==""){?>
    <?= $this->Html->script("http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"); ?>
    <?= $this->Html->script("http://code.jquery.com/ui/1.10.4/jquery-ui.js"); ?>
    <?php }?>
    <?= $this->Html->script('admin/admin_ajax.js');?>