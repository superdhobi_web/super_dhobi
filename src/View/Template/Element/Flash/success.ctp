<!-- <div class="message success" onclick="this.classList.add('hidden')"></div> <--></-->
<div class="container">
	<div id="card-alert" class="card green message success" onclick="this.classList.add('hidden')">
 	<div class="card-content white-text">
    	<p><?= h($message) ?></p>
  	</div>
  	<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
    	<span aria-hidden="true">×</span>
  	</button>
</div>

</div>