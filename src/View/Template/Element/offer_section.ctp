<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="offers">
    <div class="container">		
        <div class="row">
            <div class="col-md-12">
                <h2>Featured Offers</h2>				
                <div class="colored-line-left"></div>
            </div>
            <div class="row app-brief">
                <div class="col-md-6 wow fadeInLeft">
                    <p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, neque, aperiam. Recusandae, mollitia commodi nemo, necessitatibus, ducimus praesentium temporibus modi numquam doloribus eos odio. Sed rerum hic fugiat earum. Neque.
                    </p>
                    <p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, neque, aperiam. Recusandae, mollitia commodi nemo, necessitatibus, ducimus praesentium temporibus modi numquam doloribus eos odio. Sed rerum hic fugiat earum. Neque.
                    </p>
                </div>
                <div class="col-md-6 wow fadeInRight">
                    <div id="carousel2" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php
                            $i = 0;
                            foreach ($coupon as $v) {
                                ?>
                                <li data-target="#carousel2" data-slide-to="<?= $i ?>" <?php if ($i == 0) {
                                echo "active";
                            } ?>></li>
                                <?php $i++;
                            } ?>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                            $i = 0;
                            foreach ($coupon as $v) {
                                if ($v['img'] != '') {
                                    ?>
                                    <div class="item <?php if ($i == 0) {
                                echo "active";
                            } ?>">
                                        <img src="<?php echo $this->request->webroot; ?>img/<?= $v['img'] ?>"/>           
                                    </div>
                                    <?php $i++;
                                } else { ?>
                                    <div class="item <?php if ($i == 0) {
                                        echo "active";
                                    } ?>">
                                        <div class="discount-big-box">
                                            <h2><?= $v['discount'] ?>%</h2>
                                            <p>DISCOUNT</p>
                                            <h5><b>Coupon Code:</b> <?= $v['coupon_code'] ?></h5>
                                            <h6>* All Terms &amp; Conditions are apply</h6>
                                        </div>
                                    </div>
                                    <?php $i++;
                                }
                            } ?>
                        </div>
                    </div>
                </div>
            </div>				
        </div>
    </div>
</section>