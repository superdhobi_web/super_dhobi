<section class="our-services grey-bg" id="service">
    <div class="container">	
        <!-- SECTION HEADER -->
        <div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">

            <!-- SECTION TITLE -->
            <h2 class="dark-text">Our Services</h2>
            <div class="colored-line">
            </div>

            <div class="section-description">
                We offer the Services that fulfils your requirement.
            </div>

            <div class="colored-line">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay=".5s">
                <div class="service-img">
                    <img src="images/services/pi.png" class="img-responsive">
                </div>
                <h3>Premium Iron</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, neque, aperiam. 
                </p>
            </div><!--single-services-->

            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay=".8s">
                <div class="service-img">
                    <img src="images/services/si.png" class="img-responsive">
                </div>
                <h3>Steam Iron</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, neque, aperiam.
                </p>
            </div><!--single-services-->

            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay="1.1s">
                <div class="service-img">
                    <img src="images/services/wf.png" class="img-responsive">
                </div>
                <h3>Wash & Fold</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, neque, aperiam.
                </p>
            </div><!--single-services-->

            <div class="col-sm-4 col-sm-offset-2 single-services wow fadeInUp" data-wow-delay=".5s">
                <div class="service-img">
                    <img src="images/services/wi.png" class="img-responsive">
                </div>
                <h3>Wash & Iron</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, neque, aperiam.
                </p>
            </div><!--single-services-->

            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay=".8s">
                <div class="service-img">
                    <img src="images/services/dc.png" class="img-responsive">
                </div>
                <h3>Dry cleaning</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, neque, aperiam.
                </p>
            </div><!--single-services-->

        </div><!--row-->
    </div><!--container-->
</section>