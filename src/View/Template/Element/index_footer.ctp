<footer>
	<div class="container">
		
		<div class="contact-box wow rotateIn animated" data-wow-offset="10" data-wow-duration="1.5s">
			
			<!-- CONTACT BUTTON TO EXPAND OR COLLAPSE FORM -->
			
			<a class="btn contact-button expand-form expanded"><i class="icon_mail_alt"></i></a>
			
			<!-- EXPANDED CONTACT FORM -->
			<div class="row expanded-contact-form">
				
				<div class="col-md-8 col-md-offset-2">
					
					<!-- FORM -->
					<form class="contact-form" id="contact" role="form">
						
						<!-- IF MAIL SENT SUCCESSFULLY -->
						<h4 class="success">
							<i class="icon_check"></i> Your message has been sent successfully.
						</h4>
						
						<!-- IF MAIL SENDING UNSUCCESSFULL -->
						<h4 class="error">
							<i class="icon_error-circle_alt"></i> E-mail must be valid and message must be longer than 1 character.
						</h4>
						
						<div class="col-md-6">
							<input class="form-control input-box" id="name" type="text" name="name" placeholder="Your Name">
						</div>
						
						<div class="col-md-6">
							<input class="form-control input-box" id="email" type="email" name="email" placeholder="Your Email">
						</div>
						
						<div class="col-md-12">
							<input class="form-control input-box" id="subject" type="text" name="subject" placeholder="Subject">
							<textarea class="form-control textarea-box" id="message" rows="8" placeholder="Message"></textarea>
						</div>
						
						<button class="btn btn-primary standard-button2 ladda-button" type="submit" id="submit" name="submit" data-style="expand-left">Send Message</button>
						
					</form>
					<!-- /END FORM -->
					
				</div>
				
			</div>
			<!-- /END EXPANDED CONTACT FORM -->
			
		</div>
		<!-- /END CONTACT BOX -->
		
    <div class="col-md-12" style="border-bottom: 1px solid #BFBFBF;margin-bottom:7px;">
		<!--LOGO &quot; SOCIAL ICONS -->
        <div class="col-md-6">
            <img src="images/logo1.png" alt="LOGO" class="footer-logo img-responsive">
            <ul class="social-icons">
                <li><a href="https://www.facebook.com/SUPER-DHOBI-1010169048993992/?ref=hl"><i class="social_facebook_square"></i></a></li>
                <li><a href="https://twitter.com/MySuperDhobi"><i class="social_twitter_square"></i></a></li>
                <li><a href="https://in.pinterest.com/SuperDhobi/superdhobi"><i class="social_pinterest_square"></i></a></li>
                <li><a href="https://plus.google.com/u/5/105875490934370267660"><i class="social_googleplus_square"></i></a></li>
                <li><a href="https://www.linkedin.com/in/mysuper-dhobi-a8701310b?trk=nav_responsive_tab_profile_pic"><i class="social_linkedin_square"></i></a></li>
                <!--<li><a href=""><i class="social_dribbble_square"></i></a></li>-->
                <li><a href="http://superdhobi.blogspot.in/?view=timeslide"><i class="social_flickr_square"></i></a></li>
            </ul>
		</div>
        <div class="col-md-6 contact-info">
        	<p><i aria-hidden="true" class="icon_phone"></i> +919549005123, +917891399900</p>
        	<p><i aria-hidden="true" class="icon_mail"></i> info@superdhobi.in, feedbacksupedhobi@gmail.com</p>
        	<p><i aria-hidden="true" class="icon_pin"></i> Shubhash Nagar, Kota</p>
        </div>
 	</div>
		<!-- COPYRIGHT TEXT -->
        <div class="col-md-12">		
            <p class="copyright">
                <span class="pull-left">
                    Copyright © 2016 SuperDhobi, All Rights Reserved
                </span>
                <span class="pull-right text-center col-xs-12 col-md-4">
                    <a href="#" data-toggle="modal" data-target="#ppModal">
                        <small>Privacy Policy</small>
                    </a>
                    |
                    <a href="#" data-toggle="modal" data-target="#tcModal">
                        <small>Terms & Conditions</small>
                    </a>
                </span>
            </p>
        </div>
	</div>
	<!-- /END CONTAINER --> 
</footer>
<!-- /END FOOTER -->
<!--Modal Contents-->
<div id="servModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title theme">Areas We Serve</h4>
      </div>
      <div class="modal-body" style="overflow:auto;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> SRINATHPURAM</h5>
                    <p>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> RTU </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Srinathpuram- A, B, C, D, E </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> RK Puram -A, B, C, D, E </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Swami Vivekanand Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Veer Sawarkar Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Manish Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Mahaveer Nagar Extention </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Shivpura </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Medical College</small>
                    </p>
                </div>
                <div class="col-md-4">
                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> TALWANDI</h5>
                    <p>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Talwandi All Sectors  </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Keshavpura </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> New Jawahar Nagar </small>
                    </p>
                </div>
                <div class="col-md-4">
                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> VIGYAN NAGAR</h5>
                    <p>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Vigyan Nagar (ALL SECTORS)  </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Chatrappur  </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> PNT Colony</small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Prem Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Indra Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Sanjaya Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Garish Nagar </small>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> DADABARI</h5>
                    <p>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Basant Vihar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Waqf Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Shakti Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Shastri Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> RPS Colony </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Balaji Nagar </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Pratap Nagar</small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Kishorepura </small>
                    </p>
                </div>
                <div class="col-md-4">
                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> INDIRA VIHAR</h5>
                    <p>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Rajeev Gandhi Nagar  </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Mahaveer Nagar-1,2,3 </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> IL Colony </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Parijat Colony </small>
                    </p>
                </div>
                <div class="col-md-4">
                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> GUMANPURA</h5>
                    <p>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Ballabh Nagar  </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Ballabh Bari  </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Guman Pura</small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> New Gumanpura </small><br>
                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Durga Nagar </small>
                        
                    </p>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">            
      </div>
    </div>
  </div>
</div>
<div id="ppModal" class="modal fade in" role="dialog" aria-hidden="false">
  	<div class="modal-dialog modal-lg">
	    <!-- Modal content-->
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">×</button>
		        <h4 class="modal-title tcp-title"><i class="icon_documents_alt"></i> Privacy Policy</h4>
	      	</div>
	      	<div class="modal-body tcp">
		        <p>
                    <?php echo $pv['details'];?>
		          <!-- <i class="arrow_triangle-right_alt2"></i> This privacy policy sets out how SuperDhobi uses and protects any information that you give to SuperDhobi over Phone, Website or Mobile App. <br>

		          <i class="arrow_triangle-right_alt2"></i> SuperDhobi is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement. <br>

		          <i class="arrow_triangle-right_alt2"></i> SuperDhobi may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 2nd of April 2015. 
		        </p>
		        <h4><i class="arrow_carrot-2right"></i> What we collect?</h4>
		        <p>
		          We may collect the following information: <br>
		          	<i class="icon_circle-slelected"></i> name and job title <br>
		          	<i class="icon_circle-slelected"></i> contact information including email address <br>
		          	<i class="icon_circle-slelected"></i> demographic information such as postcode, preferences and interests <br>
		          	<i class="icon_circle-slelected"></i> other information relevant to customer surveys and/or offers
		        </p>
		        <h4><i class="arrow_carrot-2right"></i> What we do with the information we gather?</h4>
		        <p>
		          We require this information to understand your needs and provide you with a better service, and in particular for the following reasons: <br>
		          <i class="arrow_triangle-right_alt2"></i>  Internal record keeping. <br>

		          <i class="arrow_triangle-right_alt2"></i>  We may use the information to improve our products and services. <br>

		          <i class="arrow_triangle-right_alt2"></i> We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided. <br>

		          <i class="arrow_triangle-right_alt2"></i>  From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customize the website according to your interests. <br>

		          <i class="arrow_triangle-right_alt2"></i>  The information you provide will be shared with the servers on which we host the web content and your name and location may be shared with the laundry processing centre(s) and Rider(s) we have been working with.          
		        </p>
		        <h4><i class="arrow_carrot-2right"></i> Security</h4>
		        <p>
		          <i class="arrow_triangle-right_alt2"></i>  We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.
		        </p>
		        <h4><i class="arrow_carrot-2right"></i> How we use cookies?</h4>
		        <p>
		          <i class="arrow_triangle-right_alt2"></i> A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences. <br>

		          <i class="arrow_triangle-right_alt2"></i> We use traffic log cookies to identify which pages are being used. This helps us analyze data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system. <br>

		          <i class="arrow_triangle-right_alt2"></i> Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us. <br>

		         	<i class="arrow_triangle-right_alt2"></i> You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website. 
		        </p>
		        <h4><i class="arrow_carrot-2right"></i> Contacting Us</h4>
		        <p>
		          If there are any questions regarding this privacy policy you may contact us at our Call Centre or write to :- <a href="mailto:mysuperdhobhi@gmail.com">mysuperdhobhi@gmail.com</a> -->
		        </p>
	      	</div>          
	      	<div class="modal-footer">            
	      	</div>        
	    </div>
  	</div>
</div>
<div id="tcModal" class="modal fade in" role="dialog" aria-hidden="false">
  	<div class="modal-dialog modal-lg">
	    <!-- Modal content-->
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">×</button>
		        <h4 class="modal-title tcp-title"><i class="icon_document_alt"></i> Terms & Conditions</h4>
	      	</div>
	      	<div class="modal-body tcp">
	      		<h4><i class="arrow_carrot-2right"></i> Demo Heading</h4>
	      		<p>
	      			<i class="arrow_triangle-right_alt2"></i>
	      			<?php echo $tc['details'];?>
	      		</p>
	      	</div>
		</div>
	</div>
</div>
<div id="vdoModal" class="modal fade in" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title tcp-title"><i class="icon_document_alt"></i> Videos</h4>
            </div>
            <div class="modal-body tcp">
                <div id="carousel1" class="carousel slide" data-ride="carousel" data-interval="false">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <iframe style="width:100%; height:350px;" src="https://www.youtube.com/embed/OXu4BxIXG3M" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="item">
                          <iframe style="width:100%; height:350px;" src="https://www.youtube.com/embed/bXMcjUglO7A" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="item">
                          <iframe style="width:100%; height:350px;" src="https://www.youtube.com/embed/0MvFcteS8Q4" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--SCRIPTS-->
<script src="js/bootstrap.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.localScroll.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/nivo-lightbox.min.js"></script>
<script src="js/simple-expand.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/matchMedia.js"></script>
<script src="js/jquery.backgroundvideo.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/custom.js"></script>
<script src="js/Index_ajax.js"></script>
<script src="js/Index_validation.js"></script>