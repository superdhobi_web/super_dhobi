<section class="download" id="download">
	<div class="color-overlay">

		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					
					<!-- DOWNLOAD BUTTONS AREA -->
					<div class="download-container">
						<h2 class=" wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">Download the app on</h2>
						
						<!-- BUTTONS -->
						<div class="buttons wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
							
					<!--<a href="" class="btn btn-default btn-lg standard-button"><i class="icon-app-store"></i>App Store</a>-->
							<a href="" class="btn btn-default btn-lg standard-button"><i class="icon-google-play"></i>Play Store</a>
							
						</div>
						<!-- /END BUTTONS -->
						
					</div>
					<!-- END OF DOWNLOAD BUTTONS AREA -->

				</div> 
				<!-- END COLUMN -->
               <div class="col-md-12 maskot-con">
               		<div class="col-md-6">
                    	<img src="images/sd_wash2.png" class="img-responsive wow slideInRight" data-wow-delay=".2s"/>
                	</div>
               		<div class="col-md-6">
                    	<h4><b>SuperDhobi</b> At Your Door Step</h4>
                	</div>                
               </div> <!-- maskot-con-->
				
			</div> 
			<!-- END ROW -->
			
		</div>
		<!-- /END CONTAINER -->
	</div>
	<!-- /END COLOR OVERLAY -->
</section>