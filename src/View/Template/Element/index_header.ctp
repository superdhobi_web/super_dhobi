<header class="header" id="home">
<!-- COLOR OVER IMAGE -->
	<div class="color-overlay">
		<!-- STICKY NAVIGATION -->
		<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
			<div class="container">
				<div class="navbar-header">				
					<!-- LOGO ON STICKY NAV BAR -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?= BASE_URL . "index" ?>">
                        <img src="<?php echo $this->request->webroot; ?>images/logo1.png" alt="Superdhobi" style="height:30px;">
                    </a>			
				</div>			
				<!-- NAVIGATION LINKS -->
				<div class="navbar-collapse collapse" id="kane-navigation">
					<ul class="nav navbar-nav navbar-right main-navigation">
						<li><a href="#home">Home</a></li>
						<li><a href="<?= BASE_URL."index/offers"?>">Offers</a></li>
						<li><a href="#service">Services</a></li>
						<li><a href="<?= BASE_URL."index/price"?>">Price</a></li>
						<li><a href="#features">Features</a></li>
						<li><a href="#process">Process</a></li>
						<li><a href="#service-location">Service Location</a></li>
						<li><a href="<?= BASE_URL."index/testimonial"?>">Testimonial</a></li>
						<li><a href="#brief2">About Us</a></li>
						<li><a href="#contact">Contact Us</a></li>
					</ul>
				</div>
			</div> <!-- /END CONTAINER -->
		</div> <!-- /END STICKY NAVIGATION -->	
		<!-- CONTAINER -->
		<div class="container">		
			<!-- ONLY LOGO ON HEADER -->
			<div class="only-logo">
				<div class="navbar">
					<div class="navbar-header">
						<img src="images/logo2.png" alt="">
					</div>
				</div>
			</div> <!-- /END ONLY LOGO ON HEADER -->		
			<div class="row">
				<div class="col-md-8 col-md-offset-2">				
					<!-- HEADING AND BUTTONS -->
					<div class="intro-section">					
						<!-- WELCOM MESSAGE -->
						<h1 class="intro">Now Clean Clothes On Just A Click</h1>
						<h5>Available on App Store and Play Store</h5>					
						<!-- BUTTON -->
						<div class="buttons" id="download-button">						
							<a href="#download" class="btn btn-default btn-lg standard-button">
								<i class="icon-app-download"></i>Download App
							</a>						
						</div>
						<!-- /END BUTTONS -->					
					</div>
					<!-- /END HEADNING AND BUTTONS -->				
				</div>
			</div>
			<!-- /END ROW -->		
		</div>
		<!-- /END CONTAINER -->
	</div>
<!-- /END COLOR OVERLAY -->
</header>