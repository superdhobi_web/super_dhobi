<div class="card-reveal">
    <span class="card-title grey-text text-darken-4">
        <span class="green-text">Add New User Details</span>
        <i class="mdi-navigation-close right"></i>
    </span>
    <div class="row">
        <form class="col s12 m8 offset-m2 right-alert" id="usersubmit" action="<?= ADMIN_URL . "users/edit" ?>" method="post">
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-action-account-circle prefix"></i>
                    <input id="first_input2" class="validate" type="text" value="" name="name"/>
                    <label for="first_input2" data-error="Please enter name." data-success="Perfect!">Full Name</label>
                </div>
            </div>
            <!-- <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-action-account-circle prefix"></i>
                    <input disabled id="user_input2" class="validate" type="text" value="" readonly name="username" placeholder="Your Username..."/>
                     <label for="user_input2" data-error="" data-success="Perfect!" class="active">Username</label>
                </div>
            </div> -->
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-communication-email prefix"></i>
                    <input id="email_input2" class="validate" type="email" value="" name="email" onblur="uniquedetails(1)"/>
                    <label for="email_input2" class="" data-error="Please enter valid email." data-success="I Like it!">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-communication-email prefix"></i>
                    <input id="mobile_input2" class="validate" type="number" value="" minlength="10" name="mobile" onblur="uniquedetails(2);"/>
                    <label for="mobile_input2" class="" data-error="Please enter valid mobile." data-success="I Like it!">Mobile</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-communication-email prefix"></i>
                    <input id="ref_by" class="validate" type="text" value="" name="ref_by" onblur="return checkreferby();"/>
                    <label for="ref_by" class="" data-error="Please enter valid reference code." data-success="I Like it!">Refer By</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn green waves-effect waves-light right" id="userdetail" type="submit" name="action" disabled>Save
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>