<!-- START LEFT SIDEBAR NAV-->
<?php
$cont = $this->request->params['controller'];
$act = $this->request->params['action'];
?>
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation">
        <li class="user-details cyan darken-2">
            <div class="row">
                <div class="col col s4 m4 l4">
                    <img src="<?= $this->request->webroot;?>images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                </div>
                <div class="col col s8 m8 l8">
                    <ul id="profile-dropdown" class="dropdown-content">
                        <li>
                            <a href="<?= ADMIN_URL?>Dashboard/checkpassword"><i class="mdi-action-face-unlock"></i> Profile</a>
                        </li>
                        <!-- <li>
                            <a href=""><i class="mdi-action-settings"></i> Settings</a>
                        </li> -->
                        <li class="divider"></li>
                        <li>
                            <a href="<?= ADMIN_URL;?>Dashboard/logout"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                        </li>
                    </ul>
                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">Jane Doe<i class="mdi-navigation-arrow-drop-down right"></i></a>
                    <p class="user-roal">Administrator</p>
                </div>
            </div>
        </li>
        <li class="bold <?php if ($cont=='Dashboard' && $act=='index') { ?>active<?php } ?>">
            <a href="<?= BASE_URL."admin/Dashboard"?>" class="waves-effect waves-cyan">
                <i class="mdi-action-dashboard"></i> Dashboard
            </a>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold <?php if (($cont == 'Adminusers' && $act == 'index') || ($cont == "Departments" && $act == "index") || ($cont == "Dashboard" && $act == "checkpassword")) { ?>active<?php } ?>">
                    <a class="collapsible-header waves-effect waves-cyan <?php if (($cont == 'Adminusers' && $act == 'index') || ($cont == "Departments" && $act == "index") || ($cont == "Dashboard" && $act == "checkpassword")) { ?>active<?php } ?>">
                        <i class="mdi-hardware-security"></i> ADMIN
                    </a>
                    <div class="collapsible-body" style="display: none;">
                        <ul>
                            <li <?php if ($cont == "Adminusers" && $act == "index") { ?>class="active"<?php } ?>><a href="<?= BASE_URL."admin/adminusers"?>">Employee Management</a>
                            </li>
                            <li <?php if ($cont == "Departments" && $act == "index") { ?>class="active"<?php } ?> ><a href="<?= BASE_URL."admin/departments"?>">Manage Department</a>
                            </li>
                            <li <?php if ($cont == "Dashboard" && $act == "checkpassword") { ?>class="active"<?php } ?>><a href="<?= BASE_URL."admin/Dashboard/checkpassword"?>">Manage Profile</a>
                            </li>
                            <li <?php if ($cont == "Dashboard" && $act == "rolemanagement") { ?>class="active"<?php } ?>><a href="<?= BASE_URL."admin/Dashboard/rolemanagement"?>">Role Management</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="bold <?php if ($cont == 'Users' && $act == 'index') { ?>active<?php } ?>">
            <a href="<?= BASE_URL."admin/Users"?>" class="waves-effect waves-cyan <?php if ($cont == 'Users' && $act == 'index') { ?>active<?php } ?>">
                <i class="mdi-social-person-add"></i>User Management
            </a>
        </li>
        <li class="bold <?php if ($cont == 'Locations' && $act == 'index') { ?>active<?php } ?>">
            <a href="<?= BASE_URL."admin/locations"?>" class="waves-effect waves-cyan <?php if ($cont == 'Locations' && $act == 'index') { ?>active<?php } ?>">
                <i class="mdi-maps-place"></i>Location Management
            </a>
        </li>
        <li class="bold <?php if ($cont == 'Clothtypes' && $act == 'index') { ?>active<?php } ?>">
            <a href="<?= BASE_URL."admin/clothtypes"?>" class="waves-effect waves-cyan <?php if ($cont == 'Clothtypes' && $act == 'index') { ?>active<?php } ?>">
                <i class="mdi-maps-local-mall"></i>Clothes Management
            </a>
        </li>
        <li class="bold <?php if ($cont == 'Services' && $act == 'index') { ?>active<?php } ?>">
            <a href="<?= BASE_URL."admin/services"?>" class="waves-effect waves-cyan <?php if ($cont == 'Services' && $act == 'index') { ?>active<?php } ?>">
                <i class="mdi-maps-local-laundry-service"></i>Service Management
            </a>
        </li>
        <li class="bold <?php if ($cont == 'Price' && $act == 'index') { ?>active<?php } ?>">
            <a href="<?= BASE_URL."admin/price"?>" class="waves-effect waves-cyan <?php if ($cont == 'Price' && $act == 'index') { ?>active<?php } ?>">
                <i class="mdi-action-account-balance-wallet"></i>Price Management
            </a>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold <?php if ($cont == 'Coupons' && ($act == 'index' || $act=='addcoupon')) { ?>active<?php } ?>">
                    <a class="collapsible-header waves-effect waves-cyan <?php if ($cont == 'Coupons' && ($act == 'index' || $act=='addcoupon')) { ?>active<?php } ?>">
                        <i class="mdi-maps-local-play"></i> Coupon Management
                    </a>
                    <div class="collapsible-body" style="display: none;">
                        <ul>
                            <li <?php if ($cont == 'Coupons' && $act == 'index') { ?>class="active"<?php } ?>>
                                <a href="<?= BASE_URL."admin/coupons/"?>">
                                    Coupon List 
                                </a>
                            </li>
                            <li <?php if ($cont == 'Coupons' && $act == 'addcoupon') { ?>class="active"<?php } ?>>
                                <a href="<?= BASE_URL."admin/coupons/addcoupon"?>">
                                    Add Coupon
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="bold <?php if ($cont == 'TimeSlots' && $act == 'index') { ?>active<?php } ?>">
            <a href="<?= BASE_URL."admin/timeSlots"?>" class="waves-effect waves-cyan <?php if ($cont == 'TimeSlots' && $act == 'index') { ?>active<?php } ?>">
                <i class="mdi-action-alarm"></i>Timeslot Management
            </a>
        </li>        
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold <?php if ($cont == 'Orders' && ($act == 'index' || $act=='addorder' || $act=='orderdetail' || $act == 'invoice')) { ?>active<?php } ?>">
                    <a class="collapsible-header waves-effect waves-cyan <?php if ($cont == 'Orders' && ($act == 'index' || $act=='addorder' || $act=='orderdetail' || $act == 'invoice')) { ?>active<?php } ?>">
                        <i class="mdi-maps-local-grocery-store"></i> Order Management
                    </a>
                    <div class="collapsible-body" style="display: none;">
                        <ul>
                            <li <?php if ($cont == 'Orders' && $act == 'index') { ?>class="active"<?php } ?>>
                                <a href="<?= BASE_URL."admin/orders"?>">
                                    Order List 
                                </a>
                            </li>
                            <!-- <li <?php if ($cont == 'Orders' && $act == 'invoice') { ?>class="active"<?php } ?>>
                                <a href="<?= BASE_URL."admin/orders/invoice"?>">
                                    Invoice 
                                </a>
                            </li> -->
                            <li <?php if ($cont == 'Orders' && $act == 'addorder') { ?>class="active"<?php } ?>>
                                <a href="<?= BASE_URL."admin/orders/addorder"?>">
                                    Add Order
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="bold <?php if ($cont == 'Taskmanages' && $act == 'index') { ?>active<?php } ?>">
            <a href="<?= BASE_URL."admin/taskmanages"?>" class="waves-effect waves-cyan <?php if ($cont == 'Taskmanages' && $act == 'index') { ?>active<?php } ?>">
                <i class="mdi-action-perm-data-setting"></i>Task Management
            </a>
        </li>  
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold <?php if (($cont == 'Termsandconditions' && $act == 'index')||($cont == 'Privacyandpolicies' && $act == 'index')) { ?>active<?php } ?>">
                    <a class="collapsible-header waves-effect waves-cyan <?php if (($cont == 'Termsandconditions' && $act == 'index')||($cont == 'Privacyandpolicies' && $act == 'index')) { ?>active<?php } ?>">
                        <i class="mdi-action-settings-applications"></i> Site Setting
                    </a>
                    <div class="collapsible-body" style="display: none;">
                        <ul>
                            <li <?php if ($cont == 'Termsandconditions' && $act == 'index') { ?>class="active"<?php } ?>>
                                <a href="<?= BASE_URL."admin/termsandconditions"?>">
                                    Terms and Conditions
                                </a>
                            </li>
                            <li <?php if ($cont == 'Privacyandpolicies' && $act == 'index') { ?>class="active"<?php } ?>>
                                <a href="<?= BASE_URL."admin/privacyandpolicies"?>">
                                    Privacy and Policies
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="bold <?php if ($cont == 'Offers' && $act == 'index') { ?>active<?php } ?>">
            <a href="<?= BASE_URL."admin/offers"?>" class="waves-effect waves-cyan <?php if ($cont == 'Offers' && $act == 'index') { ?>active<?php } ?>">
                <i class="mdi-action-shopping-basket"></i>Offer Management
            </a>
        </li>
        
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold <?php if (($cont == 'Faqcats' && $act == 'index')||($cont == 'Faqlistings' && $act == 'index')) { ?>active<?php } ?>">
                    <a class="collapsible-header waves-effect waves-cyan <?php if (($cont == 'Faqcats' && $act == 'index')||($cont == 'Faqlistings' && $act == 'index')) { ?>active<?php } ?>">
                        <i class="mdi-communication-live-help"></i> FAQ
                    </a>
                    <div class="collapsible-body" style="display: none;">
                        <ul>
                            <li <?php if ($cont == 'Faqcats' && $act == 'index') { ?>class="active"<?php } ?>><a href="<?= BASE_URL."admin/faqcats"?>">FAQ Category</a>
                            </li>
                            <li <?php if ($cont == 'Faqlistings' && $act == 'index') { ?>class="active"<?php } ?>><a href="<?= BASE_URL."admin/faqlistings"?>">FAQ List</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="li-hover"><div class="divider"></div></li>
        <li class="li-hover"><p class="ultra-small margin more-text">Daily Sales</p></li>
        <li class="li-hover">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="sample-chart-wrapper">                            
                        <div class="ct-chart ct-golden-section" id="ct2-chart"></div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan">
        <i class="mdi-navigation-menu"></i>
    </a>
</aside>
<!-- END LEFT SIDEBAR NAV-->