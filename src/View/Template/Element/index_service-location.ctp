<section class="screenshots" id="service-location">
	<div class="container">
		
		<!-- SECTION HEADER -->
		<div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
			
			<!-- SECTION TITLE -->
			<h2 class="dark-text">Service Locations</h2>
			
			<div class="colored-line">
			</div>
			<div class="section-description">
				Give us a call between 9:00AM to 6:00PM we will be there for you.
			</div>
			<div class="colored-line">
			</div>
			
		</div>
		<!-- /END SECTION HEADER -->
		
		<div class="row">
	                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay=".3s">
	                        <div class="media service-box">
	                            <div class="pull-left">
	                                <i aria-hidden="true" class="icon_pin_alt"></i>
	                            </div>
	                            <div class="media-body">
	                                <h4 class="media-heading">SRINATHPURAM</h4>
	                                <p>
	                                    <i aria-hidden="true" class="icon_box-checked "></i> RTU <br>
	                                    <i aria-hidden="true" class="icon_box-checked "></i> Srinathpuram- A, B, C, D, E <br>                                    
	                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
	                                </p>
	                            </div>
	                        </div>
	                    </div>		
	                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay=".6s">
	                        <div class="media service-box">
	                            <div class="pull-left">
	                                <i aria-hidden="true" class="icon_pin_alt"></i>
	                            </div>
	                            <div class="media-body">
	                                <h4 class="media-heading">TALWANDI</h4>
	                                <p>
	                                    <i aria-hidden="true" class="icon_box-checked "></i> Talwandi All Sectors 
	 									<br>
	                                    <i aria-hidden="true" class="icon_box-checked "></i>  Keshavpura <br>                                    
	                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
	                                </p>
	                            </div>
	                        </div>
	                    </div>	        
	                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay=".9s">
	                        <div class="media service-box">
	                            <div class="pull-left">
	                                <i aria-hidden="true" class="icon_pin_alt"></i>
	                            </div>
	                            <div class="media-body">
	                                <h4 class="media-heading">VIGYAN NAGAR</h4>
	                                <p>
	                                    <i aria-hidden="true" class="icon_box-checked "></i>  Vigyan Nagar (ALL SECTORS) 
	 									<br>
	                                    <i aria-hidden="true" class="icon_box-checked "></i>  Chatrappur <br>                                    
	                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
	                                </p>
	                            </div>
	                        </div>
	                    </div>	        
	                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay="1.2s">
	                        <div class="media service-box">
	                            <div class="pull-left">
	                                <i aria-hidden="true" class="icon_pin_alt"></i>
	                            </div>
	                            <div class="media-body">
	                                <h4 class="media-heading">DADABARI</h4>
	                                <p>
	                                    <i aria-hidden="true" class="icon_box-checked "></i>  Basant Vihar  
	 									<br>
	                                    <i aria-hidden="true" class="icon_box-checked "></i>  Waqf Nagar  <br>                                    
	                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
	                                </p>
	                            </div>
	                        </div>
	                    </div>        
	                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay="1.5s">
	                        <div class="media service-box">
	                            <div class="pull-left">
	                                <i aria-hidden="true" class="icon_pin_alt"></i>
	                            </div>
	                            <div class="media-body">
	                                <h4 class="media-heading">INDIRA VIHAR</h4>
	                                <p>
	                                    <i aria-hidden="true" class="icon_box-checked "></i>  Rajeev Gandhi Nagar  
	 									<br>
	                                    <i aria-hidden="true" class="icon_box-checked "></i>  Mahaveer Nagar-1,2,3   <br>                                    
	                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
	                                </p>
	                            </div>
	                        </div>
	                    </div>         
	                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay="1.8s">
	                        <div class="media service-box">
	                            <div class="pull-left">
	                                <i aria-hidden="true" class="icon_pin_alt"></i>
	                            </div>
	                            <div class="media-body">
	                                <h4 class="media-heading">GUMANPURA</h4>
	                                <p>
	                                    <i aria-hidden="true" class="icon_box-checked "></i>  Ballabh Nagar   
	 									<br>
	                                    <i aria-hidden="true" class="icon_box-checked "></i> Srinathpuram- A, B, C, D, E    <br>                                    
	                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
	                                </p>
	                            </div>
	                        </div>
	                    </div>                 
	        
			<!-- /END SCREENSHOTS -->
			
		</div>
		<!-- /END ROW -->	
	</div>
	<!-- /END CONTAINER -->
</section>
