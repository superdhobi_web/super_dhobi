<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- SITE TITLE -->
        <title>SUPERDHOBI :: Offers</title>
        <!--FAV AND TOUCH ICONS-->
        <link rel="icon" href="<?php echo $this->request->webroot; ?>images/favicon/favicon-32x32.png">
        <!--STYLESHEETS-->
        <!--BOOTSTRAP-->
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/bootstrap.min.css">
        <!-- FONT ICONS -->
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>assets/elegant-icons/style.css">
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>assets/app-icons/styles.css">
        <!--[if lte IE 7]><script src="lte-ie7.js"></script><![endif]-->
        <!-- WEB FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic' rel='stylesheet' type='text/css'>
        <!-- CAROUSEL AND LIGHTBOX -->
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/owl.theme.css">
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/nivo-lightbox.css">
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/nivo_themes/default/default.css">
        <!-- ANIMATIONS -->
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/animate.min.css">
        <!-- CUSTOM STYLESHEETS -->
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/styles.css">
        <!-- COLORS -->
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/colors/blue.css"><!-- DEFAULT COLOR/ CURRENTLY USING -->
        <!-- RESPONSIVE FIXES -->
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/responsive.css">
        <!--[if lt IE 9]>
                                <script src="js/html5shiv.js"></script>
                                <script src="js/respond.min.js"></script>
	<![endif]-->
        <!-- JQUERY -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    </head>

    <body>
        <!--PRE LOADER-->
        <div class="preloader">
            <div class="status">&nbsp;</div>
        </div>
        <!-- HEADER-->
        <header class="header" id="home">
            <!-- STICKY NAVIGATION -->
            <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top no-sticky-navigation">
                <div class="container">
                    <div class="navbar-header">

                        <!-- LOGO ON STICKY NAV BAR -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?= BASE_URL . "index" ?>">
                            <img src="<?php echo $this->request->webroot; ?>images/logo1.png" alt="Superdhobi" style="height:30px;">
                        </a>				
                    </div>

                    <!-- NAVIGATION LINKS -->
                    <div class="navbar-collapse collapse" id="kane-navigation">
                        <ul class="nav navbar-nav navbar-right main-navigation">
                            <li><a href="<?= BASE_URL."index"?>">Home</a></li>
                            <li><a href="<?= BASE_URL."index/offers"?>">Offers</a></li>
                            <li><a href="<?= BASE_URL."index"?>#service">Services</a></li>
                            <li><a href="<?= BASE_URL."index/price"?>">Price</a></li>
                            <li><a href="<?= BASE_URL."index"?>#features">Features</a></li>
                            <li><a href="<?= BASE_URL."index"?>#process">Process</a></li>
                            <li><a href="<?= BASE_URL."index"?>#service-location">Service Location</a></li>
                            <li><a href="<?= BASE_URL."index/testimonial" ?>">Testimonial</a></li>
                            <li><a href="<?= BASE_URL."index"?>#brief2">About Us</a></li>
                            <li><a href="<?= BASE_URL."index"?>#contact">Contact Us</a></li>
                        </ul>
                    </div>
                </div> <!-- /END CONTAINER -->
            </div> <!-- /END STICKY NAVIGATION -->
        </header>
        <!-- /END HEADER -->
        <!--Offer Section-->
        <section class="app-brief">
            <div class="container">			
                <div class="row">				
                    <div class="col-md-8 col-sm-12 left-align wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">				
                        <h2>Latest Offers</h2>					
                        <div class="colored-line-left">
                        </div>	
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                if (isset($coupon)) {
                                    foreach ($coupon as $couponDetail) {
                                        ?>
                                        <div class="offer-card">
                                            <div class="row">	
                                                <div class="col-md-2 col-sm-12">
                                                    <div class="discount-box">
                                                        <span class="amount">
                                                            <?=$couponDetail['discount'];?>%
                                                        </span>
                                                        <span class="amount-title">
                                                            Discount
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 col-sm-12">
                                                    <span class="offer-valid">
                                                        <b>
                                                            <i class="icon_calendar"></i>
                                                        </b>
                                                        <?= $couponDetail['start_date']; ?> 
                                                        <i class="arrow_left-right_alt"></i>
                                                        <?= $couponDetail['end_date']; ?>
                                                    </span>
                                                    <span class="offer-title">
                                                        <?= $couponDetail['coupon_name']; ?>
                                                    </span>
                                                    <span class="offer-desc">
                                                        <?= $couponDetail['coupon_desc']; ?>(Only applicable on the following days <?= $couponDetail['weekdays']; ?>)
                                                    </span>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <button class="btn btn-warning btn-block" id="code<?= $couponDetail['coupon_code']; ?>" onclick="showcode('<?= $couponDetail['coupon_code']; ?>')">
        											GET CODE
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                } else {?>
                                <span class="text-danger">There is no Latest Offers</span>
                                <?php }?>
                            </div>
                        </div>									
                    </div>
                    <div class="col-md-4 col-sm-12 wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
                        <div class="phone-image">
                            <img src="<?php echo $this->request->webroot; ?>images/offers.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END Offer Section SECTION -->
        <!--FOOTER-->
        <footer>
            <div class="container">			
                <div class="col-md-12" style="border-bottom: 1px solid #BFBFBF;margin-bottom:7px;">
                    <!--LOGO &quot; SOCIAL ICONS -->
                    <div class="col-md-6">
                        <img src="<?php echo $this->request->webroot; ?>images/logo1.png" alt="LOGO" class="footer-logo img-responsive">
                        <ul class="social-icons">
                            <li><a href="https://www.facebook.com/SUPER-DHOBI-1010169048993992/?ref=hl"><i class="social_facebook_square"></i></a></li>
                            <li><a href="https://twitter.com/MySuperDhobi"><i class="social_twitter_square"></i></a></li>
                            <li><a href="https://in.pinterest.com/SuperDhobi/superdhobi"><i class="social_pinterest_square"></i></a></li>
                            <li><a href="https://plus.google.com/u/5/105875490934370267660"><i class="social_googleplus_square"></i></a></li>
                            <li><a href="https://www.linkedin.com/in/mysuper-dhobi-a8701310b?trk=nav_responsive_tab_profile_pic"><i class="social_linkedin_square"></i></a></li>
                            <!--<li><a href=""><i class="social_dribbble_square"></i></a></li>-->
                            <li><a href="http://superdhobi.blogspot.in/?view=timeslide"><i class="social_flickr_square"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 contact-info">
                        <p><i aria-hidden="true" class="icon_phone"></i> +919549005123, +917891399900</p>
                        <p><i aria-hidden="true" class="icon_mail"></i> info@superdhobi.in, feedbacksupedhobi@gmail.com</p>
                        <p><i aria-hidden="true" class="icon_pin"></i> Shubhash Nagar, Kota</p>
                    </div>
                </div>
                <!-- COPYRIGHT TEXT -->
                <div class="col-md-12">		
                    <p class="copyright">
                        <span class="pull-left">
	                    Copyright © 2016 SuperDhobi, All Rights Reserved
                        </span>
                        <span class="pull-right text-center col-xs-12 col-md-4">
                            <a href="#" data-toggle="modal" data-target="#ppModal">
                                <small>Privacy Policy</small>
                            </a>
	                    |
                            <a href="#" data-toggle="modal" data-target="#tcModal">
                                <small>Terms & Conditions</small>
                            </a>
                        </span>
                    </p>
                </div>
            </div>
            <!-- /END CONTAINER --> 
        </footer>
        <!-- /END FOOTER -->
        <!--Modal Contents-->
        <div id="ppModal" class="modal fade in" role="dialog" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title tcp-title"><i class="icon_documents_alt"></i> Privacy Policy</h4>
                    </div>
                    <div class="modal-body tcp">
                        <p>
                            <i class="arrow_triangle-right_alt2"></i> This privacy policy sets out how SuperDhobi uses and protects any information that you give to SuperDhobi over Phone, Website or Mobile App. <br>

                            <i class="arrow_triangle-right_alt2"></i> SuperDhobi is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement. <br>

                            <i class="arrow_triangle-right_alt2"></i> SuperDhobi may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 2nd of April 2015. 
                        </p>
                        <h4><i class="arrow_carrot-2right"></i> What we collect?</h4>
                        <p>
			          We may collect the following information: <br>
                            <i class="icon_circle-slelected"></i> name and job title <br>
                            <i class="icon_circle-slelected"></i> contact information including email address <br>
                            <i class="icon_circle-slelected"></i> demographic information such as postcode, preferences and interests <br>
                            <i class="icon_circle-slelected"></i> other information relevant to customer surveys and/or offers
                        </p>
                        <h4><i class="arrow_carrot-2right"></i> What we do with the information we gather?</h4>
                        <p>
			          We require this information to understand your needs and provide you with a better service, and in particular for the following reasons: <br>
                            <i class="arrow_triangle-right_alt2"></i>  Internal record keeping. <br>

                            <i class="arrow_triangle-right_alt2"></i>  We may use the information to improve our products and services. <br>

                            <i class="arrow_triangle-right_alt2"></i> We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided. <br>

                            <i class="arrow_triangle-right_alt2"></i>  From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customize the website according to your interests. <br>

                            <i class="arrow_triangle-right_alt2"></i>  The information you provide will be shared with the servers on which we host the web content and your name and location may be shared with the laundry processing centre(s) and Rider(s) we have been working with.          
                        </p>
                        <h4><i class="arrow_carrot-2right"></i> Security</h4>
                        <p>
                            <i class="arrow_triangle-right_alt2"></i>  We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.
                        </p>
                        <h4><i class="arrow_carrot-2right"></i> How we use cookies?</h4>
                        <p>
                            <i class="arrow_triangle-right_alt2"></i> A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences. <br>

                            <i class="arrow_triangle-right_alt2"></i> We use traffic log cookies to identify which pages are being used. This helps us analyze data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system. <br>

                            <i class="arrow_triangle-right_alt2"></i> Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us. <br>

                            <i class="arrow_triangle-right_alt2"></i> You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website. 
                        </p>
                        <h4><i class="arrow_carrot-2right"></i> Contacting Us</h4>
                        <p>
			          If there are any questions regarding this privacy policy you may contact us at our Call Centre or write to :- <a href="mailto:mysuperdhobhi@gmail.com">mysuperdhobhi@gmail.com</a>
                        </p>
                    </div>          
                    <div class="modal-footer">            
                    </div>        
                </div>
            </div>
        </div>
        <div id="tcModal" class="modal fade in" role="dialog" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title tcp-title"><i class="icon_document_alt"></i> Terms & Conditions</h4>
                    </div>
                    <div class="modal-body tcp">
                        <h4><i class="arrow_carrot-2right"></i> Demo Heading</h4>
                        <p>
                            <i class="arrow_triangle-right_alt2"></i>
		      			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil ullam voluptatem neque qui ab suscipit magnam magni, doloribus eum perspiciatis debitis harum reiciendis odio voluptates excepturi eos! Rem, quaerat sint.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--SCRIPTS-->
        <script src="<?php echo $this->request->webroot; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/smoothscroll.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/jquery.localScroll.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/owl.carousel.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/nivo-lightbox.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/simple-expand.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/wow.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/jquery.stellar.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/retina.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/matchMedia.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/jquery.backgroundvideo.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/jquery.nav.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/jquery.ajaxchimp.min.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/jquery.fitvids.js"></script>
        <script src="<?php echo $this->request->webroot; ?>js/custom.js"></script>
        <script>
            function showcode(code){
                $("#code"+code).html(code);
                $("#code"+code).removeAttr("onclick");
            }    
        </script>
    </body>
</html>