<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Users</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Employee</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end--> 
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content filter">
                            <div class="row">
                                <div class="col m5 s12">
                                    <h6 class="left teal-text">
                                        Search Employee
                                    </h6>  
                                    <div class="col m12">
                                        <form action="" method="get">
                                            <div class="col m5">
                                                <div class="input-field">
                                                    <select name="userCondition" id="userCondition" class="validate">
                                                        <option value="all" disabled selected>Search By...</option>
                                                        <option value="emp_id" <?php
if ($this->request->query("userCondition") == "emp_id") {
    echo "selected";
}
?>>Employee ID</option>
                                                        <option value="department" <?php
                                                                if ($this->request->query("userCondition") == "department") {
                                                                    echo "selected";
                                                                }
?>>Department</option>
                                                        <option value="username" <?php
                                                                if ($this->request->query("userCondition") == "username") {
                                                                    echo "selected";
                                                                }
?>>Employee Name</option>
                                                        <option value="mobile" <?php
                                                                if ($this->request->query("userCondition") == "mobile") {
                                                                    echo "selected";
                                                                }
?>>Mobile No</option>
                                                        <option value="email" <?php
                                                                if ($this->request->query("userCondition") == "email") {
                                                                    echo "selected";
                                                                }
?>>Email ID</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col m5">
                                                <div class="input-field">
                                                    <input id="getUser_src" name="userData" onkeyup="return getAutolist();" type="text" value="<?php
                                                                if ($this->request->query("userData")) {
                                                                    echo $this->request->query("userData");
                                                                }
?>" class="validate">
                                                    <label for="getUser_src" class="">Enter Data...</label>
                                                </div>
                                            </div>
                                            <div class="col m2">
                                                <br>
                                                <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit">
                                                    <i class="mdi-content-forward"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card" style="overflow:auto;">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    User Details
                                </span>
                                <i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Add User Details"></i>
                            </span>                                    
                        </div>
                        <div class="col s12 m12 l12">
                            <table class="striped responsive-table">
                                <thead>
                                    <tr class="white-text">
                                        <th class="teal">Action</th>
                                        <th class="teal"><?= $this->Paginator->sort('userid', ['label' => 'User Id']); ?></th>
                                        <th class="teal">Password</th>
                                        <th class="teal"><?= $this->Paginator->sort('username', ['label' => 'User Detail']); ?></th>
                                        <th class="teal">Address</th>                                        
                                        <th class="teal"><?= $this->Paginator->sort('deprtment_name', ['label' => 'Department Name']); ?></th>
                                        <th class="teal">Status</th>                                          
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    foreach ($adminusers as $adminuser): ?>
                                        <tr>
                                            <td>   
                                               <!--  <a data-activates="chat-out" class="btn-floating btn-small cyan waves-effect waves-light chat-collapse">
                                                    <i class="mdi-action-launch"></i>
                                                </a> -->
                                                <a class="btn-floating btn-small  green waves-effect waves-light " title="Edit" href="<?= ADMIN_URL . "adminusers/edit/" . $adminuser->id ?>">
                                                    <i class="mdi-content-create"></i>
                                                </a>
                                                <?php
                                                echo $this->Form->postLink(
                                                "<i class='mdi-action-delete'> </i>",
                                                ['action' => 'delete', $adminuser->id],
                                                ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete employee {0}?', $adminuser->username)] // third
                                                );

//echo $this->Form->postLink('<i class="mdi-action-delete"> </i>', array('action' => 'delete', $user->id), array('class' => 'btn btn-primary btn-sm'), 'Are you sure?');
                                                ?>
                                            </td>
                                            <td><?= h($adminuser->emp_id); ?></td>
                                            <td><?= h($adminuser->password); ?></td>
                                            <td class="">                                                
                                                <span class="teal-text text-darken-3">
                                                    <i class="mdi-social-person"></i> <b><?= h($adminuser->username); ?></b>
                                                </span> <br>
                                                <span class="green-text text-darken-1">
                                                    <i class="mdi-hardware-smartphone"></i> <?= h($adminuser->mobile); ?> 
                                                </span><br>
                                                <span class="blue-text text-darken-1">
                                                    <i class="mdi-communication-email"></i> <?= h($adminuser->email); ?>               
                                                </span>
                                            </td>
                                            <td>
                                                <span class="green-text text-darken-1">
                                                    <i class="mdi-maps-pin-drop"></i>
                                                    <b><?= h($adminuser->address); ?></b>
                                                </span> <br>
                                                <?= h($adminuser->address) . ","; ?> <?= h($adminuser->state) . ","; ?><?= h($adminuser->pin); ?>
                                            </td>
                                             <td>
                                                <?php
                                                $dept_id=$adminuser->department;
                                                foreach($department as $dept){
                                                    if($dept_id==$dept->id)
                                                    echo $dept->department_name;
                                                }
?>
                                               </td>
                                            <td><div class="switch">
                                                    <label>
                                                         <?php $controller=$this->request->params['controller'];?>
                                                        <input type="checkbox" <?php if($adminuser->status==1){ echo "Checked";}?> value="<?php if($adminuser->status==1){ echo 1; } else echo 2;?>"class="Addcheckbox" id="defaultAdd<?=  $adminuser->id; ?>" name='status' onchange="changeStatus(this.value,<?= $adminuser->id; ?>,'<?= $controller; ?>');"/>
                                                        <span class="lever"></span>
                                                    </label>
                                                </div></td>

                                        </tr>
                                    <?php endforeach;  ?>
                                </tbody>
                            </table>
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next(__('next') . ' >') ?>
                                </ul>
                                <p><?= $this->Paginator->counter() ?></p>
                            </div>
                            
                        </div>
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">
                                <span class="green-text">Add New User Details</span>
                                <i class="mdi-navigation-close right"></i>
                            </span>
                            <div class="row">
                                <form class="col s12 m8 offset-m2 right-alert" id="addadmin" action="<?= ADMIN_URL . "adminusers/add" ?>" method="post">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="mdi-action-account-circle prefix"></i>
                                            <input id="empname" class="validate" required type="text" value="" name="username"/>
                                            <label for="empname" data-error="Please enter name." data-success="Perfect!">Full Name</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="mdi-communication-email prefix"></i>
                                            <input id="email_input2" required class="validate" type="email" value="" name="email"/>
                                            <label for="email_input2" class="" data-error="Please enter valid email." data-success="I Like it!">Email</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="mdi-communication-email prefix"></i>
                                            <input id="mobile_input2" class="validate" required type="number" value="" name="mobile"/>
                                            <label for="mobile_input2" class="" data-error="Please enter valid mobile." data-success="I Like it!">Mobile</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <select class="browser-default validate" name="department" id="department">
                                                <option value="">Choose Department</option>
                                                <?php foreach($department as $dept){?>
                                                <option value="<?= $dept->id;?>"><?= $dept->department_name;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="mdi-communication-email prefix"></i>
                                            <input id="address" class="validate" type="text" value="" name="address"/>
                                            <label for="address" class="" data-error="Please enter address." data-success="I Like it!">Address</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <select name="state" id="state" class="validate browser-default">
                                                <option value="">Choose State</option>
                                                <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                                <option value="Assam">Assam</option>
                                                <option value="Bihar">Bihar</option>
                                                <option value="Chhattisgarh">Chhattisgarh</option>
                                                <option value="Chhattisgarh">Chhattisgarh</option>
                                                <option value="Goa">Goa</option>
                                                <option value="Gujarat">Gujarat</option>
                                                <option value="Haryana">Haryana</option>
                                                <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                <option value="Jharkhand">Jharkhand</option>
                                                <option value="Karnataka">Karnataka</option>
                                                <option value="Kerala">Kerala</option>
                                                <option value="Madhya Pradesh">Madhya Pradesh</option>
                                                <option value="Maharashtra">Maharashtra</option>
                                                <option value="Manipur">Manipur</option>
                                                <option value="Meghalaya">Meghalaya</option>
                                                <option value="Mizoram">Mizoram</option>
                                                <option value="Nagaland">Nagaland</option>
                                                <option value="Odisha">Odisha</option>
                                                <option value="Punjab">Punjab</option>
                                                <option value="Rajasthan">Rajasthan</option>
                                                <option value="Sikkim">Sikkim</option>
                                                <option value="Tamil Nadu">Tamil Nadu</option>
                                                <option value="Telangana">Telangana</option>
                                                <option value="Tripura">Tripura</option>
                                                <option value="Uttar Pradesh">Uttar Pradesh</option>
                                                <option value="Uttarakhand">Uttarakhand</option>
                                                <option value="West Bengal">West Bengal</option>
                                            </select>
<!--                                            <input id="state" class="validate" type="text" value="" name="state"/>-->
                                            <label for="state" class="" data-error="Please choose state." data-success="I Like it!">State</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="mdi-communication-email prefix"></i>
                                            <input id="pincode" class="validate" required type="number" value="" name="pin"/>
                                            <label for="pincode" class="" data-error="Please enter pin." data-success="I Like it!">Pin</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button class="btn green waves-effect waves-light right" type="submit" name="action" id="usersubmit" onclick="validateemp();">Save
                                                <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--card stats end-->

        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
            <a class="btn-floating btn-large">
                <i class="mdi-content-create"></i>
            </a>
            <ul>
                <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
            </ul>
        </div>
        <!-- Floating Action Button -->
    </div>
</section>
<!-- END CONTENT -->