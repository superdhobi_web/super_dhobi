<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Department</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Departments</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    Department Details
                                </span>
                                <i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Add Service Details"></i>
                            </span>                                    
                        </div>
                        <div class="row">
                                                    <div class="col s12 m12 l12">
                            <table class="striped responsive-table centered">
                                <thead>
                                    <tr class="white-text">
                                        <th class="teal">Actions</th>
                                        <th class="teal"><?= $this->Paginator->sort('deprtment_name', ['label' => false]); ?>Department Name</th>
                                        <th class="teal"><?= $this->Paginator->sort('status', ['label' => 'Status']); ?></th>
                                        <th class="teal"><?= $this->Paginator->sort('created_date', ['label' => false]); ?>Created Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($departments as $department): ?>
                                        <tr>
                                            <td class="actions">
                                                <?=
                                                $this->Html->link("<i class='mdi-content-create'></i>", ['action' => 'edit', $department->id],
                                                ['escape' => false, 'title' => 'Edit', 'class' => 'btn-floating btn-small green waves-effect waves-light']
                                                );
                                                ?>
                                                <?=
                                                $this->Form->postLink(
                                                "<i class='mdi-action-delete'> </i>",
                                                ['action' => 'delete', $department->id],
                                                ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete {0}?',$department->department_name." department")] // third
                                                );
                                                ?>
                                            </td>
                                            <td><?= h($department->department_name) ?></td> 
                                            <td><div class="switch">
                                                    <label>
                                                         <?php $controller=$this->request->params['controller'];?>
                                                        <input type="checkbox" <?php if($department->status==1){ echo "Checked";}?> value="<?php if($department->status==1){ echo 1; } else echo 2;?>"class="Addcheckbox" id="defaultAdd<?=  $department->id; ?>" name='status' onchange="changeStatus(this.value,<?= $department->id; ?>,'<?= $controller; ?>');"/>
                                                        <span class="lever"></span>
                                                    </label>
                                                </div></td>
                                            <td><?= h(date('jS F Y / H:i A', strtotime($department->created_date))); ?></td>

                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next(__('next') . ' >') ?>
                                </ul>
                                <p><?= $this->Paginator->counter() ?></p>
                            </div>
                        </div>
                        </div>
                        <?= $this->element('adddepartment'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
