<div class="container">
    <div class="row">
        <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Manage Users</h5>
            <ol class="breadcrumbs">
                <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                <li class="active">Manage Users</li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <!--card stats start-->
    <div id="card-stats">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card">
                    <span class="card-title grey-text text-darken-4">
                        <span class="green-text">Edit Department</span>
                    </span>
                    <div class="row">
                        <form class="col s12 m8 offset-m2 right-alert" id="adddepartment" action="<?= ADMIN_URL . "departments/add/$department->id";?>" method="post">
                            <input type="hidden" name="id" value="<?= $department->id; ?>"/>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-action-account-circle prefix"></i>
                                    <input id="department_input2" class="validate" type="text" name="department_name" value="<?= $department->department_name; ?>"/>
                                    <label for="department_input2" data-error="Please enter Department name." data-success="Perfect!">Department Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn green waves-effect waves-light right" href="javascript:void(0);" name="action" id="usersubmit" onclick="uservalidate();">Save
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function uservalidate(){
        var department_name=$("#department_input2").val();
        if(department_name==""){
            $("#department_input2").data("data-error");
            //alert(department_name);
        }        
    }
    
</script>