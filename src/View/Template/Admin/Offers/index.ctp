<?php
$offer_manage = array(
    '1' => 'New User',
    '2' => 'Reffered by User');
    ?>
<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Users</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Employee</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end--> 
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card" style="overflow:auto;">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    Offer Details
                                </span>
                                <!-- <i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Add Offer Details"></i> -->
                            </span>                                    
                        </div>
                        <div class="col s12 m12 l12">
                            <table class="striped responsive-table">
                                <thead>
                                    <tr class="white-text">
                                        <th class="teal">Action</th>
                                        <th class="teal"><?= $this->Paginator->sort('offer_type', ['label' => 'Offer Type']); ?></th>
                                        <th class="teal"><?= $this->Paginator->sort('price', ['label' => 'Price']); ?></th>
                                        <th class="teal">Date</th>                                        
                                        <!-- <th class="teal">Status</th>  -->                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    foreach ($offers as $offer): ?>
                                        <tr>
                                            <td>   
                                               <!--  <a data-activates="chat-out" class="btn-floating btn-small cyan waves-effect waves-light chat-collapse">
                                                    <i class="mdi-action-launch"></i>
                                                </a> -->
                                                <a class="btn-floating btn-small  green waves-effect waves-light " href="#" onclick="price_edit('<?=$offer->id?>');">
                                                    <i class="mdi-content-create"></i>
                                                </a>
                                                
                                            </td>
                                            <td><?php if ($offer['offer_type']==1) { echo "New User";
                                            }elseif ($offer['offer_type']==2) {
                                                echo "Reffered User";
                                            }
                                            ?></td>
                                            <td><span id="price<?=$offer->id?>" style="display:block;"><?= h($offer->price); ?></span>
                                                <span style="display:none;" id="editpri<?=$offer->id?>"><input type="text" value="" onblur="save_price('<?=$offer->id?>',this.value);"></span></td>
                                            <td><?= h($offer->adddate); ?></td>
                                        </tr>
                                    <?php endforeach;  ?>
                                </tbody>
                            </table>
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next(__('next') . ' >') ?>
                                </ul>
                                <p><?= $this->Paginator->counter() ?></p>
                            </div>
                            
                        </div>
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">
                                <span class="green-text">Add New Offer</span>
                                <i class="mdi-navigation-close right"></i>
                            </span>
                            <div class="row">
                                <form class="col s12 m8 offset-m2 right-alert" action="<?= ADMIN_URL . "offers/" ?>" method="post">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <select class="validate" name="offer_type">
                                                <option value="">Choose User type</option>
                                                <option value="1" name="offer_type">New User</option>
                                                <option value="2" name="offer_type">Reffered User</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="mdi-communication-email prefix"></i>
                                            <input id="price" class="validate" type="number" value="" name="price"/>
                                            <label for="price" class="" data-error="Please enter pin." data-success="I Like it!">Price</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button class="btn green waves-effect waves-light right" type="submit">Save
                                                <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--card stats end-->

        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
            <a class="btn-floating btn-large">
                <i class="mdi-content-create"></i>
            </a>
            <ul>
                <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
            </ul>
        </div>
        <!-- Floating Action Button -->
    </div>
</section>
<!-- END CONTENT -->