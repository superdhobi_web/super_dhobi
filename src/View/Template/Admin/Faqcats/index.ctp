<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage FAQ Category</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage FAQ Category</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    Category Details
                                </span>
                                <i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Add Category Details"></i>
                            </span> 
                            <div class="row">
                            <div class="col s12 m12 l12">
                                <table class="striped responsive-table centered">
                                    <thead>
                                        <tr class="white-text">
                                            <th class="teal">Actions</th>
                                            <th class="teal"><?= $this->Paginator->sort('name', ['label' => false]); ?>Category Name</th>
                                            <th class="teal"><?= $this->Paginator->sort('status', ['label' => false]); ?>Status</th>
                                            <th class="teal"><?= $this->Paginator->sort('createddate', ['label' => false]); ?>Created Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($faqcats as $faqcat): ?>
                                            <tr>
                                                <td class="actions">
                                                    <?=
                                                    $this->Html->link("<i class='mdi-content-create'></i>", ['action' => 'edit', $faqcat->id],
                                                    ['escape' => false, 'title' => 'Edit', 'class' => 'btn-floating btn-small  green waves-effect waves-light']
                                                    );
                                                    ?>
                                                    <?=
                                                    $this->Form->postLink(
                                                    "<i class='mdi-action-delete'> </i>",
                                                    ['action' => 'delete', $faqcat->id],
                                                    ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete # {0}?', $faqcat->id)] // third
                                                    );
                                                    ?>
                                                </td>
                                                <td><?= h($faqcat->name) ?></td>
                                                <td><div class="switch">
                                                    <label>
                                                         <?php $controller=$this->request->params['controller'];?>
                                                        <input type="checkbox" <?php if($faqcat->status==1){ echo "Checked";}?> value="<?php if($faqcat->status==1){ echo 1; } else echo 2;?>"class="Addcheckbox" id="defaultAdd<?=  $faqcat->id; ?>" name='add' onchange="changeStatus(this.value,<?= $faqcat->id; ?>,'<?= $controller; ?>');"/>
                                                        <span class="lever"></span>
                                                    </label>
                                                </div></td>
                                                <td><?= h(date('jS F Y / h:i A', strtotime($faqcat->createddate))); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>

                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next(__('next') . ' >') ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>                                           
                        </div>
                                        
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">
                                <span class="green-text">Add New FAQ Category</span>
                                <i class="mdi-navigation-close right"></i>
                            </span>
                            <div class="row">
                                <form class="col s12 m8 offset-m2 right-alert" id="addfaq" action="<?= ADMIN_URL . "faqcats/add" ?>" method="post">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="mdi-editor-format-line-spacing prefix"></i>
                                            <input id="categoryname" class="validate" type="text" name="name"/>
                                            <label for="categoryname" data-error="Please enter Category name." data-success="Perfect!">Category Name</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button class="btn green waves-effect waves-light right" name="action">Save
                                                <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
