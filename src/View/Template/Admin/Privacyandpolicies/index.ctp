<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Privacy and Policies</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Privacy and Policies</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
        <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card" style="height:465px;">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    Privacy &AMP; Policies
                                </span>
                                <i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Edit T&C"></i>
                            </span>                                    
                        </div><br/>
                        <div class="container">
                            <div class="row">
                                <div class="col s12 m12 l12">
                                    <p class="content view">
                                        <?php if(!empty($pc)){
                                            echo $pc['details'];
                                            }
                                        ?>
                                    </p>
                                </div>  
                            </div>                 
                        </div>
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">
                                <span class="green-text">Edit Privacy&AMP;Policies Details</span>
                                <i class="mdi-navigation-close right"></i>
                            </span>
                            <div class="row">
                                <form method="post">
                                    <input type="hidden" name="id" value="<?= $pc['id']; ?>">
                                    <div class="col s12">
                                        <textarea name="editor1"><?php echo $pc['details']; ?></textarea>
                                    </div>
                                    <div class="col s12">
                                        <p>&nbsp;</p>
                                        <button class="btn green waves-effect waves-light right" type="submit" id="usersubmit">Save<i class="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>