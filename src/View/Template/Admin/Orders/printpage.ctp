<section id="content">        
  <!--breadcrumbs start-->
  <div id="breadcrumbs-wrapper">
    <!-- Search for small screen -->
    <div class="header-search-wrapper grey hide-on-large-only">
      <i class="mdi-action-search active"></i>
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
    </div>
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Assign Task Management</h5>
          <ol class="breadcrumbs">
            <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
            <li class="active">Assign Task Print</li>
          </ol>
        </div>

      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <a href="#" class="tooltipped btn-floating waves-effect waves-light green secondary-content" data-position="left" data-tooltip="Print The Details" onclick="printtask();">
        <i class="mdi-action-print"></i>
      </a>
    </div></div>
    <!--breadcrumbs end--> 
    <div class="container invoice-print" id="blog-posts">
      <link href="<?php echo $this->request->webroot; ?>css/materialize.css" rel="stylesheet" media="print">
      <link href="<?php echo $this->request->webroot; ?>css/style.css" rel="stylesheet" media="print">
      <link href="<?php echo $this->request->webroot; ?>css/custom/custom.css" rel="stylesheet" media="print">
      <?= $this->Html->css("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"); ?>
      <div class="invoice-header">
        <div class="row section">
          <table>
            <tr>
              <td>
                <img src="http://superdhobi.in/assets/images/logo.png" class="logo" alt="company logo">                
              </td>
              <td>
                <span class="ultra-small">
                  <?= date("jS F, Y");?>
                </span>
                <span class="ultra-small">
                  <?= date("h:i a");?>
                </span>
              </td>
              <td>
                <div class="invoce-company-address right-align">
                  <span class="strong">
                    <i class="mdi-social-location-city cyan-text" style="font-size: 20px;"></i> <?=$empdata->username?>
                  </span>
                  <br>
                  <span>EmpID: <?=$empdata->emp_id?></span>                      
                  <br>
                  <span><?=$empdata->mobile?></span>                      
                </div>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="invoice-table" style="padding-top:0 !important;">
        <div class="row">
          <div class="col s12 m12 l12">
            <table class="bordered orderForprnt centered">
              <thead>
                <tr class="cyan white-text trow">
                  <th>Sl.</th>
                  <th>Order-ID</th>
                  <th>Details</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th>Contact</th>
                  <th>$/Total</th>
                  <th>Remark</th>
                </tr>
              </thead>
              <tbody>
                <?php if(empty($data)){ echo "<tr><td colspan='8'>No Task Found</td></tr>";}else{
                  $i=1;
                  foreach($data as $va){  
                    ?>
                    <tr>
                      <td><?=$i?></td>
                      <td>
                        <span>
                          <?=$va['order_id']?>
                        </span>
                      </td>
                      <td>
                        <span>
                          <?=$va['user_id']?>
                        </span>
                        <br>
                        <span class="ultra-small">
                          <?=$va['user_name']?>
                        </span>
                      </td>
                      <td>
                        <span>
                         <?=$va['address']?>, <?=$va['area']?> <br>
                         <?=$va['location']?>, <?=$va['circle']?><br>
                         <?=$va['city']?>-<?=$va['pin']?>
                       </span>
                     </td>
                     <td>
                      <span class="task-cat teal">
                        <?=$va['status']?>
                      </span>
                      <br>
                      <span class="ultra-small">
                        <?=$va['totalcloth']?> pc
                      </span>
                    </td>
                    <td>
                      <span>
                        +91-<?=$va['mobile']?>
                      </span>
                    </td>
                    <td>
                      <div class="print-det">
                        <span>&nbsp;</span>
                        <span><?=$va['totalprice']?></span>
                      </div>     
                    </td>
                    <td>
                      <div class="print-det">
                        <span>&nbsp;</span>
                        <span class="grey lighten-3">&nbsp;</span>
                      </div> 
                    </td>
                  </tr>
                  <?php $i++;} } ?>
                </tbody>
              </table>
            </div>

          </div>
        </div>




      </div>
    </section>
