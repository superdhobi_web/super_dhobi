<?php
$status = array(
    '1' => 'Order Placed',
    '2' => 'Order Confirmed',
    '3' => 'Ready To Pickup',
    '4' => 'Out For Pickup',
    '5' => 'Picked up',
    '6' => 'Pickup Reschedule',
    '7' => 'Intransit To Warehouse',
    '8' => 'Order Processing',
    '9' => 'Ready To deliver',
    '10' => 'Out for Deliver',
    '11' => 'Delivered',
    '12' => 'Deliver Reschedule',
    '13' => 'Cancel');
?>
<section id="content">
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Order Details</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . " admin/dashboard " ?>">Dashboard</a></li>
                        <li class="active">Order Details</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title activator">
                            Order Details
                        </span>
                    </div>
                    <div class="row">
                        <div class="col s12 m8 offset-m2 grey lighten-4" id="order-details">
                            <ul class="">
                                <li class="li-hover">
                                    <ul class="chat-collapsible" data-collapsible="expandable">
                                        <li>
                                            <div class="collapsible-header orange white-text active ">
                                                <i class="mdi-action-assignment-ind"></i>User Order Details
                                            </div>
                                            <div class="collapsible-body recent-activity">
                                                <div class="recent-activity-list chat-out-list row">
                                                    <div class="col s12 recent-activity-list-text">
                                                        <span>
                                                            <i class="mdi-action-assignment"></i>
                                                            <span class="ultra-small">
                                                                <b><?= $orderdetail['user_name']; ?></b> <br>
                                                                <i class="mdi-action-event"></i> <?= date("jS F, Y", strtotime($orderdetail['ordereddate'])); ?> <br>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="favorite-associate-list chat-out-list row">
                                                    <div class="col s12 row">
                                                        <div class="col s12 m6 ultra-small teal-text card-panel">
                                                            <p class="">
                                                                PICKUP
                                                            </p>
                                                            <div class="divider"></div>
                                                            <span>
                                                                <i class="mdi-action-event"></i> <b><?= $orderdetail['pickupdate']; ?></b>
                                                            </span>
                                                            <br>
                                                            <span>
                                                                <i class="mdi-maps-pin-drop"></i> <?= $orderdetail['pickaddress'] ?>,<?= $orderdetail['pickarea'] ?>
                                                                <br/>
                                                                <i class="mdi-maps-pin-drop"></i><?= $orderdetail['picklocation'] ?>,<?= $orderdetail['pickcircle'] ?>
                                                                <br/>
                                                                <i class="mdi-maps-pin-drop"></i><?= $orderdetail['pickcity'] ?>
                                                            </span>
                                                            <br>
                                                            <span>
                                                                <i class="mdi-action-alarm"></i> <?= $orderdetail['pickupslot'] ?>
                                                            </span>
                                                        </div>
                                                        <div class="col s12 m6 ultra-small text-accent-4 green-text card-panel">
                                                            <p class="">
                                                                DELIVERY
                                                            </p>
                                                            <div class="divider"></div>
                                                            <span>
                                                                <i class="mdi-action-event"></i> <b><?= $orderdetail['deliverydate'] ?></b>
                                                            </span>
                                                            <br>
                                                            <span>
                                                                <i class="mdi-maps-pin-drop"></i> <?= $orderdetail['deliaddress'] ?>,<?= $orderdetail['delikarea'] ?>
                                                                <br/>
                                                                <i class="mdi-maps-pin-drop"></i><?= $orderdetail['delilocation'] ?>,<?= $orderdetail['delicircle'] ?>
                                                                <br/>
                                                                <i class="mdi-maps-pin-drop"></i><?= $orderdetail['delicity'] ?>
                                                            </span>
                                                            <br>
                                                            <span>
                                                                <i class="mdi-action-alarm"></i> <?= $orderdetail['deliveryslot'] ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="recent-activity-list chat-out-list row">
                                                    <div class="col s12">
                                                        <h6 class="teal-text">Ordered Clothes</h6>
                                                    </div>
                                                </div>
                                                <?php foreach ($clothdetails as $v) { ?>
                                                    <div class="recent-activity-list chat-out-list row">
                                                        <div class="col s12 recent-activity-list-text">
                                                            <div class="col s5">
                                                                <span class="ultra-small">
                                                                    <i class="mdi-maps-local-laundry-service"></i> <?= $clothtypearr[$v->cloth_id] ?>
                                                                </span>
                                                            </div>
                                                            <div class="col s5">
                                                                <span class="green-text ultra-small"> <?= $servarr[$v->service_id] ?> </span>
                                                            </div>
                                                            <div class="col s2">
                                                                <span class="task-cat cyan ultra-small"> <?= $v->quantity ?> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="recent-activity-list chat-out-list row">
                                                    <div class="col s12">
                                                        <span class="secondary-content">Total Rs. <?= $orderdetail['totalprice'] ?>/-</span>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="collapsible-header cyan darken-1 white-text">
                                                <i class="mdi-maps-local-shipping"></i> Order History
                                            </div>
                                            <div class="collapsible-body favorite-associates">
                                                <div class="recent-activity-list chat-out-list row">
                                                    <div class="col s12	">
                                                        <h6 class="green-text">Details For <b><?= $orderdetail['order_id'] ?></b></h6>
                                                    </div>
                                                </div>

                                                <div class="favorite-associate-list chat-out-list row">
                                                    <div class="col s12">
                                                        <div class="tline-main">
                                                            <?php foreach ($statusdetails as $v) { ?>
                                                                <div class="tline-contain">
                                                                    <span class="tline-icon">
                                                                        <i class="mdi-action-grade"></i>
                                                                    </span>
                                                                    <div class="tline-content west">
                                                                        <?= $status[$v->order_status] ?> <br>
                                                                        <span class="ultra-small">
                                                                            <?= date("jS F, Y h:i a", strtotime($v->timestamp)); ?>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="collapsible-header teal darken-1 white-text">
                                                <i class="mdi-action-account-balance-wallet"></i> Payment History
                                            </div>
                                            <div class="collapsible-body favorite-associates">
                                                <div class="recent-activity-list chat-out-list row">
                                                    <h6 class="green-text">Payment For <b><?= $orderdetail['order_id'] ?></b></h6>
                                                </div>
                                                <div class="favorite-associate-list chat-out-list row">
                                                    <div class="tline-main">
                                                        <?php if (!empty($transorder)) {
                                                            foreach ($transorder as $vt) { ?>
                                                                <div class="tline-contain">
                                                                    <span class="tline-icon">
                                                                        <i class="mdi-action-grade"></i>
                                                                    </span>
                                                                    <div class="tline-content west">
                                                                            <?php echo "Rs " . $vt['amount'];
                                                                            if ($vt['for_whom'] == 1) {
                                                                                echo " Paid By cash";
                                                                            } else {
                                                                                echo " Paid From Wallet";
                                                                            } ?> <br>
                                                                        <span class="ultra-small">
        <?= date("jS F, Y h:i a", strtotime($vt['add_date'])); ?>
                                                                        </span>
                                                                    </div>
                                                                </div>
    <?php }
} else {
    echo "<div class='tline-content west'>No Payment done Yet </div>";
} ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>