<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Coupon Management</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Invoice</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end--> 
<div class="container invoice-container">
          <div id="invoice">
            <div class="invoice-header">
              <div class="row section">
                <div class="col s12 m4 l4">
                  <img src="http://superdhobi.in/assets/images/logo.png" class="logo" alt="company logo">
                </div>
                <div class="col s12 m4 l4">
                  <div class="invoce-company-address left-align">
                    <p><span class="strong"><i class="mdi-social-location-city cyan-text" style="font-size: 20px;
"></i> SuperDhobi,</span><br />
                      <span>125, ABC Street,</span><br />
                      <span>New Yourk, USA</span><br />
                      <span>+91-(444)-(333)-(221)</span>
                    </p>
                  </div>

                </div>                
             <div class="col s12 m4 l4">
                  <div class="invoce-company-address right-align">
                      <p>To,
                        <br/>
                        <span class="strong">Jonathan Doe</span>
                        <br/>
                        <span>125, ABC Street,</span>
                        <br/>
                        <span>New Yourk, USA</span>
                        <br/>
                        <span>+91-(444)-(333)-(221)</span>
                      </p>
                  </div>
                </div>                 


              </div>
            </div>

            <div class="invoice-lable">
              <div class="row">
                <div class="col s12 m4 l4 cyan">
                  <h4 class="white-text invoice-text">INVOICE</h4>
                </div>
                <div class="col s12 m8 l8 invoice-brief cyan white-text">
                  <div class="row">
                    <div class="col s12 m4 l4">
                      <p class="strong">Order Id</p>
                      <h4 class="header">SD2052016</h4>
                    </div>
                    <div class="col s12 m4 l4">
                      <p class="strong">Order Date</p>
                      <h4 class="header">00/00/00</h4>
                    </div>
                    <div class="col s12 m4 l4">
                      <p class="strong">Invoice Date</p>
                      <h4 class="header">00/00/00</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="invoice-table">
              <div class="row">
                <div class="col s12 m12 l12">
                  <table class="striped">
                    <thead>
                      <tr class="">
                        <th data-field="no">No</th>
                        <th data-field="item">Item</th>
                        <th data-field="uprice">Unit Price</th>
                        <th data-field="price">Unit</th>
                        <th data-field="price">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.</td>
                        <td>MacBook Pro</td>
                        <td>$ 1,299.00</td>
                        <td>2</td>
                        <td>$ 1,100.00</td>
                      </tr>
                      <tr>
                        <td>2.</td>
                        <td>iMAC</td>
                        <td>$ 1,099.00</td>
                        <td>2</td>
                        <td>$ 2,198.00</td>
                      </tr>
                      <tr>
                        <td>3.</td>
                        <td>iPhone</td>
                        <td>$ 299.00</td>
                        <td>5</td>
                        <td>$ 1,495.00</td>
                      </tr>
                      <tr>
                        <td>4.</td>
                        <td>iPad 3</td>
                        <td>$399.00</td>
                        <td>1</td>
                        <td>$ 399.00</td>  
                      </tr>
                      <tr>
                        <td>5.</td>
                        <td>iPod</td>
                        <td>$49.00</td>
                        <td>2</td>
                        <td>$ 98.00</td>
                      </tr>
                      <tr>
                        <td colspan="3"></td>
                        <td><p><b>Sub Total:</b></p></td>
                        <td><p><strong>$ 5,290.00</strong></p></td>
                      </tr>
                      <tr>
                        <td colspan="3"></td>
                        <td><p><b>Service Tax</b></p></td>
                        <td><p><strong>11.00%</strong></p></td>
                      </tr>
                      <tr>
                        <td colspan="3"></td>
                        <td class="cyan white-text">Grand Total</td>
                        <td class="cyan strong white-text">$ 5,871.90</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                
              </div>
            </div>
            
            <div class="invoice-footer">
              <div class="row">
                <div class="col s12 m6 l6">
                  <p class="strong">Payment Method</p>
                  <p>Please make the cheque to: AMANDA ORTON</p>
                  <p class="strong">Terms & Condition</p>
                  <ul>
                    <li>You know, being a test pilot isn't always the healthiest business in the world.</li>
                    <li>We predict too much for the next year and yet far too little for the next 10.</li>
                  </ul>
                  <p class="invoice-note">Note: This is computer generated invoice.</p>
                </div>
                <div class="col s12 m6 l6 ty-text">
					<h3>Thank<br /> You</h3>
                </div>
              </div>
            </div>
            
          </div>

        </div>    
    
    
    
</section>
