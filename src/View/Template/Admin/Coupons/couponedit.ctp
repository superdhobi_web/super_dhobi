<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Coupon Management</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Edit Coupon</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end--> 
    <div class="container">
    <p class="caption text-info">Edit Coupon</p>
        <div class="divider"></div>
        <div class="row">
		<form class="section" method="POST" action="<?= BASE_URL . "admin/coupons/addcoupon/$coupon->id" ?>" id="addcoupon" >
            <div class="col s12 m12">
                <div class="col s12 m6">
                  <div class="card-panel" style="padding-bottom: 35px;">
                    <h4 class="header2">Basic Info1</h4>
                    <div class="row">
                          <div class="input-field col s12">
                              <label for="coupon1">Coupon Name</label>
                            <input type="hidden" name="id" value="<?=$coupon->id?>">
                            <input id="coupon1" type="text" name="coupon_name" data-error=".errorTxt1" value="<?= $coupon->coupon_name?>">
                            <div class="errorTxt1"></div>
                          </div>
                          <div class="input-field col s12">
                            <input id="coupon2" type="text" name="coupon_code" value="<?= $coupon->coupon_code?>">
                            <label for="coupon2" data-error="Please enter coupon code." data-success="I Like it!">Coupon Code</label>
                          </div> 
              <div class="input-field col s12">
                            <label for="coupon3">How many times?</label>
                            <input id="coupon3" type="text" name="no_of_times" data-error=".errorTxt2" value="<?= $coupon->no_of_times?>">
                            <div class="errorTxt2"></div>
                          </div> 
                          <div class="input-field col s12">
                            <textarea id="coupon4" class="materialize-textarea" name="coupon_desc"><?= $coupon->coupon_desc?></textarea>
                            <label for="coupon4"  data-error="Please enter coupon description." data-success="I Like it!">Description</label>
                          </div>                          
                         <div class="input-field col s12">
                            <p for="" style="margin: 0 0 5px 0;">User Type</p>
                               <div class="col s4">
                                  <input type="radio" id="test1" name="customer_type" class="validate" value="1"  data-error=".errorTxt8" <?php if($coupon->customer_type==1){ echo "checked";}?>>
                                  <label for="test1">New User</label>
                            </div>
                               <div class="col s4">
                                  <input type="radio" id="test2" name="customer_type" value="2" class="validate" <?php if($coupon->customer_type==2){ echo "checked";}?>>
                                  <label for="test2">Existing User</label>
                            </div>
                               <div class="col s4">
                                  <input type="radio" id="test3" name="customer_type" value="3" class="validate" <?php if($coupon->customer_type==3){ echo "checked";}?>>
                                  <label for="test3">Both</label>
                            </div>
                            <div class="input-field" style="margin-top:55px;">
                                                <div class="errorTxt8"></div>
                                            </div>
                        </div>
                          
                      </div>
                    </div>
                  </div><!--col s12 m6-->
        <div class="col s12 m6">
                  <div class="card-panel" style="padding-bottom: 35px;">
                    <h4 class="header2">Basic Info2</h4>
                    <?php
                    $daytype=json_decode($coupon->weekday);
                    $wastype=json_decode($coupon->washtype);
                    ?>
                    <div class="row">
                         <div class="input-field col s12">
                            <p for="" style="margin: 0 0 1px 0;">Day</p>
                               <div class="col s3" style="padding-top:10px;">
                                    <input type="checkbox" id="test8" name="day[]" value="1" class="day" data-error=".errorTxt9" <?php if(in_array(1,$daytype)){ echo "checked";}?>>
                                    <label for="test8">Mon</label>
                                </div>
                               <div class="col s3" style="padding-top:10px;">
                                    <input type="checkbox" id="test9" name="day[]" value="2" class="day" data-error=".errorTxt9" <?php if(in_array(2,$daytype)){ echo "checked";}?>>
                                    <label for="test9">Tue</label>
                                </div>                                
                               <div class="col s3" style="padding-top:10px;">
                                    <input type="checkbox" id="test10" name="day[]" value="3" class="day" data-error=".errorTxt9" <?php if(in_array(3,$daytype)){ echo "checked";}?>>
                                    <label for="test10">Wed</label>
                                </div>                                
                               <div class="col s3" style="padding-top:10px;">
                                    <input type="checkbox" id="test4" name="day[]" value="4" class="day" data-error=".errorTxt9" <?php if(in_array(4,$daytype)){ echo "checked";}?>>
                                    <label for="test4">Thu</label>
                                </div>                                
                               <div class="col s3" style="padding-top:10px;">
                                    <input type="checkbox" id="test5" name="day[]" value="5" class="day" data-error=".errorTxt9" <?php if(in_array(5,$daytype)){ echo "checked";}?>>
                                    <label for="test5">Fri</label>
                                </div>                                
                               <div class="col s3" style="padding-top:10px;">
                                    <input type="checkbox" id="test6" name="day[]" value="6" class="day" data-error=".errorTxt9" <?php if(in_array(6,$daytype)){ echo "checked";}?>>
                                    <label for="test6">Sat</label>
                                </div>                                
                               <div class="col s3" style="padding-top:10px;">
                                    <input type="checkbox" id="test7" name="day[]" value="7" class="day" data-error=".errorTxt9" <?php if(in_array(7,$daytype)){ echo "checked";}?>>
                                    <label for="test7">Sun</label>
                                </div>
                            <div class="input-field" style="margin-top:100px;">
                                                <div class="errorTxt9"></div>
                                            </div>
                        </div>
                         <div class="input-field col s12" style="margin-top: 35px;">
                          <label for="coupon10" class="validate" data-error="Please enter coupon date." data-success="I Like it!">Start Date</label>
                            <input type="date" class="datepicker_from" id="coupon10" name="start_date" value="<?= $coupon->start_date?>">
                          </div>
                         <div class="input-field col s12" style="margin-top: 35px;">
                          <label for="coupon9" class="validate" data-error="Please enter coupon expired date." data-success="I Like it!">End Date</label>
                            <input type="text" class="datepicker" id="coupon9" name="end_date" value="<?= $coupon->end_date?>">
                          </div>
                         <div class="input-field col s12">
                            <p for="" style="margin: 0 0 1px 0;">Wash Type</p>
                                <?php if (!empty($wash)) {
                                foreach ($wash as $washes) {
                                ?>
                                <div class="col s6" style="padding-top:10px;">
                                  <input type="checkbox" id="wash<?= $washes->id; ?>" name="wtype[]" value="<?= $washes->id; ?>" <?php if(in_array($washes->id,$wastype)){ echo "checked";}?> class="wastype" data-error=".errorTxt10">
                                    <label for="wash<?= $washes->id; ?>" class="validate" data-error="Please choose wash type." data-success="I Like it!"><?= $washes->sname; ?></label>
                                 </div> 
                                <?php } }?> 
                            <div class="input-field" style="margin-top:100px;">
                                                <div class="errorTxt10"></div>
                                            </div>
                        </div>                         
                          
                      </div>
                    </div>
                  </div><!--col s12 m6--> 
          </div><!--col s12 m12--> 
          <div class="col s12 m12">
        <div class="col s12 m6">
                  <div class="card-panel" style="padding-bottom: 35px;">
                    <h4 class="header2">Basic Info3</h4>
                    <div class="row">
                          <div class="input-field col s12">
                            <input id="coupon5" type="text" name="min_apply_amt" class="validate" value="<?= $coupon->min_apply_amt?>">
                            <label for="coupon5" data-error="Please enter min Billing amt.">Min. Billing Amount</label>
                          </div>                           
                          <div class="input-field col s12">
                            <input id="coupon6" type="text" name="discount" class="validate" value="<?= $coupon->discount?>" pattern="\d+(\.\d{1,2})?">
                            <label for="coupon6"  data-error="Please enter the discount amount.">Discount</label>
                          </div>              
                          <div class="input-field col s12">
                            <input id="coupon7" type="text" name="min_discount" class="validate" value="<?= $coupon->min_discount?>">
                            <label for="coupon7" data-error="Please enter min discounted amt.">Min. Discount(&#8377;)</label>
                          </div> 
               <div class="input-field col s12">
                            <input id="coupon8" type="text" name="max_discount" class="validate" value="<?= $coupon->max_discount?>">
                            <label for="coupon8"  data-error="Please enter max discounted amt.">Max. Discount(&#8377;)</label>
                          </div> 
                               
                      </div>
                    </div>
                  </div><!--col s12 m6-->
      <div class="col s12 m6">
                  <div class="card-panel" style="padding-bottom: 35px;">
                    <h4 class="header2">Basic Info4</h4>
                    <div class="row">
          <div class="file-field col s10 input-field">
                      <div class="btn cyan darken-2">
                        <span>browse</span>
                        <input type="file">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path" id="webimage" type="text" placeholder="Upload Image for Web" name="image_web">
                       
                      </div>
                    </div> 
				<div class="file-field col s2 input-field">
					 <img src="<?= $this->request->webroot;?>thumbimg/<?= $coupon->image_web ?>" class="img-responsive" style="    height: 45px;"></img>
				</div> 
          <div class="file-field col s10 input-field">
                      <div class="btn cyan darken-2">
                        <span>browse</span>
                        <input type="file">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path" id="mobimage" type="text" placeholder="Upload Image for Android" name="image_mob">
                        
                      </div>
                    </div> 
				<div class="file-field col s2 input-field">
					<img src="<?= $this->request->webroot; ?>thumbimg/<?= $coupon->image_mob ?>" class="img-responsive" style="    height: 45px;"></img>
				</div>             
                        </div>
                      </div>
      <div class="col s12 m12">
        <div class="row">
                  <div class="card-panel cyan" style="padding-bottom: 35px;">
                    <div class="row">
            <div class="input-field col s12 center">
                              <button class="btn white pink-text waves-effect waves-light" type="submit">Proceed
                                <i class="mdi-content-send right"></i>
                              </button>
                        </div>
        </div>
         </div>
       </div>
      </div>          
          
                  </div><!--col s12 m6-->
         </div><!--col s12 m12--> 
            </div>
       </form>
        </div><!--row-->
     </div><!--container-->
</section><!--content-->