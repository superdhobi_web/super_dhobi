<section id="content">        
  <!--breadcrumbs start-->
  <div id="breadcrumbs-wrapper">
    <!-- Search for small screen -->
    <div class="header-search-wrapper grey hide-on-large-only">
      <i class="mdi-action-search active"></i>
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
    </div>
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Coupon Management</h5>
          <ol class="breadcrumbs">
            <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
            <li class="active">Coupon List</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--breadcrumbs end--> 
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="card">
          <div class="card-content filter" style="padding: 8px 15px;">
            <div class="row">
              <div class="col m12 s12">
                <h6 class="left teal-text">
                  Filter
                </h6>  
                <div class="col m12">
                 <div class="row">
                  <form method="GET" action="<?= BASE_URL?>admin/coupons/" id="addcoupon">
                    <div class="col m5">
                     <div class="input-field col m4 s4">
                      <input type="hidden" id="id" name="id">
                      <input type="radio" id="filter1" name="customer_type" value="1" <?php if(!empty($this->request->query['customer_type'])==1){ echo $this->request->query('customer_type'); }?>>
                      <label for="filter1">New User</label>
                    </div>
                    <div class="input-field col m5 s4">
                      <input type="radio" id="filter2" name="customer_type" value="2" <?php if(!empty($this->request->query['customer_type'])==2){ echo $this->request->query('customer_type'); } ?>>
                      <label for="filter2">Existing User</label>
                    </div>	
                    <div class="input-field col m3 s4">
                      <input type="radio" id="filter3" name="customer_type" value="3" <?php if(!empty($this->request->query['customer_type'])==3){ echo $this->request->query('customer_type'); } ?>>
                      <label for="filter3">Both</label>
                    </div>	
                  </div>											
                  <div class="input-field col m2 s4">
                    <input type="checkbox" id="expirecoupon" name="expiredcoupon" value="1" <?php if($this->request->query("expiredcoupon")){ echo "checked" ;}?>>
                    <label for="expirecoupon">Expired Coupon</label>
                  </div>	
                  <div class="input-field col m2 s6">
                   <input id="search" type="text" class="validate" onkeyup="return getAutolist();" name="coupondata" value="<?php if ($this->request->query("coupondata")) { echo $this->request->query("coupondata");}?>">
                   <label for="search" class="">Search By...</label>
                 </div>	
                 <div class="col m1 s3">
                  <br>
                  <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit" value="search">
                    <i class="mdi-content-forward"></i>
                  </button>
                </div>																				
                <div class="col m2 s3">
                  <br>
                  <button  class="btn waves-effect waves-light grey" type="button" ><a href="<?= BASE_URL."admin/coupons"?>" class="white-text waves-effect waves-cyan">Clear
                  </a>
                </button>
              </div>	
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<div class="row">
  <div class="col s12 m12 coupon-tbl-list">
    <div class="card" style="overflow:auto">
      <div class="card-content">
        <span class="card-title activator grey-text text-darken-4">
          <span class="left">
            Coupon List
          </span>
        </span>                                    
      </div>
      <div class="col s12 m12 table-responsive">
        <table>
          <thead>
            <tr class="white-text">
              <th class="teal">Action</th>
              <th class="teal"><?= $this->Paginator->sort('coupon_code', ['label' => 'Coupon Code']); ?></th>
              <th class="teal"><?= $this->Paginator->sort('min_apply_amt', ['label' => 'Min Billing Amount']); ?></th>
              <th class="teal"><?= $this->Paginator->sort('discount', ['label' => 'Discount']); ?></th>
              <th class="teal"><?= $this->Paginator->sort('start_date', ['label' => 'Start Date']); ?></th>
              <th class="teal"><?= $this->Paginator->sort('end_date', ['label' => 'End Date']); ?></th>
              <th class="teal"><?= $this->Paginator->sort('status', ['label' => false]); ?>Status</th>
            </tr>
          </thead>

          <tbody>

            <?php
            if(count($coupons)==0) {
              echo "<tr><td>No Coupon found..</td></tr>";
            }else{
              foreach ($coupons as $couponinfo):
              $id=$couponinfo['id']; ?>
              <tr class="<?php if(date("Y-m-d") > date("Y-m-d",strtotime($couponinfo->end_date))){echo "red";}else{echo "indigo";}?> lighten-5" >
                <td>
                  <a class="btn-floating btn-small  green waves-effect waves-light " href="javascript:void(0)" title="View" onclick="viewmodal(<?=$id?>);">
                    <i class=" mdi-action-exit-to-app"></i>
                  </a>

                  <?=$this->Html->link("<i class='mdi-content-create'></i>", ['action' => 'couponedit', $couponinfo->id],
                  ['escape' => false, 'title' => 'Edit', 'class' => 'btn-floating btn-small  orange waves-effect waves-light']);?>
                  <?=$this->Form->postLink("<i class='mdi-action-delete'> </i>",['action' => 'deletecoupon', $couponinfo->id],
                  ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete Coupon Code {0}?', $couponinfo->coupon_code)]);?>           
                </td>
                <td>
                  <p><?= h($couponinfo->coupon_code) ;?>
                  </p>
                </td>
                <td>
                  <p>₹<?= h($couponinfo->min_apply_amt) ;?></p>
                </td>
                <td>
                  <p><?= h($couponinfo->discount) ;?> %</p>
                </td>
                <td>
                  <label><?= date('jS F Y ', strtotime(h($couponinfo->start_date))) ;?></label>
                </td>
                <td>
                  <label><?= date('jS F Y ', strtotime(h($couponinfo->end_date))) ;?></label>
                </td>
                <td><div class="switch">
                  <label>
                   <?php $controller=$this->request->params['controller'];?>
                   <input type="checkbox" <?php if($couponinfo->status==1){ echo "Checked";}?> value="<?php if($couponinfo->status==1){ echo 1; } else echo 2;?>"class="Addcheckbox" id="defaultAdd<?=  $couponinfo->id; ?>" name='status' onchange="changeStatus(this.value,<?= $couponinfo->id; ?>,'<?= $controller; ?>');"/>
                   <span class="lever"></span>
                 </label>
               </div></td>
             </tr> 

           <?php endforeach;
         }  ?>
       </tbody>
     </table>
     <div class="paginator">
      <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
      </ul>
      <p><?= $this->Paginator->counter() ?></p>
    </div>
  </div>
</div>
</div>
</div>
</div>
</section>
<div id="couponDetals" class="modal modal-fixed-footer proceed">
  
              
</div>
