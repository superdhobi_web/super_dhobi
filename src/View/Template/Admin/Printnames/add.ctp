<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Printnames'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="printnames form large-9 medium-8 columns content">
    <?= $this->Form->create($printname) ?>
    <fieldset>
        <legend><?= __('Add Printname') ?></legend>
        <?php
            echo $this->Form->input('pname');
            echo $this->Form->input('created_date');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
