<?= $this->Html->script('ckeditor/ckeditor');?>
<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage FAQ Listing</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage FAQ Listing</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    FAQ Lists
                                </span>
                                <i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Add New FAQ"></i>
                            </span>  
                            <div class="row">
                                <div class="col s12 m12 l12">
                                    <table class="striped responsive-table centered">
                                        <thead>
                                            <tr class="white-text">
                                                <th class="teal">Actions</th>
                                                <th class="teal"><?= $this->Paginator->sort('cat_id', ['label' => false]); ?>Category</th>
                                                <th class="teal"><?= $this->Paginator->sort('qn', ['label' => false]); ?>Question</th>
                                                <th class="teal"><?= $this->Paginator->sort('ans', ['label' => false]); ?>Answer</th>
                                                <th class="teal"><?= $this->Paginator->sort('createddate', ['label' => false]); ?>Created Date</th>
                                                <th class="teal"><?= $this->Paginator->sort('status', ['label' => false]); ?>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($faqlistings as $faqlisting): ?>
                                                <tr>
                                                    <td class="actions">
                                                        <?=
                                                        $this->Html->link("<i class='mdi-content-create'></i>", ['action' => 'edit', $faqlisting->id],
                                                        ['escape' => false, 'title' => 'Edit','class' => 'btn-floating  btn-small green waves-effect waves-light']
                                                        );
                                                        ?>
                                                        <?=
                                                        $this->Form->postLink(
                                                        "<i class='mdi-action-delete'> </i>",
                                                        ['action' => 'delete', $faqlisting->id],
                                                        ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete # {0}?', $faqlisting->id)] // third
                                                        );
                                                        ?>
                                                    </td>
                                                     <td>
                                                <?php
                                                $category=$faqlisting->cat_id;
                                                foreach($faqcategory as $faqcats){
                                                    if($category==$faqcats->id)
                                                    echo $faqcats->name;
                                                }
?>
                                               </td>
                                                    <td><?= $faqlisting->qn;?></td>
                                                    <td><?= $faqlisting->ans;?></td>
                                                    <td><?= h(date('jS F Y / h:i A', strtotime(($faqlisting->createddate)))); ?></td>
                                                    <td><div class="switch">
                                                    <label>
                                                        <?php $controller=$this->request->params['controller'];?>
                                                        <input type="checkbox" <?php if($faqlisting->status==1){ echo "Checked";}?> value="<?php if($faqlisting->status==1){ echo 1; } else echo 2;?>"class="Addcheckbox" id="defaultAdd<?=  $faqlisting->id; ?>" name='add' onchange="changeStatus(this.value,<?= $faqlisting->id; ?>,'<?= $controller; ?>');"/>
                                                        <span class="lever"></span>
                                                    </label>
                                                </div></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('next') . ' >') ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>                                  
                        </div>                        
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">
                                <span class="green-text">Add New FAQ Details</span>
                                <i class="mdi-navigation-close right"></i>
                            </span>
                            <div class="row">
                                <form class="col s12 m8 offset-m2 right-alert" id="addfaqlist" action="<?= ADMIN_URL . "faqlistings/add" ?>" method="post">
                                        <div class="input-field col s12">
                                            <select name="cat_id">
                                                <option value="" selected>Category Name</option>
                                                <?php 
                                                foreach ($faqcategory as $faqcat) {
                                                    ?>
                                                <option value="<?= $faqcat->id?>"><?= $faqcat->name;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <textarea name="qn" id="first_input2" class=" materialize-textarea"></textarea>
                                            <label for="first_input2" data-error="Please enter Category name." data-success="Perfect!">Question</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <textarea name="ans" id="first_input3" class="validate materialize-textarea"></textarea>
                                            <label for="first_input3" data-error="Please enter Category name." data-success="Perfect!">Answer</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <button class="btn green waves-effect waves-light right" type="submit" name="action" id="usersubmit" onclick="uservalidate();">Save
                                                <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
