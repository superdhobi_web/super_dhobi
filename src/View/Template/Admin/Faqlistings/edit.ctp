<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Edit FAQ Listings</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Edit FAQ Listings</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">               
                            <span class="card-title grey-text text-darken-4 left">
                                <span class="green-text">Edit FAQ Details</span>
                            </span>
                            <div class="row">
                                <div class="col s12 m8 offset-m2">
                                    <form class="right-alert" id="addfaqlist" action="<?= ADMIN_URL ."faqlistings/add/$faqlisting->id"; ?>" method="post">
                                        <input type="hidden" name="id" value="<?= $faqlisting->id; ?>"/>
                                        <div class="input-field col s12">
                                            <select name="cat_id">
                                                <?php foreach ($faqcategory as $faqcat) {
                                                    ?>
                                                <option value="<?= $faqcat->id?>" <?php if($faqlisting->cat_id == $faqcat->id){echo "selected";}?>><?= $faqcat->name;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="input-field col s12">
                                            <textarea name="qn" class="validate materialize-textarea" id="first_input2"><?= $faqlisting->qn;?></textarea>
                                            <label for="first_input2" data-error="Please enter Category name." data-success="Perfect!">Question</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <textarea name="ans" class="validate materialize-textarea" id="first_input3"><?= $faqlisting->ans;?></textarea>
                                            <label for="first_input3" data-error="Please enter Category name." data-success="Perfect!">Answer</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <button class="btn green waves-effect waves-light right" name="action">   UPDATE
                                                <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>