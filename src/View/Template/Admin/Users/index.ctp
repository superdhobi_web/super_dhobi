<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Users</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Users</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end--> 
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content filter">
                            <div class="row">
                                <div class="col m5 s12">
                                    <h6 class="left teal-text">
                                        Search User
                                    </h6>  
                                    <div class="col m12">
                                        <form action="" method="get">
                                            <div class="col m5">
                                                <div class="input-field">
                                                    <select name="userCondition" id="userCondition" class="filter-select browser-default">
                                                        <option value="all" disabled selected>Search By...</option>
                                                        <option value="userid" <?php
                                                        if ($this->request->query("userCondition") == "userid") {
                                                            echo "selected";
                                                        }
                                                        ?>>Customer ID</option>
                                                        <option value="name" <?php
                                                        if ($this->request->query("userCondition") == "name") {
                                                            echo "selected";
                                                        }
                                                        ?>>Customer Name</option>
                                                        <option value="mobile" <?php
                                                        if ($this->request->query("userCondition") == "mobile") {
                                                            echo "selected";
                                                        }
                                                        ?>>Mobile No</option>
                                                        <option value="email" <?php
                                                        if ($this->request->query("userCondition") == "email") {
                                                            echo "selected";
                                                        }
                                                        ?>>Email ID</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col m5">
                                                <div class="input-field">
                                                    <input id="getUser_src" name="userData" onkeyup="return getAutolist();" type="text" value="<?php
                                                    if ($this->request->query("userData")) {
                                                        echo $this->request->query("userData");
                                                    }
                                                    ?>" class="validate">
                                                    <label for="getUser_src" class="">Enter Data...</label>
                                                </div>
                                            </div>
                                            <div class="col m2">
                                                <br>
                                                <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit">
                                                    <i class="mdi-content-forward"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>                            
                                </div>
                                <div class="col m7 s12 border-l">
                                    <h6 class="left teal-text">
                                        Search By Location Details
                                    </h6> 
                                    <div class="row">
                                        <div class="col m12">
                                            <form action="" method="get">
                                                <div class="col m2x">
                                                    <select name="city" onchange="getChild(1,this.value);" class="filter-select browser-default">
                                                        <option value="">City...</option>
                                                        <?php foreach ($city as $k => $val) { ?>
                                                        <option value="<?= $val->id; ?>" <?php
                                                            if ($this->request->query("city") == $val->id) {
                                                                echo "selected";
                                                            }
                                                            ?>><?= $val->location_name; ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col m2x">
                                                    <select name="circle" class="filter-select browser-default" id="get_1" onchange="getChild(2,this.value);">
                                                        <option value="">Circle..</option>
                                                        <?php
                                                        if (!empty($child)) {
                                                            foreach ($child['circle'] as $key => $val) {
                                                                ?>
                                                                <option value="<?php echo $key; ?>" <?php
                                                                    if ($this->request->query("circle") == $key) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $val; ?> </option> 
                                                                    <?php }
                                                                } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col m2x">
                                                            <select name="location" class="filter-select browser-default" id="get_2" onchange="getChild(3,this.value);">
                                                                <option value="">Location..</option>
                                                                <?php
                                                                if (!empty($child)) {
                                                                    foreach ($child['location'] as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>" <?php
                                                                            if ($this->request->query("location") == $key) {
                                                                                echo "selected";
                                                                            }
                                                                            ?>><?php echo $val; ?> </option> 
                                                                            <?php }
                                                                        } ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col m2x">
                                                                    <select name="area" class="filter-select browser-default" id="get_3">
                                                                        <option value="">Area..</option>
                                                                        <?php
                                                                        if (!empty($child)) {
                                                                            foreach ($child['area'] as $key => $val) {
                                                                                ?>
                                                                                <option value="<?php echo $key; ?>" <?php
                                                                                    if ($this->request->query("area") == $key) {
                                                                                        echo "selected";
                                                                                    }
                                                                                    ?>><?php echo $val; ?> </option> 
                                                                                    <?php }
                                                                                } ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col m1">
                                                                            <br>
                                                                            <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit">
                                                                                <i class="mdi-content-forward"></i>
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                    <div class="col m1">
                                                                        <br>
                                                                        <a href="<?=BASE_URL?>admin/users/exportdata" class="btn-floating waves-effect waves-light blue"> 
                                                                            <i class="mdi-maps-directions"></i>          
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 m12 l12">
                                            <div class="card" style="overflow:auto; min-height:515px;">
                                                <div class="card-content">
                                                    <span class="card-title activator grey-text text-darken-4">
                                                        <span class="left">
                                                            User Details
                                                        </span>
                                                        <i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Add User Details"></i>
                                                    </span>                                    
                                                </div>
                                                <div class="col s12 m12 l12">

                                                    <table class="striped responsive-table">
                                                        <thead>
                                                            <tr class="white-text">
                                                                <th class="teal">Action</th>
                                                                <th class="teal"><?= $this->Paginator->sort('userid', ['label' => 'User Id']); ?></th>
                                                                <th class="teal">User Details</th>
                                                                <th class="teal">Address</th>                                        
                                                                <th class="teal"><?= $this->Paginator->sort('wallet', ['label' => 'Wallet']); ?></th>
                                                                <th class="teal">Status</th>                                          
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($users as $user): ?>
                                                                <tr>
                                                                    <td>   
                                                                        <a class="btn-floating btn-small  green waves-effect waves-light " href="<?= ADMIN_URL . "users/edit/" . $user->id ?>">
                                                                            <i class="mdi-content-create"></i>
                                                                        </a>
                                                                        <a class="btn-floating btn-small blue waves-effect waves-light " href="#" title="Add Wallet" onclick="editwallet('<?=$user->userid?>');">
                                                                            <i class="mdi-action-account-balance-wallet"></i>
                                                                        </a>
                                                                        <?php
                                                                        echo $this->Form->postLink(
                                                                            "<i class='mdi-action-delete'> </i>",
                                                                            ['action' => 'delete', $user->id],
                                                ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete User {0}?', $user->name)] // third
                                                );
                                                ?>
                                            </td>
                                            <td><a href="#" onclick="getuserorderdetails('<?= $user->userid ?>');" ><?= h($user->userid); ?></a></td>
                                            <td class="">                                                
                                                <span class="teal-text text-darken-3">
                                                    <i class="mdi-social-person"></i> <b><?= h($user->name); ?></b>
                                                </span> <br>
                                                <span class="green-text text-darken-1">
                                                    <i class="mdi-hardware-smartphone"></i> <?= h($user->mobile); ?> 
                                                </span><br>
                                                <span class="blue-text text-darken-1">
                                                    <i class="mdi-communication-email"></i> <?= h($user->email); ?>               
                                                </span><br>
                                                <span class="green-text text-darken-1">
                                                 <i class="mdi-action-account-balance-wallet"></i> <?= h($user->ref_code); ?>               
                                             </span>
                                         </td>
                                         <td>
                                            <a href="<?= ADMIN_URL; ?>users/address/<?= base64_encode($user->userid); ?>/<?= base64_encode($user->name);?>" class="wa$userves-effect waves-blue block grey-text text-darken-1 modal-trigger" href="#modal5">                                   
                                                <?php
                                                if ($user->address) {
                                                    ?>
                                                    <span class="green-text text-darken-1">
                                                        <i class="mdi-maps-pin-drop"></i>
                                                        <b><?= h($user->address); ?></b>
                                                    </span> <br>
                                                    <?= h($user->area) . ","; ?> <?= h($user->circle) . ","; ?><?= h($user->location) . ","; ?><?= h($user->city); ?>
                                                    <?php
                                                } else {
                                                    echo "<span class='green-text'><i class='mdi-maps-pin-drop'></i><b>Enter Address</b></span>";
                                                }
                                                ?>

                                            </a>
                                        </td>
                                        <td>
                                            <span id="wall<?=$user->userid?>" style="display:block;">
                                                <a href="#" onclick="getwalletdetails('<?= $user->userid ?>');">
                                                    Rs.<?= h($user->wallet); ?>
                                                </a>
                                            </span>
                                            <span style="display:none;" id="editwal<?=$user->userid?>"><input type="text" value="" onblur="savewallet('<?=$user->userid?>',this.value,<?=$user->wallet?>);"></span>
                                        </td>
                                        <td>
                                            <div class="switch">
                                                <label>
                                                    <?php $controller=$this->request->params['controller'];?>
                                                    <input type="checkbox" <?php if($user->status==1){ echo "Checked";}?> value="<?php if($user->status==1){ echo 1; } else echo 2;?>"class="Addcheckbox" id="defaultAdd<?=  $user->id; ?>" name='add' onchange="changeStatus(this.value,<?= $user->id; ?>,'<?= $controller; ?>');"/>
                                                    <span class="lever"></span>
                                                </label>
                                            </div>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                    <?= $this->element('adduser'); ?>
                </div>
            </div>
        </div>
    </div>
    <!--card stats end-->

    <!-- Floating Action Button -->
    <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
        <a class="btn-floating btn-large">
            <i class="mdi-content-create"></i>
        </a>
        <ul>
            <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
            <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
            <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
            <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
        </ul>
    </div>
    <!-- Floating Action Button -->
</div>
</section>
<!-- END CONTENT -->
<!-- START RIGHT SIDEBAR NAV-->
<aside id="right-sidebar-nav">
    <ul id="chat-out" class="side-nav rightside-navigation">
        <!--       data put here after fetch in ajax-->
    </ul>
    <!-- <ul id="wallet-out" class="side-nav rightside-navigation">
        <li class="li-hover">
            <ul class="chat-collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header amber darken-2 white-text active"><i class="mdi-social-whatshot"></i>Wallet History</div>
                    <div class="collapsible-body recent-activity">
                        <ul class="collection">
                          <li class="collection-item avatar">
                            <i class="mdi-action-exit-to-app circle green"></i>
                            <span class="title">Lorem Ipsum</span>
                            <br>
                            <span class="ultra-small">Doler Sit</span>
                            <span class="teal-text secondary-content">+5.00/- </span>                            
                          </li>
                          <li class="collection-item avatar">
                            <i class="mdi-action-exit-to-app circle red deb"></i>
                            <span class="title">Lorem Ipsum</span>
                            <br>
                            <span class="ultra-small">Doler Sit</span>
                            <span class="red-text secondary-content">-3.00/-</span>
                          </li>
                        </ul>
                </li>
            </ul>
        </li>
    </ul> -->
</aside>
<!--END RIGHT SIDEBAR NAV-->