<div class="container">
    <div class="row">
        <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Manage Users</h5>
            <ol class="breadcrumbs">
                <li><a href="<?= BASE_URL . "admin/Dashboard" ?>">Dashboard</a></li>
                <li class="active">Manage Users</li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <!--card stats start-->
    <div id="card-stats">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card">
                    <span class="card-title grey-text text-darken-4">
                        <span class="green-text">Edit User Details</span>
                    </span>
                    <div class="row">
                        <form class="col s12 m8 offset-m2 right-alert" id="userdetail" action="<?= ADMIN_URL . "users/edit/$user->id"; ?>" method="post">
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-action-account-circle prefix"></i>
                                    <input id="first_input2" class="validate" type="text" value="<?= $user->name;?>" name="name"/>
                                    <label for="first_input2" data-error="Please enter name." data-success="Perfect!">Full Name</label>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-action-account-circle prefix"></i>
                                    <input disabled id="user_input2" class="validate" value="<?= $user->username;?>" type="text" value="" readonly name="username" placeholder="Your Username..."/>
                                    <label for="user_input2" data-error="" data-success="Perfect!" class="active">Username</label>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-communication-email prefix"></i>
                                    <input id="email_input2" class="validate" type="email" value="<?= $user->email;?>" name="email" onblur="uniquedetails(1,'<?=$user->id?>')"/>
                                    <label for="email_input2" class="" data-error="Please enter valid email." data-success="I Like it!">Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-communication-email prefix"></i>
                                    <input id="mobile_input2" class="validate" type="number" value="<?= $user->mobile;?>" name="mobile" onblur="uniquedetails(2,'<?=$user->id?>')"/>
                                    <label for="mobile_input2" class="" data-error="Please enter valid mobile." data-success="I Like it!">Mobile</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-communication-email prefix"></i>
                                    <input id="ref_by" class="validate" type="text" value="<?= $user->ref_by;?>" name="ref_by"/>
                                    <label for="ref_by" class="" data-error="Please enter Reference Code." data-success="I Like it!">Refer By</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn green waves-effect waves-light right" type="submit" name="action" id="usersubmit">Save
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>