<?php
$this->Mpdf->init();
$thebody = $this->$response->body($userinfo);
$this->Mpdf->WriteHTML($thebody);
$thefilename = "mail_merge_".date("d.m.Y_His").".pdf";
$this->Mpdf->Output(WWW_ROOT. 'files/pdf' .$thefilename, 'F');
$thefilenametxt = "mail_merge_".date("d.m.Y_His").".txt";
    $ourFileHandle = fopen(WWW_ROOT. 'files/pdf' .$thefilename, 'w');
    fwrite($ourFileHandle, $thebody); 
    fclose($ourFileHandle); 

    return $thefilename;
?>
<table class="striped responsive-table">
                                <thead>
                                    <tr class="white-text">
                                        <th class="teal">Action</th>
                                        <th class="teal">User Id</th>
                                        <th class="teal">User Details</th>
                                        <th class="teal">Address</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
<?php foreach ($userinfo as $user): ?>
                                        <tr>
                                            <td><?= h($user->userid); ?></td>
                                            <td class="">                                                
                                                <span class="teal-text text-darken-3">
                                                    <i class="mdi-social-person"></i> <b><?= h($user->name); ?></b>
                                                </span> <br>
                                                <span class="green-text text-darken-1">
                                                    <i class="mdi-hardware-smartphone"></i> <?= h($user->mobile); ?> 
                                                </span><br>
                                                <span class="blue-text text-darken-1">
                                                    <i class="mdi-communication-email"></i> <?= h($user->email); ?>               
                                                </span>
                                            </td>
                                            <td>Rs.<?= h($user->wallet); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>