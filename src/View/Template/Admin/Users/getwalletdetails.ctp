<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<li class="li-hover">
    <a href="#" onclick="closeview();" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
</li>
<li class="li-hover">
    <ul class="chat-collapsible" data-collapsible="expandable">
        <li>
            <div class="collapsible-header orange white-text active"><i class="mdi-social-whatshot"></i>Wallet Transaction</div>
            <div class="collapsible-body recent-activity">
                <ul class="collection">
                    <?php foreach($transorder as $va){?>
                    <li class="collection-item avatar">
                        <i class="mdi-action-exit-to-app circle <?php if($va['type']==2){?>green<?php }else{ echo "red deb";}?>"></i>
                        <span class="title"><?=$va['remark'];?></span>
                        <br>
                        <span class="ultra-small"> <?=date("jS F, Y h:i a",strtotime($va['add_date']));?></span>
                        <br>
                        <span class="teal task-cat">
                            <?php if($va['type']==2){?>+<?php }else{ echo "-";}?><?=$va['amount']?>/- 
                        </span>                                                 
                    </li>
                    <?php } ?>
                </ul>
            </li>
        </ul>
    </li>