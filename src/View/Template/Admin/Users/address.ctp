    <section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Address</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Address</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div id="">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content filter">
                            <div class="row">                                     
                                <div class="col s12 m6 l6">                                        
                                    <div class="row">
                                        <div class="col s12">
                                            <h6 class="left teal-text">
                                                <?= base64_decode($this->request->params['pass'][1]);?> (UID-<?= base64_decode($this->request->params['pass'][0]);?>)'s Address Details
                                            </h6> 
                                        </div>
                                        <div class="address-scroll">

                                            <?php
                                            if (!empty($address)) {
                                                foreach ($address as $k => $value) {
                                                   // pr($value);//exit;
                                                    ?>
                                                    <div onclick="editAddress(<?= $value->id; ?>);" id="Add<?= $value->id; ?>">
                                                        <div class="col s12 m6 l6">
                                                            <div class="card">

                                                                <div class="card-content <?php if($value->set_default == "1"){echo "cyan";}else{echo "grey";}?> white-text add_div<?= $value->id; ?>" id="add_div<?= $value->id; ?>">
                                                                    <div class="adrs-actions">                                                                        
                                                                        <a href="javascript:void(0);" onclick="editAddress(<?= $value->id; ?>);" class="edt-adrs">
                                                                            <i class="mdi-image-edit"></i>
                                                                        </a>
                                                                        <a href="javascript:void(0);" class="del-adrs" onclick="addressDelete(<?= $value->id; ?>);">
                                                                            <i class="mdi-content-clear"></i>
                                                                        </a>
                                                                    </div>                                                                    
                                                                    <p class="card-stats-title">
                                                                        <i class="mdi-maps-pin-drop"></i><?= $value->add_heading; ?>
                                                                    </p>
                                                                    <p class="ultra-small">
                                                                        <?php echo $value->address, $value->landmark, $value->area; ?><br>
                                                                        <?php echo $value->location, ",", $value->circle, ",", $value->city; ?>
                                                                        <br>
                                                                    </p>
                                                                </div>
                                                                <div class='card-action  <?php if($value->set_default == "1"){echo "cyan";}else{echo "grey";}?> darken-2 add_div<?= $value->id; ?>' id="add_div<?= $value->id; ?>">
                                                                    <div class="switch">
                                                                        <span class="white-text">
                                                                            Set As Default
                                                                        </span> 
                                                                        <label>
                                                                            <input type="checkbox" class="Addcheckbox" id="defaultAdd<?= $value->id; ?>" name='add' onchange="setdefault(<?= $value->id; ?>,'<?= $value->user_id; ?>');" <?php if($value->set_default == "1"){echo "checked";}?>/>
                                                                            <span class="lever"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                        <?php }
                                                    } ?>
                                                    <input type="hidden" value="<?php if(!empty($value)){echo $value->user_id;} ?>" id="user_id">
                                                </div>
                                            </div>
                                        </div>                                    
                                        <div class="col s12 m6 l6">
                                            <h6 class="left teal-text">
                                                Add New Address Details
                                            </h6> 
                                            <div class="teal-text" id="ajaxLoding" style="display:none">Loding...</div>
                                            <form action="" method="post" id="add_address" class="right-alert">
                                                <input type="hidden" value="" name="id" id="address_id"/>
                                                <div class="input-field col s12 m12">
                                                    <input id="add_heading" type="text" required class="validate" name="add_heading"/>
                                                    <label for="add_heading" data-error="Please enter Address Heading" data-success="Perfect!">Add Heading</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <select name="city" id="city" required class="browser-default" onchange="getChild(1,this.value);" >
                                                        <option value="">City</option>
                                                        <?php foreach ($citylist as $k => $val) { ?>
                                                        <option value="<?= $val->id; ?>"><?= $val->location_name;
                                                        } ?>
                                                    </select>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <select name="circle" class="browser-default" required id="get_1" onchange="getChild(2,this.value);">
                                                        <option value="" disabled selected>Circle</option>
                                                    </select>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <select name="location" class="browser-default" required id="get_2" onchange="getChild(3,this.value);">
                                                        <option value="" disabled selected>Location</option>
                                                    </select>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <select name="area" class="browser-default" required id="get_3">
                                                        <option value="" disabled selected>Area</option>
                                                    </select>
                                                </div>
                                                <div class="input-field col s12 m12">
                                                    <textarea id="address" required name="address" class="materialize-textarea" placeholder="Address"></textarea>
<!--                                                    <label for="address" class="" data-error="Please enter Full Address here." data-success="Perfect!">Address</label>                           -->
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <input id="landmark" required name="landmark" type="text" class="validate">
                                                    <label for="landmark" class="" data-error="Please enter Nearby Landmark." data-success="Perfect!">Landmark</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <input id="pincode" name="pincode" required type="number" class="validate">
                                                    <label for="pincode" class="" data-error="Please enter the area pincode." data-success="Perfect!">Pincode</label>
                                                </div>
                                                <div class="input-field col s12 m12 right-align">
                                                    <button class="btn waves-effect waves-light grey" type="reset" name="action" onclick="return clearForm();">Reset</button>
                                                    <button class="btn waves-effect waves-light cyan" id="usrAddsave" type="submit" name="action" onclick="validateaddress();">save</button>                                                    
                                                </div>
                                            </form>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<script>
    
</script>