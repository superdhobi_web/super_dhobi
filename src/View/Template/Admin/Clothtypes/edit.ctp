<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $clothtype->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $clothtype->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Clothtype'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="clothtype form large-9 medium-8 columns content">
    <?= $this->Form->create($clothtype) ?>
    <fieldset>
        <legend><?= __('Edit Clothtype') ?></legend>
        <?php
            echo $this->Form->input('cloth_name');
            echo $this->Form->input('cloth_parent');
            echo $this->Form->input('status');
            echo $this->Form->input('adddate');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
