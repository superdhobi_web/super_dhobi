<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Notice'), ['action' => 'edit', $notice->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Notice'), ['action' => 'delete', $notice->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notice->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Notice'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Notice'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="notice view large-9 medium-8 columns content">
    <h3><?= h($notice->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Notice Head') ?></th>
            <td><?= h($notice->notice_head) ?></td>
        </tr>
        <tr>
            <th><?= __('Notice Name') ?></th>
            <td><?= h($notice->notice_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($notice->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created By') ?></th>
            <td><?= $this->Number->format($notice->created_by) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $notice->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
