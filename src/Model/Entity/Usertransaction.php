<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmployeeTask Entity.
 *
 * @property int $id_task
 * @property int $id_employee
 * @property string $order_id
 * @property \App\Model\Entity\Order $order
 * @property int $status
 * @property \Cake\I18n\Time $assign_date
 */
class Usertransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
