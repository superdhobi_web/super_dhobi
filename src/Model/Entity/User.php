<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity.
 *
 * @property int $id
 * @property string $userid
 * @property string $username
 * @property string $name
 * @property string $email
 * @property int $mobile
 * @property string $profile_photo
 * @property string $ref_code
 * @property string $ref_by
 * @property \Cake\I18n\Time $created_date
 * @property bool $status
 * @property bool $is_deleted
 * @property bool $mail_varified
 * @property bool $mobile_varified
 * @property int $otp
 * @property \Cake\I18n\Time $otp_time
 * @property string $created_by
 * @property float $wallet
 * @property \App\Model\Entity\UserAddress[] $user_addresses
 */
class User extends Entity
{

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * Note that when '*' is set to true, this allows all unspecified fields to
 * be mass assigned. For security purposes, it is advised to set '*' to false
 * (or remove it), and explicitly make individual fields accessible as needed.
 *
 * @var array
 */
protected $_accessible = ['*' => true, 'id' => false];

//protected function _setPassword($password) {
//    
//    return (new DefaultPasswordHasher)->hash($password);
//    }
//
//}
}
