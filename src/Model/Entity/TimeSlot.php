<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TimeSlot Entity.
 *
 * @property int $id
 * @property int $day
 * @property string $from_time
 * @property string $to_time
 * @property bool $is_leave
 * @property bool $status
 * @property bool $is_deleted
 * @property \Cake\I18n\Time $created
 */
class TimeSlot extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
