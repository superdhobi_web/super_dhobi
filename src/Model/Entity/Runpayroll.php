<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Adminuser Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $mobile
 * @property string $email
 * @property string $password
 * @property int $department
 * @property string $address
 * @property string $landmark
 * @property string $state
 * @property string $pin
 * @property string $branch_name
 * @property string $account_no
 * @property string $ifsc
 * @property string $pan
 * @property string $last_qualification
 * @property string $institute
 * @property string $yearofpassing
 * @property string $result
 * @property string $basic
 * @property string $ta
 * @property string $da
 * @property string $gross_salary
 * @property int $status
 * @property int $is_delete
 * @property \Cake\I18n\Time $createddate
 */
class Runpayroll extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
}
