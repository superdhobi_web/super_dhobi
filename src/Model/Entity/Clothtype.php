<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Clothtype Entity.
 *
 * @property int $id
 * @property string $cloth_name
 * @property int $cloth_parent
 * @property int $status
 * @property int $adddate
 */
class Clothtype extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
    '*' => true,
    'id' => false,
    ];
}