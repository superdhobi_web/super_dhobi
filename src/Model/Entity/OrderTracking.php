<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity.
 *
 * @property int $id
 * @property int $pickupdate
 * @property int $pickupslot
 * @property int $pickupadd
 * @property int $deliverydate
 * @property int $deliveryslot
 * @property int $deliveryadd
 * @property int $cloth_id
 * @property \App\Model\Entity\Cloth $cloth
 * @property int $service_id
 * @property \App\Model\Entity\Service $service
 * @property int $quanity
 * @property int $totalprice
 * @property int $order_status
 * @property int $ordereddate
 * @property int $status
 * @property string $remarks
 * @property int $ordered_by
 * @property int $pickedup_by
 * @property int $delivered_by
 * @property \App\Model\Entity\Tracking[] $trackings
 */
class OrderTracking extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
