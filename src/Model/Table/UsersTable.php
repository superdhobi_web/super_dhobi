<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $UserAddresses
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->hasMany('UserAddresses', [
            'foreignKey' => 'user_id'
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
        ->integer('id')
        ->allowEmpty('id', 'create');

        $validator
        ->requirePresence('name', 'create')
        ->notEmpty('name');

        $validator
        ->email('email')
        ->allowEmpty('email');
        
        $validator
        ->integer('mobile')
        ->requirePresence('mobile', 'create')
        ->notEmpty('mobile')
        ->add('mobile', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
        ->allowEmpty('ref_by');


        $validator
        ->integer('otp')
        ->allowEmpty('otp');

        $validator
        ->dateTime('otp_time')
        ->allowEmpty('otp_time');

        $validator
        ->allowEmpty('created_by');

        $validator
        ->numeric('wallet')
        ->allowEmpty('wallet');

        return $validator;
    }

}