<?php

namespace App\Model\Table;

use App\Model\Entity\Service;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Services Model
 *
 */
class ServicesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('services');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('sname', 'create')
                ->notEmpty('sname');

        $validator
                ->requirePresence('sicon', 'create')
                ->notEmpty('sicon');

        $validator
        ->requirePresence('sicon', 'create')
        ->notEmpty('sicon')
        ->add('processImageUpload', 'custom', [
        'rule' => 'processImageUpload'
        ]);
        return $validator;
    }

}
