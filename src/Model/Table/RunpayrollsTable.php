<?php
namespace App\Model\Table;

use App\Model\Entity\Adminuser;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Adminusers Model
 *
 */
class RunpayrollsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('runpayrolls');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('basic', 'create')
            ->allowEmpty('basic');

        $validator
            ->requirePresence('ta', 'create')
            ->allowEmpty('ta');

        $validator
            ->requirePresence('da', 'create')
            ->allowEmpty('da');

//
//        $validator
//            ->allowEmpty('address');
//
//        $validator
//            ->allowEmpty('landmark');
//
//        $validator
//            ->allowEmpty('state');
//
//        $validator
//            ->allowEmpty('pin');
//
//        $validator
//            ->allowEmpty('branch_name');
//
//        $validator
//            ->allowEmpty('account_no');
//
//        $validator
//            ->allowEmpty('ifsc');
//
//        $validator
//            ->allowEmpty('pan');
//
//        $validator
//            ->allowEmpty('last_qualification');
//
//        $validator
//            ->allowEmpty('institute');
//
//        $validator
//            ->allowEmpty('yearofpassing');
//
//        $validator
//            ->allowEmpty('result');
//
//        $validator
//            ->allowEmpty('basic');
//
//        $validator
//            ->allowEmpty('ta');
//
//        $validator
//            ->allowEmpty('da');
//
//        $validator
//            ->allowEmpty('gross_salary');
//
              return $validator;
    }

   }
