<?php
namespace App\Model\Table;

use App\Model\Entity\TimeSlot;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TimeSlots Model
 *
 */
class TimeSlotsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('timeslots');
        $this->displayField('id');
        $this->primaryKey('id');

        //$this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('day')
            ->requirePresence('day', 'create')
            ->notEmpty('day');

        $validator
            ->requirePresence('from_time', 'create')
            ->notEmpty('from_time');

        $validator
            ->requirePresence('to_time', 'create')
            ->notEmpty('to_time');

        return $validator;
    }
}
