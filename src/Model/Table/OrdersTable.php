<?php
namespace App\Model\Table;

use App\Model\Entity\Order;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cloths
 * @property \Cake\ORM\Association\BelongsTo $Services
 * @property \Cake\ORM\Association\HasMany $Trackings
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('orders');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Clothtypes', [
            'foreignKey' => 'cloth_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Services', [
            'foreignKey' => 'service_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Trackings', [
            'foreignKey' => 'order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('pickupdate')
            ->requirePresence('pickupdate', 'create')
            ->notEmpty('pickupdate');

        $validator
            ->integer('pickupslot')
            ->requirePresence('pickupslot', 'create')
            ->notEmpty('pickupslot');

        $validator
            ->integer('pickupadd')
            ->requirePresence('pickupadd', 'create')
            ->notEmpty('pickupadd');

        $validator
            ->integer('deliverydate')
            ->requirePresence('deliverydate', 'create')
            ->notEmpty('deliverydate');

        $validator
            ->integer('deliveryslot')
            ->requirePresence('deliveryslot', 'create')
            ->notEmpty('deliveryslot');

        $validator
            ->integer('deliveryadd')
            ->requirePresence('deliveryadd', 'create')
            ->notEmpty('deliveryadd');

        $validator
            ->integer('quanity')
            ->requirePresence('quanity', 'create')
            ->notEmpty('quanity');

        $validator
            ->integer('totalprice')
            ->requirePresence('totalprice', 'create')
            ->notEmpty('totalprice');

        $validator
            ->integer('order_status')
            ->requirePresence('order_status', 'create')
            ->notEmpty('order_status');

        $validator
            ->integer('ordereddate')
            ->requirePresence('ordereddate', 'create')
            ->notEmpty('ordereddate');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->requirePresence('remarks', 'create')
            ->notEmpty('remarks');

        $validator
            ->integer('ordered_by')
            ->requirePresence('ordered_by', 'create')
            ->notEmpty('ordered_by');

        $validator
            ->integer('pickedup_by')
            ->requirePresence('pickedup_by', 'create')
            ->notEmpty('pickedup_by');

        $validator
            ->integer('delivered_by')
            ->requirePresence('delivered_by', 'create')
            ->notEmpty('delivered_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
//    public function buildRules(RulesChecker $rules)
//    {
//        $rules->add($rules->existsIn(['cloth_id'], 'Clothtypes'));
//        $rules->add($rules->existsIn(['service_id'], 'Services'));
//        return $rules;
//    }
}