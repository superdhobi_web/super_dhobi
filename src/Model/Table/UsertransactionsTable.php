<?php
namespace App\Model\Table;

use App\Model\Entity\EmployeeTask;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmployeeTasks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Orders
 */
class UsertransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('usertransactions');
        $this->displayField('id');
        $this->primaryKey('id');
        
    }

}
