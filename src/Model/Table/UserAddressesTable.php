<?php
namespace App\Model\Table;

use App\Model\Entity\UserAddress;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserAddresses Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class UserAddressesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('user_addresses');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'userid',
            'joinType' => 'INNER'
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
        ->integer('id')
        ->allowEmpty('id', 'create');

        $validator
        ->requirePresence('add_heading', 'create')
        ->notEmpty('add_heading');
        
//        $validator
//        ->requirePresence('add_headin', 'create')
//        ->notEmpty('add_headin');

        $validator
        ->integer('city')
        ->requirePresence('city', 'create')
        ->notEmpty('city');

        $validator
        ->integer('circle')
        ->requirePresence('circle', 'create')
        ->notEmpty('circle');

        $validator
        ->integer('location')
        ->requirePresence('location', 'create')
        ->notEmpty('location');

        $validator
        ->integer('area')
        ->requirePresence('area', 'create')
        ->notEmpty('area');

        $validator
        ->requirePresence('address', 'create')
        ->notEmpty('address');

        $validator
        ->requirePresence('landmark', 'create')
        ->notEmpty('landmark');

        $validator
        ->requirePresence('pincode', 'create')
        ->notEmpty('pincode');

//        $validator
//            ->requirePresence('longi', 'create')
//            ->notEmpty('longi');
//
//        $validator
//            ->requirePresence('lati', 'create')
//            ->notEmpty('lati');

        return $validator;
    }

    
}