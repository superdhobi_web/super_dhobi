<?php
namespace App\Model\Table;

use App\Model\Entity\Feedback;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Prints Model
 *
 */
class FeedbacksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('feedbacks');
        $this->displayField('id');
        $this->primaryKey('id');
    }

}

