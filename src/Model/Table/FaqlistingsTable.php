<?php
namespace App\Model\Table;

use App\Model\Entity\Faqlisting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Faqlistings Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cats
 */
class FaqlistingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('faqlistings');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Cats', [
            'foreignKey' => 'cat_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('qn', 'create')
            ->notEmpty('qn');

        $validator
            ->requirePresence('ans', 'create')
            ->notEmpty('ans');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
//    public function buildRules(RulesChecker $rules)
//    {
//        //$rules->add($rules->existsIn(['cat_id'], 'Faqcats'));
//        //return $rules;
//    }
}
