<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html lang="en">
    <head>
       <?= $this->Html->charset() ?>
        <?= $this->element("index_head"); ?>
    </head>
    <body>       
        <!--PRE LOADER-->
		<div class="preloader">
		  <div class="status">&nbsp;</div>
		</div>
		<!--HEADER-->
            <?= $this->element("index_header"); ?>
        <!-- /END HEADER -->
        <?= $this->element("index_services"); ?>
		<!--FEATURES-->
            <?= $this->element("index_feature"); ?>
		<!-- /END FEATURES SECTION -->
		<!--PROCESS-->
            <?= $this->element("index_process"); ?>
		<!-- /END PROCESS SECTION -->			
		<!--SERVICE LOCATION-->
            <?= $this->element("index_service-location"); ?>
		<!-- /END SERVICE LOCATION SECTION -->
		<!-- /Offer LOCATION SECTION -->
            <?= $this->element("offer_section"); ?>
<!--                < offer end here>-->
            
<!--                < offer end here>-->
		<!--TESTIMONIALS-->
            <?= $this->element("index_testimonial"); ?>
		<!-- /END TESTIMONIALS SECTION -->
		<!--ABOUT 1 SECTION-->
            <?= $this->element("index_about-one"); ?>
		<!-- /END ABOUT 1 SECTION -->
		<!--ABOUT 2 SECTION WITH VIDEO-->
            <?= $this->element("index_about-two"); ?>
		<!-- /END ABOUT 2 SECTION -->	
		<!-- DOWNLOAD NOW-->
             <?= $this->element("index_download"); ?>
		<!-- /END DOWNLOAD SECTION -->
		<!--FOOTER, SCRIPTS, MODALS-->
            <?= $this->element("index_footer"); ?>
    </body>
</html>
