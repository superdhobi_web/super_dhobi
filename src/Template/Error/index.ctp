<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- SITE TITLE -->
	<title>SUPERDHOBI::Premium</title>
	<!--FAV AND TOUCH ICONS-->
	<link rel="icon" href="images/icon.png">
	<!--STYLESHEETS-->
	<!--BOOTSTRAP-->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- FONT ICONS -->
	<link rel="stylesheet" href="assets/elegant-icons/style.css">
	<link rel="stylesheet" href="assets/app-icons/styles.css">
	<!--[if lte IE 7]><script src="lte-ie7.js"></script><![endif]-->
	<!-- WEB FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic' rel='stylesheet' type='text/css'>
	<!-- CAROUSEL AND LIGHTBOX -->
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/nivo-lightbox.css">
	<link rel="stylesheet" href="css/nivo_themes/default/default.css">
	<!-- ANIMATIONS -->
	<link rel="stylesheet" href="css/animate.min.css">
	<!-- CUSTOM STYLESHEETS -->
	<link rel="stylesheet" href="css/styles.css">
	<!-- COLORS -->
	<link rel="stylesheet" href="css/colors/blue.css"><!-- DEFAULT COLOR/ CURRENTLY USING -->
	<!-- RESPONSIVE FIXES -->
	<link rel="stylesheet" href="css/responsive.css">
	<!--[if lt IE 9]>
				<script src="js/html5shiv.js"></script>
				<script src="js/respond.min.js"></script>
	<![endif]-->
	<!-- JQUERY -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>

<body>
<!-- =========================
     PRE LOADER       
============================== -->
<div class="preloader">
  <div class="status">&nbsp;</div>
</div>

<!-- =========================
     HEADER   
============================== -->
<header class="header" id="home">

<!-- COLOR OVER IMAGE -->
<div class="color-overlay"> <!-- To make header full screen. Use .full-screen class with color overlay. Example: <div class="color-overlay full-screen">  -->

	<!-- STICKY NAVIGATION -->
	<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
		<div class="container">
			<div class="navbar-header">
				
				<!-- LOGO ON STICKY NAV BAR -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="#"><img src="images/logo.png" alt=""></a>
				
			</div>
			
			<!-- NAVIGATION LINKS -->
			<div class="navbar-collapse collapse" id="kane-navigation">
				<ul class="nav navbar-nav navbar-right main-navigation">
					<li><a href="#home">Home</a></li>
					<li><a href="#features">Features</a></li>
					<li><a href="#services">Process</a></li>
					<li><a href="#brief2">About Us</a></li>
					<li><a href="#service-location">Service Location</a></li>
					<li><a href="#packages">Packages</a></li>
					<li><a href="#download">Download</a></li>
				</ul>
			</div>
		</div> <!-- /END CONTAINER -->
	</div> <!-- /END STICKY NAVIGATION -->
	
	
	<!-- CONTAINER -->
	<div class="container">
		
		<!-- ONLY LOGO ON HEADER -->
		<div class="only-logo">
			<div class="navbar">
				<div class="navbar-header">
					<img src="images/logo.png" alt="">
				</div>
			</div>
		</div> <!-- /END ONLY LOGO ON HEADER -->
		
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				
				<!-- HEADING AND BUTTONS -->
				<div class="intro-section">
					
					<!-- WELCOM MESSAGE -->
					<h1 class="intro">Now Clean Clothes On Just A Click</h1>
					<h5>Available on App Store and Play Store</h5>
					
					<!-- BUTTON -->
					<div class="buttons" id="download-button">
						
						<a href="#download" class="btn btn-default btn-lg standard-button"><i class="icon-app-download"></i>Download App</a>
						
					</div>
					<!-- /END BUTTONS -->
					
				</div>
				<!-- /END HEADNING AND BUTTONS -->
				
			</div>
		</div>
		<!-- /END ROW -->
		
	</div>
	<!-- /END CONTAINER -->
</div>
<!-- /END COLOR OVERLAY -->
</header>
<!-- /END HEADER -->

<!-- =========================
     FEATURES 
============================== -->
<section class="features" id="features">

<div class="container">
	
	<!-- SECTION HEADER -->
	<div class="section-header wow fadeIn animated" data-wow-offset="120" data-wow-duration="1.5s">
		
		<!-- SECTION TITLE -->
		<h2 class="white-text">Amazing Features</h2>
		<div class="colored-line">
		</div>
		<div class="section-description">
			We work our best for your satisfaction.
		</div>
		<div class="colored-line">
		</div>
	</div>
	<!-- /END SECTION HEADER -->
	
	
	<div class="row">
		
		<!-- FEATURES LEFT -->
		<div class="col-md-4 col-sm-4 features-left wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_map_alt"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Express Service</h4>
					<p>
						 Lorem ipsum dolor sit amet, ed do eiusmod tempor incididunt ut labore et dolore magna.
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_gift_alt"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Fabric Care</h4>
					<p>
						 Lorem ipsum dolor sit amet, ed do eiusmod tempor incididunt ut labore et dolore magna.
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_tablet"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Hygenic Wash</h4>
					<p>
						 Lorem ipsum dolor sit amet, ed do eiusmod tempor incididunt ut labore et dolore magn.
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
			
		</div>
		<!-- /END FEATURES LEFT -->
		
		<!-- PHONE IMAGE -->
		<div class="col-md-4 col-sm-4">
			<div class="phone-image wow bounceIn animated" data-wow-offset="120" data-wow-duration="1.5s">
				<img src="images/single-iphone.png" alt="">
			</div>
		</div>
		
		<!-- FEATURES RIGHT -->
		<div class="col-md-4 col-sm-4 features-right wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_genius"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Eco Friendly</h4>
					<p>
						 Lorem ipsum dolor sit amet, ed do eiusmod tempor incididunt ut labore et dolore magna.
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_lightbulb_alt"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Clothes Rejuvenator</h4>
					<p>
						 Lorem ipsum dolor sit amet, ed do eiusmod tempor incididunt ut labore et dolore magna.
					</p>
				</div>
				
			</div>
			
			<!-- /END SINGLE FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_ribbon_alt"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Clothes Protector</h4>
					<p>
						 Lorem ipsum dolor sit amet, ed do eiusmod tempor incididunt ut labore et dolore magna.
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
			
		</div>
		<!-- /END FEATURES RIGHT -->
		
	</div>
	<!-- /END ROW -->
	
</div>
<!-- /END CONTAINER -->

</section>
<!-- /END FEATURES SECTION -->

<!-- =========================
     SERVICES
============================== -->
<section class="services deep-dark-bg" id="services">

<div class="container">
	
	<!-- SECTION HEADER -->
	<div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
		
		<!-- SECTION TITLE -->
		<h2 class="white-text">Our Process</h2>
		
		<div class="colored-line">
		</div>
		<div class="section-description">
			You Spend an average of 3 years in your lifetime washing clothes. Can You believe it? relieve yourself, outsource your laundry!
		</div>
		<div class="colored-line">
		</div>
		
	</div>
	<!-- /END SECTION HEADER -->
	
	<div class="row">
		
		<!-- SINGLE SERVICE -->
		<div class="col-md-3 single-service wow fadeInLeft animated" data-wow-delay=".5s">
			
			<!-- SERVICE ICON -->
			<div class="service-icon">
				<i aria-hidden="true" class="icon_mobile"></i>
			</div>
			
			<!-- SERVICE HEADING -->
			<h3>Order</h3>
			
			<!-- SERVICE DESCRIPTION -->
			<p>
				 Forget carrying your clothes to the dry cleaner or washing them at home. Just try SUPERDHOBI
			</p>
			
		</div>
		<!-- /END SINGLE SERVICE -->
		
		<!-- SINGLE SERVICE -->
		<div class="col-md-3 single-service wow fadeInLeft animated" data-wow-delay=".8s">
			
			<!-- SERVICE ICON -->
			<div class="service-icon">
				<i aria-hidden="true" class="icon_gift_alt"></i>
			</div>
			
			<!-- SERVICE HEADING -->
			<h3>Pick Up</h3>
			
			<!-- SERVICE DESCRIPTION -->
			<p>
				Super Dhobi armed with ecofriendly bags will arrive at your door to pick up your dry cleaning and laundry.
			</p>
			
		</div>
		<!-- /END SINGLE SERVICE -->
		
		<!-- SINGLE SERVICE -->
		<div class="col-md-3 single-service wow fadeInLeft animated" data-wow-delay="1.1s">
			
			<!-- SERVICE ICON -->
			<div class="service-icon">
				<i aria-hidden="true" class="icon_genius"></i>
			</div>
			
			<!-- SERVICE HEADING -->
			<h3>Clean</h3>
			
			<!-- SERVICE DESCRIPTION -->
			<p>
				 Our expert cleaners will ensure that your clothes come back looking fresh and beautiful every time.
			</p>
			
		</div>
		<!-- /END SINGLE SERVICE -->
		<!-- SINGLE SERVICE -->
		<div class="col-md-3 single-service wow fadeInLeft animated" data-wow-delay="1.4s">
			
			<!-- SERVICE ICON -->
			<div class="service-icon">
				<i aria-hidden="true" class="icon_lifesaver"></i>
			</div>
			
			<!-- SERVICE HEADING -->
			<h3>Deliver</h3>
			
			<!-- SERVICE DESCRIPTION -->
			<p>
				Your clothes will be delivered on time to your doorstep. Now getting your laundry done is that simple.
			</p>
			
		</div>
		<!-- /END SINGLE SERVICE -->        
        
	</div>
	<!-- /END ROW -->
	
</div>
<!-- /END CONTAINER -->

</section>
<!-- /END FEATURES SECTION -->

<!-- =========================
     BRIEF RIGHT SECTION 
============================== -->
<section class="app-brief" id="brief2">

<div class="container">
	
	<div class="row">
		
		<!-- BRIEF -->
		<div class="col-md-6 left-align wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">
			
			<!-- SECTION TITLE -->
			<h2 class="white-text">About Super Dhobi</h2>
			
			<div class="colored-line-left">
			</div>
			
			<p>
				Everyone chooses best clothes to wear, then why should you settle even for second best when it comes to laundering them !!! We at “Superdhobi” takes proper care of your garments so that your ‘New clothes looks new forever’.  <br/><br/>
                Laundry is not an art, its a science. Superdhobi is committed to deliver quality laundry services at your convenience. Our state-of-the-art equipments combined with professional handling techniques replenishes the life of your garments, making them as fresh as brand new.                 
			</p>
            <p class="privacy-policy"><i>*</i>Know about our <a href="#" data-toggle="modal" data-target="#ppModal">Privacy Policy</a></p>
			
		</div>
		<!-- /ENDBRIEF -->
		
		<!-- PHONES IMAGE -->
		<div class="col-md-6 wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
			<div class="phone-image">
				<img src="images/sd-abt.png" alt="">
			</div>
		</div>
		<!-- /END PHONES IMAGE -->
		
	</div>
	<!-- /END ROW -->
	
</div>
<!-- /END CONTAINER -->

</section>
<!-- /END SECTION -->


<!-- =========================
     BRIEF LEFT SECTION WITH VIDEO 
============================== -->
<section class="app-brief deep-dark-bg" id="brief1">

<div class="container">
	
	<div class="row">
		
		<!-- PHONES IMAGE -->
		<div class="col-md-6 wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
			<div class="video-container">
				
                <!--
				<div class="video">
					
					<iframe src="//player.vimeo.com/video/88902745?byline=0&amp;portrait=0&amp;color=ffffff" width="600" height="338" frameborder="0" allowfullscreen>
					</iframe> 
				</div>
				-->
				
				<div class="video">
					
					<iframe width="640" height="360" src="https://www.youtube.com/embed/OXu4BxIXG3M" frameborder="0" allowfullscreen></iframe>
				</div>
				
			</div>

		</div>
		
		<!-- RIGHT SIDE WITH BRIEF -->
		<div class="col-md-6 left-align wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">
			
			<!-- SECTION TITLE -->
			<h2 class="white-text">Super Dhobi Intro</h2>
			
			<div class="colored-line-left">
			</div>
			
			<p>
				Superdhobi do your laundry and dry cleaning and deliver at just the tap of a button. superdhobi.in is simply the best laundry & dry cleaning service in India. Don’t believe Us ? <br/>
				Try US !! Services we offer
			</p>
            <div class="col-md-12">
            <div class="row">
				<div class="col-md-4">
                	<label><span aria-hidden="true" class="icon_check_alt2"></span> Normal Wash</label>
                </div>
				<div class="col-md-4">
                	<label><span aria-hidden="true" class="icon_check_alt2"></span> Super Wash</label>
                </div>                
				<div class="col-md-4">
                	<label><span aria-hidden="true" class="icon_check_alt2"></span> Doorstep Delivery</label>
                </div>                
				<div class="col-md-4">
                	<label><span aria-hidden="true" class="icon_check_alt2"></span> Drycleaning</label>
                </div>                
				<div class="col-md-4">
                	<label><span aria-hidden="true" class="icon_check_alt2"></span> Ironing</label>
                </div>                
            </div> 
			</div>
		</div>
		<!-- /END RIGHT BRIEF -->
		
	</div>
	<!-- /END ROW -->
	
</div>
<!-- /END CONTAINER -->

</section>
<!-- /END SECTION -->


<!-- =========================
     TESTIMONIALS 
============================== -->
<section class="testimonials">

<div class="color-overlay">
	
	<div class="container wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
		
		<!-- FEEDBACKS -->
		<div id="feedbacks" class="owl-carousel owl-theme">
			
			<!-- SINGLE FEEDBACK -->
			<div class="feedback">
				
				<!-- IMAGE -->
				<div class="image">
					<!-- i class=" icon_quotations"></i -->
					<img src="images/clients-pic/01.jpg" alt="">
				</div>
				
				<div class="message">
					 Highly impressed by the professionalism, these  guys have demonstrated.They not only picked it from my doorstep but also delivered it within the given time and at a much cheaper rates. Keep up the good work!.
				</div>
				
				<div class="white-line">
				</div>
				
				<!-- INFORMATION -->
				<div class="name">
					Amit Sukla
				</div>
				<div class="company-info">
					Sr Faculty, Allen Career Institute
				</div>
				
			</div>
			<!-- /END SINGLE FEEDBACK -->
			
			<!-- SINGLE FEEDBACK -->
			<div class="feedback">
				
				<!-- IMAGE -->
				<div class="image">
					<!-- i class=" icon_quotations"></i -->
					<img src="images/clients-pic/02.jpg" alt="">
				</div>
				
				<div class="message">
					 " Have had them since their inauguration, after others service didn't work out. They have been outstanding. I have weekly pickups. They're like laundry fairies. They're incredibly professional, punctual, reliable, and do a fantastic job. "
				</div>
				
				<div class="white-line">
				</div>
				
				<!-- INFORMATION -->
				<div class="name">
					Sachin Singh
				</div>
				<div class="company-info">
					Sr Faculty, Allen Career Institute
				</div>
				
			</div>
			<!-- /END SINGLE FEEDBACK -->
			
			<!-- SINGLE FEEDBACK -->
			<div class="feedback">
				
				<!-- IMAGE -->
				<div class="image">
					<!-- i class=" icon_quotations"></i -->
					<img src="images/clients-pic/03.jpg" alt="">
				</div>
				
				<div class="message">
					 " I got their flyer few days back and I am a regular user of their service since then. The quality they provide is extraordinary and the turn around time is pretty fast. No one gives back in just 24 Hours. The best laundry service for house wives like me. "
				</div>
				
				<div class="white-line">
				</div>
				
				<!-- INFORMATION -->
				<div class="name">
					Aditi Trivedi
				</div>
				<div class="company-info">
					House Wife
				</div>
				
			</div>
			<!-- /END SINGLE FEEDBACK -->
			
		</div>
		<!-- /END FEEDBACKS -->
		
	</div>
	<!-- /END CONTAINER -->
	
</div>
<!-- /END COLOR OVERLAY -->

</section>
<!-- /END TESTIMONIALS SECTION -->





<!-- =========================
     SCREENSHOTS
============================== -->
<section class="screenshots deep-dark-bg" id="service-location">

<div class="container">
	
	<!-- SECTION HEADER -->
	<div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
		
		<!-- SECTION TITLE -->
		<h2 class="white-text">Service Locations</h2>
		
		<div class="colored-line">
		</div>
		<div class="section-description">
			Give us a call between 9:00AM to 6:00PM we will be there for you.
		</div>
		<div class="colored-line">
		</div>
		
	</div>
	<!-- /END SECTION HEADER -->
	
	<div class="row">
                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay=".3s">
                        <div class="media service-box">
                            <div class="pull-left">
                                <i aria-hidden="true" class="icon_pin_alt"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">SRINATHPURAM</h4>
                                <p>
                                    <i aria-hidden="true" class="icon_box-checked "></i> RTU <br>
                                    <i aria-hidden="true" class="icon_box-checked "></i> Srinathpuram- A, B, C, D, E <br>                                    
                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
                                </p>
                            </div>
                        </div>
                    </div>		
                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay=".6s">
                        <div class="media service-box">
                            <div class="pull-left">
                                <i aria-hidden="true" class="icon_pin_alt"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">TALWANDI</h4>
                                <p>
                                    <i aria-hidden="true" class="icon_box-checked "></i> Talwandi All Sectors 
 <br>
                                    <i aria-hidden="true" class="icon_box-checked "></i>  Keshavpura <br>                                    
                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
                                </p>
                            </div>
                        </div>
                    </div>	        
                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay=".9s">
                        <div class="media service-box">
                            <div class="pull-left">
                                <i aria-hidden="true" class="icon_pin_alt"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">VIGYAN NAGAR</h4>
                                <p>
                                    <i aria-hidden="true" class="icon_box-checked "></i>  Vigyan Nagar (ALL SECTORS) 
 <br>
                                    <i aria-hidden="true" class="icon_box-checked "></i>  Chatrappur <br>                                    
                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
                                </p>
                            </div>
                        </div>
                    </div>	        
                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay="1.2s">
                        <div class="media service-box">
                            <div class="pull-left">
                                <i aria-hidden="true" class="icon_pin_alt"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">DADABARI</h4>
                                <p>
                                    <i aria-hidden="true" class="icon_box-checked "></i>  Basant Vihar  
 <br>
                                    <i aria-hidden="true" class="icon_box-checked "></i>  Waqf Nagar  <br>                                    
                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
                                </p>
                            </div>
                        </div>
                    </div>        
                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay="1.5s">
                        <div class="media service-box">
                            <div class="pull-left">
                                <i aria-hidden="true" class="icon_pin_alt"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">INDIRA VIHAR</h4>
                                <p>
                                    <i aria-hidden="true" class="icon_box-checked "></i>  Rajeev Gandhi Nagar  
 <br>
                                    <i aria-hidden="true" class="icon_box-checked "></i>  Mahaveer Nagar-1,2,3   <br>                                    
                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
                                </p>
                            </div>
                        </div>
                    </div>         
                <div class="col-md-4 col-sm-6 wow fadeInUp animated" data-wow-delay="1.8s">
                        <div class="media service-box">
                            <div class="pull-left">
                                <i aria-hidden="true" class="icon_pin_alt"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">GUMANPURA</h4>
                                <p>
                                    <i aria-hidden="true" class="icon_box-checked "></i>  Ballabh Nagar   
 <br>
                                    <i aria-hidden="true" class="icon_box-checked "></i> Srinathpuram- A, B, C, D, E    <br>                                    
                                    <small><a data-toggle="modal" data-target="#servModal">View All</a></small> 
                                </p>
                            </div>
                        </div>
                    </div>                 
        
		<!-- /END SCREENSHOTS -->
		
	</div>
	<!-- /END ROW -->
	
</div>
<!-- /END CONTAINER -->

</section>
<!-- /END SCREENSHOTS SECTION -->


<!-- =========================
     PRICING TABLE | Added on version 1.7   
============================== -->
<section class="packages" id="packages">

<div class="container">
	
	<!-- SECTION HEADER -->
	<div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
		
		<!-- SECTION TITLE -->
		<h2 class="white-text">Affordable Packages</h2>
		<div class="colored-line">
		</div>
		
		<div class="section-description">
			 We charges that suits your pocket
		</div>
		
		<div class="colored-line">
		</div>
	</div>
	<!-- /END SECTION HEADER -->
	<div class="row">
     <div class="col-md-8 col-md-offset-2 packages-content">
            <ul class="nav nav-tabs">
               <a data-toggle="tab" href="#men" class="active">Men</a>
               <a data-toggle="tab" href="#women">Women</a>
               <a data-toggle="tab" href="#household">Houehold</a>
             </ul>

  <div class="tab-content">
    <div id="men" class="tab-pane fade in active">
		<div class="accordion-con">
            <div class="panel-group" id="accordion1">
              <div class="panel panel-default">
                <div class="panel-heading">
	                <a data-toggle="collapse" data-parent="#accordion1" href="#men1">
		                  <h4 class="panel-title">
		                    Upper
		                  </h4>
	                 </a>
                </div>
                <div id="men1" class="panel-collapse collapse in">
                  <div class="panel-body">
                  	<div class="sub-tab-con">
                  		<ul class="nav nav-tabs">
						    <li class="active"><a data-toggle="tab" href="#menup1">T-Shirt</a></li>
						    <li><a data-toggle="tab" href="#menup2">Cotton Shirt</a></li>
						    <li><a data-toggle="tab" href="#men-more">More</a></li>
  						</ul>
  						<div class="tab-content">
    						<div id="menup1" class="tab-pane fade in active">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="menup2" class="tab-pane fade">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="men-more" class="tab-pane fade">
	                  			<div class="tab-more">
	                  				<h3>Download SuperDhobi App</h3>
	                  				<h2>More Exciting Packages are Waiting for You :)</h2>
	                        	</div>
	                        </div>

	                    </div>
					</div><!--sub-tab-con-->
                </div>
            </div>
        </div><!--panel-default-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion1" href="#men2">
	                   <h4 class="panel-title">
	                    Lower
	                  </h4>
                </a>
            </div>
            <div id="men2" class="panel-collapse collapse">
                <div class="panel-body">
					<div class="sub-tab-con">
                  		<ul class="nav nav-tabs">
						    <li class="active"><a data-toggle="tab" href="#menlw1">Jeans Pant</a></li>
						    <li><a data-toggle="tab" href="#menlw2">Cotton Pant</a></li>
						    <li><a data-toggle="tab" href="#men-more2">More</a></li>
  						</ul>
  						<div class="tab-content">
    						<div id="menlw1" class="tab-pane fade in active">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="menlw2" class="tab-pane fade">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="men-more2" class="tab-pane fade">
	                  			<div class="tab-more">
	                  				<h3>Download SuperDhobi App</h3>
	                  				<h2>More Exciting Packages are Waiting for You :)</h2>
	                        	</div>
	                        </div>

	                    </div>
					</div><!--sub-tab-con-->
                  </div>
                </div>
              </div><!--panel-default-->

        	</div><!--panel-group-->      
      	</div><!--accordion-con-->
    </div><!--Men-->
    <div id="women" class="tab-pane fade">
		<div class="accordion-con">
            <div class="panel-group" id="accordion2">
              	<div class="panel panel-default">
                	<div class="panel-heading">
                		<a data-toggle="collapse" data-parent="#accordion2" href="#women1">
                  			<h4 class="panel-title">
                    			Upper
                  			</h4>
                 		</a>
                	</div>
                <div id="women1" class="panel-collapse collapse in">
                  	<div class="panel-body">
                  	<div class="sub-tab-con">
                  		<ul class="nav nav-tabs">
						    <li class="active"><a data-toggle="tab" href="#womenup1">T-Shirt</a></li>
						    <li><a data-toggle="tab" href="#womenup2">Cotton Shirt</a></li>
						    <li><a data-toggle="tab" href="#women-more1">More</a></li>
  						</ul>
  						<div class="tab-content">
    						<div id="womenup1" class="tab-pane fade in active">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="womenup2" class="tab-pane fade">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="women-more1" class="tab-pane fade">
	                  			<div class="tab-more">
	                  				<h3>Download SuperDhobi App</h3>
	                  				<h2>More Exciting Packages are Waiting for You :)</h2>
	                        	</div>
	                        </div>

	                    </div>
					</div><!--sub-tab-con-->
                    </div>
                </div>
            </div><!--panel-default-->
            <div class="panel panel-default">
                <div class="panel-heading">
                 	<a data-toggle="collapse" data-parent="#accordion2" href="#women2">
                   		<h4 class="panel-title">
                    		Lower
                  		</h4>
                  	</a>
                </div>
                <div id="women2" class="panel-collapse collapse">
                  	<div class="panel-body">
						<div class="sub-tab-con">
	                  		<ul class="nav nav-tabs">
							    <li class="active"><a data-toggle="tab" href="#womenlw1">Jeans Pant</a></li>
							    <li><a data-toggle="tab" href="#womenlw2">Cotton Pant</a></li>
							    <li><a data-toggle="tab" href="#women-more2">More</a></li>
	  						</ul>
  						<div class="tab-content">
    						<div id="womenlw1" class="tab-pane fade in active">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="womenlw2" class="tab-pane fade">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="women-more2" class="tab-pane fade">
	                  			<div class="tab-more">
	                  				<h3>Download SuperDhobi App</h3>
	                  				<h2>More Exciting Packages are Waiting for You :)</h2>
	                        	</div>
	                        </div>

	                    </div>
					</div><!--sub-tab-con-->
                    </div>
                </div>
            </div><!--panel-default-->
        </div><!--panel-group-->         
    </div><!--accordion-con-->
</div><!--Women-->
<div id="household" class="tab-pane fade">
    <div class="accordion-con">
        <div class="panel-group" id="accordion3">
            <div class="panel panel-default">
                <div class="panel-heading">
                	<a data-toggle="collapse" data-parent="#accordion3" href="#hh1">
                  		<h4 class="panel-title">
                    		Upper
                  		</h4>
                 	</a>
                </div>
                <div id="hh1" class="panel-collapse collapse in">
                  <div class="panel-body">
                  		                  	<div class="sub-tab-con">
                  		<ul class="nav nav-tabs">
						    <li class="active"><a data-toggle="tab" href="#hhup1">T-Shirt</a></li>
						    <li><a data-toggle="tab" href="#hhup2">Cotton Shirt</a></li>
						    <li><a data-toggle="tab" href="#hh-more1">More</a></li>
  						</ul>
  						<div class="tab-content">
    						<div id="hhup1" class="tab-pane fade in active">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="hhup2" class="tab-pane fade">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="hh-more1" class="tab-pane fade">
	                  			<div class="tab-more">
	                  				<h3>Download SuperDhobi App</h3>
	                  				<h2>More Exciting Packages are Waiting for You :)</h2>
	                        	</div>
	                        </div>

	                    </div>
					</div><!--sub-tab-con-->
                  	</div>
                </div>
            </div><!--panel-default-->
            <div class="panel panel-default">
                <div class="panel-heading">
                 	<a data-toggle="collapse" data-parent="#accordion3" href="#hh2">
                   		<h4 class="panel-title">
                    		Lower
                  		</h4>
                  	</a>
                </div>
                <div id="hh2" class="panel-collapse collapse">
                  	<div class="panel-body">
						<div class="sub-tab-con">
	                  		<ul class="nav nav-tabs">
							    <li class="active"><a data-toggle="tab" href="#hhlw1">Jeans Pant</a></li>
							    <li><a data-toggle="tab" href="#hhlw2">Cotton Pant</a></li>
							    <li><a data-toggle="tab" href="#hh-more2">More</a></li>
	  						</ul>
  						<div class="tab-content">
    						<div id="hhlw1" class="tab-pane fade in active">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="hhlw2" class="tab-pane fade">
	                  			<div class="table-responsive">
	                        		<table class="table">
									    <thead>
									      <tr>
									        <th>Service Type</th>
									        <th>Description</th>
									        <th>Price</th>
									      </tr>
									    </thead>
									    <tbody>
									      <tr>
									        <td>Noraml Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 100/-</td>
									      </tr>
									      <tr>
									        <td>Super Wash</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 200/-</td>
									      </tr>
									      <tr>
									        <td>Dry Clean</td>
									        <td>Lorem Ipsum</td>
									        <td>Rs. 300/-</td>
									      </tr>      

									    </tbody>
									</table>
	                        	</div>
	                        </div>
    						<div id="hh-more2" class="tab-pane fade">
	                  			<div class="tab-more">
	                  				<h3>Download SuperDhobi App</h3>
	                  				<h2>More Exciting Packages are Waiting for You :)</h2>
	                        	</div>
	                        </div>

	                    </div>
					</div><!--sub-tab-con-->
                    </div>
                </div>
            </div><!--panel-default-->
        </div><!--panel-group-->        
     </div><!--accordion-con-->
 </div><!--household-->
</div><!--tab-content-->    
</div><!--col-md-8-->
</div><!-- /END ROW -->
<!-- /END CONTAINER -->
</section>
<!-- /END PRICING TABLE SECTION -->


<!-- =========================
     DOWNLOAD NOW 
============================== -->
<section class="download" id="download">

<div class="color-overlay">

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				
				<!-- DOWNLOAD BUTTONS AREA -->
				<div class="download-container">
					<h2 class=" wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">Download the app on</h2>
					
					<!-- BUTTONS -->
					<div class="buttons wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
						
				<!--<a href="" class="btn btn-default btn-lg standard-button"><i class="icon-app-store"></i>App Store</a>-->
						<a href="" class="btn btn-default btn-lg standard-button"><i class="icon-google-play"></i>Play Store</a>
						
					</div>
					<!-- /END BUTTONS -->
					
				</div>
				<!-- END OF DOWNLOAD BUTTONS AREA -->
				
				
				<!-- SUBSCRIPTION FORM WITH TITLE -->
				<div class="subscription-form-container">
					
					<h2 class="wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">Subscribe Now!</h2>
					
					<!-- =====================
					     MAILCHIMP FORM STARTS 
					     ===================== -->
					
					<form class="subscription-form mailchimp form-inline wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s" role="form">
						
						<!-- SUBSCRIPTION SUCCESSFUL OR ERROR MESSAGES -->
						<h4 class="subscription-success"></h4>
						<h4 class="subscription-error"></h4>
						
						<!-- EMAIL INPUT BOX -->
						<input type="email" name="email" id="subscriber-email" placeholder="Your Email" class="form-control input-box">
						
						<!-- SUBSCRIBE BUTTON -->
						<button type="submit" id="subscribe-button" class="btn btn-default standard-button">Subscribe</button>
						
					</form>
					<!-- /END MAILCHIMP FORM STARTS -->

				</div>
                <!-- END OF SUBSCRIPTION FORM WITH TITLE -->
				
			</div> 
			<!-- END COLUMN -->
			
		</div> 
		<!-- END ROW -->
		
	</div>
	<!-- /END CONTAINER -->
</div>
<!-- /END COLOR OVERLAY -->

</section>
<!-- /END DOWNLOAD SECTION -->


	<!--FOOTER-->
	<footer>
		<div class="container">
			
			<div class="contact-box wow rotateIn animated" data-wow-offset="10" data-wow-duration="1.5s">
				
				<!-- CONTACT BUTTON TO EXPAND OR COLLAPSE FORM -->
				
				<a class="btn contact-button expand-form expanded"><i class="icon_mail_alt"></i></a>
				
				<!-- EXPANDED CONTACT FORM -->
				<div class="row expanded-contact-form">
					
					<div class="col-md-8 col-md-offset-2">
						
						<!-- FORM -->
						<form class="contact-form" id="contact" role="form">
							
							<!-- IF MAIL SENT SUCCESSFULLY -->
							<h4 class="success">
								<i class="icon_check"></i> Your message has been sent successfully.
							</h4>
							
							<!-- IF MAIL SENDING UNSUCCESSFULL -->
							<h4 class="error">
								<i class="icon_error-circle_alt"></i> E-mail must be valid and message must be longer than 1 character.
							</h4>
							
							<div class="col-md-6">
								<input class="form-control input-box" id="name" type="text" name="name" placeholder="Your Name">
							</div>
							
							<div class="col-md-6">
								<input class="form-control input-box" id="email" type="email" name="email" placeholder="Your Email">
							</div>
							
							<div class="col-md-12">
								<input class="form-control input-box" id="subject" type="text" name="subject" placeholder="Subject">
								<textarea class="form-control textarea-box" id="message" rows="8" placeholder="Message"></textarea>
							</div>
							
							<button class="btn btn-primary standard-button2 ladda-button" type="submit" id="submit" name="submit" data-style="expand-left">Send Message</button>
							
						</form>
						<!-- /END FORM -->
						
					</div>
					
				</div>
				<!-- /END EXPANDED CONTACT FORM -->
				
			</div>
			<!-- /END CONTACT BOX -->
			
			<!-- LOGO -->
			<img src="images/logo.png" alt="LOGO" class="responsive-img">
			
			<!-- SOCIAL ICONS -->
			<ul class="social-icons">
				<li><a href="https://www.facebook.com/SUPER-DHOBI-1010169048993992/?ref=hl"><i class="social_facebook_square"></i></a></li>
				<li><a href="https://twitter.com/MySuperDhobi"><i class="social_twitter_square"></i></a></li>
				<li><a href="https://in.pinterest.com/SuperDhobi/superdhobi"><i class="social_pinterest_square"></i></a></li>
				<li><a href="https://plus.google.com/u/5/105875490934370267660"><i class="social_googleplus_square"></i></a></li>
				<li><a href="https://www.linkedin.com/in/mysuper-dhobi-a8701310b?trk=nav_responsive_tab_profile_pic"><i class="social_linkedin_square"></i></a></li>
				<!--<li><a href=""><i class="social_dribbble_square"></i></a></li>-->
				<li><a href="http://superdhobi.blogspot.in/?view=timeslide"><i class="social_flickr_square"></i></a></li>
			</ul>
			
			<!-- COPYRIGHT TEXT -->
			<p class="copyright">
				<span class="pull-left">
					Copyright © 2016 SuperDhobi, All Rights Reserved
				</span>
				<span class="pull-right text-center col-xs-12 col-md-4">
					<a href="#" data-toggle="modal" data-target="#ppModal">
						<small>Privacy Policy</small>
					</a>
					|
					<a href="#" data-toggle="modal" data-target="#tcModal">
						<small>Terms & Conditions</small>
					</a>
				</span>
			</p>
		</div>
	</footer>
	<!-- /END FOOTER -->
	<!--Modal Contents-->
	<div id="servModal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title theme">Areas We Serve</h4>
	      </div>
	      <div class="modal-body" style="overflow:auto;">
	        <div class="col-md-12">
	            <div class="row">
	                <div class="col-md-4">
	                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> SRINATHPURAM</h5>
	                    <p>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> RTU </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Srinathpuram- A, B, C, D, E </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> RK Puram -A, B, C, D, E </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Swami Vivekanand Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Veer Sawarkar Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Manish Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Mahaveer Nagar Extention </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Shivpura </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Medical College</small>
	                    </p>
	                </div>
	                <div class="col-md-4">
	                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> TALWANDI</h5>
	                    <p>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Talwandi All Sectors  </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Keshavpura </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> New Jawahar Nagar </small>
	                    </p>
	                </div>
	                <div class="col-md-4">
	                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> VIGYAN NAGAR</h5>
	                    <p>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Vigyan Nagar (ALL SECTORS)  </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Chatrappur  </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> PNT Colony</small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Prem Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Indra Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Sanjaya Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Garish Nagar </small>
	                    </p>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-12">
	            <div class="row">
	                <div class="col-md-4">
	                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> DADABARI</h5>
	                    <p>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Basant Vihar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Waqf Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Shakti Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Shastri Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> RPS Colony </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Balaji Nagar </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Pratap Nagar</small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Kishorepura </small>
	                    </p>
	                </div>
	                <div class="col-md-4">
	                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> INDIRA VIHAR</h5>
	                    <p>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Rajeev Gandhi Nagar  </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Mahaveer Nagar-1,2,3 </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> IL Colony </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Parijat Colony </small>
	                    </p>
	                </div>
	                <div class="col-md-4">
	                    <h5 class="media-heading"><i aria-hidden="true" class="icon_pin_alt"></i> GUMANPURA</h5>
	                    <p>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Ballabh Nagar  </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Ballabh Bari  </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Guman Pura</small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> New Gumanpura </small><br>
	                        <small><i aria-hidden="true" class="arrow_triangle-right_alt"></i> Durga Nagar </small>
	                        
	                    </p>
	                </div>
	            </div>
	        </div>
	      </div>
	      <div class="modal-footer">            
	      </div>
	    </div>
	  </div>
	</div>
	<div id="ppModal" class="modal fade in" role="dialog" aria-hidden="false">
	  	<div class="modal-dialog modal-lg">
		    <!-- Modal content-->
		    <div class="modal-content">
		      	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">×</button>
			        <h4 class="modal-title tcp-title"><i class="icon_documents_alt"></i> Privacy Policy</h4>
		      	</div>
		      	<div class="modal-body tcp">
			        <p>
			          <i class="arrow_triangle-right_alt2"></i> This privacy policy sets out how SuperDhobi uses and protects any information that you give to SuperDhobi over Phone, Website or Mobile App. <br>

			          <i class="arrow_triangle-right_alt2"></i> SuperDhobi is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement. <br>

			          <i class="arrow_triangle-right_alt2"></i> SuperDhobi may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 2nd of April 2015. 
			        </p>
			        <h4><i class="arrow_carrot-2right"></i> What we collect?</h4>
			        <p>
			          We may collect the following information: <br>
			          	<i class="icon_circle-slelected"></i> name and job title <br>
			          	<i class="icon_circle-slelected"></i> contact information including email address <br>
			          	<i class="icon_circle-slelected"></i> demographic information such as postcode, preferences and interests <br>
			          	<i class="icon_circle-slelected"></i> other information relevant to customer surveys and/or offers
			        </p>
			        <h4><i class="arrow_carrot-2right"></i> What we do with the information we gather?</h4>
			        <p>
			          We require this information to understand your needs and provide you with a better service, and in particular for the following reasons: <br>
			          <i class="arrow_triangle-right_alt2"></i>  Internal record keeping. <br>

			          <i class="arrow_triangle-right_alt2"></i>  We may use the information to improve our products and services. <br>

			          <i class="arrow_triangle-right_alt2"></i> We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided. <br>

			          <i class="arrow_triangle-right_alt2"></i>  From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customize the website according to your interests. <br>

			          <i class="arrow_triangle-right_alt2"></i>  The information you provide will be shared with the servers on which we host the web content and your name and location may be shared with the laundry processing centre(s) and Rider(s) we have been working with.          
			        </p>
			        <h4><i class="arrow_carrot-2right"></i> Security</h4>
			        <p>
			          <i class="arrow_triangle-right_alt2"></i>  We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.
			        </p>
			        <h4><i class="arrow_carrot-2right"></i> How we use cookies?</h4>
			        <p>
			          <i class="arrow_triangle-right_alt2"></i> A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences. <br>

			          <i class="arrow_triangle-right_alt2"></i> We use traffic log cookies to identify which pages are being used. This helps us analyze data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system. <br>

			          <i class="arrow_triangle-right_alt2"></i> Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us. <br>

			         	<i class="arrow_triangle-right_alt2"></i> You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website. 
			        </p>
			        <h4><i class="arrow_carrot-2right"></i> Contacting Us</h4>
			        <p>
			          If there are any questions regarding this privacy policy you may contact us at our Call Centre or write to :- <a href="mailto:mysuperdhobhi@gmail.com">mysuperdhobhi@gmail.com</a>
			        </p>
		      	</div>          
		      	<div class="modal-footer">            
		      	</div>        
		    </div>
	  	</div>
	</div>
	<div id="tcModal" class="modal fade in" role="dialog" aria-hidden="false">
	  	<div class="modal-dialog modal-lg">
		    <!-- Modal content-->
		    <div class="modal-content">
		    	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">×</button>
			        <h4 class="modal-title tcp-title"><i class="icon_document_alt"></i> Terms & Conditions</h4>
		      	</div>
		      	<div class="modal-body tcp">
		      		<h4><i class="arrow_carrot-2right"></i> Demo Heading</h4>
		      		<p>
		      			<i class="arrow_triangle-right_alt2"></i>
		      			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil ullam voluptatem neque qui ab suscipit magnam magni, doloribus eum perspiciatis debitis harum reiciendis odio voluptates excepturi eos! Rem, quaerat sint.
		      		</p>
		      	</div>
			</div>
		</div>
	</div>

	<!-- =========================
	     SCRIPTS 
	============================== -->

	<script src="js/bootstrap.min.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/jquery.scrollTo.min.js"></script>
	<script src="js/jquery.localScroll.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>
	<script src="js/simple-expand.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/retina.min.js"></script>
	<script src="js/matchMedia.js"></script>
	<script src="js/jquery.backgroundvideo.min.js"></script>
	<script src="js/jquery.nav.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.fitvids.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>