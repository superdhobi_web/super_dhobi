<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- SITE TITLE -->
    <title>SUPERDHOBI :: Price List</title>
    <!--FAV AND TOUCH ICONS-->
    <link rel="icon" href="<?php echo $this->request->webroot; ?>images/favicon/favicon-32x32.png">
    <!--STYLESHEETS-->
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/bootstrap.min.css">
    <!-- FONT ICONS -->
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>assets/elegant-icons/style.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>assets/app-icons/styles.css">
    <!--[if lte IE 7]><script src="lte-ie7.js"></script><![endif]-->
    <!-- WEB FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic' rel='stylesheet' type='text/css'>
    <!-- CAROUSEL AND LIGHTBOX -->
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/nivo-lightbox.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/nivo_themes/default/default.css">
    <!-- ANIMATIONS -->
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/animate.min.css">
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/styles.css">
    <!-- COLORS -->
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/colors/blue.css"><!-- DEFAULT COLOR/ CURRENTLY USING -->
    <!-- RESPONSIVE FIXES -->
    <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/responsive.css">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <!-- JQUERY -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style type="text/css">
        html, body{
            height: 100%;
            background: #fff;
        }
    </style>
</head>
<body>
    <!--PRE LOADER-->
    <div class="preloader">
        <div class="status">&nbsp;</div>
    </div>
    <!-- HEADER-->
    <header class="header" id="home">
        <!-- STICKY NAVIGATION -->
        <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top no-sticky-navigation">
            <div class="container">
                <div class="navbar-header">

                    <!-- LOGO ON STICKY NAV BAR -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= BASE_URL . "index" ?>">
                        <img src="<?php echo $this->request->webroot; ?>images/logo1.png" alt="Superdhobi" style="height:30px;">
                    </a>				
                </div>

                <!-- NAVIGATION LINKS -->
                <div class="navbar-collapse collapse" id="kane-navigation">
                    <ul class="nav navbar-nav navbar-right main-navigation">
                        <li><a href="<?= BASE_URL . "index" ?>">Home</a></li>
                        <li><a href="<?= BASE_URL . "index/offers" ?>">Offers</a></li>
                        <li><a href="<?= BASE_URL . "index/price" ?>">Price</a></li>
                        <li><a href="<?= BASE_URL . "index" ?>#features">Features</a></li>
                        <li><a href="<?= BASE_URL . "index" ?>#process">Process</a></li>
                        <li><a href="<?= BASE_URL . "index" ?>#service-location">Service Location</a></li>
                        <li><a href="<?= BASE_URL . "index/testimonial" ?>">Testimonial</a></li>
                        <li><a href="<?= BASE_URL . "index" ?>#brief2">About Us</a></li>
                        <li><a href="<?= BASE_URL . "index" ?>#contact">Contact Us</a></li>
                    </ul>
                </div>
            </div> <!-- /END CONTAINER -->
        </div> <!-- /END STICKY NAVIGATION -->
    </header>
    <!-- /END HEADER -->
    <!--PRICING TABLE-->
    <section class="packages" id="packages">
        <div class="col-md-3 main-menu-con">
            <div class="main-menu">
                <ul>
                    <?php $i = 1;
                                                foreach ($allcloth as $res) { ?>
                        <li id="sidemenu<?= $res->id ?>" class="alldataa <?php
                                                    if ($i == 1) {
                                                        echo " active ";
                                                    }
                                                    ?>" onclick="showprice(<?= $res->id ?>)"><a href="#"><i aria-hidden="true" class="arrow_carrot-right_alt2"></i> <?= $res->cloth_name; ?> <span aria-hidden="true" class="arrow_carrot-right animate-menu-icon"></span></a></li>
                        <?php $i++;
                                                } ?>
                </ul>
            </div>
            <!--main-menu-->
        </div>
        <!--col-md-3-->
        <div class="col-md-9 pricing-content-con">
            <div class="container">
                <!-- SECTION HEADER -->
                <div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
                    <!-- SECTION TITLE -->
                    <h2 class="bark-text text-left">Affordable Prices</h2>
                    <div class="colored-line pull-left">
                    </div>
                    <div class="colored-line  pull-left">
                    </div>
                </div>
                <!-- /END SECTION HEADER -->
            </div>
            <!--container-->


            <?php
                                        $i = 1;
                                        foreach ($allcloth as $res) {

                                            ?>
                <div class="pricing-contents testclass" id="new<?= $res->id ?>" <?php if ($i !=1 ) { ?> style="display:none;"
                    <?php } ?>>
                        <div class="accordion-con">
                            <div class="panel-group" id="accordion<?= $res->id ?>">
                                <?php //echo "Start";
                                if (!empty($clothtype[$res->id])) {
                                  //  echo "If".$res->id;
                                  //  pr($clothtype[$res->id]);
                                    foreach ($clothtype[$res->id] as $va) {
                                        ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion<?= $res->id ?>" href="#collapse<?= $va['id'] ?>">
                                                <?= $va['cloth_name'] ?>
                                            </a>
                                            <h4 class="panel-title"></h4>
                                        </div>
                                        <div id="collapse<?= $va['id'] ?>" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="col-xs-12">
                                                    <div class="col-sm-3">
                                                        <!-- required for floating -->
                                                        <ul class="nav nav-tabs tabs-left sideways">
                                                            <!-- Nav tabs -->
                                                            <?php
                                                                $j = 1;
                                                                if (!empty($endcloths[$va['id']])) {
                                                                    foreach ($endcloths[$va['id']] as $clothall) {
                                                                        ?>
                                                                <li class="<?php
                                                                        if ($j == 1) {
                                                                            echo " active ";
                                                                        }
                                                                        ?>">
                                                                    <a href="#t<?= $clothall['id'] ?>" data-toggle="tab">
                                                                        <?= $clothall['cloth_name'] ?>
                                                                    </a>
                                                                </li>
                                                                <?php
                                                                        $j++;
                                                                    }
                                                                }
                                                                ?>
                                                        </ul>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <?php
                                                                $j = 1;
                                                                if (!empty($endcloths[$va['id']])) {
                                                                    foreach ($endcloths[$va['id']] as $clothall) {
                                                                        ?>
                                                                <div class="tab-pane <?php
                                                                        if ($j == 1) {
                                                                            echo " active ";
                                                                        }
                                                                        ?>" id="t<?= $clothall['id'] ?>">
                                                                    <div class="table-responsive">
                                                                        <table class="table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Service Type</th>
                                                                                    <th>Description</th>
                                                                                    <th>Price</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php

                                                                                    if (!empty($pricear[$clothall['id']])) {
                                                                                        foreach ($pricear[$clothall['id']] as $cloth) {
                                                                                            ?>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <?= $cloth['wastype'] ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?= $cloth['description'] ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?= $cloth['price'] ?>/-</td>
                                                                                    </tr>
                                                                                    <?php
                                                                                        }
                                                                                        } else { //echo "jlkd";
                                                                                        echo "<tr><td colspan='3'><p class='no-price'>No Price set</p></span></tr>";
                                                                                    }
                                                                                    ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                                <?php
                                                                    $j++;
                                                                }
                                                            } else {
                                                                echo "<p class='no-cloth'>No cloth available for this category <b>:(</b></p>";
                                                            }
                                                            ?>
                                                        </div>

                                                    </div>

                                                    <!--                                                    <div class="clearfix"></div>-->

                                                </div>
                                                <!--col-xs-12-->

                                            </div>
                                            <!--panel-body-->
                                        </div>
                                    </div>

                                    <?php } ?>
                            </div>
                        </div>
                </div>
                <!--accordion-con-->
                <!--            </div>pricing-contents-->
                <?php
                        } else { //echo "Else";
                        echo "<p class='no-cloth'>No cloth available for this category <b>:(</b></p></div></div></div>";
                    } $i++;
                }
                ?>
        </div>
        <!--col-md-9-->
    </section>
    <!-- /END PRICING TABLE SECTION -->
    <!--Modal Contents-->
    <div id="ppModal" class="modal fade in" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title tcp-title"><i class="icon_documents_alt"></i> Privacy Policy</h4>
                </div>
                <div class="modal-body tcp">
                    <p>
                        <i class="arrow_triangle-right_alt2"></i> This privacy policy sets out how SuperDhobi uses and protects any information that you give to SuperDhobi over Phone, Website or Mobile App. <br>

                        <i class="arrow_triangle-right_alt2"></i> SuperDhobi is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement. <br>

                        <i class="arrow_triangle-right_alt2"></i> SuperDhobi may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 2nd of April 2015. 
                    </p>
                    <h4><i class="arrow_carrot-2right"></i> What we collect?</h4>
                    <p>
                     We may collect the following information: <br>
                     <i class="icon_circle-slelected"></i> name and job title <br>
                     <i class="icon_circle-slelected"></i> contact information including email address <br>
                     <i class="icon_circle-slelected"></i> demographic information such as postcode, preferences and interests <br>
                     <i class="icon_circle-slelected"></i> other information relevant to customer surveys and/or offers
                    </p>
                    <h4><i class="arrow_carrot-2right"></i> What we do with the information we gather?</h4>
                    <p>
                     We require this information to understand your needs and provide you with a better service, and in particular for the following reasons: <br>
                     <i class="arrow_triangle-right_alt2"></i>  Internal record keeping. <br>

                     <i class="arrow_triangle-right_alt2"></i>  We may use the information to improve our products and services. <br>

                     <i class="arrow_triangle-right_alt2"></i> We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided. <br>

                     <i class="arrow_triangle-right_alt2"></i>  From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customize the website according to your interests. <br>

                     <i class="arrow_triangle-right_alt2"></i>  The information you provide will be shared with the servers on which we host the web content and your name and location may be shared with the laundry processing centre(s) and Rider(s) we have been working with.          
                    </p>
                    <h4><i class="arrow_carrot-2right"></i> Security</h4>
                    <p>
                    <i class="arrow_triangle-right_alt2"></i>  We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.
                    </p>
                    <h4><i class="arrow_carrot-2right"></i> How we use cookies?</h4>
                    <p>
                        <i class="arrow_triangle-right_alt2"></i> A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences. <br>

                        <i class="arrow_triangle-right_alt2"></i> We use traffic log cookies to identify which pages are being used. This helps us analyze data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system. <br>

                        <i class="arrow_triangle-right_alt2"></i> Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us. <br>

                        <i class="arrow_triangle-right_alt2"></i> You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website. 
                    </p>
                    <h4><i class="arrow_carrot-2right"></i> Contacting Us</h4>
                    <p>
                     If there are any questions regarding this privacy policy you may contact us at our Call Centre or write to :- <a href="mailto:mysuperdhobhi@gmail.com">mysuperdhobhi@gmail.com</a>
                    </p>
                </div>          
                <div class="modal-footer">            
                </div>        
            </div>
        </div>
    </div>
    <div id="tcModal" class="modal fade in" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title tcp-title"><i class="icon_document_alt"></i> Terms & Conditions</h4>
                </div>
                <div class="modal-body tcp">
                    <h4><i class="arrow_carrot-2right"></i> Demo Heading</h4>
                    <p>
                        <i class="arrow_triangle-right_alt2"></i>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil ullam voluptatem neque qui ab suscipit magnam magni, doloribus eum perspiciatis debitis harum reiciendis odio voluptates excepturi eos! Rem, quaerat sint.
                    </p>
                </div>
            </div>
        </div>
    </div>
<!--SCRIPTS-->
<script src="<?php echo $this->request->webroot; ?>js/bootstrap.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/Index_ajax.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/smoothscroll.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/jquery.scrollTo.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/jquery.localScroll.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/owl.carousel.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/nivo-lightbox.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/simple-expand.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/wow.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/jquery.stellar.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/retina.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/matchMedia.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/jquery.backgroundvideo.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/jquery.nav.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/jquery.ajaxchimp.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/jquery.fitvids.js"></script>
<script src="<?php echo $this->request->webroot; ?>js/custom.js"></script>
<script>
    function showprice(id){
    //alert(id);
    $(".alldataa").removeClass('active');
    $("#sidemenu"+id).addClass('active');
    $(".testclass").attr("style","display:none;");
    $("#new"+id).css("display","block");
}    
</script>
</body>
</html>