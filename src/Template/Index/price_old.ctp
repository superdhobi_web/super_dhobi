        <section class="packages" id="packages">
            <div class="container">
                <!-- SECTION HEADER -->
                <div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
                    <!-- SECTION TITLE -->
                    <h2 class="bark-text text-left">Affordable Prices</h2>
                    <div class="colored-line pull-left">
                    </div>
                    <div class="colored-line  pull-left">
                    </div>
                </div>
                <!-- /END SECTION HEADER -->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 packages-content">
                        <ul class="nav nav-tabs">
                            <?php $i = 1;
                            foreach ($allcloth as $val) { ?>
                                <a class="allcloth <?php
                            if ($i == 1) {
                                echo 'actives';
                            }
                                ?>" onclick="activeclass(<?= $i;?>);" id="active<?= $i;?>" href="#cloths<?= $i; ?>" data-toggle="tab" ><?php echo h($val->cloth_name); ?></a>
                                   <?php $i++;
                               } ?>
                        </ul>
                        <div class="tab-content">
                            <?php
                            $i = 1;
                            foreach ($allcloth as $val) {
                                ?>
                                <div id="cloths<?= $i; ?>" class="tab-pane fade in <?php if($i==1){ echo "active";}?>">
                                    <div class="accordion-con">
                                        <div class="panel-group" id="accordion1">
                                            <?php
                                            if (!empty($clothtype[$val->id])) {
                                                $j = 1;
                                                foreach ($clothtype[$val->id] as $cloth) {
                                                    if ($cloth['cloth_parent'] == $val->id) {
                                                        ?>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <a data-toggle="collapse" data-parent="#accordion1" href="#cloth<?= $i.$j; ?>">
                                                                    <h4 class="panel-title">
                                                                        <?= $cloth['cloth_name']; ?>
                                                                    </h4>
                                                                </a>
                                                            </div>
                                                            <div id="cloth<?= $i.$j; ?>" class="panel-collapse collapse <?php if($j==1){echo 'in';}?>">
                                                                <div class="panel-body">
                                                                    <div class="sub-tab-con">
                                                                        <ul class="nav nav-tabs">
                                                                            <?php
                                                                            if (!empty($endcloths[$cloth['id']])) {
                                                                                $k = 1;
                                                                                foreach ($endcloths[$cloth['id']] as $endcloth) {
                                                                                    ?>
                                                                                    <li class="<?php if ($k == 1)
                                                                echo "active"; ?>"><a data-toggle="tab" href="#test<?= $j.$k; ?>"><?= $endcloth['cloth_name']; ?></a></li>
                                                                                        <?php
                                                                                        $k++;
                                                                                    }
                                                                                }
                                                                                ?>
                                                                        </ul>
                                                                        <div class="tab-content">
                                                                            <?php
                                                                                $l=1;
                                                                            foreach ($endcloths[$cloth['id']] as $endcloth) {
                                                                                if (!empty($pricear[$endcloth['id']])) {
                                                                                    ?>
                                                                                    <div id="test<?= $j.$l; ?>" class="tab-pane fade in <?php if($l==1){ echo "active";}?>">
                                                                                        <div class="table-responsive">
                                                                                            <table class="table">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>Service Type</th>
                                                                                                        <th>Description</th>
                                                                                                        <th>Price</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                    <?php foreach ($pricear[$endcloth['id']] as $price) {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td><?= $price['wastype']; ?></td>
                                                                                                            <td><?= $price['description']; ?></td>
                                                                                                            <td>Rs. <?= $price['price']; ?>/-</td>
                                                                                                        </tr>
                                                                                                    <?php } ?>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php }
                                                                          $l++;  } ?>
                                                                            <div id="men-more" class="tab-pane fade">
                                                                                <div class="tab-more">
                                                                                    <h3>Download SuperDhobi App</h3>
                                                                                    <h2>More Exciting Packages are Waiting for You :)</h2>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div><!--sub-tab-con-->
                                                                </div>
                                                            </div>
                                                        </div><!--panel-default-->
                                                        <?php
                                                    }
                                                    $j++;
                                                }
                                            }
                                            ?>
                                        </div><!--panel-group-->      
                                    </div><!--accordion-con-->
                                </div>                        
                                <?php $i++;
                            } ?>
                        </div><!--tab-content-->    
                    </div><!--col-md-8-->
                </div><!-- /END ROW -->
            </div><!-- /END CONTAINER -->
        </section>