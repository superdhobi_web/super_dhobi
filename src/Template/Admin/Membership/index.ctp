<?php 
?>
<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Membership Lists</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Membership Lists</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                    <span class="left">
                                        Membership Lists
                                    </span>
                                    <a href="<?= BASE_URL."admin/membership/add"?>" class="right" data-position="left" data-tooltip="Add Membership Details">
                                        <i class="mdi-av-my-library-add" ></i>
                                    </a>                            
                            </span>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <table class="striped responsive-table centered">
                                    <thead>
                                        <tr class="white-text">
                                            <th class="teal">Actions</th>
                                            <th class="teal">
                                                Plan Name
                                            </th>
                                            <th class="teal">
                                                Plan Validity
                                            </th>
                                            <th class="teal">
                                                Plan Price
                                            </th>
                                            <th class="teal">
                                                Plan Duration
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($data as $membership_data){
                                        ?>
                                        <tr>
                                            <td class="actions">
                                                <a href="<?= ADMIN_URL?>membership/add/<?= $membership_data["id"];?>" title="Edit" class="btn-floating btn-small  green waves-effect waves-light">
                                                    <i class="mdi-content-create"></i>
                                                </a>
                                                <a href="<?= ADMIN_URL?>membership/delete/<?= $membership_data["id"];?>" title="Delete" class="btn-floating btn-small red waves-effect waves-light">
                                                    <i class="mdi-action-delete"> </i>
                                                </a> 
                                            </td>
                                            <td class="text-capitalize green-text"><?= $membership_data["plan_name"]?></td>
                                            <td><?= date('jS F Y ',strtotime($membership_data["valid_from"]));?>-<?= date('jS F Y ',strtotime($membership_data["valid_to"]));?></td>
                                            <td>
                                                Rs.<?= $membership_data["price"];?>/-
                                            </td>
                                            <td>
                                                <?= $membership_data["duration"];?> Days
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>