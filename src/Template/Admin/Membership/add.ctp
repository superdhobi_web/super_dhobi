<?php
// ======= Get Parent Cloth =========== //
$clothOption = "";
foreach ($cloth_parent as $key => $clothList) {
    $clothOption.= "<option value='" . $clothList['id'] . "'>" . $clothList['cloth_name'] . "</option>";
}
// ======== Get Service List =========== //
$washOption = "";
foreach ($service_types as $key => $washList) {
    $washOption.= "<option value='" . $washList['id'] . "'>" . $washList['sname'] . "</option>";
}
?>
<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Add Membership Plan</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Add Membership Plan</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <?php //pr($editmembership);?>
        <div class="section">
            <div class="row">
                <div class="col s12">
                    <div class="card-panel">
                        <h4 class="header2"> Add Membership Plan</h4>
                        <div class="row">
                            <form class="right-alert col m8 offset-m2" method="post" action="<?= ADMIN_URL ?>membership/add<?php
        if (!empty($editmembership)) {
            echo "/".$editmembership->id;
        }?>">
                                <input type="hidden" value="<?php
        if (!empty($editmembership)) {
            echo $editmembership->id;
        }
        ?>" name="id">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="plan_name" class="validate" type="text" required="" value="<?php
                                       if (!empty($editmembership)) {
                                           echo $editmembership->plan_name;
                                       }
        ?>" name="plan_name">
                                        <label for="plan_name" data-error="Please enter name." data-success="Perfect!" class="active">
											Plan Name
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input type="date" class="datepicker2" id="datepicker2" name="valid_from" value="<?php
                                               if (!empty($editmembership)) {
                                                   echo date('jS F Y ', strtotime($editmembership->valid_from));
                                               }
        ?>" placeholder="Select Valid From...">
                                        <!-- <input id="valid_from" class="validate" type="date" required="" value="" name=""> -->
                                        <label for="valid_from" data-error="Please select date" data-success="Perfect!" class="active">
											Valid From
                                        </label>	                                	                                    
                                    </div>
                                    <div class="input-field col m6">
                                        <input type="date" class="datepicker2" id="datepicker2" value="<?php
                                               if (!empty($editmembership)) {
                                                   echo date('jS F Y ', strtotime($editmembership->valid_to));
                                               }
        ?>" name="valid_to" placeholder="Select Valid To...">
                                        <label for="valid_to" data-error="Please enter date." data-success="Perfect!" class="active">
											Valid to
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="price" class="validate" type="number" step='0.01' required value="<?php
                                               if (!empty($editmembership)) {
                                                   echo $editmembership->price;
                                               } else {
                                                   echo "0.00";
                                               }
        ?>" name="price">
                                        <label for="price" data-error="Please enter number." data-success="Perfect!" class="active">
											Price
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <ul id="planlist">
                                        <li>
                                            <p class="col s12" style="margin-bottom:-18px;">SET PLANS</p>
                                            <div id="append_data">
                                                <div class="input-field col s4">
                                                    <?php
                                                    if (!empty($editmembership)) {
                                                        $discount = json_decode($editmembership["discount"]);
                                                    }
                                                    ?>
                                                    <select name="clothParent[]" class="filter-select browser-default">
                                                        <?php foreach ($cloth_parent as $ke => $cloth) { ?>
                                                            <option value="<?= $cloth["id"]; ?>" <?php
                                                        if (!empty($membershipData)) {
                                                            if ($membershipData->categoty == $cloth["id"]) {
                                                                echo "selected";
                                                            }
                                                        }
                                                            ?>><?= $cloth["cloth_name"]; ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="input-field col s4">
                                                    <select name="washType[]" class="filter-select browser-default">
                                                        <?php foreach ($service_types as $k => $service) { ?>
                                                            <option value="<?= $service["id"]; ?>" <?php
                                                        if (!empty($membershipData)) {
                                                            if ($membershipData->service == $service["id"]) {
                                                                echo "selected";
                                                            }
                                                        }
                                                        ?>><?= $service["sname"]; ?></option>
<?php } ?>
                                                    </select>
                                                </div>
                                                <div class="input-field col s2">
                                                    <input id="amount" class="validate" type="number" value="<?php
if (!empty($membershipData)) {
    echo $membershipData->quantity;
}
?>" required name="pcs[]" >
                                                    <label for="amount" data-error="Please enter pcs." data-success="Perfect!" class="active">
													Quantity
                                                    </label>
                                                </div>
                                                <div class="input-field col s2">
                                                    <br>
                                                    <a class="btn-floating btn-small  green waves-effect waves-light " href="#" onclick="insertnewprice();">
                                                        <i class="mdi-content-add"></i>
                                                    </a>
                                                    <a class="btn-floating btn-small  red waves-effect waves-light " href="#">
                                                        <i class="mdi-content-clear"></i>
                                                    </a>
                                                </div>
                                            </div>								
                                        </li>
                                    </ul>
                                </div>	 

                                <div class="row">
                                    <p class="col s12" style="margin-bottom:-18px;">BENEFITS</p>
                                    <div class="input-field col s12">
                                        <div class="col s4">
                                            <input class="with-gap" name="benefit" type="radio" <?php
                                                        if (!empty($editmembership)) {
                                                            if ($editmembership["benefit"] == 2) {
                                                                echo "checked";
                                                            }
                                                        }
?> id="cashback_radio" data-id="cashback" value="2"/>
                                            <label for="cashback_radio">Cashback Offer</label>
                                        </div>
                                        <div class="col s4">
                                            <input class="with-gap" name="benefit" type="radio" <?php
                                                        if (!empty($editmembership)) {
                                                            if ($editmembership["benefit"] == 1) {
                                                                echo "checked";
                                                            }
                                                        }
?> id="discount_radio" data-id="discount" value="1"/>
                                            <label for="discount_radio">Discount Offer</label>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 <?php if(!empty($editmembership) && $editmembership['benefit']==1){echo "none";}?>" id="cashback">
                                        <input id="cashback_amt" type="text" value="<?php if(!empty($editmembership["benefit"])){ echo $editmembership["cashback_amt"]; }?>" name="cashback_amt">
                                        <label for="cashback_amt" data-error="Please enter name." data-success="Perfect!" class="active">
											Cashback Amount
                                        </label>
                                    </div>
                                    <div id="discount" class="<?php if(!empty($editmembership) && $editmembership['benefit']==2){echo "none";}?>">
                                        <div class="input-field col s4">
                                            <select name="discount[categoty]">
                                                <option value="" disabled selected>Category</option>
                                                <?php foreach ($cloth_parent as $ke => $cloth) { ?>
                                                            <option value="<?= $cloth["id"]; ?>" <?php
                                                        if (!empty($editmembership)) {
                                                            if ($discount->categoty == $cloth["id"]) {
                                                                echo "selected";
                                                            }
                                                        }
                                                            ?>><?= $cloth["cloth_name"]; ?></option>
                                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="input-field col s4">
                                            <select name="discount[service]">
                                                <option value="" disabled selected>Wash Type</option>
                                                  <?php foreach ($service_types as $k => $service) { ?>
                                                            <option value="<?= $service["id"]; ?>" <?php
                                                        if (!empty($editmembership)) {
                                                            if ($discount->service == $service["id"]) {
                                                                echo "selected";
                                                            }
                                                        }
                                                        ?>><?= $service["sname"]; ?></option>
<?php } ?>
                                            </select>
                                        </div>
                                        <div class="input-field col s4">
                                            <input id="amount"  type="number" value="<?php
if (!empty($editmembership)) {
    echo $discount->quantity;
}
?>" name="discount[quantity]">
                                            <label for="amount"  class="active">
												Quantity
                                            </label>
                                        </div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="plan_dure" class="validate" value="<?php
                                                        if (!empty($editmembership)) {
                                                            echo $editmembership["duration"];
                                                        }
?>" type="text" required value="" name="duration">
                                        <label for="plan_dure" data-error="Please enter number of days." data-success="Perfect!" class="active">
											Plan Duration
                                        </label>
                                    </div>
                                </div>  
                                <div class="row">
                                    <p class="col s12" style="margin-bottom:-5px;">Terms & Conditions</p>
                                    <div class="input-field col s12">  
                                        <textarea id="editor1" name="terms"><?php
                                            if (!empty($editmembership)) {
                                                echo $editmembership["terms"];
                                            }
?></textarea>
                                    </div>
                                </div>            
                                <div class="row">
                                    <div class="input-field col s12">
                                        <button class="btn cyan waves-effect waves-light right" type="submit" name="action">SAVE
                                            <i class="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<style>
    .none{
        display: none;
    }
</style>
<script>
    $(':radio').change(function (event) {
        var id = $(this).data('id');
        $('#' + id).addClass('none').siblings().removeClass('none');
    });
</script>
