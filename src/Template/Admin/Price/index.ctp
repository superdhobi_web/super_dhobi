<div class='container' style="display:none" id="priceId">
<div id='card-alert1' class='card green message success' onclick="this.classList.add('hidden')">
<div class="card-content white-text">
    <p>
       Price Added Successfully 
    </p>
</div>
</div>
</div>
<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Price</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/Dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Price</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <br>
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs tab-demo z-depth-1">
                                <?php $i = 1;
                                foreach ($allcloth as $val) { ?>
                                <li class="tab col s2">
                                    <a class="<?php
                                    if ($i == 1) {
                                        echo 'active';
                                    }
                                    ?>" href="#test<?php echo $i; ?>"><?php echo h($val->cloth_name); ?></a>
                                </li>
                                <?php $i++;
                            } ?>
                        </ul>
                    </div>
                    <div class="col s12">
                        <?php
                        $i = 1;
                        foreach ($allcloth as $val) {
                            ?>
                            <div id="test<?php echo $i; ?>" class="col s12 grey lighten-3">
                                <div class="row">
                                    <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                                        <?php
                                        if (!empty($newcloth[$val->id])) {
                                            foreach ($newcloth[$val->id] as $valnew) {
                                                if ($valnew['cloth_parent'] == $val->id) {
                                                    ?>
                                                    <li>
                                                        <div class="collapsible-header active teal-text text-lighten-2">
                                                            <i class="mdi-navigation-arrow-forward"></i><?php echo h($valnew['cloth_name']); ?>
                                                            <a class="waves-effect waves-light btn-floating btn-small teal modal-trigger" href="#" onclick="addClothid(<?= $valnew['id'];?>);">
                                                                        <i class="mdi-content-add"></i>
                                                                    </a>
                                                        </div>
                                                        <div class="collapsible-body">
                                                            <ul class="collection with-header">
                                                                <li class="collection-header grey lighten-2">
                                                                    <h6 class="condensed bold grey-text text-darken-3">ASSIGNED ITEMS</h6>

                                                                </li>
                                                                <?php
                                                                if (!empty($newnewcloth[$valnew['id']])) {
                                                                    foreach ($newnewcloth[$valnew['id']] as $valnnew) {
                                                                        ?>
                                                                        <li class="collection-item">
                                                                            <div class="row">
                                                                                <div class="col s4">
                                                                                    <i class="mdi-action-star-rate"></i> <?= h($valnnew['cloth_name']) ?>
                                                                                    <a class="waves-effect waves-light btn-floating btn-small cyan modal-trigger" href="javascript:void(0);" onclick="addClothid(<?= $valnnew['id'];?>);">
                                                                                        <i class="mdi-content-add"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col s3 right">
                                                                                    <div class="secondary-content">
                                                                                        <a href="" class="waves-effect waves-light btn-floating btn-small cyan modal-trigger" onclick="modalopen(<?= $valnnew['id']; ?>);">
                                                                                            <i class="mdi-content-add"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col s12">
                                                                                <br>
                                                                                <ul class="collection with-header">
                                                                                    <?php if (!empty($pricearr[$valnnew['id']])) {
                                                                                        foreach ($pricearr[$valnnew['id']] as $prci) { ?>
                                                                                        <li class="collection-item">
                                                                                            <div class="price-pic center">
                                                                                                <img src="<?= BASE_URL; ?>thumbimg/<?= $prci['sicon']; ?>" height="30" width="30" class="circle"/>
                                                                                            </div>
                                                                                            <div class="price-elements">
                                                                                                <span class="task-cat cyan">
                                                                                                    <?= h($prci['wastype']) ?>
                                                                                                </span>
                                                                                            </div>
                                                                                            <div class="price-desc">
                                                                                                <span class="" id="descspan_<?= h($prci['price_id']); ?>"><?= h($prci['description']); ?></span>
                                                                                                <div style="display:none" id="editdescdiv_<?= $prci['price_id']; ?>"><input type="text" name="description" id="editdesc_id_<?= $prci['price_id']; ?>" value="<?= h($prci['description']) ?>"/></div>
                                                                                            </div>
                                                                                            <div class="price-rate">
                                                                                                <span id="pricespan_<?= $prci['price_id']; ?>" class="yellow-text text-darken-4">Rs. <?= h($prci['price']) ?>/-</span>
                                                                                                <div style="display:none" id="editpricediv_<?= $prci['price_id']; ?>">
                                                                                                    <input type="text" name="price" id="editprice_id_<?= $prci['price_id']; ?>" value="<?= h($prci['price']) ?>"/>
                                                                                                    <button id="price_save_<?= $prci['price_id']; ?>" class="btn-floating btn-small waves-effect waves-light cyan">
                                                                                                        <i class="mdi-content-save"></i>
                                                                                                    </button>
                                                                                                </div>
                                                                                            </div>

                                                                                            <?php
                                                                                          //  pr($prci);//exit;
                                                                                            $id = $prci['price_id'];
                                                                                            //$service = $prci['sname'];
                                                                                            echo $this->Form->postLink(
                                                                                                "<i class='mdi-action-delete'> </i>",
                                                                                                ['action' => 'delete', $id],
                                                                                                ['escape' => false, 'title' => 'Delete', 'class' => 'waves-effect waves-light btn-floating btn-small red secondary-content price-act', 'confirm' => __('Are you sure you want to delete this service for {0}?', $valnew['cloth_name'])]);
                                                                                                ?>
                                                                                                <!--                                                                                                    <div>editmodalopen(<?php // $prci['price_id'];  ?>,<?php // $prci['price'];  ?>,<?php //$prci['wash_id'];  ?>,<?php // $valnnew['id']; ?>);</div>-->
                                                                                                <a href="#" class="waves-effect waves-light btn-floating btn-small green modal-trigger secondary-content price-act modal-trigger" onclick='editprice(<?= $prci['price_id']; ?>,"<?= $prci['price']; ?>","<?= trim($prci['description']);?>");'> 
                                                                                                    <i class="mdi-content-create"></i>
                                                                                                </a>
                                                                                            </li>
                                                                                            <?php }
                                                                                        } ?>
                                                                                    </ul>
                                                                                    <br>
                                                                                </div>
                                                                            </li>
                                                                            <?php }
                                                                        } ?>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div>	                      	
                                    </div>
                                    <?php $i++;
                                } ?>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="modaladd_price" class="modal">
        <div class="modal-content">
            <h6 class="teal-text">
                Add New Item Details
            </h6> 
            <a href="#" class="waves-effect waves-red btn-floating btn-small grey lighten-3 right modal-action modal-close right" style="margin: -48px -20px 0 0px;">
                <i class="mdi-content-clear red-text"></i>
            </a>
            <div class="row">
                <div class="col s12">                                    
                    <form action="<?= ADMIN_URL; ?>price/add" method="post" id="itemprice" class="right-alert">
                        <input type="hidden" name="cloth_id" value="" id="cloth_id"/>
                        <div class="input-field col s12">
                            <select name="service_id" class="" id="washtype" data-error="Please select wash type." data-success="I Like it!" >
                                <option value="">--select wash type--</option>
                                <?php foreach ($allservices as $services) { ?>
                                <option value="<?= $services->id; ?>"><?= $services->sname; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="input-field col s12">
                            <input id="price" name="price" type="number" class="validate" step="0.01">
                            <label for="price"  data-error="Please enter item of price." data-success="Perfect!">Price</label>
                        </div>
                        <div class="input-field col s12">
                            <textarea name="description" id="desc" class="materialize-textarea validate"></textarea>
                            <label for="desc">Description</label>
                        </div>
                        <div class="input-field col s12 m12">
                            <button class="btn waves-effect waves-light cyan right" type="submit" name="action">ADD</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="modaledit_price" class="modal">
        <div class="modal-content">
            <h6 class="teal-text">
                Edit Item Details
            </h6> 
            <a href="#" class="waves-effect waves-red btn-floating btn-small grey lighten-3 right modal-action modal-close right" style="margin: -48px -20px 0 0px;">
                <i class="mdi-content-clear red-text"></i>
            </a>
            <div class="row">
                <div class="col s12">                                    
                    <form action="<?= ADMIN_URL; ?>price/add" method="post" id="itemprice" >
                        <input type="hidden" id="id_price" name="id" value="">
                        <input type="hidden" name="cloth_id" value="" id="cloth_id_id"/>
                        <div class="input-field col s12">
                            <select name="service_id" class="validate browser-default" id="service_id_id">
                                <option value="">--select wash type--</option>
                                <?php foreach ($allservices as $services) { ?>
                                <option value="<?= $services->id; ?>"><?= $services->sname; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="input-field col s12">
                            <input id="price_id" placeholder='' name="price" type="text" value="" class="validate">
                            <label for="price" class="active" data-error="Please enter item of price." data-success="I Like it!">Price</label>
                        </div>
                        <div class="input-field col s12">
                            <textarea name="description" id="desc" cols="30" rows="10" class="validate"></textarea>
                            <label for="desc"  data-error="Please enter description." data-success="I Like it!" >Description</label>
                        </div>
                        <div class="input-field col s12 m12">
                            <button class="btn waves-effect waves-light cyan right" type="submit" name="action">UPDATE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="addimg" class="modal modal-fixed-footer" style="height: 310px;">
          <div class="modal-content">
              <form enctype="multipart/form-data" method="post" action="<?= ADMIN_URL?>price/addClothimage">
                  <input type="hidden" id="clothId" name="clothId"/>
                  <div style="width: 75%;float: left;margin-top: 50px;">
                   <div class="file-field col s8 input-field">
                    <div class="btn cyan darken-2">
                      <span>browse</span>
                      <input type="file" name="clothImg" required>
                  </div>
                  <div class="file-path-wrapper">
                      <input class="file-path" id="" type="text" placeholder="Upload Image for this cloth">
                  </div>
              </div> 
          </div>
          <div class="col s4" style="text-align:center;margin-top: 57px;width: 20%;float: left;">
              <button class="btn waves-effect waves-light cyan btn-large" type="submit" name="action">Submit</button>
          </div>
      </form>	
  </div>
  <div class="modal-footer">
    <a href="#" class="waves-effect waves-red btn-flat modal-action modal-close">Close</a>
</div>
</div>