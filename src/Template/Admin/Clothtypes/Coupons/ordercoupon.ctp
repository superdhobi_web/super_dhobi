<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Order Coupon</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Order Coupon</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="">
                    <div class="card-content">
                        <div class="row">
                            <div class="col m12 s12">
                                <h5 class="left green-text">
                                    Order Coupon
                                </h5>
                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m12 l6">
                                            <div class="card">
                                                <div class="card-content grey lighten-3 teal-text">
                                                    <div class="col s12 m12 l6">
                                                        <p class="card-stats-title">
                                                            <i class="mdi-action-account-balance-wallet"></i> Billing Amount
                                                        </p>
                                                        <p class="ultra-small">Rs. <?= $billing_amount; ?>/-</p></div>
                                                    <div class="col s12 m12 l6">
                                                        <p class="card-stats-title"> 
                                                            <i class="mdi-action-add-shopping-cart"></i> Order Id
                                                        </p>
                                                        <p class="ultra-small"><?= $order_id; ?></p></div>
                                                    </p>
                                                    <form action="" method="post">

                                                        <?php
                                                        if (!empty($coupon)) {
                                                            foreach ($coupon as $coupondata) {
                                                                ?>
                                                                <p class="card-stats-compare">Eligible Coupons  <span class="deep-purple-text text-lighten-5"></span>
                                                                    <span class="col s12 m12 l6">
                                                                        <input class="with-gap" name="coupon" 
                                                                               id="coupon" type="radio" onclick="couponcount(<?= $billing_amount; ?>,<?= $coupondata['discount']; ?>);"  
                                                                               value="<?= $coupondata['coupon_code']; ?>">
                                                                        <label for="coupon"><?= $coupondata['coupon_code']; ?></label>
                                                                    </span>
                                                                    <span class="col s12 m12 l6">
                                                                        <span id="discount1"><?= $coupondata['discount']; ?></span>% discount
                                                                    </span>
                                                                    <?php
                                                                    $discount = ($billing_amount) * ($coupondata['discount'] / 100);
                                                                    $total_amount = $billing_amount - $discount;
                                                                    ?>
                                                                    <input type="hidden" value="<?= $discount; ?>" name="discount_amount">
                                                                    <input type="hidden" value="<?= $total_amount; ?>" name="total_price">
                                                                <?php } ?>
                                                                <button type="submit" value="Continue">Continue</button>
                                                            <?php } else { ?>
                                                            <div>
                                                                <script>
                                                                    setTimeout(function () {
                                                                        window.location.href= "<?= ADMIN_URL?>orders/index"; // the redirect goes here
                                                                    },5000);
                                                                </script>
                                                                <span class="red-text text-lighten-1">
                                                                    No coupon Code Applicable to this Order
                                                                </span>
                                                            </div>

                                                        <?php } ?>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<?php if (!empty($coupon)) { ?>-->
                                            <!--<?php } ?>-->
                                        <div class="col s12 m12 l6">
                                            <div class="card">
                                                <div class="card-content grey lighten-4 white-text">
                                    <!-- <p class="card-stats-title">
                                        Total Price: Rs.
                                                    <?= $billing_amount ?>/-
                                    </p>

                                    Amount Rs.<?= $discount; ?>/- will be deducted from your Billing Amount.

                                    <hr>
                                    Discount Amount=Rs.<?= $discount; ?>
                                    Billing Amount=Rs.<?= $total_amount; ?> -->
                                                    <p class="card-stats-title grey-text">
                                                        Total Price: Rs.<?= $billing_amount ?>/-
                                                    </p>
                                                    <p class="grey-text ultra-small">
                                                        Amount Rs.<span id="discount">0</span>.00/- will be deducted from your Billing Amount.
                                                    </p>
                                                    <!-- <p class="grey-text ultra-small">
                                                        Discount Amount Rs. <span id="discount">0</span>.00/-
                                                    </p> -->
                                                    <h5 class="cyan-text text-lighten-1">
                                                        <i class="mdi-action-receipt"></i> Billing Amount Rs. <span id="total_price"><?= $billing_amount ?></span>.00/-
                                                    </h5>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--card stats start-->
</div>
</section>
