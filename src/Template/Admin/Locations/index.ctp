 <?= $this->Html->css("http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"); ?>
 <?= $this->Html->css("http://www.jqueryscript.net/css/jquerysctipttop.css"); ?>
 <?= $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');?>
 <?= $this->Html->script('http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js');?>
 <?= $this->Html->script('admin/easyTree.js');?>
 <section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Locations</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Locations</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-2 left">
                                <span class="">
                                    Location Details
                                </span>                                
                            </span>                                                             
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l6 right right-alert" id="addCloth">
                                <div class="input-field col s12 m9 l19">
                                    <input type="text" placeholder="Create parent Location" id="pLocationid" class="validate">
                                    <label for="pLocationid" class="" data-error="Please enter the area Location." data-success="Perfect!">Location</label>
                                </div>   
                                <div class="col s12 m3 l3">
                                    <button onclick="return saveLocation();" class="btn green waves-effect waves-light left">Submit</button>
                                </div>                                 
                                <input type="hidden" class="form-control" id="getParentID">
                                <input type="hidden" class="form-control" id="getClothLevel">
                                <input type="hidden" class="form-control" id="get_for" value="location">
                            </div>   
                        </div>
                        <br>
                        <div class="easy-tree">
                            <?php
                            function renderMenu($items) {
                                $render = '<ul>';
                                foreach ($items as $item) {
                                    $render .= '<li>';
                                    $render .= $item->id."^/*/^".$item->location."^/*/^".$item->level."^/*/^"."2";;
                                    if (!empty($item->subs)) {
                                        $render .= renderMenu($item->subs);
                                    }
                                    $render .= '</li>';
                                }
                                return $render . '</ul>';
                            }
                            echo renderMenu($location);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
  (function ($) {
    function init() {
      $('.easy-tree').EasyTree({
        addable: true,
        editable: true,
        deletable: true
    });
  }

  window.onload = init();
})(jQuery)
</script>