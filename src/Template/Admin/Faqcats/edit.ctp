<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Edit FAQ Category</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Edit FAQ Category</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div id="">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title grey-text text-darken-4">
                                <span class="green-text left">Edit FAQ Category</span>
                            </span>
                            <div class="row">
                                <form class="col s12 m8 offset-m2 right-alert" id="addfaq" action="<?= ADMIN_URL . "faqcats/add/$faqcat->id"; ?>" method="post">
                                    <input type="hidden" name="id" value="<?= $faqcat->id; ?>"/>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="mdi-editor-format-line-spacing prefix"></i>
                                            <input id="categoryname" class="validate" type="text" name="name" value="<?= $faqcat->name;?>"/>
                                            <label for="categoryname" data-error="Please enter Category name." data-success="Perfect!">Category Name</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button class="btn green waves-effect waves-light right" name="action" type="submit">Save
                                                <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>                  
                    </div>
                </div>
            </div>
        </div>
    </div>
</section?