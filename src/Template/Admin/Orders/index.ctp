<?php
$status = array(
    '1' => 'Order Placed',
    '2' => 'Order Confirmed',
    '3' => 'Ready To Pickup',
    '4' => 'Out For Pickup',
    '5' => 'Picked up',
    '6' => 'Pickup Reschedule',
    '7' => 'Intransit To Warehouse',
    '8' => 'Order Processing',
    '9' => 'Ready To deliver',
    '10' => 'Out for Deliver',
    '11' => 'Delivered',
    '12' => 'Deliver Reschedule',
    '13' => 'Cancel');
    ?>
    <section id="content">
        <div class="container">
            <!--card stats start-->
            <div id="card-stats">
                <form action="" name="searchForm" id="searchForm" >
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div class="card" style="padding: 4px;">
                                <div class="row">
                                    <div class="col s12 m6">
                                        <form action="">
                                            <div class="date-fil-con">
                                                <select name="userCondition" id="" class="filter-select browser-default">
                                                    <option value="">Search By</option>
                                                    <option value="userid" <?php
                                                    if ($this->request->query("userCondition") == "userid") {
                                                        echo "selected";
                                                    }
                                                    ?>>Customer ID</option>
                                                    <option value="name" <?php
                                                    if ($this->request->query("userCondition") == "name") {
                                                        echo "selected";
                                                    }
                                                    ?>>Name</option>
                                                    <option value="mobile" <?php
                                                    if ($this->request->query("userCondition") == "mobile") {
                                                        echo "selected";
                                                    }
                                                    ?>>Mobile No</option>
                                                    <option value="email" <?php
                                                    if ($this->request->query("userCondition") == "email") {
                                                        echo "selected";
                                                    }
                                                    ?>>Email ID</option>
                                                </select>
                                            </div>
                                            <div class="date-fil-con">
                                                <input id="getUser_src" placeholder="Enter data..."  name="userData" style="width:75%;"  onkeyup="return getAutolist();" type="text" value="<?php
                                                if ($this->request->query("userData")) {
                                                    echo $this->request->query("userData");
                                                }
                                                ?>" class="filter-date">
                                            </div>                      
<!--                                        <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit">
                                            <i class="mdi-content-forward"></i>
                                        </button>-->
                                    </div>
                                    <div class="col s12 m6 border-l right">
                                        <select name="orderStatus" id="" class="filter-select browser-default">
                                            <option value="">Search By Status</option>
                                            <?php foreach ($status as $key => $val) { ?>
                                            <option value='<?= $key?>'>
                                             <?= $val?>
                                         </option>    
                                         <?php } ?>
                                     </select>
                                 </div>
                             </div>
                             <hr>
                             <div class="row">
                                <div class="col s12 m6">

                                    <div class="date-fil-con">
                                        <label for="orderedDate" class="filter-label">Date</label>
                                        <input type="date" class="orderedDate" style="width:75%;" id="datepicker2" name="orderdate" placeholder="Select Date..." value="<?=$this->request->query("orderdate") ?>" />
                                    </div>
                                    <div class="date-fil-con">
                                        <select name="oredrslot" id="SetSlot_p" class="filter-select browser-default">
                                            <option value="">Slot</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col s12 m6 border-l right">
                                    <select name="city" id="city" class="filter-select browser-default loc-fil"  onchange="getChild(1,this.value);">
                                        <option value="">City</option>
                                        <?php foreach ($city as $k => $val) { ?>
                                        <option value="<?= $val->id; ?>"  <?php
                                            if ($this->request->query("city") == $val->id) {
                                                echo "selected";
                                            }
                                            ?>><?= $val->location_name; ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                    <select name="circle" class="filter-select browser-default loc-fil" id="get_1" onchange="getChild(2,this.value);">
                                        <option value="">Circle</option>
                                        <?php
                                        if (!empty($child['circle'])) {
                                            foreach ($child['circle'] as $key => $val) {
                                                ?>
                                                <option value="<?php echo $key; ?>" <?php
                                                    if ($this->request->query("circle") == $key) {
                                                        echo "selected";
                                                    }
                                                    ?>><?php echo $val; ?> </option> 
                                                    <?php }
                                                } ?>
                                            </select>
                                            <select name="location" class="filter-select browser-default loc-fil" id="get_2" onchange="getChild(3,this.value);">
                                                <option value="">Location</option>
                                                <?php
                                                if (!empty($child['location'])) {
                                                    foreach ($child['location'] as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $key; ?>" <?php
                                                            if ($this->request->query("location") == $key) {
                                                                echo "selected";
                                                            }
                                                            ?>><?php echo $val; ?> </option> 
                                                            <?php }
                                                        } ?>
                                                    </select>
                                                    <select name="area" class="filter-select browser-default loc-fil" id="get_3">
                                                        <option value="">Area</option>
                                                        <?php
                                                        if (!empty($child['area'])) {
                                                            foreach ($child['area'] as $key => $val) {
                                                                ?>
                                                                <option value="<?php echo $key; ?>" <?php
                                                                    if ($this->request->query("area") == $key) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $val; ?> </option> 
                                                                    <?php }
                                                                } ?>
                                                            </select>
                                                            <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit">
                                                                <i class="mdi-content-forward"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m12 l12">
                                                <div class="card" style="padding: 4px;margin: -8px 0 10px 0;">
                                                    <div class="row">
                                                        <div class="col s12 m6">
                                                            <div class="date-fil-con">
                                                                <label for="datepicker2" class="filter-label">From</label>
                                                                <input type="date" class="orderedDate" style="width:75%;" id="datepicker1" <?php if ($this->request->query("from_date")){?> value="<?=$this->request->query("from_date")?>" <?php }?> name="from_date" placeholder="Select Date..."/>
                                                                <!--                                        <input type="date" name="todate" class="filter-date" style="width:75%;">-->
                                                            </div>

                                                            <div class="date-fil-con">
                                                                <label for="datepicker2" class="filter-label">To</label>
                                                                <!--                                        <input type="date" name="fromdate" class="filter-date" style="width:75%;">-->
                                                                <input type="date" class="orderedDate" style="width:75%;" id="datepicker2" <?php if ($this->request->query("to_date")){?> value="<?=$this->request->query("to_date")?>" <?php }?> name="to_date" placeholder="Select Date..."/>
                                                            </div>
                                                            <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit">
                                                                <i class="mdi-content-forward" ></i>
                                                            </button>
                                                            <div class="col s12">
                                                                <span class="ultra-small">
                                                                    <i>
                                                                        <i class="mdi-action-grade amber-text"></i>
                                                                        To show Data between order placed date
                                                                    </i>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <!-- <div class="col s12 m1 right">
                                                            <a class="btn-floating cyan waves-effect waves-light" title="Export to excel">
                                                                <i class="mdi-maps-directions"></i>
                                                            </a>
                                                        </div> -->
                                                        <div class="col s12 m1 right border-l">
                                                            <a class="btn-floating blue lighten-1 waves-effect waves-light" title="Print" onclick="return printorder();">
                                                                <i class="mdi-action-print"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col s12 m12 l12">
                                            <div class="card">
                                                <div class="card-content">
                                                    <span class="card-title activator grey-text text-darken-4">
                                                        <span class="left">
                                                            Order Details
                                                        </span>                                
                                                    </span>                                    
                                                </div>
                                                <div class="row" id="printModule">
                                                    <link href="<?php echo BASE_URL; ?>css/materialize.css" rel="stylesheet" media="print">
                                                    <link href="<?php echo BASE_URL; ?>css/style.css" rel="stylesheet" media="print">
                                                    <link href="<?php echo BASE_URL; ?>css/custom/custom.css" rel="stylesheet" media="print"> 
                                                    
                                                    <?= $this->Html->css("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"); ?>
                                                    <div class="col s12 m12 l12">
                                                        <table class="striped responsive-table" >
                                                            <thead>
                                                                <tr class="white-text">
                                                                    <th class="teal center">Sl. No.</th>
                                                                    <th class="teal center">Order ID</th>
                                                                    <th class="teal center">Status</th>
                                                                    <th class="teal">User Details</th>
                                                                    <th class="teal">Pickup</th>                                        
                                                                    <th class="teal">Delivery</th>
                                                                    <th class="teal center" style="width: 110px;">Amount To Paid</th>                                          
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (count($data) == 0) {
                                                                    echo "<tr><td colspan='7'>NO record found...</td><tr>";
                                                                } else {
                                                                    $i = 1;
                                                                    foreach ($data as $k => $vaa) {
                                                                        ?>
                                                                        <tr id="order-row">                                                
                                                                            <td class="center">
                                                                                <div id="order-action">
                                                                                    <a href="<?= ADMIN_URL."Orders/orderedit/".$vaa['order_id']?>"  class="btn-floating btn-small  green waves-effect waves-light ">    
                                                                                        <i class="mdi-content-create"></i>\
                                                                                    </a>
                                                                                    <a class="btn-floating btn-small blue waves-effect waves-light " href="#" title="Add Wallet" onclick="orderpayment('<?= $vaa['order_id'] ?>','<?= $vaa['user_id'] ?>','<?= $vaa['totalprice'] ?>','<?= $vaa['wallet'] ?>');">
                                                                                        <i class="mdi-action-account-balance-wallet"></i>
                                                                                    </a>
                                                                                </div>       
                                                                                <?= $i; ?>                                            
                                                                            </td>
                                                                            <td class="center">
                                                                                <!--                                                <a data-activates="chat-out" class="waves-effect waves-light chat-collapse" id="clikid"></a>                                               -->
                                                                                <a  href="#" onclick="getorderdetails('<?= $vaa['order_id'] ?>');">                                               
                                                                                    <?= $vaa['order_id']; ?>

                                                                                    <div class="or-stat">
                                                                                        <div class="or-ic or-dn">
                                                                                            <i class="mdi-hardware-phone-iphone white-text" title="Ordered"></i>
                                                                                        </div>
                                                                                        <div class="or-join or-dn"></div>
                                                                                        <div class="or-ic <?php if (($vaa['order_status'] >= 5) && ($vaa['order_status'] != 6)) { ?>or-dn<?php } ?>">
                                                                                            <i class="mdi-maps-local-shipping white-text" title="Picked Up"></i>
                                                                                        </div>
                                                                                        <div class="or-join <?php if (($vaa['order_status'] >= 5) && ($vaa['order_status'] != 6)) { ?>or-dn<?php } ?>"></div>
                                                                                        <div class="or-ic <?php if ($vaa['order_status'] >= 9) { ?>or-dn<?php } ?>">
                                                                                            <i class="mdi-maps-local-laundry-service white-text" title="Cleaned"></i>
                                                                                        </div>
                                                                                        <div class="or-join <?php if ($vaa['order_status'] >= 9) { ?>or-dn<?php } ?>"></div>
                                                                                        <div class="or-ic <?php if (($vaa['order_status'] >= 11) && ($vaa['order_status'] != 12)) { ?>or-dn<?php } ?>">
                                                                                            <i class="mdi-maps-local-mall white-text" title="Delivered"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </td>
                                                                            <td>
                                                                                <a class="dropdown-button block center" id="status<?= $vaa['order_id'] ?>" href="#" data-activates="dropdown<?= $vaa['order_id'] ?>">
                                                                                    <?php echo $status[$vaa['order_status']]; ?>
                                                                                </a>
                                                                                <ul id="dropdown<?= $vaa['order_id'] ?>" class="dropdown-content dp-dwn">
                                                                                    <?php foreach ($status as $key => $val) { ?>
                                                                                    <li>
                                                                                        <a href="#" onclick="orderStatusChange(<?= $key; ?>,'<?= $vaa['order_id'] ?>','<?=$val?>');" class="-text"><?php echo $val; ?></a>
                                                                                    </li>    
                                                                                    <?php } ?>
                                                                                </ul>
                                                                            </td>
                                                                            <td> 
                                                                                <span class="blue-text text-darken-3">
                                                                                    <i class="mdi-social-person"></i> <?= $vaa['user_id'] ?>
                                                                                </span><br>
                                                                                <span class="teal-text text-darken-3">
                                                                                    <i class="mdi-social-person"></i> <b><?= $vaa['user_name'] ?></b>
                                                                                </span><br>
                                                                                <span class="green-text text-darken-1">
                                                                                    <i class="mdi-hardware-smartphone"></i> <?= $vaa['mobile'] ?>
                                                                                </span><br>
                                                                                <span class="blue-text text-darken-1">
                                                                                    <i class="mdi-communication-email"></i> <?= $vaa['email'] ?>        
                                                                                </span>
                                                                            </td>
                                                                            <td>
                                                                                <a class="waves-effect waves-blue block grey-text text-darken-1 modal-trigger" href="#modal<?= $vaa['order_id'] ?>">                                   
                                                                                    <span class="cyan-text text-darken-1">
                                                                                        <i class="mdi-action-event"></i> <b><?= $vaa['pickupdate'] ?></b>
                                                                                    </span> 
                                                                                    <br>
                                                                                    <span>
                                                                                        <i class="mdi-maps-pin-drop"></i> <?= $vaa['pickaddress'] ?>,<?= $vaa['pickarea'] ?>
                                                                                    </span>
                                                                                    <br>
                                                                                    <span>
                                                                                        <i class="mdi-action-alarm"></i> <?= $vaa['pickupslot'] ?>
                                                                                    </span>
                                                                                </a>
                                                                                <div id="modal<?= $vaa['order_id'] ?>" class="modal">
                                                                                    <div class="modal-content">
                                                                                        <h6>
                                                                                            Pickup Details                                                        
                                                                                        </h6>
                                                                                        <a href="#" class="waves-effect waves-red btn-floating btn-small grey lighten-3 right modal-action modal-close right" style="margin: -48px -20px 0 0px;">
                                                                                            <i class="mdi-content-clear red-text"></i>
                                                                                        </a>
                                                                                        <div class="col s12">
                                                                                            <p>
                                                                                                <b>Pickup Date:</b> <?= $vaa['pickupdate'] ?> <br>
                                                                                                <b>Pickup Slot:</b> <?= $vaa['pickupslot'] ?> <br>
                                                                                                <b>Address:</b> <?= $vaa['pickaddress'] ?>,<?= $vaa['pickarea'] ?>,<?= $vaa['picklocation'] ?>,<?= $vaa['pickcircle'] ?>,<?= $vaa['pickcity'] ?><br>
                                                                                                <!--                                                                <b>Pickup By:</b> Arron Taylor-->

                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <a class="waves-effect waves-blue block grey-text text-darken-1 modal-trigger" href="">                                   
                                                                                    <span class="cyan-text text-darken-3">
                                                                                        <i class="mdi-action-event"></i> <b><?= $vaa['deliverydate'] ?></b>
                                                                                    </span> 
                                                                                    <br>
                                                                                    <span>
                                                                                        <i class="mdi-maps-pin-drop"></i> <?= $vaa['deliaddress'] ?>,<?= $vaa['delikarea'] ?>
                                                                                    </span>
                                                                                    <br>
                                                                                    <span>
                                                                                        <i class="mdi-action-alarm"></i><?= $vaa['deliveryslot'] ?>
                                                                                    </span>
                                                                                </a>
                                                                            </td>
                                                                            <td class="center">
                                                                                <div class="chip teal white-text">
                                                                                    <i style="font-size: 14px"> &#8377;</i> <?= $vaa['totalprice'] ?>                     
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                        $i++;
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>                        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--card stats end-->
                            </div>
                        </section>
                        <!-- Modal for reschedule -->
                        <div id="modal_reschedule" class="modal" style="min-height:535px;">
                            <div class="modal-content">
                                <form action="<?=ADMIN_URL?>orders/reschdule" method="post">
                                    <div class="row">
                                        <div class="col s12 m8 offset-m2">
                                            <div class="input-field">
                                                <label for="datepicker3" class="filter-label">Date</label>
                                                <input type="date" class="datepicker_new"  name="orderdate_new" placeholder="Select Date..."/>
                                            </div>
                                            <div class="input-field">
                                                <select name="oredrslot" id="SetSlot_d" class="filter-select browser-default">
                                                    <option value="">Slot</option>
                                                </select>
                                            </div>
                                            <input type="hidden" value="" name="type" id="type">
                                            <input type="hidden" value="" name="orderid" id="orderid">
                                        </div>
                                        <div class="col s12 center">
                                            <p>&nbsp;</p>
                                            <a href="#" class="waves-effect waves-red btn grey modal-action modal-close">Cancel</a>
                                            <button type="submit" class="waves-effect waves-green btn green modal-action">Save</button>
                                        </div>
                                    </div>  
                                </form>
                                <div class="modal-footer center">

                                </div>
                            </div>
                        </div>
                        <!-- Modal for reschedule end -->
                        <!-- Modal for payment Start -->
                        <div id="modal_payment" class="modal">
                            <div class="modal-content">
                                <form action="<?=ADMIN_URL?>orders/payment" method="post">
                                    <div class="row">
                                        <div class="col s12 m8 offset-m2">
                                            <div class="col s6">
                                                <p>
                                                    <span class="ultra-small cyan-text">Order Price</span></br>
                                                    <span id="ordPrc">Rs. 500.00</span>
                                                </p>
                                            </div>
                                            <div class="col s6">
                                                <p>
                                                    <span class="ultra-small cyan-text">Wallet Amount</span></br>
                                                    <span id="waletamnt">Rs. 500.00</span>
                                                </p>
                                            </div>
                                            <div class="col s6">
                                                <p>
                                                    <input id="paidPrc" type="text" name="amountPaid"/>
                                                    <label for="paidPrc">Given Amount</label>
                                                </p>
                                            </div>
                                            <div class="col s6">
                                                <p>
                                                    <input type="checkbox" id="walletAccept" name="fromwallet" value='1'>
                                                    <label for="walletAccept">Payment From Wallet</label>
                                                </p>
                                            </div>
                                            <input type="hidden" id="cusTmerid" name="cusTmerid">
                                            <input type="hidden" id="ORder_ID" name="ORder_ID">
                                            <input type="hidden" id="totalPrice" name="totalPrice">
                                        </div>
                                        <div class="col s12 center">
                                            <p>&nbsp;</p>
                                            <a href="#" class="waves-effect waves-red btn grey modal-action modal-close">Cancel</a>
                                            <button type="submit" class="waves-effect waves-green btn green modal-action">Save</button>
                                        </div>
                                    </div>  
                                </form>
                                <div class="modal-footer center">
                                </div>
                            </div>
                        </div>
                        <!-- Modal for payment end -->
                        <!-- START RIGHT SIDEBAR NAV-->
                        <aside id="right-sidebar-nav">
                            <ul id="chat-out" class="">

                            </ul>
                        </aside>
<!-- LEFT RIGHT SIDEBAR NAV-->