<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Add Order</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Add Order</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <!--card stats start-->
        <div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col m12 s12">
                                    <h6 class="left teal-text">
                                        Search User
                                    </h6>  
                                    <div class="col m12">
                                        <form action="" method="get" onsubmit="return searchValidate();">
                                            <div class="col m5">
                                                <div class="input-field">
                                                    <select name="userCondition" id="userCondition" class="validate" >
                                                        <option value="all" disabled="" selected="">Search By...</option>
                                                        <option value="userid" <?php
if ($this->request->query("userCondition") == "userid") {
    echo "selected";
}
?>>Customer ID
                                                        </option>
                                                        <option value="name" <?php
                                                                if ($this->request->query("userCondition") == "name") {
                                                                    echo "selected";
                                                                }
?>>Customer Name
                                                        </option>
                                                        <option value="mobile" <?php
                                                                if ($this->request->query("userCondition") == "mobile") {
                                                                    echo "selected";
                                                                }
?>>Mobile No
                                                        </option>
                                                        <option value="email" <?php
                                                                if ($this->request->query("userCondition") == "email") {
                                                                    echo "selected";
                                                                }
?>>Email ID
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col m5">
                                                <div class="input-field">
                                               <!--  onblur="AutoSearch('<?php $this->request->query("userCondition"); ?>',this.value);" -->
                                                    <input id="getUser_src"  name="getUser_src" type="text" value="<?php
                                                                if ($this->request->query("getUser_src")) {
                                                                    echo $this->request->query("getUser_src");
                                                                }
?>" class="validate">
                                                    <label for="getUser_src" class="">Enter Data...</label>
                                                </div>
                                            </div>
                                            <div class="col m2">
                                                <br>
                                                <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit">
                                                    <i class="mdi-content-forward"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form action="<?= ADMIN_URL ?>Orders/insertorderdata" method="post" id="FormorderData">
                <input type="hidden" value="<?php
                                                           if (!empty($userData['userid'])) {
                                                               echo $userData['userid'];
                                                           } else {
                                                               echo 0;
                                                           }
?>" id="user_id" name="user_id"/>
                       <?php if (!empty($userData)) { ?>
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="row">
                                        <div class="col m12 s12">
                                            <h5 class="left amber-text">
                                                User Details
                                            </h5>
                                            <p>

                                            </p>
                                            <div class="col s12">
                                                <div class="col s12 m6">
                                                    <p class="">
                                                        <i class="mdi-social-person cyan-text text-darken-2"></i> <?= $userData['name']; ?>
                                                    </p>
                                                    <p class="">
                                                        <i class="mdi-action-perm-identity cyan-text text-darken-2"></i> <span id="userid"><?= $userData['userid']; ?></span>
                                                    </p>
                                                </div>
                                                <div class="col s12 m6">
                                                    <p class="">
                                                        <i class="mdi-communication-email cyan-text text-darken-2"></i> 
                                                        <?= $userData['email']; ?>
                                                    </p>
                                                    <p class="">
                                                        <i class="mdi-action-perm-identity cyan-text text-darken-2"></i> <?= $userData['mobile']; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="card ">
                            <div class="card-content">
                                <span class="card-title cyan-text">Pickup Details</span>
                                <div class="row">
                                    <div class="col s12">
                                        <span>Address</span>
                                        <?php
                                        if (isset($userData)) {
                                            if (!empty($userData['address_heading'])) {
                                                foreach ($userData['address_heading'] as $k => $v) {
                                                    ?>
                                                    <a href="javascript:void(0);" onclick="select_address(<?= $v['id']; ?>,'pickupadd');"><input type="radio" name="pickupadd" <?php
                                        if ($v['set_default'] == 1) {
                                            echo "checked";
                                        } else {
                                            echo "";
                                        }
                                                    ?> id="pickupadd<?= $v['id']; ?>" value="<?= $v['id']; ?>"/>
                                                        <div class="chip <?php
                                                                                                                                     if ($v['set_default'] == 1) {
                                                                                                                                         echo "cyan";
                                                                                                                                     } else {
                                                                                                                                         echo "grey";
                                                                                                                                     }
                                                    ?> white-text pickupadd"  id="paddRess1_<?= $v['id']; ?>">
                                                            <i class="mdi-communication-location-on"></i>
                                                            <?= $v['add_heading']; ?>
                                                        </div>
                                                    </a>
                                                    <input type="hidden" name="pickup" id="pickup" value="<?= $v['id']; ?>">
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <a href="<?= ADMIN_URL; ?>users/address/<?= base64_encode($userData['userid']); ?>/<?= base64_encode($userData['name']); ?>" class="waves-effect waves-light btn-floating btn-medium blue right" >Add</a>
                                            <?php }
                                        } ?>
                                    </div>
                                    <div class="input-field col s12">
                                        <i class="mdi-action-event prefix cyan-text"></i>
                                        <input type="date" class="datepicker2" id="datepicker2" name="pickupdate" placeholder="Select Pickup Date...">
                                    </div>
                                    <div class="input-field col s12">
                                        <div class="row">
                                            <div class="col s1">
                                                <i class="mdi-action-alarm prefix cyan-text" style="position:relative;"></i>
                                            </div>
                                            <div class="col s11">
                                                <select name="pickupslot" class="browser-default " id="SetSlot_p">
                                                    <!--                                                    err  class for error-->
                                                    <option value="">Select Slot</option>
                                                </select>
                                            </div>
                                        </div>                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="card ">
                            <div class="card-content">
                                <span class="card-title green-text">Delivery Details</span>
                                <div class="row">
                                    <div class="col s12">
                                        <span>Address</span>
                                        <?php
                                        if (isset($userData)) {
                                            if (!empty($userData['address_heading'])) {
                                                foreach ($userData['address_heading'] as $k => $v) {
                                                    ?>
                                                    <a href="javascript:void(0);" onclick="select_address(<?= $v['id']; ?>,'deliveryadd');"><input type="radio" name="deliveryadd" <?php
                                        if ($v['set_default'] == 1) {
                                            echo "checked";
                                        } else {
                                            echo "";
                                        }
                                                    ?> value="<?= $v['id']; ?>" id="deliveryadd<?= $v['id']; ?>"/>
                                                        <div class="chip <?php
                                                                                                                                       if ($v['set_default'] == 1) {
                                                                                                                                           echo "green";
                                                                                                                                       } else {
                                                                                                                                           echo "grey";
                                                                                                                                       }
                                                    ?> white-text deliveryadd" <?php
                                                 if ($v['set_default'] == 1) {
                                                     echo "checked";
                                                 } else {
                                                     echo "checked";
                                                 }
                                                    ?> id="paddRess2_<?= $v['id']; ?>">
                                                            <i class="mdi-communication-location-on"></i>
                                                            <?= $v['add_heading']; ?>
                                                        </div></a>
                                                    <?php
                                                }
                                            } else {
                                                ?>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="input-field col s12">
                                        <i class="mdi-action-event prefix green-text"></i>
                                        <input type="date" class="datepicker3" id="datepicker3" name="deliverydate" placeholder="Select Delivery Date..."/>
                                    </div>
                                    <div class="input-field col s12">
                                        <div class="row">
                                            <div class="col s1">
                                                <i class="mdi-action-alarm prefix green-text" style="position:relative;"></i>
                                            </div>
                                            <div class="col s11">
                                                <select name="deliveryslot" class="browser-default" id="SetSlot_d">
                                                    <option value="">Select Slot</option>
                                                </select>
                                            </div>
                                        </div>                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <h5 class="grey-text">
                            Add Orders Here
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m9 l9">
                        <div class="col s12">
                            <ul class="tabs tab-demo z-depth-1">
                                <?php $i = 1;
                                foreach ($allcloth as $val) { ?>
                                    <li class="tab col s2">
                                        <a class="<?php
                                if ($i == 1) {
                                    echo 'active';
                                }
                                    ?>" href="#test<?php echo $i; ?>"><?php echo h($val->cloth_name); ?></a>
                                    </li>
                                    <?php $i++;
                                } ?>
                            </ul>
                        </div>
                        <div class="col s12">
                            <?php
                            $i = 1;
                            foreach ($allcloth as $val) {
                                // pr($allcloth);exit;
                                ?>
                                <div id="test<?php echo $i; ?>" class="col s12 grey lighten-3">
                                    <div class="row">
                                        <ul class="collapsible collapsible-accordion popout" data-collapsible="accordion">
                                            <?php
                                            if (!empty($newcloth[$val->id])) {
                                                foreach ($newcloth[$val->id] as $valnew) {
                                                    if ($valnew['cloth_parent'] == $val->id) {
                                                        ?>
                                                        <li>
                                                            <div class="collapsible-header active">
                                                                <i class="mdi-navigation-arrow-forward"></i> <?php echo h($valnew['cloth_name']); ?>
                                                            </div>
                                                            <div class="collapsible-body">
                                                                <ul class="collection with-header">
                                                                    <li class="collection-header grey lighten-2">
                                                                        <h6 class="condensed bold grey-text text-darken-3">ASSIGNED ITEMS</h6>
                                                                    </li>
                                                                    <?php
                                                                    if (!empty($Childcloth[$valnew['id']])) {
                                                                        // pr($Childcloth);exit;
                                                                        foreach ($Childcloth[$valnew['id']] as $valnnew) {
                                                                            ?>
                                                                            <li class="collection-item">
                                                                                <div class="row">
                                                                                    <div class="col s4">
                                                                                        <i class="mdi-action-star-rate"></i> <?= h($valnnew['cloth_name']) ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col s12">
                                                                                    <br>
                                                                                    <ul class="collection with-header">
                                                                                        <?php if (!empty($pricearr[$valnnew['id']])) {
                                                                                            foreach ($pricearr[$valnnew['id']] as $prci) { ?>
                                                                                                <li class="collection-item">
                                                                                                    <div class="price-pic center">
                                                                                                        <img src="<?= BASE_URL; ?>thumbimg/<?= $prci['sicon']; ?>" height="30" width="30" class="circle"/>
                                                                                                    </div>
                                                                                                    <div class="price-elements">
                                                                                                        <span class="task-cat cyan">
                                                                                                            <?= h($prci['wastype']) ?>
                                                                                                        </span>
                                                                                                    </div>
                                                                                                    <div class="price-elements">
                                                                                                        <span  class="yellow-text text-darken-4"> Rs.<span id="pricespan_<?= $prci['price_id']; ?>"><?= h($prci['price']) ?></span>/-</span>
                                                                                                    </div>
                                                                                                    <div class="price-rate">
                                                                                                        <div class="input-field">
                                                                                                            <input id="remarks" name="" type="text" onblur="remarkAdd(<?= $prci['price_id']; ?>,<?= $prci['wash_id']; ?>,<?= $prci['cloth_id']; ?>,<?= $prci['price']; ?>,0,this.value);" class="validate">
                                                                                                            <label for="remarks" class="">Remarks</label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="price-elements">
                                                                                                        <button type="button" onclick="AddClothForWash(<?= $prci['price_id']; ?>,<?= $prci['wash_id']; ?>,<?= $prci['cloth_id']; ?>,<?= $prci['price']; ?>,2);" class="waves-effect waves-light btn-floating btn-small red right"> 
                                                                                                            <i class="mdi-content-remove"></i>
                                                                                                        </button>
                                                                                                        <span class="orange-text text-darken-3 task-cat grey lighten-3 right" id="quantity<?= $prci['price_id']; ?>">
                                                                                                            0
                                                                                                        </span>
                                                                                                        <input type="hidden" value="0" id="clothQuantity_<?= $prci['price_id']; ?>">  
                                                                                                        <button type="button" onclick="AddClothForWash(<?= $prci['price_id']; ?>,<?= $prci['wash_id']; ?>,<?= $prci['cloth_id']; ?>,<?= $prci['price']; ?>,1);" class="waves-effect waves-light btn-floating btn-small green darken-2 right"> 
                                                                                                            <i class="mdi-content-add"></i>
                                                                                                        </button> 
                                                                                                        <span id="addcloth<?= $prci['price_id']; ?>">

                                                                                                        </span>
                                                                                                        <span id="remark<?= $prci['price_id']; ?>">
                                                                                                            <input type='hidden' id='remark_input<?= $prci['price_id']; ?>' value="<?php if (!empty($prci['remark']))
                                                                                echo $prci['remark']; ?>"/>
                                                                                                        </span>                            
                                                                                                    </div>

                                                                                                </li>
                                                                                            <?php }
                                                                                        } ?>
                                                                                    </ul>
                                                                                    <br>
                                                                                </div>
                                                                            </li>
                                                                        <?php }
                                                                    } ?>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <?php $i++;
                            } ?>

            <!-- <span id="addcloth<?= $prci['price_id']; ?>">

            </span>
            <span id="remark<?= $prci['price_id']; ?>">
                <input type='hidden' id='remark_input<?= $prci['price_id']; ?>' value=""/>
            </span> -->
                            <input type="hidden" value="0.00" name="totalprice" id="total_price"/>
                            <input type="hidden" value="0.00" name="order_price" id="order_price"/>
                            <input type="hidden" value="0.00" name="discount_amount" id="discount_amount"/>
                            <input type="hidden" value="0" name="" id="total_quantity"/>





                        </div>
                        <div class="col s12">
                            <div class="col s12 grey lighten-3">
                                <div class="col s7">
                                    <div class="input-field">
                                        <input id="remarks" type="text"  name="remarknew" class="validate">
                                        <label for="remarks" class="">Remarks</label>
                                    </div>
                                </div>
                                <?php //if(!empty($this->request->query('userCondition'))){ $slug=1;}   ?>
                                <button class="btn-floating waves-effect waves-light cyan right" onclick="return validateOrder();" type="button"> 
                                    <i class="mdi-content-save"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m3 l3">
                        <div id="flight-card" class="card">
                            <div class="card-header cyan">
                                <div class="card-title">
                                    <h5 class="center white-text">
                                        <span id="totalprice">Total Price Rs. 0/-</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="card-content grey lighten-4">
                                <div class="row">
                                    <div class="col s12">
                                        <h6 class="condensed bold grey-text text-darken-3">Total Quantity: <span id="totalquantity">0</span>pc</h6>
                                        <!-- <div class="input-field">
                                            <input id="c_code" type="text" class="validate" name="coupon_code">
                                            <label for="c_code" class="">Coupon Code</label>
                                        </div>
                                        <div class="input-field center">
                                            <button class="btn waves-effect waves-light teal" type="button" onclick="return validatecouponchk();">Add Coupon</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--card stats end-->
    </div>
</section>
<script>
    var order_price=0;
    var total_quantity=0;
    $("#totalprice").text("Total Price Rs."+order_price+"/-");
    /*
    //buttonType : 1 -> Add, 2 -> Reduce
     */
    function AddClothForWash(price_id,wash_id,cloth_id,price,buttonType)
    {
        // alert(price_id);
        var remark=$("#remark_input"+price_id).val();
        if(buttonType==1){
            var value = parseInt(document.getElementById('quantity'+price_id).innerHTML, 10);
            value = isNaN(value) ? 0 : value;
            value = value + 1;
            total_quantity=total_quantity+1;
            $("#total_quantity").val(total_quantity);
            document.getElementById('quantity'+price_id).innerHTML = value; //class='cloth_info"+i+"'
            $("#clothQuantity_"+price_id).val(value);
            $("#addcloth"+price_id).html("<input type='hidden' id='addcloth"+price_id+"' name='cloth_info[]' value='"+wash_id+":@:"+cloth_id+":@:"+price+":@:"+value+":@:"+remark+"'>");
            order_price=order_price+price;
            // alert(order_price);
            $("#order_price").val(order_price);
            $("#order_price").val(order_price);
            $("#totalquantity").text(total_quantity);
            $("#totalprice").text("Total Price Rs."+order_price+"/-");
            $("#addcloth"+price_id).attr("style","display:block");
        } else if(buttonType==2){
            var value = parseInt(document.getElementById('quantity'+price_id).innerHTML, 10);
            if(value>0){
                value = isNaN(value) ? 0 : value;
                value = value - 1;
                document.getElementById('quantity'+price_id).innerHTML = value;
                order_price=order_price-price;
                total_quantity=total_quantity-1;
                $("#total_quantity").val(total_quantity);
                $("#order_price").val(order_price);
                $("#order_price").val(order_price);
                $("#totalquantity").text(total_quantity);
                $("#totalprice").text("Total Price Rs."+order_price+"/-");
                $("#addcloth"+price_id).html("<input type='hidden' id='addcloth"+price_id+"' name='cloth_info[]' value='"+wash_id+":@:"+cloth_id+":@:"+price+":@:"+value+":@:"+remark+"'>");
                if(value==0){
                    $("#addcloth"+price_id).attr("style","display:none");
                }
            }                                   
        } 
        else 
        {
            var value = parseInt(document.getElementById('quantity'+price_id).innerHTML, 10);
            $("#addcloth"+price_id).html("<input type='hidden' id='addcloth"+price_id+"' name='cloth_info[]' value='"+wash_id+":@:"+cloth_id+":@:"+price+":@:"+value+":@:"+remark+"'>");
            if(value==0)
            {
                $("#addcloth"+price_id).attr("style","display:none");
            }
        }
    }
    
    function validateOrder(){
        if(validate()){
            $("#FormorderData").submit();
        }

    }
    function validate(){ 
        if(searchValidate()){
            var pickupadd=$("#pickup").val();
            var pickup=$("#datepicker3").val();
            var pickupslot=$("#SetSlot_p").val();
            var deliveryslot=$("#SetSlot_d").val();
            var hiddenUserId=$("#user_id").val();
            if(pickupadd == undefined || pickupadd ==''){
                alert("Please add the address for the user");
                return false;
            }else if(pickup==0){
                alert("Please Choose the pickup Date");
                return false;
            } else if(pickupslot==0 && deliveryslot==0){
                return false;
            } else if(hiddenUserId==0){
                alert("Please Search the User");
                return false;   
            }else {
                return true;
            }
        }
    }
    function searchValidate(){
        var userCondition= $("#userCondition").val();
        var getUser_src=$("#getUser_src").val();
        if(userCondition==null){
            alert("Please Find a User");
            $("#userCondition").focus();
            return false;
        } else if(getUser_src==0){
            alert("Please Enter "+userCondition );
            $("#getUser_src").focus();
            return false;

        }
        else{
            return true;
        }
    }
    function Datablank(){
        $("#getUser_src").val("");
    }
    function remarkAdd(price_id,wash_id,cloth_id,price,buttonType,remark){
        $("#remark"+price_id).html("<input type='hidden' id='remark_input"+price_id+"' value="+remark+">");
        AddClothForWash(price_id,wash_id,cloth_id,price,buttonType);
        
    }
    
</script>


