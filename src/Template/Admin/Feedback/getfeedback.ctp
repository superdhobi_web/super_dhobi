 <?php
                        foreach ($alldata as $ad) {
                            if ($ad->feed_by == 'user') {
                                ?>
                                <div class="email-content-wrap">
                                    <div class="row">
                                        <div class="col s10 m10 l10">
                                            <ul class="collection">
                                                <li class="collection-item avatar">
                                                    <span class="circle cyan"><?= ucfirst(substr($data['name'], 0, 1)) ?></span>
                                                    <span class="email-title"><?= $data['name'] ?> <span class="ultra-small">(<?= $ad['order_id'] ?>)</span></span>
                                                    <p class="truncate grey-text ultra-small">To Administrator</p>
                                                    <p class="grey-text ultra-small"><?= date("jS M,Y", strtotime($ad['add_date'])) ?>  <i><?= date("h:i a", strtotime($ad['add_date'])) ?></i></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="email-content">
                                        <p><?= ucfirst($ad['reason']) ?></p>
                                    </div>
                                </div>
                                <hr>
                            <?php } else { ?>
                                <div class="email-content-wrap">
                                    <div class="row">
                                        <div class="col s10 m10 l10">
                                            <ul class="collection">
                                                <li class="collection-item avatar">
                                                    <img src="<?= $this->request->webroot; ?>images/avatar.jpg" alt="Admin" class="circle">
                                                    <span class="email-title cyan-text">Superdhobi (admin)</span>
                                                    <p class="truncate grey-text ultra-small">To <?= $data['name'] ?></p>
                                                    <p class="grey-text ultra-small"><?= date("jS M,Y", strtotime($ad['add_date'])) ?>  <i><?= date("h:i a", strtotime($ad['add_date'])) ?></i></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="email-content">
                                        <p><?= ucfirst($ad['reason']) ?></p>
                                    </div>
                                </div>
                            <?php }
                        } ?>
                        <div class="email-reply">
                            <div class="row">
                                <div class="col s12 right-align">
                                    <a href="#" onclick="opendatamodal('<?= $alldata[0]['order_id'] ?>','<?= $data['userid'] ?>','<?= $data['name'] ?>');" class="btn green white-text waves-effect waves-light">
                                        Reply
                                    </a>
                                </div>
                            </div>
                        </div>