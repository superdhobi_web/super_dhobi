<?php 
?>
<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">All Feedbacks</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Feedbacks</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <div id="mail-app" class="section">
            <div class="row">
                <div class="col s12">
                    <nav class="teal">
                        <div class="nav-wrapper">
                            <div class="left col s6">
                                <ul>
                                    <li>
                                        <i class="mdi-communication-chat"></i>
                                    </li>
                                    <li>
                                        <span class="white-text">
                                            Feedbacks
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div class="col s6">
                                <div class="paginator">
                                    <ul class="pagination right white-text">
                                        <?= $this->Paginator->prev('<i class="mdi-hardware-keyboard-arrow-left white-text"></i>',array('escape' => false)) ?>
                                        <?= $this->Paginator->next('<i class="mdi-hardware-keyboard-arrow-right white-text"></i>',array('escape' => false)) ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="col s12">
                    <div id="email-list" class="col s4 card-panel z-depth-1">
                        <ul class="collection">
                            <?php
                            foreach ($data as $val) {
                                $rating = (int) $val['rating'];
                                ?>
                                <li class="collection-item avatar email-unread" onclick="getfeedback('<?= $val['order_id'] ?>')">
                                    <span class="circle cyan"><?= ucfirst(substr($val['user'], 0, 1)) ?></span>
                                    <span class="email-title text-capitalize">
                                        <?= $val['user'] ?> 
                                    </span>
                                    <p class="truncate grey-text text-darken-2 ultra-small">
                                        <i>
                                            (<?= $val['order_id'] ?>)
                                        </i>
                                    </p>
                                    <p class="truncate grey-text ultra-small text-dott">
                                        <?= $val['reason'] ?>
                                    </p>
                                    <span class="secondary-content email-time">
                                        <div class="rating">
                                            <?php if ($rating == 1) { ?>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                            <?php } else if ($rating == 2) { ?> 
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                            <?php } else if ($rating == 3) { ?>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                            <?php } else if ($rating == 4) { ?>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                            <?php } else if ($rating == 5) { ?>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                            <?php } ?>
                                        </div>   
                                    </span>
                                </li>
                            <?php } ?>
                        </ul>
                       
                    </div>

                    <div id="email-details" class="col s8 card-panel">
                        <?php
                        foreach ($alldata as $ad) {
                            if ($ad->feed_by == 'user') {
                                ?>
                                <div class="email-content-wrap">
                                    <div class="row">
                                        <div class="col s10 m10 l10">
                                            <ul class="collection">
                                                <li class="collection-item avatar">
                                                    <span class="circle cyan"><?= ucfirst(substr($data[0]['user'], 0, 1)) ?></span>
                                                    <span class="email-title"><?= $data[0]['user'] ?> <span class="ultra-small">(<?= $data[0]['order_id'] ?>)</span></span>
                                                    <p class="truncate grey-text ultra-small">To Administrator</p>
                                                    <p class="grey-text ultra-small"><?= date("jS M,Y", strtotime($ad['add_date'])) ?>  <i><?= date("h:i a", strtotime($ad['add_date'])) ?></i></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="email-content">
                                        <p><?= ucfirst($ad['reason']) ?></p>
                                    </div>
                                </div>
                                <hr>
                            <?php } else { ?>
                                <div class="email-content-wrap">
                                    <div class="row">
                                        <div class="col s10 m10 l10">
                                            <ul class="collection">
                                                <li class="collection-item avatar">
                                                    <img src="<?= $this->request->webroot; ?>images/avatar.jpg" alt="Admin" class="circle">
                                                    <span class="email-title cyan-text">Superdhobi (admin)</span>
                                                    <p class="truncate grey-text ultra-small">To <?= $data[0]['user'] ?></p>
                                                    <p class="grey-text ultra-small"><?= date("jS M,Y", strtotime($ad['add_date'])) ?>  <i><?= date("h:i a", strtotime($ad['add_date'])) ?></i></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="email-content">
                                        <p><?= ucfirst($ad['reason']) ?></p>
                                    </div>
                                </div>
                            <?php }
                        } ?>
                        <div class="email-reply">
                            <div class="row">
                                <div class="col s12 right-align">
                                    <a href="#" onclick="opendatamodal('<?= $data[0]['order_id'] ?>','<?= $data[0]['userid'] ?>','<?= $data[0]['user'] ?>');" class="btn green white-text waves-effect waves-light">
                                        Reply
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Compose Email Structure -->
            <div id="modal-rep" class="modal">
                <div class="modal-content">
                    <nav class="teal lighten-1">
                        <div class="nav-wrapper">
                            <div class="left col s12 m5 l5">
                                <ul>
                                    <li><a href="#!" class="email-menu"><i class="modal-action modal-close  mdi-hardware-keyboard-backspace"></i></a>
                                    </li>
                                    <li>
                                        <span id="replyto"></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="model-email-content">
                    <div class="row">
                        <form class="col s12" method="post" action="<?= ADMIN_URL; ?>feedback/feedbackreply">
                            <div class="row">
                                <input type="hidden" value="" id="userid" name="userid">
                                <input type="hidden" value="" id="orderid" name="order_id">
                                <div class="input-field col s12">
                                    <textarea id="compose" class="materialize-textarea" name="reason" required length="500"></textarea>
                                    <label for="compose">Compose Message</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 right-align">
                                    <button class="btn cyan waves-effect waves-light">Send</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
