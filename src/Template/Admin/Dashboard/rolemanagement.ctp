<section id="content">        
  <!--breadcrumbs start-->
  <div id="breadcrumbs-wrapper">
    <!-- Search for small screen -->
    <div class="header-search-wrapper grey hide-on-large-only">
      <i class="mdi-action-search active"></i>
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
    </div>
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Role Management</h5>
          <ol class="breadcrumbs">
            <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
            <li class="active">Role Management</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--breadcrumbs end--> 
  <div class="container">
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="card">
                <div class="card-content filter" style="padding: 8px 15px;">
                  <div class="row">
                    <div class="col m12 s12">
                      <h6 class="left teal-text">
                        Search
                      </h6>  
                      <div class="col m12">
                       <div class="row">
                        <form method="" action="" id="">
                        <div class="col m8 s6">
                          <div class="input-field col s12">
                              <select>
                                <option value="" disabled selected>Select Employee</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                              </select>
                          </div>
                       </div> 
                       <div class="col m3 s3">
                        <br>
                        <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit" value="search">
                          <i class="mdi-content-forward"></i>
                        </button>
                      </div>                                         
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="search-content">
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="card">
                <div class="card-content filter" style="padding: 8px 15px;">
                  <div class="row">
                    <div class="col m12 s12">
                      <h6 class="left teal-text">
                        Details
                      </h6>  
                      <div class="col m12">
                       <div class="row rm-details">
                          <div class="col s4">
                            <p><b><i class="mdi-action-account-circle"></i> Name:</b> John Doe</p>
                          </div>
                          <div class="col s4">
                            <p><b><i class="mdi-toggle-radio-button-on"></i> Id:</b> sd00001</p>
                          </div>
                          <div class="col s4">
                            <p><b><i class="mdi-action-stars"></i> Post:</b> Lorem Ipsum</p>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="card">
                <div class="card-content filter" style="padding: 8px 15px;">
                  <div class="row">
                    <div class="col m12 s12">
                      <h6 class="left teal-text">
                        Roles
                      </h6>  
                      <div class="col m12">
                       <div class="row">
                          <table class="striped" style="margin-top: 17px;">
                            <thead>
                              <tr class="white-text">
                                <th data-field="" class="teal">Name/modules</th>
                                <th data-field="" class="teal">View</th>
                                <th data-field="" class="teal">Add</th>
                                <th data-field="" class="teal">Update</th>
                                <th data-field="" class="teal">Delete</th>
                                <th data-field="" class="teal">All</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td><p>Cloths</p></td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm1" />
                                    <label for="rm1"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm2" />
                                    <label for="rm2"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm3" />
                                    <label for="rm3"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox" id="rm4" />
                                    <label for="rm4"></label>
                                  </p>
                                </td>
                                <td>
                                  <p>
                                    <input type="checkbox"  class="" checked="checked"  id="rm5" />
                                    <label for="rm5"></label>
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
						 <div class="col m12" style="margin: 20px 0;">
						 	<button type="button" class="btn waves-effect waves-light cyan darken-2 pull-right">Save</button>
						 </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>





  </div><!--search-content-->
</div>
</section>
				 