<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Admin Services</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Admin Services</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div>
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    Change Password
                                </span>                                
                            </span>
                            <form class="col s12 m8 offset-m2 right-alert" id="password_change" action=<?= ADMIN_URL . "dashboard/changepassword"?> method="post" onsubmit="return validatepassword();">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <i class="mdi-hardware-keyboard prefix"></i>
                                        <input id="user_id"  type="hidden" name="userid" value="<?= $userid;?>"/>
                                        <input id="oldpassword" class="validate" type="text" name="oldpassword"/>
                                        <label for="oldpassword" >Current Password</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <i class="mdi-action-verified-user prefix"></i>
                                        <input id="newpassword" class="validate" type="text" name="newpassword" onfocus="resetpassword(<?= $userid;?>);"/>
                                        <label for="newpassword" >New Password</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <i class="mdi-action-verified-user prefix"></i>
                                        <input id="confirmpassword" class="validate" type="text" name="password"/>
                                        <label for="confirmpassword" data-error="Please enter password." data-success="Perfect!">Confirm Password</label>
                                        <span id="err_msg"></span>
                                    </div>
                                </div>
                                <div class="g-recaptcha" data-sitekey="6Lf0RSETAAAAAHoCuILPZ3qiiNTs4ayJ1mwIWlV1"></div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <div class="pass_error red-text"></div>
                                        <button class="btn green waves-effect waves-light right" name="save" type="submit">Save
                                            <i class="mdi-content-send right"></i>
                                        </button>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src='https://www.google.com/recaptcha/api.js'></script>