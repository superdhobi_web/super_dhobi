<!-- START CONTENT -->
<?php
$status = array(
    '1' => 'Order Placed',
    '2' => 'Order Confirmed',
    '3' => 'Ready To Pickup',
    '4' => 'Out For Pickup',
    '5' => 'Picked up',
    '6' => 'Pickup Reschedule',
    '7' => 'Intransit To Warehouse',
    '8' => 'Order Processing',
    '9' => 'Ready To deliver',
    '10' => 'Out for Deliver',
    '11' => 'Delivered',
    '12' => 'Deliver Reschedule',
    '13' => 'Cancel');
    ?>
    <section id="content">

        <!--start container-->
        <div class="container">                    

            <!--card stats start-->
            <div id="card-stats">
                <div class="row">
                    <div class="col s12 m6 l3">
                        <div class="card">
                            <div class="card-content  green white-text">
                                <p class="card-stats-title"><i class="mdi-social-people"></i> Total Users</p>
                                <h4 class="card-stats-number"><?= $UserCount; ?></h4>
                                <p class="card-stats-compare">
                                    <!-- <i class="mdi-hardware-keyboard-arrow-up"></i> --> 
                                    <!-- 15% <span class="green-text text-lighten-5">from yesterday</span> -->
                                </p>
                            </div>

                        </div>
                    </div>
                    <div class="col s12 m6 l3">
                        <div class="card">
                            <div class="card-content pink lighten-1 white-text">
                                <p class="card-stats-title"><i class="mdi-action-shopping-basket"></i> Pickup Request</p>
                                <h4 class="card-stats-number"><?= $PickupCount; ?></h4>
                                <p class="card-stats-compare">
                                    <!-- <i class="mdi-hardware-keyboard-arrow-down"></i> 3% <span class="deep-purple-text text-lighten-5">from last month</span> -->
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 l3">
                        <div class="card">
                            <div class="card-content teal white-text">
                                <p class="card-stats-title"><i class="mdi-maps-local-shipping"></i> Today Delivery</p>
                                <h4 class="card-stats-number"><?= $todaydelivery; ?></h4>
                                <p class="card-stats-compare">
                                    <!-- <i class="mdi-hardware-keyboard-arrow-up"></i> 80% <span class="blue-grey-text text-lighten-5">from yesterday</span> -->
                                </p>
                            </div>

                        </div>
                    </div>
                    <div class="col s12 m6 l3">
                        <div class="card">
                            <div class="card-content purple lighten-2 white-text">
                                <p class="card-stats-title"><i class="mdi-editor-attach-money"></i>Total Sales</p>
                                <h4 class="card-stats-number">Rs.<?= $totalsale[0];?></h4>
                                <p class="card-stats-compare">
                                    <!-- <i class="mdi-hardware-keyboard-arrow-up"></i> 70% <span class="purple-text text-lighten-5">last month</span> -->
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--card stats end-->

            <!--chart dashboard start-->
            <div id="chart-dashboard">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-move-up waves-effect waves-block waves-light">
                                <div class="move-up cyan darken-1">
                                    <div>
                                        <span class="chart-title white-text">Order Overview</span>
                                        <div class="chart-revenue cyan darken-2 white-text">
                                            <p class="chart-revenue-total">$4,500.85</p>
                                            <p class="chart-revenue-per"><i class="mdi-navigation-arrow-drop-up"></i> 21.80 %</p>
                                        </div>
                                        <div class="switch chart-revenue-switch right">
                                            <label class="cyan-text text-lighten-5">
                                                Month
                                                <input type="checkbox">
                                                <span class="lever"></span> Year
                                            </label>
                                        </div>
                                    </div>
                                    <div class="trending-line-chart-wrapper">
                                        <canvas id="trending-line-chart" height="70"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="card-content">
                                <a class="btn-floating btn-move-up waves-effect waves-light darken-2 right">
                                    <i class="mdi-content-add activator"></i>
                                </a>
                                <h6 class="teal-text">
                                    Details of total order, pickup & delivery.
                                </h6>
                            </div>

                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Revenue by Month <i class="mdi-navigation-close right"></i></span>
                                <table class="responsive-table">
                                    <thead>
                                        <tr>
                                            <th data-field="id">ID</th>
                                            <th data-field="month">Month</th>
                                            <th data-field="item-sold">Item Sold</th>
                                            <th data-field="item-price">Item Price</th>
                                            <th data-field="total-profit">Total Profit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>January</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>February</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>March</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>April</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>May</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>June</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>July</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>August</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Septmber</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Octomber</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>November</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>December</td>
                                            <td>122</td>
                                            <td>100</td>
                                            <td>$122,00.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!--chart dashboard end-->

            <!--work collections start-->
            <div id="work-collections">
                <div class="row">
                    <div class="col s12 m12 l6">
                        <ul id="projects-collection" class="collection">
                            <li class="collection-item avatar">
                                <i class="mdi-action-shopping-basket circle light-blue darken-2"></i>
                                <span class="collection-header">Latest Orders</span>
                                <p>Your Recents Orders</p>
                                <a href="<?=BASE_URL?>admin/orders" class="secondary-content tooltipped" data-position="left" data-tooltip="View All Orders">
                                    <i class="mdi-action-launch small grey-text"></i>
                                </a>
                            </li>
                            <li class="collection-item">
                                <?php if (!empty($userdata)) {
                                    foreach ($userdata as $key => $value) {
                                        $statusid=$value["order_status"];
                                        ?>
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title"><?= $value["order_id"]?></p>
                                                <!-- <p class="collections-content">House Starks</p> -->
                                            </div>
                                            <div class="col s2">
                                                &nbsp;
                                            </div>
                                            <div class="col s4">
                                                <div class="chip cyan white-text block center">
                                                    <?= $status[$statusid]?>
                                                </div>
                                            </div>
                                        </div><br>
                                        <?php } }?>
                                    </li>
                        <!-- <li class="collection-item">
                            <div class="row">
                                <div class="col s6">
                                    <p class="collections-title">Daenerys Targaryen</p>
                                    <p class="collections-content">House Targaryens</p>
                                </div>
                                <div class="col s2">
                                    &nbsp;
                                </div>
                                <div class="col s4">
                                    <div class="chip  blue lighten-1 white-text block center">
                                        Superwashed
                                    </div>
                                </div>
                            </div>
                        </li>                                    
                        <li class="collection-item">
                            <div class="row">
                                <div class="col s6">
                                    <p class="collections-title">Arya Stark</p>
                                    <p class="collections-content">House Starks</p>
                                </div>
                                <div class="col s2">
                                    &nbsp;
                                </div>
                                <div class="col s4">
                                    <div class="chip green white-text block center">
                                        Picked Up
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="collection-item">
                            <div class="row">
                                <div class="col s6">
                                    <p class="collections-title">Margery Tyrell</p>
                                    <p class="collections-content">House Tyrells</p>
                                </div>
                                <div class="col s2">
                                    &nbsp;
                                </div>
                                <div class="col s4">
                                    <div class="chip orange white-text block center">
                                        Delivery Ready
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="collection-item">
                            <div class="row">
                                <div class="col s6">
                                    <p class="collections-title">Sansa Stark</p>
                                    <p class="collections-content">House Starks</p>
                                </div>
                                <div class="col s2">
                                    &nbsp;
                                </div>
                                <div class="col s4">
                                    <div class="chip deep-purple lighten-1 white-text block center">
                                        Order Recieved
                                    </div>
                                </div>
                            </div>
                        </li> -->
                    </ul>
                </div>
                <div class="col s12 m12 l6">
                    <ul id="issues-collection" class="collection">
                        <li class="collection-item avatar">
                            <i class="mdi-communication-forum circle teal darken-2"></i>
                            <span class="collection-header">Feedback</span>
                            <p>Feedback from customers</p>
                            <a href="<?=ADMIN_URL?>feedback/feedbacklist" class="secondary-content tooltipped" data-position="left" data-tooltip="View All Messages">
                                <i class="mdi-action-launch small grey-text"></i>
                            </a>
                        </li>
                        <?php foreach($dataarr as $arr){ $rating = (int) $arr['rating']; ?>
                        <li class="collection-item">
                            <div class="row">
                                <div class="col s9">
                                    <p class="collections-title"><?= $arr['user']?> <span class="ultra-small">(<?= $arr['order_id']?>)</span></p>
                                    <p class="ultra-small grey-text text-dott">
                                       <?= $arr['reason']?>
                                    </p>
                                </div>
                                <div class="col s3">
                                    <div class="rating">
                                        <?php if ($rating == 1) { ?>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                            <?php } else if ($rating == 2) { ?> 
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                            <?php } else if ($rating == 3) { ?>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                            <?php } else if ($rating == 4) { ?>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade grey-text"></i>
                                            <?php } else if ($rating == 5) { ?>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                                <i class="mdi-action-grade orange-text"></i>
                                            <?php } ?>
                                    </div>    
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <!--work collections end-->

        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 55px; right: 20px;">
            <a class="btn-floating btn-large">
                <i class="mdi-content-create"></i>
            </a>
            <ul>
                <li>
                    <a href="#" class="btn-floating red">
                        <i class="large mdi-communication-live-help"></i>
                    </a>
                </li>
                <li>
                    <a href="#" class="btn-floating yellow darken-1">
                        <i class="large mdi-device-now-widgets"></i>
                    </a>
                </li>
                <li>
                    <a href="#" class="btn-floating green">
                        <i class="large mdi-editor-insert-invitation"></i>
                    </a>
                </li>
                <li>
                    <a href="#" class="btn-floating blue">
                        <i class="large mdi-communication-email"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Floating Action Button -->

    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->