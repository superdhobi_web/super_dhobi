<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $printname->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $printname->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Printnames'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="printnames form large-9 medium-8 columns content">
    <?= $this->Form->create($printname) ?>
    <fieldset>
        <legend><?= __('Edit Printname') ?></legend>
        <?php
            echo $this->Form->input('pname');
            echo $this->Form->input('created_date');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
