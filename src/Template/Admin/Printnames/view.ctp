<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Printname'), ['action' => 'edit', $printname->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Printname'), ['action' => 'delete', $printname->id], ['confirm' => __('Are you sure you want to delete # {0}?', $printname->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Printnames'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Printname'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="printnames view large-9 medium-8 columns content">
    <h3><?= h($printname->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($printname->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created Date') ?></th>
            <td><?= h($printname->created_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $printname->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Pname') ?></h4>
        <?= $this->Text->autoParagraph(h($printname->pname)); ?>
    </div>
</div>
