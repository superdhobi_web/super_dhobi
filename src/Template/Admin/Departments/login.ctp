<div id="login-page" class="row">
  <div class="col m4 s12 offset-m4 z-depth-2 card-panel">
    <form class="login-form" method="post" id="loginForm">
      <div class="row">
        <div class="input-field col s12 center">
          <img src="<?php echo $this->request->webroot; ?>images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login">
          <p class="center login-form-text">Admin Superdhobi</p>
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="mdi-social-person-outline prefix"></i>
          <input id="username" required type="text" name="username" class="validate"/>
          <label for="username" class="active" data-error="Please enter the correct username." data-success="Perfect!">Username</label>
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="mdi-action-lock-outline prefix"></i>
          <input id="password" type="password" required name="password" class="validate"/>
          <label for="password" class="active" data-error="Please enter the correct password." data-success="Perfect!">Password</label>
<!--          <label for="password" class="active">Password</label>-->
        </div>
      </div>
      <!-- <div class="row">          
        <div class="input-field col s12 m12 l12  login-text">
          <input type="checkbox" id="remember-me" />
          <label for="remember-me">Remember me</label>
        </div>
      </div> -->
      <div class="row">
        <div class="input-field col s12">
          <button class="btn waves-effect waves-light col s12 cyan" type="submit" onclick="loginvalidate();">Login</button>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6 m6 l6">
          <p class="margin medium-small">&nbsp;</p>
        </div>
        <!-- <div class="input-field col s6 m6 l6">
          <p class="margin right-align medium-small"><a href="#">Forgot password ?</a></p>
        </div> -->          
      </div>

    </form>
  </div>
</div>