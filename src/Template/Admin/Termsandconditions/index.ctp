<!-- <?= $this->Html->script('ckeditor/ckeditor'); ?> -->
<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Terms and Conditions</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Terms and Conditions</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card" style="min-height:465px;">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    Terms &AMP; Conditions
                                </span>
                                <i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Edit T&C"></i>
                            </span>                                    
                        </div>
                        <br/>
                        <div class="container">
                            <div class="row">
                                <div class="col s12 m12 l12">
                                    <p class="content view">
                                        <?php
                                        if (!empty($tc)) {
                                            echo $tc['details'];
                                        }
                                        ?>
                                    </p>
                                </div>  
                            </div>                 
                        </div>
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">
                                <span class="green-text">Edit T&AMP;C Details</span>
                                <i class="mdi-navigation-close right"></i>
                            </span>
                            <div class="row">
                                <form method="post">
                                    <input type="hidden" name="id" value="<?= $tc['id']; ?>">
                                    <div class="col s12">
                                        <textarea name="editor1"><?php echo $tc['details']; ?></textarea>
                                    </div>
                                    <div class="col s12">
                                        <p>&nbsp;</p>
                                        <button class="btn green waves-effect waves-light right" name="action" id="TandCsubmit">Save
                                            <i class="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
