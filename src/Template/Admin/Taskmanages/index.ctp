<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Task Management</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Task Management</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <div id="card-stats">
            <div class="row">
                <form class="section" method="GET" action="<?= BASE_URL . "admin/taskmanages/" ?>" id="">
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="row">
                                <div class="col s12 m8">
                                    <div class="date-fil-con">
                                        <label for="orderedDate" class="">Date</label>
                                        <input type="date" class="orderedDate" style="width:75%;" id="datepicker2" name="orderdate" <?php if(!empty($this->request->query("orderdate"))){?> value="<?php echo date("jS F, Y", strtotime($this->request->query("orderdate")))?>"<?php } ?>placeholder="Select Pickup Date..."/>
                                    </div>

                                    <div class="date-fil-con">
                                        <select name="timeslot" id="SetSlot_p" class="filter-select browser-default">
                                            <option value="">Slot</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col s12 m4 border-l right">
                                    <select name="orderstatus" id="" class="filter-select browser-default">
                                        <option value="">Sort by Status</option>
                                        <option value="9" <?php
if ($this->request->query("orderstatus") == 9) {
    echo "selected";
}
?>>Ready to deliver</option>
                                        <option value="3" <?php
                                                if ($this->request->query("orderstatus") == 3) {
                                                    echo "selected";
                                                }
?>>Ready to pickup</option>
                                    </select>
                                </div>
                            </div>
                            <hr class="teal-text">
                            <div class="row">                            
                                <div class="col s12">
                                    <select name="city" id="city" class="filter-select browser-default loc-fil"  onchange="getChild(1,this.value);">
                                        <option value="">City</option>
                                        <?php foreach ($city as $k => $val) { ?>
                                            <option value="<?= $val->id; ?>"
                                            <?php
                                            if ($this->request->query("city") == $val->id) {
                                                echo "selected";
                                            }
                                            ?>><?= $val->location_name; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <select name="circle" class="filter-select browser-default loc-fil" id="get_1" onchange="getChild(2,this.value);">
                                        <option value="">Circle..</option>
                                        <?php
                                        if (!empty($child['circle'])) {
                                            foreach ($child['circle'] as $key => $val) {
                                                ?>
                                                <option value="<?php echo $key; ?>" <?php
                                        if ($this->request->query("circle") == $key) {
                                            echo "selected";
                                        }
                                                ?>><?php echo $val; ?> </option> 
                                                    <?php }
                                                } ?>
                                    </select>
                                    <select name="location" class="filter-select browser-default loc-fil" id="get_2" onchange="getChild(3,this.value);">
                                        <option value="">Location..</option>
                                        <?php
                                        if (!empty($child['location'])) {
                                            foreach ($child['location'] as $key => $val) {
                                                ?>
                                                <option value="<?php echo $key; ?>" <?php
                                        if ($this->request->query("location") == $key) {
                                            echo "selected";
                                        }
                                                ?>><?php echo $val; ?> </option> 
                                            <?php }
                                        } ?>
                                    </select>
                                    <select name="area" class="filter-select browser-default loc-fil" id="get_3">
                                        <option value="">Area..</option>
                                        <?php
                                        if (!empty($child['area'])) {
                                            foreach ($child['area'] as $key => $val) {
                                                ?>
                                                <option value="<?php echo $key; ?>" <?php
                                        if ($this->request->query("area") == $key) {
                                            echo "selected";
                                        }
                                                ?>><?php echo $val; ?> </option> 
                                            <?php }
                                        } ?>
                                    </select>
                                    <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit" value="search" name="serach">
                                        <i class="mdi-content-forward"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="mail-app" class="section">
                <div class="row">
                    <div class="col s12">
                        <nav class="grey darken-2">
                            <div class="nav-wrapper">
                                <div class="left col s12 m5 l5">
                                    <ul>
                                        <li>
                                            <span class="email-menu">
                                                <i class="large mdi-device-now-widgets"></i>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="email-type">
                                                &nbsp; TASK MANAGEMENT
                                            </span>
                                        </li>
                                    </ul>
                                </div>
<!--                                <div class="col s12 m7 l7 hide-on-med-and-down">
                                    <ul class="right">
                                        <li>
                                            <a href="#" class="tooltipped" data-position="left" data-tooltip="Print Details" onclick="return printtask();">
                                                <i class="mdi-action-print"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>-->

                            </div>
                        </nav>
                    </div>
                    <div class="col s12">
                        <div id="email-list" class="col s10 m4 l4 card-panel z-depth-1 e-list">
                            <ul id="task-manage" class="collection">
                                <?php
                                if (!empty($rows)) {
                                    foreach ($rows as $orderData) {
                                        ?>
                                        <li class="collection-item">
                                            <div class="row">
                                                <div class="col s1">
                                                    <input type="checkbox" id="testchk<?= $orderData['order_id'] ?>" <?php
                                if (!empty($orderData[$orderData['order_id']])) {
                                    echo "checked";
                                    echo " disabled";
                                }
                                        ?> class="checkorder" onchange="appendorder('<?= $orderData['order_id'] ?>','<?= $orderData['order_status'] ?>');">
                                                    <label for="testchk<?= $orderData['order_id'] ?>"></label>
                                                </div>
                                                <div class="col s7">
                                                    <span class="collections-title block teal-text"><?= $orderData['order_id'] ?></span>
                                                    <span class="collections-content ultra-small block">
                                                        <i class="mdi-social-person"></i>
                                                        <?= $orderData['user_id'] ?>(<?= $orderData['name'] ?>)     <?= $orderData['ordereddate'] ?> <?php
                                                if ($orderData['order_status'] == 3) {
                                                    echo " Ready to Pickup";
                                                } else {
                                                    echo "Ready to Deliver";
                                                }
                                                        ?>
                                                    </span>
                                                </div>  
                                                <a href="#" onclick="getorderdetails('<?= $orderData['order_id'] ?>');" class="btn-floating btn-small cyan waves-effect waves-light chat-collapse secondary-content">
                                                    <i class="mdi-action-launch"></i>
                                                </a>                      
                                            </div>
                                        </li>
                                        <?php
                                    }
                                } else {
                                    echo "No Order Found..";
                                }
                                ?>
                            </ul>
                        </div>
                        <div id="email-details" class="col s12 m8 l8 card-panel">
                            <div id="blog-posts" class="row">
                                <link href="<?php echo $this->request->webroot; ?>css/materialize.css" rel="stylesheet" media="print">
                                <link href="<?php echo $this->request->webroot; ?>css/style.css" rel="stylesheet" media="print">
                                <link href="<?php echo $this->request->webroot; ?>css/custom/custom.css" rel="stylesheet" media="print">
                                <div class="blog-sizer"></div>
                                <?php foreach ($alluser as $da) { ?>
                                    <div class="blog">
                                        <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
                                            <li class="active">
                                                <div class="collapsible-header active grey lighten-3 cyan-text">
                                                    <span>
                                                        <input class="with-gap" name="empuser" type="radio" id="test<?= $da->emp_id; ?>" value="<?= $da->emp_id; ?>" class="empuser">
                                                        <label for="test<?= $da->emp_id; ?>"></label>
                                                    </span>
                                                    <span class="ultra-small grey-text"><?= $da->emp_id; ?> <small>(<?= explode(" ", $da->username)[0]; ?>)</small></span>
                                                </div>
                                                <div style="display: block;" class="collapsible-body apdorder<?= $da->emp_id; ?>">
                                                    <?php
                                                    if (!empty($rows)) {
                                                        foreach ($rows as $orderData) {
                                                            if (!empty($orderData[$orderData['order_id']])) {
                                                                if ($orderData[$orderData['order_id']]['id_employee'] == $da->emp_id) {
                                                                    ?>
                                                                    <div class="chip teal white-text" id='rmv<?= $orderData['order_id'] ?>'>
                                                                        <img src="<?= $this->request->webroot; ?>images/<?php
                                                if ($orderData[$orderData['order_id']]['status'] == 1)
                                                    echo "truck_pick.png";else {
                                                    echo "truck_delv.png";
                                                }
                                                                    ?>" alt="pick/delivery">
                                                                        <span class="ord<?= $da->emp_id; ?>"><?= $orderData['order_id'] ?></span>
                                                                        <i class="material-icons mdi-navigation-close" onclick="removeorder('<?= $orderData['order_id'] ?>','<?= $da->emp_id ?>',<?= $orderData[$orderData['order_id']]['id'] ?>);"></i>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <div class="row">
                                                    <div class="col s12">
                                                        <a href="#" class="tooltipped btn-floating btn-small waves-effect waves-light green secondary-content" data-position="left" data-tooltip="Print The Details" onclick="printusertask('<?= $da->emp_id ?>');" style="margin:3px;">
                                                            <i class="mdi-action-print"></i>
                                                        </a>
                                                    </div>
                                                </div> 
                                            </li>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
        <form action="<?= BASE_URL?>admin/orders/printpage" method="post" id="alldataid">
        <input type="hidden" name="userid" value="" id="senduserid">
        <input type="hidden" name="dataall" value="" id="allorder">
        </form>
</section>
<!-- START RIGHT SIDEBAR NAV-->
<aside id="right-sidebar-nav">
    <ul id="chat-out" class="side-nav rightside-navigation">
    </ul>
</aside>
<!-- LEFT RIGHT SIDEBAR NAV-->