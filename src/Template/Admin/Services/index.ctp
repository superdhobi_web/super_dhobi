<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Services</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Services</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    Service Details
                                </span>
                                <i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Add Service Details"></i>
                            </span>                                    
                        </div>
                        <div class="row">
                                                    <div class="col s12 m12 l12">
                            <table class="striped responsive-table centered">
                                <thead>
                                    <tr class="white-text">
                                        <th class="teal">Actions</th>
                                        <th class="teal"><?= $this->Paginator->sort('sicon', ['label' => false]); ?>Service Icon</th>
                                        <th class="teal"><?= $this->Paginator->sort('sname', ['label' => 'Service Name']); ?></th>
                                        <th class="teal"><?= $this->Paginator->sort('status', ['label' => 'Status']); ?></th>
                                        <th class="teal"><?= $this->Paginator->sort('created_date', ['label' => false]); ?>Created Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($services as $service): ?>
                                        <tr>
                                            <td class="actions">
                                                <?=
                                                $this->Html->link("<i class='mdi-content-create'></i>", ['action' => 'edit', $service->id],
                                                ['escape' => false, 'title' => 'Edit', 'class' => 'btn-floating btn-small  green waves-effect waves-light']
                                                );
                                                ?>
                                                <?=
                                                $this->Form->postLink(
                                                "<i class='mdi-action-delete'> </i>",
                                                ['action' => 'delete', $service->id],
                                                ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete Service {0}?', $service->sname)] // third
                                                );
                                                ?>
                                            </td>
                                            <td><img src="<?= BASE_URL; ?>thumbimg/<?= $service->sicon ?>" height="30" width="30" class="circle"></td>
                                            <td><?= h($service->sname) ?></td> 
                                            <td><div class="switch">
                                                    <label>
                                                        <?php $controller=$this->request->params['controller'];?>
                                                        <input type="checkbox" <?php if($service->status==1){ echo "Checked";}?> value="<?php if($service->status==1){ echo 1; } else echo 2;?>"class="Addcheckbox" id="defaultAdd<?=  $service->id; ?>" name='add' onchange="changeStatus(this.value,<?= $service->id; ?>,'<?= $controller; ?>');"/>
                                                        <span class="lever"></span>
                                                    </label>
                                                </div></td>
                                            <td><?= h(date('jS F Y / h:i A', strtotime($service->created_date))); ?></td>

                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next(__('next') . ' >') ?>
                                </ul>
                                <p><?= $this->Paginator->counter() ?></p>
                            </div>
                        </div>
                        </div>
                        <?= $this->element('addservice'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
