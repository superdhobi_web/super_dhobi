<div class="container">
    <div class="row">
        <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Manage Users</h5>
            <ol class="breadcrumbs">
                <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                <li class="active">Manage Users</li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <!--card stats start-->
    <div id="card-stats">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card">
                    <span class="card-title grey-text text-darken-4">
                        <span class="green-text">Edit Service Details</span>
                    </span>
                    <div class="row">
                        <?php // pr($service);?>
                        <form class="col s12 m8 offset-m2 right-alert" id="addservice" action="<?= ADMIN_URL . "services/add/$service->id";?>" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?= $service->id; ?>"/>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-action-account-circle prefix"></i>
                                    <input id="first_input2" class="validate" type="text" name="sname" value="<?= $service->sname; ?>"/>
                                    <label for="first_input2" data-error="Please enter Service name." data-success="Perfect!">Service Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-action-perm-media prefix"></i>
                                    <input id="user_input2" class="validate" type="file" name="sicon" onchange="ValidateSingleInput(this);">
                                    <input value="<?= $service->sicon;?>" class="validate" type="hidden" name="prev_sicon" />
                                    <label for="user_input2" data-error="" data-success="Perfect!"></label>
                                </div>
                                
                            </div>
                            <div><img src="<?= BASE_URL; ?>thumbimg/<?= $service->sicon ?>" height="30" width="30"></img></div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn green waves-effect waves-light right" href="javascript:void(0);" name="action" id="usersubmit" onclick="submitDisable();">Save
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function uservalidate(){
        var full_name=$("#first_inpu2").val();
        if(full_name==""){
            $("#first_input2").data("data-error");
            alert(full_name);
        }
        var email=$("#user_input2").val();
        var email=$("#email_input2").val();
        var mobile=$("#mobile_input2").val();
        var ref_by=$("#ref_by").val();
        
    }
    
</script>