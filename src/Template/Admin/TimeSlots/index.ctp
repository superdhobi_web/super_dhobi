<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Time Slots</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Users</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end--> 
    <div class="container">
        <div class="row">
            <div class="col s12 m8 l8">
                <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                    <li>
                        <?php // for($i=1;$i<=7;$i++){
                            ?>
                            <?php // echo ($i==1)?"Monday":(($i==2)?"Tuesday":(($i==3)?"Wednesday":(($i==4)?"Thursday":(($i==5)?"Friday":(($i==6)?"Satuarday":"Sunday")))));?>
                            <div class="collapsible-header active teal-text text-lighten-2">
                                <i class="mdi-action-today"></i>Monday
                                <div class="secondary-content">
                                    <a href="#modalslot" class="waves-effect waves-light modal-trigger" onclick="addParentId(1);">
                                        <i class="mdi-content-add"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="collapsible-body">
                                <ul class="collection with-header" id="dayslot1">
                                    <li class="collection-header grey">
                                        <h6 class="condensed bold white-text">ALLOTED SLOTS</h6>
                                    </li>
                                    <?php
                                    if (isset($slotArray['1'])) {
                                        foreach ($slotArray['1'] as $timing1) {
                                            $from_time1 = $timing1['from'];
                                            $to_time1 = $timing1['to'];
                                            $id1 = $timing1['id'];
                                            ?>
                                            <li class="collection-item">
                                                <div>
                                                    <i class="mdi-image-timer"></i> <span id="slotLine_<?php echo $id1; ?>"><?php echo $from_time1; ?> - <?php echo $to_time1; ?></span>
                                                    <div class="secondary-content">                                               
                                                        <a href="#modalslot" id="slotclick_<?php echo $id1; ?>" class="btn-floating btn-small  green waves-effect waves-light modal-trigger" onclick="UpdateSlot('<?php echo $from_time1; ?>','<?php echo $to_time1; ?>',1,'<?php echo $id1; ?>');">
                                                            <i class="mdi-content-create"></i>
                                                        </a>
                                                        <?php
                                                        echo $this->Form->postLink(
                                                            "<i class='mdi-action-delete'> </i>",
                                                            ['action' => 'delete', $id1],
                                                            ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete # {0}?', $id1)]
                                                            );
                                                            ?> 
                                                        </div>                                        
                                                    </div>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="collapsible-header active teal-text text-lighten-2">
                                    <i class="mdi-action-today"></i>  Tuesday
                                    <div class="secondary-content">
                                        <a href="#modalslot" class="waves-effect waves-light modal-trigger" onclick="addParentId(2);">
                                            <i class="mdi-content-add"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapsible-body">
                                    <ul class="collection with-header" id="dayslot2">
                                        <li class="collection-header grey">
                                            <h6 class="condensed bold white-text">ALLOTED SLOTS</h6>
                                        </li>
                                        <?php
                                        if (isset($slotArray['2']) && !empty($slotArray['2'])) {
                                            foreach ($slotArray['2'] as $timing2) {
                                                $from_time2 = $timing2['from'];
                                                $to_time2 = $timing2['to'];
                                                $id2 = $timing2['id'];
                                                ?>
                                                <li class="collection-item">
                                                    <div>
                                                        <i class="mdi-image-timer"></i> <span id="slotLine_<?php echo $id2; ?>"><?php echo $from_time2; ?> - <?php echo $to_time2; ?></span>
                                                        <div class="secondary-content">                                               

                                                            <a href="#modalslot" class="btn-floating btn-small  green waves-effect waves-light modal-trigger" onclick="UpdateSlot('<?php echo $from_time2; ?>','<?php echo $to_time2; ?>',2,'<?php echo $id2; ?>');">
                                                                <i class="mdi-content-create"></i>
                                                            </a>
                                                            <?php
                                                            echo $this->Form->postLink(
                                                                "<i class='mdi-action-delete'> </i>",
                                                                ['action' => 'delete', $id2],
                                                    ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete # {0}?', $id2)] // third
                                                    );
                                                    ?> 
                                                </div>                                        
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header active teal-text text-lighten-2">
                            <i class="mdi-action-today"></i>  Wednesday
                            <div class="secondary-content">
                                <a href="#modalslot" class="waves-effect waves-light modal-trigger" onclick="addParentId(3);">
                                    <i class="mdi-content-add"></i>
                                </a>
                            </div>
                        </div>
                        <div class="collapsible-body">
                            <ul class="collection with-header" id="dayslot3">
                                <li class="collection-header grey">
                                    <h6 class="condensed bold white-text">ALLOTED SLOTS</h6>
                                </li>
                                <?php
                                if (isset($slotArray['3'])) {
                                    foreach ($slotArray['3'] as $timing3) {
                                        $from_time3 = $timing3['from'];
                                        $to_time3 = $timing3['to'];
                                        $id3 = $timing3['id'];
                                        ////pr($timing3)
                                        ?>
                                        <li class="collection-item">
                                            <div>
                                                <i class="mdi-image-timer"></i><span id="slotLine_<?php echo $id3; ?>"> <?php echo $from_time3; ?> - <?php echo $to_time3; ?></span>
                                                <div class="secondary-content">                                               

                                                    <a href="#modalslot" class="btn-floating btn-small  green waves-effect waves-light modal-trigger" onclick="UpdateSlot('<?php echo $from_time3; ?>','<?php echo $to_time3; ?>',3,'<?php echo $id3; ?>');">
                                                        <i class="mdi-content-create"></i>
                                                    </a>
                                                    <?php
                                                    echo $this->Form->postLink(
                                                        "<i class='mdi-action-delete'> </i>",
                                                        ['action' => 'delete', $id3],
                                                    ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete # {0}?', $id3)] // third
                                                    );
                                                    ?> 
                                                </div>                                        
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header active teal-text text-lighten-2">
                            <i class="mdi-action-today"></i>  Thursday
                            <div class="secondary-content">
                                <a href="#modalslot" class="waves-effect waves-light modal-trigger" onclick="addParentId(4);">
                                    <i class="mdi-content-add"></i>
                                </a>
                            </div>
                        </div>
                        <div class="collapsible-body">
                            <ul class="collection with-header" id="dayslot4">
                                <li class="collection-header grey">
                                    <h6 class="condensed bold white-text">ALLOTED SLOTS</h6>
                                </li>
                                <?php
                                if (isset($slotArray['4'])) {
                                    foreach ($slotArray['4'] as $timing4) {
                                        $from_time4 = $timing4['from'];
                                        $to_time4 = $timing4['to'];
                                        $id4 = $timing4['id'];
                                        ?>
                                        <li class="collection-item">
                                            <div>
                                                <i class="mdi-image-timer"></i><span id="slotLine_<?php echo $id2; ?>"> <?php echo $from_time4; ?> - <?php echo $to_time4; ?></span>
                                                <div class="secondary-content">                                               

                                                    <a href="#modalslot" class="btn-floating btn-small  green waves-effect waves-light modal-trigger" onclick="UpdateSlot('<?php echo $from_time4; ?>','<?php echo $to_time4; ?>',4,'<?php echo $id4; ?>');">
                                                        <i class="mdi-content-create"></i>
                                                    </a>
                                                    <?php
                                                    echo $this->Form->postLink(
                                                        "<i class='mdi-action-delete'> </i>",
                                                        ['action' => 'delete', $id4],
                                                    ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete # {0}?', $id4)] // third
                                                    );
                                                    ?>     
                                                </div>                                        
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header active teal-text text-lighten-2">
                            <i class="mdi-action-today"></i>  Friday
                            <div class="secondary-content">
                                <a href="#modalslot" class="waves-effect waves-light modal-trigger" onclick="addParentId(5);">
                                    <i class="mdi-content-add"></i>
                                </a>
                            </div>
                        </div>
                        <div class="collapsible-body">
                            <ul class="collection with-header" id="dayslot5">
                                <li class="collection-header grey">
                                    <h6 class="condensed bold white-text">ALLOTED SLOTS</h6>
                                </li>
                                <?php
                                if (isset($slotArray['5'])) {
                                    foreach ($slotArray['5'] as $timing5) {//pr($timing5)
                                        ?>
                                        <li class="collection-item">
                                            <div>
                                                <i class="mdi-image-timer"></i><span id="slotLine_<?php echo $id2; ?>">  <?php echo $timing5['from'] ?> - <?php echo $timing5['to'] ?></span>
                                                <div class="secondary-content">                                               

                                                    <a href="#" class="btn-floating btn-small  green waves-effect waves-light">
                                                        <i class="mdi-content-create"></i>
                                                    </a>
                                                    <?php
                                                    echo $this->Form->postLink(
                                                        "<i class='mdi-action-delete'> </i>",
                                                        ['action' => 'delete', $timing5['id']],
                                                    ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete # {0}?', $timing5['id'])] // third
                                                    );
                                                    ?>                                                       
                                                </div>                                        
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header active teal-text text-lighten-2">
                            <i class="mdi-action-today"></i>  Saturday
                            <div class="secondary-content">
                                <a href="#modalslot" class="waves-effect waves-light modal-trigger" onclick="addParentId(6);">
                                    <i class="mdi-content-add"></i>
                                </a>
                            </div>
                        </div>
                        <div class="collapsible-body">
                            <ul class="collection with-header" id="dayslot6">
                                <li class="collection-header grey">
                                    <h6 class="condensed bold white-text">ALLOTED SLOTS</h6>
                                </li>
                                <?php
                                if (isset($slotArray['6'])) {
                                    foreach ($slotArray['6'] as $timing6) {//pr($timing6)
                                        ?>
                                        <li class="collection-item">
                                            <div>
                                                <i class="mdi-image-timer"></i><span id="slotLine_<?php echo $id2; ?>">  <?php echo $timing6['from'] ?> - <?php echo $timing6['to'] ?></span>
                                                <div class="secondary-content">                                               

                                                    <a href="#" class="btn-floating btn-small  green waves-effect waves-light">
                                                        <i class="mdi-content-create"></i>
                                                    </a>
                                                    <?php
                                                    echo $this->Form->postLink(
                                                        "<i class='mdi-action-delete'> </i>",
                                                        ['action' => 'delete', $timing6['id']],
                                                    ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete # {0}?', $timing6['id'])] // third
                                                    );
                                                    ?>
                                                </div>                                        
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header active teal-text text-lighten-2">
                            <i class="mdi-action-today"></i>  Sunday
                            <div class="secondary-content">
                                <a href="#modalslot" class="waves-effect waves-light modal-trigger" onclick="addParentId(7);">
                                    <i class="mdi-content-add"></i>
                                </a>
                            </div>
                        </div>
                        <div class="collapsible-body">
                            <ul class="collection with-header" id="dayslot7">
                                <li class="collection-header grey">
                                    <h6 class="condensed bold white-text">ALLOTED SLOTS</h6>
                                </li>
                                <?php
                                if (isset($slotArray['7'])) {
                                    foreach ($slotArray['7'] as $timing7) {//pr($timing7)
                                        ?>
                                        <li class="collection-item">
                                            <div>
                                                <i class="mdi-image-timer"></i><span id="slotLine_<?php echo $id2; ?>">  <?php echo $timing7['from'] ?> - <?php echo $timing7['to'] ?></span>
                                                <div class="secondary-content">                                               

                                                    <a href="#" class="btn-floating btn-small  green waves-effect waves-light">
                                                        <i class="mdi-content-create"></i>
                                                    </a>
                                                    <?php
                                                    echo $this->Form->postLink(
                                                        "<i class='mdi-action-delete'> </i>",
                                                        ['action' => 'delete', $timing7['id']],
                                                    ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete # {0}?', $timing7['id'])] // third
                                                    );
                                                    ?>
                                                    <!--<a href="<?php //ADMIN_URL."Timeslots/delete/"   ?>" class="btn-floating btn-small red waves-effect waves-light">
                                                        <i class="mdi-action-delete"> </i>
                                                    </a>-->
                                                </div>                                        
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col s12 m4 l4">
                <!-- <div id="flight-card" class="card">
                    <div class="card-header amber darken-3">
                        <div class="card-title">
                            <h4 class="flight-card-title">Current Slot</h4>
                            <p class="flight-card-date">April 25, Mon 04:50</p>
                        </div>
                    </div>
                    <div class="card-content-bg white-text">
                        <div class="card-content">
                            <div class="row flight-state-wrapper">
                                <div class="col s5 m5 l5 center-align">
                                    <div class="flight-state">
                                        <h4 class="margin">WNT</h4>
                                        <p class="ultra-small">Winterfall</p>
                                    </div>
                                </div>
                                <div class="col s2 m2 l2 center-align">
                                    <i class="mdi-maps-local-shipping truck-icon"></i>
                                </div>
                                <div class="col s5 m5 l5 center-align">
                                    <div class="flight-state">
                                        <h4 class="margin">VLY</h4>
                                        <p class="ultra-small">Valeriya</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s6 m6 l6 center-align">
                                    <div class="flight-info">
                                        <p class="small"><span class="grey-text text-lighten-4">Depart:</span> 04.50</p>
                                        <p class="small"><span class="grey-text text-lighten-4">Terminal:</span> B</p>
                                    </div>
                                </div>
                                <div class="col s6 m6 l6 center-align flight-state-two">
                                    <div class="flight-info">
                                        <p class="small"><span class="grey-text text-lighten-4">Arrive:</span> 08.50</p>
                                        <p class="small"><span class="grey-text text-lighten-4">Terminal:</span> C</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>        
    </div>
    <div>
        <mdp-time-picker mdp-auto-switch="true" ng-model="currentTime"></mdp-time-picker>
    </div>
    <div id="modalslot" class="modal">
        <div class="modal-content">
            <form class="col s12" id="FormAddSlot" method="post">
                <input type="hidden" name="id" id="slotId"/>
                <div class="row">
                    <div class="input-field col s6">
                        <i class="mdi-action-alarm-add prefix"></i>
                        <input type="time" class="validate timepicker" name="from_time" id="from_time"/>
                        <label class="active">Slot Start Time</label>
                    </div>
                    <div class="input-field col s6">
                        <i class="mdi-action-alarm prefix"></i>
                        <input type="time" class="validate timepicker" name="to_time" id="to_time">
                        <label class="active">Slot End Time</label>
                    </div>
                    <input type="hidden" id="day" name="day" >
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="javascript:void(0);" class="waves-effect waves-green btn-flat cyan white-text modal-action modal-close" onclick="addSlot();">Done</a>
            <a href="javascript:void(0);" class="waves-effect waves-red btn-flat modal-action modal-close">Cancel</a>
        </div>
    </div>
</section>