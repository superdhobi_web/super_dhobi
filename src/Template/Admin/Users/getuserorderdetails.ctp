<?php
$status = array(
    '1' => 'Order Placed',
    '2' => 'Order Confirmed',
    '3' => 'Ready To Pickup',
    '4' => 'Out For Pickup',
    '5' => 'Picked up',
    '6' => 'Pickup Reschedule',
    '7' => 'Intransit To Warehouse',
    '8' => 'Order Processing',
    '9' => 'Ready To deliver',
    '10' => 'Out for Deliver',
    '11' => 'Delivered',
    '12' => 'Deliver Reschedule',
    '13' => 'Cancel');
    ?>
<li class="li-hover">
    <a href="#" onclick="closeview();" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
</li>
<li class="li-hover">
    <ul class="chat-collapsible" data-collapsible="expandable">
        <li>
            <div class="collapsible-header orange white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
            <div class="collapsible-body recent-activity">
                <?php if(!empty($orderdetail)){foreach($orderdetail as $va){?>
                <div class="recent-activity-list chat-out-list row">
                    <div class="col s8 ">
                        <a  target="_blank" href="<?=ADMIN_URL?>orders/orderdetail/<?= $va['order_id']; ?>" class="u-det">                                               
                            <?= $va['order_id']; ?>
                        </a>
                        <div class="or-stat">
                            <div class="or-ic or-dn">
                                <i class="mdi-hardware-phone-iphone white-text" title="Ordered"></i>
                            </div>
                            <div class="or-join or-dn"></div>
                            <div class="or-ic <?php if (($va['order_status'] >= 5) && ($va['order_status'] != 6)) { ?>or-dn<?php } ?>">
                                <i class="mdi-maps-local-shipping white-text" title="Picked Up"></i>
                            </div>
                            <div class="or-join <?php if (($va['order_status'] >= 5) && ($va['order_status'] != 6)) { ?>or-dn<?php } ?>"></div>
                            <div class="or-ic <?php if ($va['order_status'] >= 9) { ?>or-dn<?php } ?>">
                                <i class="mdi-maps-local-laundry-service white-text" title="Cleaned"></i>
                            </div>
                            <div class="or-join <?php if ($va['order_status'] >= 9) { ?>or-dn<?php } ?>"></div>
                            <div class="or-ic <?php if (($va['order_status'] >= 11) && ($va['order_status'] != 12)) { ?>or-dn<?php } ?>">
                                <i class="mdi-maps-local-mall white-text" title="Delivered"></i>
                            </div>
                        </div>
                        <span class="ultra-small">
                            Total Clothes: <?php echo $va['totalcloth'];?>pc
                        </span>                            
                    </div>
                    <div class="col s4">
                        <a  href="#" style="font-size: 10px;padding: 0;font-weight: 400;">                                               
                            <?= $va['ordereddate']; ?>
                        </a>
                        <span class="ultra-small">
                            Total Cost
                            <br> 
                            Rs: <?php echo $va['totalprice'];?>/-
                        </span>
                    </div>
                </div>
                <?php }}else{ echo "<span class='orange-text'>No order requested by this user</span>";} ?>
            </div>
        </li>
    </ul>
</li>