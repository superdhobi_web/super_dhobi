<section id="content">        
  <!--breadcrumbs start-->
  <div id="breadcrumbs-wrapper">
    <!-- Search for small screen -->
    <div class="header-search-wrapper grey hide-on-large-only">
      <i class="mdi-action-search active"></i>
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
    </div>
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Coupon Management</h5>
          <ol class="breadcrumbs">
            <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
            <li class="active">Create Coupon</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--breadcrumbs end--> 
  <div class="container">
    <p class="caption text-info">Add/Create Coupon</p>
    <div class="divider"></div>
    <div class="row">
      <form class="section" method="post" action="<?= BASE_URL . "admin/coupons/addcoupon"?>" id="addcoupon" enctype="multipart/form-data" onsubmit="return validatecoupon();">
        <div class="col s12 m12">
          <div class="col s12 m6">
            <div class="card-panel" style="padding-bottom: 35px;">
              <h4 class="header2">Basic Info1</h4>
              <div class="row">
                <div class="input-field col s12">
                  <i class="mdi-action-perm-identity prefix"></i>
                  <input id="coupon1" type="text" name="coupon_name" data-error=".errorTxt1">
                  <label for="coupon1">Coupon Name</label>
                  <div class="errorTxt1"></div>
                </div>
                <div class="input-field col s12">
                  <i class="mdi-action-redeem prefix"></i>
                  <input id="coupon" type="text" name="coupon_code" onblur="validate_couponcode(this.value);">
                  <label for="coupon" data-error="Please enter coupon code.">Coupon Code</label>
                  <div id="divCheckcoupon"></div>
                </div> 
                <div class="input-field col s12">
                  <i class="mdi-social-plus-one prefix"></i>
                  <label for="coupon3">How many times?</label>
                  <input id="coupon3" type="text" name="no_of_times" data-error=".errorTxt2">
                  <div class="errorTxt2"></div>
                </div> 
                <div class="input-field col s12">
                  <i class="mdi-navigation-menu prefix"></i>
                  <textarea id="coupon4" class="materialize-textarea" name="coupon_desc"></textarea>
                  <label for="coupon4"  data-error="Please enter coupon description.">Description</label>
                </div>                          
                <div class="input-field col s12">
                  <p for="" style="margin: 0 0 5px 0;">User Type</p>
                  <div class="col s4">
                    <input type="radio" id="test1" name="customer_type" class="validate" value="1"  data-error=".errorTxt8">
                    <label for="test1">New User</label>
                  </div>
                  <div class="col s4">
                    <input type="radio" id="test2" name="customer_type" value="2" class="validate">
                    <label for="test2">Existing User</label>
                  </div>
                  <div class="col s4">
                    <input type="radio" id="test3" name="customer_type" value="3" class="validate">
                    <label for="test3">Both</label>
                  </div>
                  <div class="input-field" style="margin-top:55px;">
                    <div class="errorTxt8"></div>
                  </div>
                </div>
                
              </div>
            </div>
          </div><!--col s12 m6-->
          <div class="col s12 m6">
            <div class="card-panel" style="padding-bottom: 35px;">
              <h4 class="header2">Basic Info2</h4>
              <div class="row">
               <div class="input-field col s12">
                <p for="" style="margin: 0 0 1px 0;">Day</p>
                <div class="col s3" style="padding-top:10px;">
                  <input type="checkbox" id="test8" name="day[]" value="1" class="day" data-error=".errorTxt9">
                  <label for="test8">Mon</label>
                </div>
                <div class="col s3" style="padding-top:10px;">
                  <input type="checkbox" id="test9" name="day[]" value="2" class="day" data-error=".errorTxt9">
                  <label for="test9">Tue</label>
                </div>                                
                <div class="col s3" style="padding-top:10px;">
                  <input type="checkbox" id="test10" name="day[]" value="3" class="day" data-error=".errorTxt9">
                  <label for="test10">Wed</label>
                </div>                                
                <div class="col s3" style="padding-top:10px;">
                  <input type="checkbox" id="test4" name="day[]" value="4" class="day" data-error=".errorTxt9">
                  <label for="test4">Thu</label>
                </div>                                
                <div class="col s3" style="padding-top:10px;">
                  <input type="checkbox" id="test5" name="day[]" value="5" class="day" data-error=".errorTxt9">
                  <label for="test5">Fri</label>
                </div>                                
                <div class="col s3" style="padding-top:10px;">
                  <input type="checkbox" id="test6" name="day[]" value="6" class="day" data-error=".errorTxt9">
                  <label for="test6">Sat</label>
                </div>                                
                <div class="col s3" style="padding-top:10px;">
                  <input type="checkbox" id="test7" name="day[]" value="7" class="day" data-error=".errorTxt9">
                  <label for="test7">Sun</label>
                </div>
                <div class="input-field" style="margin-top:100px;">
                  <div class="errorTxt9"></div>
                </div>
              </div>
              <div class="input-field col s12" style="margin-top: 35px;">
                <label for="coupon10" class="validate" data-error="Please enter coupon date.">Start Date</label>
                <input type="date" class="datepicker_from" id="coupon10" name="start_date">
              </div>
              <div class="input-field col s12" style="margin-top: 35px;">
                <label for="coupon9" class="validate" data-error="Please enter coupon expired date.">End Date</label>
                <input type="date" class="datepicker_to" id="coupon9" name="end_date">
              </div>
              <div class="input-field col s12">
                <p for="" style="margin: 0 0 1px 0;">Wash Type</p>
                <?php if (!empty($wash)) {
                  foreach ($wash as $washes) {
                    ?>
                    <div class="col s6" style="padding-top:10px;">
                      <input type="checkbox" id="wash<?= $washes->id; ?>" name="wtype[]" value="<?= $washes->id; ?>"  class="wastype" data-error=".errorTxt10">
                      <label for="wash<?= $washes->id; ?>" class="validate" data-error="Please choose wash type."><?= $washes->sname; ?></label>
                    </div> 
                    <?php } }?> 
                    <div class="input-field" style="margin-top:100px;">
                      <div class="errorTxt10"></div>
                    </div>
                  </div>                         
                  
                </div>
              </div>
            </div><!--col s12 m6--> 
          </div><!--col s12 m12--> 
          <div class="col s12 m12">
            <div class="col s12 m6">
              <div class="card-panel" style="padding-bottom: 35px;">
                <h4 class="header2">Basic Info3</h4>
                <div class="row">
                  <div class="input-field col s12">
                    <i class="mdi-action-add-shopping-cart prefix"></i>
                    <input id="coupon5" type="text" name="min_apply_amt" class="validate">
                    <label for="coupon5" data-error="Please enter min Billing amt.">Min. Billing Amount</label>
                  </div>                           
                  <div class="input-field col s12">
                    <i class="mdi-editor-attach-money prefix"></i>
                    <input id="coupon6" type="text" name="discount" class="validate" pattern="\d+(\.\d{1,2})?">
                    <label for="coupon6"  data-error="Please enter the discount amount.">Discount</label>
                  </div>              
                  <div class="input-field col s12">
                    <i class="mdi-editor-attach-money prefix"></i>
                    <input id="coupon7" type="text" name="min_discount" class="validate">
                    <label for="coupon7" data-error="Please enter min discounted amt." >Min. Discount(&#8377;)</label>
                  </div> 
                  <div class="input-field col s12">
                    <i class="mdi-editor-attach-money prefix"></i>
                    <input id="coupon8" type="text" name="max_discount" class="validate">
                    <label for="coupon8"  data-error="Please enter max discounted amt.">Max. Discount(&#8377;)</label>
                  </div> 
                  
                </div>
              </div>
            </div><!--col s12 m6-->
            <div class="col s12 m6">
              <div class="card-panel" style="padding-bottom: 35px;">
                <h4 class="header2">Basic Info4</h4>
                <div class="row">
                  <div class="file-field col s12 input-field">
                    <div class="btn cyan darken-2">
                      <span>browse</span>
                      <input type="file" name="image_web" onchange="ValidateSingleInput(this);">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path" id="webimage" type="text" placeholder="Upload Image for Web">
                    </div>
                  </div>  
                  <div class="file-field col s12 input-field">
                    <div class="btn cyan darken-2">
                      <span>browse</span>
                      <input type="file" name="image_mob" onchange="ValidateSingleInput(this);">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path" id="mobimage" type="text" placeholder="Upload Image for Android" >
                    </div>
                  </div>              
                </div>
              </div>
              <div class="col s12 m12">
                <div class="row">
                  <div class="card-panel cyan" style="padding-bottom: 35px;">
                    <div class="row">
                      <div class="input-field col s12 center">
                        <button class="btn white pink-text waves-effect waves-light" type="submit">Proceed
                          <i class="mdi-content-send right"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>          
              
            </div><!--col s12 m6-->
          </div><!--col s12 m12--> 
        </div>
      </form>
    </div><!--row-->
  </div><!--container-->
</section><!--content-->

<div id="proceed" class="modal modal-fixed-footer">

  <div class="modal-content">
    <div class="coupon-preview">
      <div class="coupon-preview-div1">
        <span class="coupon-code">
          <h5></h5>
        </span>
        <p><b>Start Date:</b></p>
        <p><b>End Date:</b></p>
        <p class="days"><b>Days:</b> <span class="proceed-cbox-list"><i class="mdi-action-done"></i> Mon</span> <span class="proceed-cbox-list"><i class="mdi-action-done"></i> Tue</span> <span class="proceed-cbox-list"><i class="mdi-action-done"></i> Wed</span> <span class="proceed-cbox-list"><i class="mdi-action-done"></i> Thu</span></p>
      </div>
      <div class="coupon-preview-div2">
        <h4></h4>
        <div class="middle-con">
          <span class="user-con">
            <img src="<?php echo $this->request->webroot; ?>img/Koala.jpg"/>
            <label>New User</label>
          </span>
          <span class="nos-con">
            <label><i class="mdi-toggle-radio-button-on"></i> 50 times/user</label>
          </span>
        </div>
        <h5></h5>
        <div class="bottom-btns">
          <span style="border-right:1px solid #CECECE"><p>Web</p>
            <span class="img-con">
              <img src="<?php echo $this->request->webroot; ?>img" class="responsive-img"/>
            </span>
          </span>
          <span><p>Android</p>
            <span class="img-con">
              <img src="<?php echo $this->request->webroot; ?>img" class="responsive-img"/>
            </span>             
          </span>
        </div>
      </div>
      <div class="coupon-preview-div3">
        <span class="coupon-blnc">
          <h5>Min Balance</h5>
          <p><i class="mdi-editor-attach-money"></i></p>
        </span>
        <span class="wash-type">
          <h6><i class="mdi-action-assignment-turned-in"></i> Wash Type</h6>
          <?php if (!empty($wash)) {
            foreach ($wash as $washes) {
              ?>
              <p><i class="mdi-action-done"></i><?= $washes->sname; ?></p>
              <?php }} ?>
            </span>
          </div>
        </div>coupon-preview
      </div>
      <div class="modal-footer proceed-modal-footer">
        <span>
         <input type="checkbox" id="washp" name="confirm" >
         <label for="washp">Click to confirm</label>
       </span>
       <a href="" class="waves-effect waves-red btn-flat modal-action modal-close">Close</a>
       <a href="" class="waves-effect waves-green btn-flat modal-action modal-close">Confirm</a>
     </div>
   </div>