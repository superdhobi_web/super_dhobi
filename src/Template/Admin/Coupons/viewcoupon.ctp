<?php
$day = array(
  '1' => 'Mon',
  '2' => 'Tue',
  '3' => 'Wed',
  '4' => 'Thu',
  '5' => 'Fri',
  '6' => 'Sat',
  '7' => 'Sun');
  ?>
  <div class="modal-content">
    <?php
    $daytype=json_decode($viewcoupon->weekday);
    $wastype=json_decode($viewcoupon->washtype);
    ?>          
    <div class="coupon-preview">
      <div class="coupon-preview-div1">
        <span class="coupon-code">
          <h5><?=$viewcoupon->coupon_code?></h5>
        </span>
        <p><b>Start Date:</b><?= $viewcoupon->start_date?></p>
        <p><b>End Date:</b><?= $viewcoupon->end_date?></p>
        <p class="days"><b>Days:</b>
          <?php foreach ($daytype as $key=>$days) {

            ?>
            <span class="proceed-cbox-list" value=''>
              <i class="mdi-action-done"></i><?=$day[$days];?>
              <?php } ?>
            </span> 
            <!-- <span class="proceed-cbox-list"><i class="mdi-action-done"></i> Tue</span> <span class="proceed-cbox-list"><i class="mdi-action-done"></i> Wed</span> <span class="proceed-cbox-list"><i class="mdi-action-done"></i> Thu</span> -->
          </p>
        </div>
        <div class="coupon-preview-div2">
          <h4>Coupon Title :<?= $viewcoupon->coupon_name?></h4>
          <div class="middle-con">

            <!-- <img src="<?php echo $this->request->webroot; ?>img/Koala.jpg"/> -->
            <label>
              <i class="mdi-toggle-radio-button-on"></i>
              <?php if ($viewcoupon['customer_type']==1) { echo "New User";
            }elseif ($viewcoupon['customer_type']==2) {
              echo "Existing User";
            }elseif ($viewcoupon['customer_type']==3) {
              echo "Both";
            }
            ?>
          </label>
          
            <!-- <span class="nos-con">
              <label><i class="mdi-toggle-radio-button-on"></i> 50 times/user</label>
            </span> -->
          </div>
          <h5>Coupon Description: <?= $viewcoupon->coupon_desc?></h5>
          <div class="bottom-btns">
            <span style="border-right:1px solid #CECECE"><p>Web</p>
              <span class="img-con">
                <img src="<?php echo $this->request->webroot; ?>img/<?= $viewcoupon->image_web; ?>" class="responsive-img"/>
              </span>
            </span>
            <span><p>Android</p>
              <span class="img-con">
                <img src="<?php echo $this->request->webroot; ?>img/<?= $viewcoupon->image_mob; ?>" class="responsive-img"/>
              </span>             
            </span>
          </div>
        </div>
        <div class="coupon-preview-div3">
          <span class="coupon-blnc">
            <h5>Min Balance</h5>
            <p><i class="mdi-editor-attach-money"></i><?= $viewcoupon->min_apply_amt ?></p>
          </span>
          <span class="wash-type">
            <h6><i class="mdi-action-assignment-turned-in"></i> Wash Type</h6>
            <?php if (!empty($wash_id)) {
              foreach ($wash_id as $washes) {
                ?>
                <p><i class="mdi-action-done"></i><?= $washes;?></p>
                <?php }} ?>
              </span>
            </div>
          </div>
        </div>
        <div class="modal-footer proceed-modal-footer">
        <!-- <span>
         <input type="checkbox" id="washp" name="confirm" >
         <label for="washp">Click to confirm</label>
       </span> -->
       <a class="waves-effect waves-red btn-flat modal-action modal-close" onclick="closemodal(<?=$viewcoupon->id?>);">Close</a>
       <!-- <a href="" class="waves-effect waves-green btn-flat modal-action modal-close">Confirm</a> -->
     </div>