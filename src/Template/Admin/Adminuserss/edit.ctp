<div class="container">
    <div class="row">
        <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Manage Users</h5>
            <ol class="breadcrumbs">
                <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                <li class="active">Manage Users</li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <!--card stats start-->
    <div id="card-stats">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card">
                    <span class="card-title grey-text text-darken-4">
                        <span class="green-text">Edit User Details</span>
                        <i class="mdi-navigation-close right"></i>
                    </span>
                    <div class="row">
                        <form class="col s12 m8 offset-m2 right-alert" id="addadmin" method="post" action="<?= ADMIN_URL?>adminusers/add">
                            <input type="hidden" value="<?= $adminuser->id; ?>" name="id"/>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-action-account-circle prefix"></i>
                                    <input id="empname" class="validate" type="text" value="<?= $adminuser->username; ?>" name="username"/>
                                    <label for="empname" data-error="Please enter name." data-success="Perfect!">Full Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-communication-email prefix"></i>
                                    <input id="email_input2" class="validate" type="email" value="<?= $adminuser->email; ?>" name="email"/>
                                    <label for="email_input2" class="" data-error="Please enter valid email." data-success="I Like it!">Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-communication-call
                                    prefix"></i>
                                    <input id="mobile_input2" class="validate" type="number" value="<?= $adminuser->mobile; ?>" name="mobile"/>
                                    <label for="mobile_input2" class="" data-error="Please enter valid mobile." data-success="I Like it!">Mobile</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-social-group
                                    prefix"></i>
                                    <select class="validate" name="department">
                                        <option value="">Choose Department</option>
                                        <?php foreach($department as $dept){?>
                                        <option value="<?= $dept->id;?>"
                                            <?php
                                            if ($adminuser["department"] == $dept->id) {
                                                echo "selected";
                                            }
                                            ?>>

                                            <?= $dept->department_name;?></option>
                                            <?php }?>
                                        </select>
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <i class="mdi-image-assistant-photo prefix"></i>
                                        <input id="address" class="validate" type="text" value="<?= $adminuser->address; ?>" name="address"/>
                                        <label for="address" class="" data-error="Please enter address." data-success="I Like it!">Address</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <i class="mdi-communication-location-on
                                        prefix"></i>    
                                        <select name="state" required>
                                            <option value="all">Choose State</option>
                                            <option value="Andhra Pradesh" <?php
                                            if ($adminuser["state"]=="Andhra Pradesh") {
                                                echo "selected";
                                            }
                                            ?>>Andhra Pradesh</option>
                                            <option value="Arunachal Pradesh" <?php
                                            if ($adminuser["state"]=="Arunachal Pradesh") {
                                                echo "selected";
                                            }
                                            ?>>Arunachal Pradesh</option>
                                            <option value="Assam" <?php
                                            if ($adminuser["state"]=="Assam") {
                                                echo "selected";
                                            }
                                            ?>>Assam</option>
                                            <option value="Bihar" <?php
                                            if ($adminuser["state"]=="Bihar") {
                                                echo "selected";
                                            }
                                            ?> >Bihar</option>
                                            <option value="Chhattisgarh" <?php
                                            if ($adminuser["state"]=="Chhattisgarh") {
                                                echo "selected";
                                            }
                                            ?> >Chhattisgarh</option>
                                            <option value="Goa" <?php
                                            if ($adminuser["state"]=="Goa") {
                                                echo "selected";
                                            }
                                            ?>>Goa</option>
                                            <option value="Gujarat" <?php
                                            if ($adminuser["state"]=="Gujarat") {
                                                echo "selected";
                                            }
                                            ?> >Gujarat</option>
                                            <option value="Haryana" <?php
                                            if ($adminuser["state"]=="Haryana") {
                                                echo "selected";
                                            }
                                            ?>>Haryana</option>
                                            <option value="Himachal Pradesh" <?php
                                            if ($adminuser["state"]=="Himachal Pradesh") {
                                                echo "selected";
                                            }
                                            ?> >Himachal Pradesh</option>
                                            <option value="Jammu and Kashmir" <?php
                                            if ($adminuser["state"]=="Jammu and Kashmir") {
                                                echo "selected";
                                            }
                                            ?>>Jammu and Kashmir</option>
                                            <option value="Jharkhand"  <?php
                                            if ($adminuser["state"]=="Jharkhand") {
                                                echo "selected";
                                            }
                                            ?>>Jharkhand</option>
                                            <option value="Karnataka" <?php
                                            if ($adminuser["state"]=="Karnataka") {
                                                echo "selected";
                                            }
                                            ?> >Karnataka</option>
                                            <option value="Kerala" <?php
                                            if ($adminuser["state"]=="Kerala") {
                                                echo "selected";
                                            }
                                            ?>>Kerala</option>
                                            <option value="Madhya Pradesh" <?php
                                            if ($adminuser["state"]=="Madhya Pradesh") {
                                                echo "selected";
                                            }
                                            ?>>Madhya Pradesh</option>
                                            <option value="Maharashtra" <?php
                                            if ($adminuser["state"]=="Maharashtra") {
                                                echo "selected";
                                            }
                                            ?>>Maharashtra</option>
                                            <option value="Manipur" <?php
                                            if ($adminuser["state"]=="Manipur") {
                                                echo "selected";
                                            }
                                            ?>>Manipur</option>
                                            <option value="Meghalaya" <?php
                                            if ($adminuser["state"]=="Meghalaya") {
                                                echo "selected";
                                            }
                                            ?>>Meghalaya</option>
                                            <option value="Mizoram" <?php
                                            if ($adminuser["state"]=="Mizoram") {
                                                echo "selected";
                                            }
                                            ?> >Mizoram</option>
                                            <option value="Nagaland" <?php
                                            if ($adminuser["state"]=="Nagaland") {
                                                echo "selected";
                                            }
                                            ?>>Nagaland</option>
                                            <option value="Odisha" <?php
                                            if ($adminuser["state"]=="Odisha") {
                                                echo "selected";
                                            }
                                            ?> >Odisha</option>
                                            <option value="Punjab" <?php
                                            if ($adminuser["state"]=="Punjab") {
                                                echo "selected";
                                            }
                                            ?>>Punjab</option>
                                            <option value="Rajasthan" <?php
                                            if ($adminuser["state"]=="Rajasthan") {
                                                echo "selected";
                                            }
                                            ?>>Rajasthan</option>
                                            <option value="Sikkim" <?php
                                            if ($adminuser["state"]=="Sikkim") {
                                                echo "selected";
                                            }
                                            ?> >Sikkim</option>
                                            <option value="Tamil Nadu" <?php
                                            if ($adminuser["state"]=="Tamil Nadu") {
                                                echo "selected";
                                            }
                                            ?>>Tamil Nadu</option>
                                            <option value="Telangana" <?php
                                            if ($adminuser["state"]=="Telangana") {
                                                echo "selected";
                                            }
                                            ?>>Telangana</option>
                                            <option value="Tripura" <?php
                                            if ($adminuser["state"]=="Tripura") {
                                                echo "selected";
                                            }
                                            ?> >Tripura</option>
                                            <option value="Uttar Pradesh" <?php
                                            if ($adminuser["state"]=="Uttar Pradesh") {
                                                echo "selected";
                                            }
                                            ?>>Uttar Pradesh</option>
                                            <option value="Uttarakhand" <?php
                                            if ($adminuser["state"]=="Uttarakhand") {
                                                echo "selected";
                                            }
                                            ?>>Uttarakhand</option>
                                            <option value="West Bengal" <?php
                                            if ($adminuser["state"]=="West Bengal") {
                                                echo "selected";}
                                                ?>>West Bengal</option>
                                            </select>
                                            <!--                                            <input id="state" class="validate" type="text" value="" name="state"/>-->
                                            <label for="state" class="active" data-error="Please choose state." data-success="I Like it!">State</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="mdi-image-assistant-photo prefix"></i>
                                            <input id="pincode" class="validate" type="number" value="<?= $adminuser->pin; ?>" name="pin"/>
                                            <label for="pincode" class="" data-error="Please enter pin." data-success="I Like it!">Pin</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button class="btn green waves-effect waves-light right" type="submit" name="action" id="usersubmit">Save
                                                <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--card stats end-->

        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
            <a class="btn-floating btn-large">
                <i class="mdi-content-create"></i>
            </a>
            <ul>
                <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
            </ul>
        </div>
        <!-- Floating Action Button -->
    </div>
</section>
<!-- END CONTENT -->