<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Manage Users</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Manage Employee</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end--> 
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content filter">
                            <div class="row">
                                <div class="col m5 s12">
                                    <h6 class="left teal-text">
                                        Search Employee
                                    </h6>  
                                    <div class="col m12">
                                        <form action="" method="get">
                                            <div class="col m5">
                                                <div class="input-field">
                                                    <select name="userCondition" id="userCondition" class="validate">
                                                        <option value="all" disabled selected>Search By...</option>
                                                        <option value="emp_id" <?php
if ($this->request->query("userCondition") == "emp_id") {
    echo "selected";
}
?>>Employee ID</option>
                                                        <option value="department" <?php
                                                                if ($this->request->query("userCondition") == "department") {
                                                                    echo "selected";
                                                                }
?>>Department</option>
                                                        <option value="username" <?php
                                                                if ($this->request->query("userCondition") == "username") {
                                                                    echo "selected";
                                                                }
?>>Employee Name</option>
                                                        <option value="mobile" <?php
                                                                if ($this->request->query("userCondition") == "mobile") {
                                                                    echo "selected";
                                                                }
?>>Mobile No</option>
                                                        <option value="email" <?php
                                                                if ($this->request->query("userCondition") == "email") {
                                                                    echo "selected";
                                                                }
?>>Email ID</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col m5">
                                                <div class="input-field">
                                                    <input id="getUser_src" name="userData" onkeyup="return getAutolist();" type="text" value="<?php
                                                                if ($this->request->query("userData")) {
                                                                    echo $this->request->query("userData");
                                                                }
?>" class="validate">
                                                    <label for="getUser_src" class="">Enter Data...</label>
                                                </div>
                                            </div>
                                            <div class="col m2">
                                                <br>
                                                <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit">
                                                    <i class="mdi-content-forward"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card" style="overflow:auto;">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left">
                                    Employee Details
                                </span>
                                <a href="<?= ADMIN_URL;?>adminusers/adduser"><i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Add Employee Details"></i></a>
                            </span>                                    
                        </div>
                        <div class="col s12 m12 l12">
                            <table class="striped responsive-table">
                                <thead>
                                    <tr class="white-text">
                                        <th class="teal">Action</th>
                                        <th class="teal"><?= $this->Paginator->sort('username', ['label' => 'User Detail']); ?></th>
                                        <th class="teal">Address</th>                                        
                                        <th class="teal"><?= $this->Paginator->sort('deprtment_name', ['label' => 'Department Name']); ?></th>
                                        <th class="teal">Status</th>                                          
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($adminusers as $adminuser): ?>
                                        <tr>
                                            <td>   
                                                <!--                                                <a title="View" href="" class="btn-floating btn-small cyan waves-effect waves-light chat-collapse">
                                                                                                        <i class="mdi-action-launch"></i>
                                                                                                    </a> -->
                                                <?php $userid = base64_encode($adminuser->id); ?>
                                                <a class="btn-floating btn-small  green waves-effect waves-light "  onclick="addreadonly('<?= $userid; ?>');" title="Edit" href="#">
                                                    <i class="mdi-content-create"></i>
                                                </a>
                                                <?php
                                                echo $this->Form->postLink(
                                                "<i class='mdi-action-delete'> </i>",
                                                ['action' => 'delete', $adminuser->id],
                                                ['escape' => false, 'title' => 'Delete', 'class' => 'btn-floating btn-small red waves-effect waves-light', 'confirm' => __('Are you sure you want to delete employee {0}?', $adminuser->username)] // third
                                                );

//echo $this->Form->postLink('<i class="mdi-action-delete"> </i>', array('action' => 'delete', $user->id), array('class' => 'btn btn-primary btn-sm'), 'Are you sure?');
                                                ?>
                                            </td>
                                            <td class="">                                                
                                                <span class="teal-text text-darken-3">
                                                    <i class="mdi-social-person"></i> <b><?= h($adminuser->name); ?></b>
                                                </span> <br>
                                                <span class="green-text text-darken-1">
                                                    <i class="mdi-hardware-smartphone"></i> <?= h($adminuser->mobile); ?> 
                                                </span><br>
                                                <span class="blue-text text-darken-1">
                                                    <i class="mdi-communication-email"></i> <?= h($adminuser->email); ?>               
                                                </span>
                                            </td>
                                            <td>
                                                <span class="green-text text-darken-1">
                                                    <i class="mdi-maps-pin-drop"></i>
                                                    <b><?= h($adminuser->address); ?></b>
                                                </span> <br>
                                                <?= h($adminuser->address) . ","; ?> <?= h($adminuser->state) . ","; ?><?= h($adminuser->pin); ?>
                                            </td>
                                            <td>
                                                <?php
                                                $dept_id = $adminuser->department;
                                                foreach ($department as $dept) {
                                                    if ($dept_id == $dept->id)
                                                        echo $dept->department_name;
                                                }
                                                ?>
                                            </td>
                                            <td><div class="switch">
                                                    <label>
                                                        <?php $controller = $this->request->params['controller']; ?>
                                                        <input type="checkbox" <?php
                                                    if ($adminuser->status == 1) {
                                                        echo "Checked";
                                                    }
                                                        ?> value="<?php
                                                           if ($adminuser->status == 1) {
                                                               echo 1;
                                                           } else
                                                               echo 2;
                                                           ?>"class="Addcheckbox" id="defaultAdd<?= $adminuser->id; ?>" name='status' onchange="changeStatus(this.value,<?= $adminuser->id; ?>,'<?= $controller; ?>');"/>
                                                        <span class="lever"></span>
                                                    </label>
                                                </div></td>

                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="paginator">
                                <ul class="pagination">
<?= $this->Paginator->prev('< ' . __('previous')) ?>
<?= $this->Paginator->numbers() ?>
<?= $this->Paginator->next(__('next') . ' >') ?>
                                </ul>
                                <p><?= $this->Paginator->counter() ?></p>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!--card stats end-->

        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
            <a class="btn-floating btn-large">
                <i class="mdi-content-create"></i>
            </a>
            <ul>
                <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
            </ul>
        </div>
        <!-- Floating Action Button -->
    </div>
</section>
<!-- END CONTENT -->