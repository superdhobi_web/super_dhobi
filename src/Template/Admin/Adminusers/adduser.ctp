<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Employee Management</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Add Employee</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <div class="row">
            <?php // pr($adminuser);?>
            <div class="col s12 m12 l12">
                <div class="card-panel">
                    <form id="employeeform" method="post" >
                        <input type="hidden" name="id" value="<?php
            if (!empty($adminuser)) {
                echo base64_decode($this->request->params['pass'][0]);
            }
            ?>"/>
                        <div class="field-con"> <!--field-con Start-->
                            <fieldset>
                                <legend>Basic Info</legend>
                                <?php if(!empty($adminuser)){?>
                                <input type="button" value="Edit" class="btn waves-effect waves-light  cyan darken-2" onclick="removeReadonly();"/>
                                <?php }?>
                                <div class="input-field col s8 offset-s2">
                                    <input id="clsreadonly" class="validate clsreadonly"  value="<?php
                               if (!empty($adminuser)) {
                                   echo $adminuser['name'];
                               }
            ?>" type="text" required name="name">
                                    <label for="basic1">Name</label>
                                </div>
                                <div class="input-field col s8 offset-s2">
                                    <?php
                                    $cont = $this->request->params['controller'];
                                    (!empty($adminuser)) ? ($id = base64_decode($this->request->params["pass"][0])) : $id = "";
                                    ?>
                                    <input type="email" required class="validate clsreadonly" name="email" value="<?php
                                    if (!empty($adminuser)) {
                                        echo $adminuser['email'];
                                    }
                                    ?>" id="email_input2" onblur="uniquedetails(1,'<?= $id; ?>','<?= $cont; ?>');"/>
                                    <label for="email_input2">Email</label>
                                </div>                  
                                <div class="input-field col s8 offset-s2">
                                    <input type="number" required class="validate clsreadonly" id="mobile_input2" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['mobile'];
                                           }
                                    ?>" name="mobile" onblur="uniquedetails(2,'<?= $id; ?>','<?= $cont; ?>');"/>
                                    <label for="mobile_input2">Mobile</label>
                                </div>
                                <div class="input-field col s8 offset-s2">
                                    <select class="browser-default validate clsreadonly" required name="department">
                                        <option value="">Choose Department</option>
                                        <?php foreach ($departmentlist as $dept) { ?>
                                            <option value="<?= $dept->id; ?>" <?php
                                        if (!empty($adminuser)) {
                                            if ($adminuser['department'] == $dept->id) {
                                                echo "selected";
                                            }
                                        }
                                            ?>><?php echo $dept->department_name; ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                                <div class="input-field col s8 offset-s2">
                                    <input type="text" required class="validate clsreadonly" id="designation" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['designation'];
                                           }
                                    ?>" name="designation"/>
                                    <label for="designation">Designation</label>
                                </div>
                                <div class="input-field col s8 offset-s2 gender-con">
                                    <label for="">Gender</label>
                                    <p>
                                        <input name="gender" value="Male" required type="radio" <?php
                                                if (!empty($adminuser)) {
                                                    if ($adminuser['gender'] == "Male") {
                                                        echo "checked";
                                                    }
                                                }
                                                ?>class="validate clsreadonly" id="test1" />
                                        <label for="test1">Male</label>
                                    </p>
                                    <p>
                                        <input name="gender" required type="radio" value="Female" <?php
                                               if (!empty($adminuser)) {
                                                   if ($adminuser['gender'] == "Female") {
                                                       echo "checked";
                                                   }
                                               }
                                                ?> class="validate clsreadonly" id="test2" />
                                        <label for="test2">Female</label>
                                    </p>

                                </div>                  
                            </fieldset>
                        </div><!--field-con End-->

                        <div class="field-con"> <!--field-con Start-->
                            <fieldset>
                                <legend>Address</legend>
                                <div class="input-field col s8 offset-s2">
                                    <textarea id="address1" name="address" class="materialize-textarea clsreadonly" length="120"><?php
                                               if (!empty($adminuser)) {
                                                   echo $adminuser['address'];
                                               }
                                                ?></textarea>
                                    <label for="address">Textarea</label>
                                </div>
                                <div class="input-field col s8 offset-s2">
                                    <input id="address2" type="text" name="landmark" value="<?php
                                        if (!empty($adminuser)) {
                                            echo $adminuser['landmark'];
                                        }
                                                ?>" class="clsreadonly">
                                    <label for="landmark">Landmark</label>
                                </div>                  
                                <div class="input-field col s8 offset-s2">
                                    <?php $state = array("Select State","Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chhattisgarh", "Chhattisgarh", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal"); ?>
                                    <select name="state" class="clsreadonly browser-default">
                                        <?php foreach ($state as $s) { ?>
                                            <option value="<?= $s; ?>" <?php
                                        if (!empty($adminuser['state'])) {
                                            if ($adminuser['state'] == $s) {
                                                echo "selected";
                                            }
                                        }
                                            ?>><?= $s; ?></option>
                                                <?php } ?>
                                    </select>
                                </div>  
                                <div class="input-field col s8 offset-s2 pin-con">
                                    <div class="col s12">
                                        <label>Pin</label>
                                    </div>      
                                    <?php
                                    if (!empty($adminuser)) {
                                        $pincode = str_split($adminuser['pin'], 1);
                                    } 
                                    
                                    ?>
                                    <div class="col s2" style="padding-left:0;">
                                        <input id="" type="text" class="clsPin clsreadonly" maxlength="1" name="pin[]" value="<?php
                                    if (!empty($adminuser['pin'])) {
                                        echo $pincode[0];
                                    }
                                    ?>" class="clsreadonly">
                                    </div>
                                    <div class="col s2">
                                        <input id="" type="text" class="clsPin clsreadonly" maxlength="1" name="pin[]" value="<?php
                                               if (!empty($adminuser['pin'])) {
                                                   echo $pincode[1];
                                               }
                                    ?>" class="clsreadonly">
                                    </div>                      
                                    <div class="col s2">
                                        <input id="" type="text" class="clsPin clsreadonly" maxlength="1" name="pin[]" value="<?php
                                               if (!empty($adminuser['pin'])) {
                                                   echo $pincode[2];
                                               }
                                    ?>" class="clsreadonly">
                                    </div>                      
                                    <div class="col s2">
                                        <input id="" type="text" class="clsPin clsreadonly" maxlength="1" name="pin[]" value="<?php
                                               if (!empty($adminuser['pin'])) {
                                                   echo $pincode[3];
                                               }
                                    ?>" class="clsreadonly">
                                    </div>                      
                                    <div class="col s2">
                                        <input id="" type="text" class="clsPin clsreadonly" maxlength="1" name="pin[]" value="<?php
                                               if (!empty($adminuser['pin'])) {
                                                   echo $pincode[4];
                                               }
                                    ?>" class="clsreadonly">
                                    </div>
                                    <div class="col s2">
                                        <input id="" type="text" class="clsPin clsreadonly" maxlength="1" name="pin[]" value="<?php
                                               if (!empty($adminuser['pin'])) {
                                                   echo $pincode[5];
                                               }
                                    ?>" class="clsreadonly">
                                    </div>                                              
                                </div>                                  
                            </fieldset>
                        </div><!--field-con End-->  

                        <div class="field-con"> <!--field-con Start-->
                            <fieldset>
                                <legend>Accounts</legend>
                                <div class="input-field col s8 offset-s2">
                                    <input id="accounts1" type="text" name="branch_name" value="<?php
                                               if (!empty($adminuser)) {
                                                   echo $adminuser['branch_name'];
                                               }
                                    ?>" class="clsreadonly">
                                    <label for="accounts1">Branch Name</label>
                                </div>
                                <div class="input-field col s8 offset-s2">
                                    <input id="accounts2" type="text" name="account_no" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['account_no'];
                                           }
                                    ?>" class="clsreadonly">
                                    <label for="accounts2">A/C No</label>
                                </div>                  
                                <!--                    <div class="input-field col s8 offset-s2">
                                                                                <input id="accounts3" type="text" name="ifsc">
                        <label for="accounts3">Branch</label>
                    </div>  -->
                                <div class="input-field col s8 offset-s2">
                                    <input id="accounts4" type="text" name="ifsc" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['ifsc'];
                                           }
                                    ?>" class="clsreadonly">
                                    <label for="accounts4">IFSC</label>
                                </div>  
                                <div class="input-field col s8 offset-s2">
                                    <input id="accounts4" type="text" name="pan" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['pan'];
                                           }
                                    ?>" class="clsreadonly">
                                    <label for="accounts4">PAN</label>
                                </div>                                                  
                            </fieldset>
                        </div><!--field-con End-->          

                        <div class="field-con"> <!--field-con Start-->
                            <fieldset>
                                <legend>Qualification</legend>
                                <div class="input-field col s8 offset-s2">
                                    <input id="qualification1" type="text" name="last_qualification" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['last_qualification'];
                                           }
                                    ?>" class="clsreadonly">
                                    <label for="qualification1">Last Qualification</label>
                                </div>
                                <div class="input-field col s8 offset-s2">
                                    <input id="qualification2" type="text" name="institute" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['institute'];
                                           }
                                    ?>" class="clsreadonly">
                                    <label for="qualification2">Institute</label>
                                </div>                  
                                <div class="input-field col s8 offset-s2">
                                    <?php $year = array("2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016"); ?>
                                    <select name="yearofpassing" class="clsreadonly">
                                        <option value="" disabled selected>Year of Passing</option>
                                        <?php foreach ($year as $yr) { ?>
                                            <option value="<?= $yr; ?>" <?php
                                        if (!empty($adminuser)) {
                                            if ($adminuser["yearofpassing"] == $yr) {
                                                echo "selected";
                                            }
                                        }
                                            ?>><?= $yr; ?></option>
                                                <?php } ?>
                                    </select>
                                </div>  
                                <div class="input-field col s8 offset-s2">
                                    <input id="qualification5" type="text" name="result" value="<?php
                                                if (!empty($adminuser)) {
                                                    echo $adminuser['result'];
                                                }
                                                ?>" class="clsreadonly">
                                    <label for="qualification5">Result(%)</label>
                                </div>                                                  
                            </fieldset>
                        </div><!--field-con End-->  

                        <div class="field-con"> <!--field-con Start-->
                            <fieldset>
                                <legend>Salary</legend>
                                <div class="input-field col s8 offset-s2">
                                    <input id="salary1" type="text" name="basic" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['basic'];
                                           }
                                                ?>" class="clsreadonly">
                                    <label for="salary1">Basic Salary</label>
                                </div>
                                <div class="input-field col s8 offset-s2">
                                    <input id="salary2" type="text" name="total_TA" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['total_TA'];
                                           }
                                                ?>" class="clsreadonly">
                                    <label for="salary2">TA</label>
                                </div>                  
                                <div class="input-field col s8 offset-s2">
                                    <input id="salary3" type="text" name="tatal_DA" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['total_DA'];
                                           }
                                                ?>" class="clsreadonly">
                                    <label for="salary3">DA</label>
                                </div>  
                                <div class="input-field col s8 offset-s2">
                                    <input id="salary4" type="text" name="gross_salary" value="<?php
                                           if (!empty($adminuser)) {
                                               echo $adminuser['gross_salary'];
                                           }
                                                ?>" class="clsreadonly">
                                    <label for="salary4">Gross Salary</label>
                                </div>                                                  
                            </fieldset>
                        </div><!--field-con End-->  

                        <div class="card-panel field-con" style="text-align:center;margin:20px 0;"> <!--field-con Start-->
                            <input type="submit" value="<?php
                                           if (!empty($adminuser)) {
                                               echo 'Update';
                                           } else {
                                               echo 'Save';
                                           }
                                                ?>" class="btn waves-effect waves-light  cyan darken-2" onclick="empvalidate();" />
                        </div><!--field-con End-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
<?php if (isset($adminuser)) { ?>
        $(function(){
            alert("njn");
            $("#clsreadonly").prop("readonly",true);
        });
            
        function removeReadonly(){
            $(".clsreadonly").prop("readonly",false)
        }
<?php } ?>
</script>