<html>
    <head>
        <meta charset="utf-8">
        <title>Salary Slip</title>
        <style type="text/css">
            .sal_con{
                width: 768px;
                overflow: auto;
                background-color: #fff;
                margin: 40px auto;
                border-radius: 3px;
                font-family:monospace;
                border: thin solid #444343;
            }
            .sal_con_head{
                float:left;
                width:100%;
            }
            .sal_con_head img{
                float: left;
                width: 200px;
                height: 30px;
                margin: 15px 15px -10px;
            }
            .sal_con_body{
                float:left;
                width:100%;
                text-align:center;
                background: url(http://mytraditionalchoice.com/images/logo-circle.png) center center no-repeat;
                background-size: 200px;
            }
            .sal_con_body h2{
                font-size: 19px;
                margin: 0px 10px;
            }
            .sal_con_body table{
                width: 100%;
                text-align: left;
                border-collapse: collapse;
                font-size: 13.4px;
                color: #222;
                background:rgba(255, 255, 255, 0.91);
            }
            .sal_con_body table tr td{
                border: 1px solid #AFAAAA;
                padding: 8px;
            }

            .sal_con_body .ttwoo tr td{
                width:40%;
            }
            .nonPrintable
            {
                display:none;
            }
        </style>
    <div class="sal_con">
        <div class="sal_con_head">
            <img src="<?= $this->request->webroot ?>images/logo1.png" alt="Logo" />
        </div>
        <div class="sal_con_body">
            <?php
            // pr($employee);
            if (!empty($employee)) {
                foreach ($employee as $emp) {
                    ?>
                    <h2>
                        <span>MONTH OF</span>
                        <span><?= $emp['month']; ?></span>
                        <span><?= $emp['year']; ?></span>
                    </h2>

                    <table style="">
                        <tr>
                            <td style="width:36%;">
                                <b>Name :</b><?= $emp['name']; ?>  
                                <br>
                                <b>Pan :</b><?= $emp['pan']; ?>
                            </td>
                            <td>
                                <b>Department :</b><?= $emp['year']; ?>
                                <br>
                                <b>Designation :</b><?= $emp['designation']; ?>
                                <br>
                                <b>Bank Acc No :</b><?= $emp['account_no']; ?>
                            </td>
                            <td>
                                <?php
                                $month = array_flip(array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"));
                                ?>
                                <b>Month :</b> <?= $emp['month']; ?>
                                <br>
                                <b>No of days paid :</b><?= $emp['working_days']; ?>
                                <br>
                                <b>Month of days :</b> <?= cal_days_in_month(CAL_GREGORIAN, $month[$emp['month']] + 1, $emp['year']); ?>
                            </td>
                        </tr>


                        <tr>
                            <td>
                                <strong>EARNINGS</strong>
                            </td>
                            <td>
                                <strong>DEDUCTIONS REASON</strong>
                            </td>

                            <td>
                                <strong>LEAVE</strong>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <strong>Basic :</strong><?= $emp['basic']; ?>
                                <br>
                                <strong>TA :</strong> <?= $emp['ta']; ?>
                                <br>
                                <strong>DA :</strong>  <?= $emp['da']; ?>
                                <br>
                                <strong>Incentive :</strong>   <?= $emp['incentive']; ?>                                  
                            </td>
                            <td>
                                <strong><?= $emp['deduction_reason']; ?></strong>
                            </td>

                            <td>
                                <strong>Total Leave :</strong><?php echo $emp['leave_days']; ?></br>
                                <?php $arr = json_decode($emp['leave_type']);
                                ?>
                                <strong>CL :</strong><?php echo $arr->cl; ?>
                                <br>
                                <strong>SL :</strong><?php echo $arr->sl; ?> 
                                <br>
                                <strong>PL :</strong><?php echo $arr->pl; ?>
                                <strong>EL :</strong><?php echo $arr->el; ?>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <strong>Gross Earnings :</strong><?= $emp['gross'] ?>
                            </td>
                            <td>
                                <strong>Total Deduction : </strong><?= $emp['deduction'] ?>
                            </td>
                            <td>
                                <strong>Take Home Pay : </strong><?= $emp['net_payment'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="3">
                                <strong><i>NB: - This is a computer generated salary slip which does not require signature.</i></strong>
                            </td>
                        </tr>

                        </tr>
                    </table>
                <?php }
            } ?>
        </div>
    </div>

</body>
</html>

