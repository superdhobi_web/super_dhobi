<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Run PayRoll</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Payroll</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <!--card stats start-->
        <div id="card-stats">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card payroll-list-con">
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">
                                <span class="left filters">
                                    Payroll List
                                </span>
                                <form >
                                    <span class="left filters">
                                        <?php $year = array("2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"); ?>
                                        <select name="year">
                                            <option value="" disabled selected>Year</option>
                                            <?php foreach ($year as $yr) { ?>
                                                <option value="<?= $yr; ?>" <?php if($this->request->query("year")==$yr){echo "selected";}?>><?= $yr; ?></option>
                                            <?php } ?>
                                        </select>
                                    </span>                             
                                    <span class="left filters">
                                        <?php $month = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                        <select name="month">
                                            <option value="" disabled selected>Month</option>
                                            <?php foreach ($month as $m) { ?>
                                                <option value="<?= $m; ?>" <?php if($this->request->query("month")==$m){echo "selected";}?>><?= $m; ?></option>
                                            <?php } ?>
                                        </select>
                                    </span>                                 
                                    <span class="left filters">
                                        <button class="btn-floating waves-effect waves-light cyan darken-2" type="submit">
                                            <i class="mdi-content-forward"></i>
                                        </button>
                                    </span>
                                </form>
                                <a href="<?= ADMIN_URL; ?>adminusers/addpayroll"><i class="mdi-av-my-library-add right green-text tooltipped" data-position="left" data-tooltip="Add Payroll"></i></a>
                            </span>                                    
                        </div><!--card-content-->
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <table class="striped responsive-table centered">
                                    <thead>
                                        <tr class="white-text">
                                            <th class="teal">SI No</th>
                                            <th class="teal">Name</th>
                                            <th class="teal">Designation</th>
                                            <th class="teal"><?= $this->Paginator->sort('year', ['label' => 'Year']); ?></th>
                                            <th class="teal"><?= $this->Paginator->sort('month', ['label' => 'Month']); ?></th>
                                            <th class="teal"><?= $this->Paginator->sort('net_payment', ['label' => 'Net Payment']); ?></th>
                                            <th class="teal">Generate Date</th>
                                            <th class="teal">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($employee)) {
                                            $i = 1;
                                            foreach ($employee as $emp) {
                                                ?>
                                                <tr>
                                                    <td><?= $i; ?></td>
                                                    <td><?= $emp['employee_name']; ?></td>
                                                    <td><?= $emp['designation']; ?></td>
                                                    <td><?= $emp['year']; ?></td>
                                                    <td><?= $emp['month']; ?></td>
                                                    <td>Rs.<?= $emp['net_payment']; ?>/-</td>
                                                    <td><?= date("jS F, Y", strtotime($emp['generate_date'])); ?></td>
                                                    <td>
<!--                                                        <a href="#" title="Edit" class="btn-floating btn-small  green waves-effect waves-light">
                                                            <i class="mdi-content-create"></i>
                                                        </a>
-->                                                        <a href="<?= ADMIN_URL; ?>adminusers/deletepayroll/<?= $emp["id"]; ?>" title="Delete" class="btn-floating btn-small red waves-effect waves-light">
                                                            <i class="mdi-action-delete"> </i>
                                                        </a>                                                                                            

                                                        <a href="<?= ADMIN_URL; ?>adminusers/printpayroll/<?= base64_encode($emp["id"]); ?>" target="_blank" class="btn-floating btn-small cyan darken-2 waves-effect waves-light" title="Print Payroll">
                                                            <i class="mdi-action-print"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        } else {
                                            ?>
                                        <?php  } ?>
                                    </tbody>
                                </table>
                                <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>