<section id="content">        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Run Pay Roll</h5>
                    <ol class="breadcrumbs">
                        <li><a href="<?= BASE_URL . "admin/dashboard" ?>">Dashboard</a></li>
                        <li class="active">Create Pay Roll</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card-panel payroll-tbl-con">
                    <form method="post" id="payroll" action="">
                        <table class="table bordered">
                            <tbody>
                                <tr class="payroll-top">
                                    <td colspan="3">
                                        <p>
                                            <?php $year = array("2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"); ?>
                                            <label>Year:</label>
                                            <select name="year" id="year" class="browser-default">
                                                <option value="" selected disabled >Choose Year</option>
                                                <?php foreach ($year as $yr) { ?>
                                                    <option value="<?= $yr; ?>" ><?= $yr; ?></option>
                                                <?php } ?>
                                            </select>
                                        </p>
                                        <p>
                                            <?php $month = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                            <label>Month:</label>
                                            <select name="month" id="month" class="browser-default" onchange="monthCalculate()">
                                                <option value="" selected disabled >Choose Month</option>
                                                <?php foreach ($month as $m) { ?>
                                                    <option value="<?= $m; ?>"><?= $m; ?></option>
                                                <?php } ?>
                                            </select>
                                        </p>                                

                                    </td>
                                </tr>
                                <tr class="payroll-middle">
                                    <td>
                                        <p class="select-box">
                                            <label>Name:</label>
                                            <select name="employee_id" onchange="empDetail(this.value)" id="employee_id" class="browser-default validate">
                                                <option value="" selected >Choose Name</option>
                                                <?php foreach ($adminuser as $emp) { ?>
                                                    <option value="<?= $emp["id"]; ?>"><?= $emp["name"]; ?></option>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden" name="employee_name" id="employee_name">
                                        </p>
                                        <p><label>Basic:</label> <input type="text" class="validate" name="basic" id="basic" onblur="totalCalculate();"/></p>
                                        <input type="hidden" id="basic_hid" value="0">
                                        <p><label>TA:</label> <input type="text" value="" class="validate" name="ta" id="ta" value="" onblur="totalCalculate();"></p>
                                        <input type="hidden" id="ta_hid" value="0">
                                        <p><label>DA:</label> <input type="text" value="" class="validate" name="da" id="da" value="" onblur="totalCalculate();"></p>
                                        <input type="hidden" id="da_hid" value="0">
                                        <p><label>Incentive:</label> <input type="text" value="0" name="incentive" class="validate" id="incentive" onblur="totalCalculate();"></p>
                                    </td>
                                    <td>
                                        <p class="filled"><label>Department: <span id="dept_span"></span></label>
                                        </p>
                                        <p><label>Bank:</label> <input type="text" name="bank_name" id="bank_name" class="validate" /></p>
                                        <p><label>A/C No:</label> <input type="text" name="account_no" id="account_no"class="validate" /></p>
                                        <p class="select-box">
                                            <label>Payment Method:</label>
                                            <select name="payment_mode" class="browser-default validate" id="payment_mode">
                                                <option value="">Select Method</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="Online">Online</option>
                                            </select>                                
                                        </p>
                                    </td>
                                    <td style="width:33%;">
                                        <p><label>No of Working Days:</label> <input type="text" name="working_days" id="working_days" class="validate"/></p>
                                        <p><label>No of Leave:</label> <input type="text" class="validate" name="leave_days" id="leave_days" onblur="leaveCalculation(this.value);"/></p>
                                        <p>
                                            <label>Leave Type:</label>
                                        <div class="">
                                            <div class="col s6">
                                                <input name="leave_type1[cl]" type="text" placeholder="CL"/>                                    
                                            </div>
                                            <div class="col s6">
                                                <input name="leave_type1[sl]" type="text" placeholder="SL"/>                                    
                                            </div>
                                            <div class="col s6">
                                                <input name="leave_type1[pl]" type="text" placeholder="PL"/>                                    
                                            </div>
                                            <div class="col s6">
                                                <input name="leave_type1[el]" type="text" placeholder="EL"/>                                    
                                            </div>


                                        </div>

                                        </p>


                                    </td>                           

                                </tr>
                                <tr class="payroll-bottom">
                                    <td>
                                        <p><label>Gross Payment:</label> <input type="text" name="gross" class="validate" id="gross" value="0"></p>
                                    </td>
                                    <td>
                                        <p><label>Deduction: </label> <input type="text" name="deduction" class="validate" id="deduction" value="0"></p>
                                    </td>
                                    <td>
                                        <p><label>Net Pay:</label> <input type="text" name="net_payment" class="validate" id="net_payment" value="0"></p>
                                    </td>
                                </tr>
                                <tr class="payroll-bottom">
                                    <td colspan="3" style="padding: 20px 25px 20px;text-align: right;">
                                        <button type="submit" class="btn waves-effect waves-light  cyan darken-2" onclick="payrollValidate();" >Generate</button>
                                    </td>
                                </tr>                       


                            </tbody>
                        </table>
                    </form>


                </div>
            </div>      
        </div>
    </div>
</section>