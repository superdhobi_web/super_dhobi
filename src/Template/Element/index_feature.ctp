<section class="features" id="features">
	<div class="container">	
		<!-- SECTION HEADER -->
		<div class="section-header wow fadeIn animated" data-wow-offset="120" data-wow-duration="1.5s">		
			<!-- SECTION TITLE -->
			<h2>Amazing Features</h2>
			<div class="colored-line">
			</div>
			<div class="section-description">
				We work our best for your satisfaction.
			</div>
			<div class="colored-line">
			</div>
		</div>
		<!-- /END SECTION HEADER -->	
		<div class="row">		
			<!-- FEATURES LEFT -->
			<div class="col-md-4 col-sm-4 features-left wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">			
				<!-- FEATURE -->
				<div class="feature">				
					<!-- ICON -->
					<div class="icon-container">
						<div class="icon">
							<i class="icon_map_alt"></i>
						</div>
					</div>				
					<!-- FEATURE HEADING AND DESCRIPTION -->
					<div class="fetaure-details">
						<h4 class="main-color">Express Service</h4>
						<p>
							 Express delivery services as per requirement of customer. We normally deliver within 48 hrs.
						</p>
					</div>				
				</div>
				<!-- /END SINGLE FEATURE -->			
				<!-- FEATURE -->
				<div class="feature">				
					<!-- ICON -->
					<div class="icon-container">
						<div class="icon">
							<i class="icon_gift_alt"></i>
						</div>
					</div>				
					<!-- FEATURE HEADING AND DESCRIPTION -->
					<div class="fetaure-details">
						<h4 class="main-color">Fabric Care</h4>
						<p>
							 Use water softner and conditioner to take care of your cloths and feel like new.
						</p>
					</div>				
				</div>
				<!-- /END SINGLE FEATURE -->			
				<!-- FEATURE -->
				<div class="feature">				
					<!-- ICON -->
					<div class="icon-container">
						<div class="icon">
							<i class="icon_tablet"></i>
						</div>
					</div>				
					<!-- FEATURE HEADING AND DESCRIPTION -->
					<div class="fetaure-details">
						<h4 class="main-color">Hygenic Wash</h4>
						<p>
							 Use of high quality organic antibacterial detergent to save your family from deseases.
						</p>
					</div>				
				</div>
				<!-- /END SINGLE FEATURE -->			
			</div>
			<!-- /END FEATURES LEFT -->		
			<!-- PHONE IMAGE -->
			<div class="col-md-4 col-sm-4">
				<div class="phone-image wow bounceIn animated" data-wow-offset="120" data-wow-duration="1.5s">
					<img src="images/single-iphone.png" alt="">
				</div>
			</div>		
			<!-- FEATURES RIGHT -->
			<div class="col-md-4 col-sm-4 features-right wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">		
				<!-- FEATURE -->
				<div class="feature">				
					<!-- ICON -->
					<div class="icon-container">
						<div class="icon">
							<i class="icon_genius"></i>
						</div>
					</div>				
					<!-- FEATURE HEADING AND DESCRIPTION -->
					<div class="fetaure-details">
						<h4 class="main-color">Eco Friendly</h4>
						<p>
							 Our washing machines consume very less water than the conventional washing machines.
						</p>
					</div>				
				</div>
				<!-- /END SINGLE FEATURE -->			
				<!-- FEATURE -->
				<div class="feature">				
					<!-- ICON -->
					<div class="icon-container">
						<div class="icon">
							<i class="icon_lightbulb_alt"></i>
						</div>
					</div>				
					<!-- FEATURE HEADING AND DESCRIPTION -->
					<div class="fetaure-details">
						<h4 class="main-color">Clothes Rejuvenator</h4>
						<p>
							We use the specific blend of surfactants, emulsifiers, chelating agents and water softeners.
						</p>
					</div>				
				</div>			
				<!-- /END SINGLE FEATURE -->			
				<!-- FEATURE -->
				<div class="feature">				
					<!-- ICON -->
					<div class="icon-container">
						<div class="icon">
							<i class="icon_ribbon_alt"></i>
						</div>
					</div>				
					<!-- FEATURE HEADING AND DESCRIPTION -->
					<div class="fetaure-details">
						<h4 class="main-color">Clothes Protector</h4>
						<p>
							 fabric protector fends off soil, stains and spills on wool, cotton, and blends without impacting the fabric’s weight, look, feel, color or breathability.
						</p>
					</div>				
				</div>
				<!-- /END SINGLE FEATURE -->			
			</div>
			<!-- /END FEATURES RIGHT -->		
		</div>
		<!-- /END ROW -->	
	</div>
	<!-- /END CONTAINER -->	
</section>