<div class="card-reveal">
    <span class="card-title grey-text text-darken-4">
        <span class="green-text">Add New Service Details</span>
        <i class="mdi-navigation-close right"></i>
    </span>
    <div class="row">
        <form class="col s12 m8 offset-m2 right-alert" id="adddepartment" action="<?= ADMIN_URL . "departments/add" ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-action-account-circle prefix"></i>
                    <input id="department_input2" class="validate" type="text" name="department_name"/>
                    <label for="department_input2" data-error="Please enter Department name." data-success="Perfect!">Department Name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn green waves-effect waves-light right" name="action" id="usersubmit" onclick="uservalidate();">Save
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>