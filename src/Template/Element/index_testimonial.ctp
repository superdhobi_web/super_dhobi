<section class="testimonials" id="testimonials">
	<div class="color-overlay">
		
		<div class="container wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
			
			<!-- FEEDBACKS -->
			<div id="feedbacks" class="owl-carousel owl-theme">
				
				<!-- SINGLE FEEDBACK -->
				<div class="feedback">
					
					<!-- IMAGE -->
					<div class="image">
						<!-- i class=" icon_quotations"></i -->
						<img src="images/clients-pic/01.jpg" alt="">
					</div>
					
					<div class="message">
						 Highly impressed by the professionalism, these  guys have demonstrated.They not only picked it from my doorstep but also delivered it within the given time and at a much cheaper rates. Keep up the good work!.
					</div>
					
					<div class="white-line">
					</div>
					
					<!-- INFORMATION -->
					<div class="name">
						Amit Sukla
					</div>
					<div class="company-info">
						Sr Faculty, Allen Career Institute
					</div>
					
				</div>
				<!-- /END SINGLE FEEDBACK -->
				
				<!-- SINGLE FEEDBACK -->
				<div class="feedback">
					
					<!-- IMAGE -->
					<div class="image">
						<!-- i class=" icon_quotations"></i -->
						<img src="images/clients-pic/02.jpg" alt="">
					</div>
					
					<div class="message">
						 " Have had them since their inauguration, after others service didn't work out. They have been outstanding. I have weekly pickups. They're like laundry fairies. They're incredibly professional, punctual, reliable, and do a fantastic job. "
					</div>
					
					<div class="white-line">
					</div>
					
					<!-- INFORMATION -->
					<div class="name">
						Sachin Singh
					</div>
					<div class="company-info">
						Sr Faculty, Allen Career Institute
					</div>
					
				</div>
				<!-- /END SINGLE FEEDBACK -->
				
				<!-- SINGLE FEEDBACK -->
				<div class="feedback">
					
					<!-- IMAGE -->
					<div class="image">
						<!-- i class=" icon_quotations"></i -->
						<img src="images/clients-pic/03.jpg" alt="">
					</div>
					
					<div class="message">
						 " I got their flyer few days back and I am a regular user of their service since then. The quality they provide is extraordinary and the turn around time is pretty fast. No one gives back in just 24 Hours. The best laundry service for house wives like me. "
					</div>
					
					<div class="white-line">
					</div>
					
					<!-- INFORMATION -->
					<div class="name">
						Aditi Trivedi
					</div>
					<div class="company-info">
						House Wife
					</div>
					
				</div>
				<!-- /END SINGLE FEEDBACK -->
				
			</div>
			<!-- /END FEEDBACKS -->
			
		</div>
		<!-- /END CONTAINER -->	
	</div>
	<!-- /END COLOR OVERLAY -->
</section>