<?= $this->Html->charset(); ?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="msapplication-tap-highlight" content="no">
<meta name="description" content="">
<meta name="keywords" content="">



<!-- For iPhone -->
<meta name="msapplication-TileColor" content="#00bcd4">
<meta name="msapplication-TileImage" content="<?= $this->request->webroot; ?>images/favicon/mstile-144x144.png">
<!-- For Windows Phone -->

<?php //require_once(ROOT . DS . 'vendor' . DS . "jquery-timepicker-master" . DS . "jquery-timepicker-master.php"); ?>
<!-- CORE CSS-->   
<?= $this->Html->css('materialize'); ?>
<?= $this->Html->css('style'); ?>
<?php //echo $this->Html->css(['materialize','style']); ?>
<!-- Custome CSS-->    
<?= $this->Html->css("custom/custom.css"); ?>
<?= $this->Html->css("admin/jquery.timepicker.css"); ?>
<?= $this->Html->css("plugins/perfect-scrollbar.css"); ?>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
<?= $this->Html->css("plugins/jquery-jvectormap.css"); ?>
<?= $this->Html->css("plugins/chartist.min.css"); ?>
<?= $this->Html->css("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"); ?>


<!-- Start Page Loading -->
<!--    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>-->
<!-- End Page Loading -->

<!-- START HEADER -->
<header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
        <nav class="navbar-color">
            <div class="nav-wrapper">
                <ul class="left">                      
                    <li>
                        <h1 class="logo-wrapper">
                            <a href="<?= ADMIN_URL;?>dashboard/index" class="brand-logo darken-1">
                                <img src="<?= $this->request->webroot; ?>images/materialize-logo.png" alt="materialize logo">
                            </a> 
                            <span class="logo-text">Materialize</span>
                        </h1>
                    </li>
                </ul>
                <div class="header-search-wrapper hide-on-med-and-down">
                    <i class="mdi-action-search"></i>
                    <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Search..."/>
                </div>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                            <i class="mdi-action-settings-overscan"></i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown">
                            <i class="mdi-social-notifications"><small class="notification-badge">12</small>
                            </i>
                        </a>
                    </li>                        
                    <!-- <li>
                        <a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse">
                            <i class="mdi-communication-chat"></i>
                        </a>
                    </li> -->
                </ul>
                <!-- notifications-dropdown -->
                <ul id="notifications-dropdown" class="dropdown-content">
                    <li>
                        <h5>NOTIFICATIONS <span class="new badge orange">5</span></h5>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#" class="pink-text"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                    </li>
                    <li>
                        <a href="#" class="pink-text"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                    </li>
                    <li>
                        <a href="#" class="pink-text"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                    </li>
                    <li>
                        <a href="#" class="pink-text"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                    </li>
                    <li>
                        <a href="#" class="pink-text"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- end header nav-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</header>

<!-- END HEADER -->