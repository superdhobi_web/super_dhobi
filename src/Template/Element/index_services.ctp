<section class="our-services grey-bg" id="service">
    <div class="container">	
        <!-- SECTION HEADER -->
        <div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">

            <!-- SECTION TITLE -->
            <h2 class="dark-text">Our Services</h2>
            <div class="colored-line">
            </div>

            <div class="section-description">
                We offer the Services that fulfils your requirement.
            </div>

            <div class="colored-line">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay=".5s">
                <div class="service-img">
                    <img src="images/services/wash.png" class="img-responsive">
                </div>
                <h3>Wash</h3>
                <p>
                   Premium machine wash with ironing to feel u like king. 
                </p>
            </div><!--single-services-->

            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay=".8s">
                <div class="service-img">
                    <img src="images/services/iron.png" class="img-responsive">
                </div>
                <h3>Iron</h3>
                <p>
                    Premium iron and steam iron by experienced experts.
                </p>
            </div><!--single-services-->

            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay="1.1s">
                <div class="service-img">
                    <img src="images/services/dcln.png" class="img-responsive">
                </div>
                <h3>Dry Clean</h3>
                <p>
                    Drycleaning with care for all your premium cloth and woolens .
                </p>
            </div><!--single-services-->

            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay=".5s">
                <div class="service-img">
                    <img src="images/services/shcln.png" class="img-responsive">
                </div>
                <h3>Shoe Dry Clean</h3>
                <p>
                    Shoe cleaning, the way to look like new.
                </p>
            </div><!--single-services-->

            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay=".8s">
                <div class="service-img">
                    <img src="images/services/sofacln.png" class="img-responsive">
                </div>
                <h3>Sofa Dry Clean</h3>
                <p>
                    Using vaccum cleaning and chemical according to fabric.
                </p>
            </div><!--single-services-->

            <div class="col-sm-4 single-services wow fadeInUp" data-wow-delay=".8s">
                <div class="service-img">
                    <img src="images/services/carcln.png" class="img-responsive">
                </div>
                <h3>Car Dry Clean</h3>
                <p>
                    Dryclean your car at your home according to your timings.
                </p>
            </div><!--single-services-->

        </div><!--row-->
    </div><!--container-->
</section>