    <!-- START FOOTER -->
    <footer class="page-footer grey">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 
                <a class="grey-text text-lighten-4" href="http://www.superdhobi.com" target="_blank">Superdhobi</a> 
                All rights reserved.
                <span class="right"> 
                    Design and Developed by 
                    <a class="grey-text text-lighten-4" href="http://www.pcinfosolutions.com">PCIS Pvt. Ltd.</a>
                </span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->
    <?= $this->element("script"); ?>
    <!-- Toast Notification -->
    <script type="text/javascript">
    // Toast Notification
    // $(window).load(function() {
    //     setTimeout(function() {
    //         Materialize.toast('<span>Hey... Welcome to Superdhobi!!!</span>', 1500);
    //     }, 1500);
    //     setTimeout(function() {
    //         Materialize.toast('<span>You have <span class="yellow-text">5</span> new orders</span>', 3000);
    //     }, 5000);
    //     setTimeout(function() {
    //         Materialize.toast('<span>You have new messages.</span><a class="btn-flat blue-text" href="#">Read<a>', 3000);
    //     }, 15000);
    // });
</script>
<script type="text/javascript">
    /*
     * Masonry container for blog page
     */
     var $containerBlog = $("#blog-posts");
     $containerBlog.imagesLoaded(function() {
      $containerBlog.masonry({
        itemSelector: ".blog",
        columnWidth: ".blog-sizer",
    });
  });
 </script>
 <script src="//cdn.ckeditor.com/4.5.8/standard/ckeditor.js"></script>
 <script>
    CKEDITOR.replace( 'editor1' );
</script>
<script>
    <?php 
    if(!empty($this->request->query("orderdate")) || $this->request->params['controller']=='Orders'){ 
        if (!empty($users['pickupdate'])) {
            $pickAd=date("jS F, Y", strtotime($users['pickupdate']));
            $pickSlot=empty($users['pickupslot'])?'':$users['pickupslot'];  ?>
            getslot('<?=$pickAd?>',1,'<?=$pickSlot?>'); 
            <?php  
            $da=date("jS F, Y", strtotime($users['deliverydate']));
            $slot=empty($users['deliveryslot'])?'':$users['deliveryslot'];  ?>
            getslot('<?=$da?>',2,'<?=$slot?>'); 
            <?php  
        }
        if(!empty($this->request->query("orderdate")))
        { 
            $da=date("jS F, Y", strtotime($this->request->query("orderdate")));
            $slot=empty($this->request->query("oredrslot"))?'':$this->request->query("oredrslot");?>
            getslot('<?=$da?>',1,'<?=$slot?>'); <?php
        }?>
        
        <?php } ?>
    </script>