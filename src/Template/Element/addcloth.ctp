<div class="card-reveal">
    <span class="card-title grey-text text-darken-4">
        <span class="green-text">Add New User Details</span>
        <i class="mdi-navigation-close right"></i>
    </span>
    <div class="row">
        <form class="col s12 m8 offset-m2 right-alert" id="userdetail" action="<?= ADMIN_URL . "clothtypes/add" ?>" method="post">
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-action-account-circle prefix"></i>
                    <select class="validate" name="parent_type">
                        <option value="Men">Men</option>
                        <option value="Women">Women</option>
                        <option value="Children">Children</option>
                    </select>
                    <label for="user_input2" data-error="" data-success="Perfect!">Parent name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-action-account-circle prefix"></i>
                    <input id="first_input2" class="validate" type="text" name="cloth_name"/>
                    <label for="first_input2" data-error="Please enter name." data-success="Perfect!">Cloth Name</label>
                </div>
            </div>
            <?php
            if (!empty($cloth_parent)) {
                ?>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="mdi-action-account-circle prefix"></i>
                        <select class="validate" name="cloth_parent">
                            <option value="">Select</option>
                            <?php
                            foreach ($cloth_parent as $clothtypes) {
                               // pr($clothtypes);//exit;
                                ?>
                                <option value="<?= $clothtypes->cloth_parent; ?>"><?= $clothtypes->cloth_name; ?></option><?php } ?>
                        </select>
                        <label for="user_input2" data-error="" data-success="Perfect!">Parent name</label>
                    </div>
                </div><?php } ?>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn green waves-effect waves-light right">Save
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>