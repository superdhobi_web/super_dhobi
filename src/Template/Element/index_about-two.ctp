<section class="app-brief grey-bg" id="brief1">
	<div class="container">
		
		<div class="row">
			
			<!-- PHONES IMAGE -->
			<div class="col-md-6 left-align">
				<h2 class="dark-text">Super Dhobi Videos</h2>
				
				<div class="colored-line-left">
				</div>
				<div class="phone-video">
					<a href="#" data-toggle="modal" data-target="#vdoModal" class="view-video">
						<img src="<?php echo $this->request->webroot; ?>images/phone.png" alt="video" class="img-responsive wow fadeInLeft" data-wow-delay=".5s" style="max-height: 250px;margin: 0 auto;"/>
					</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="phone-video">
					<a href="#" data-toggle="modal" data-target="#vdoModal" class="view-video">
						<img src="<?php echo $this->request->webroot; ?>images/phone-p.png" alt="video" class="img-responsive wow bounceInDown" data-wow-delay=".8s" style="max-height:410px; margin:0 auto;"/>
					</a>
				</div>
			</div>
			
		</div>
		<!-- /END ROW -->	
	</div>
	<!-- /END CONTAINER -->
</section>