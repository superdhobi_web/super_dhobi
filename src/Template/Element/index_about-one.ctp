<section class="app-brief" id="brief2">
	<div class="container">
		
		<div class="row">
			
			<!-- BRIEF -->
			<div class="col-md-6 left-align wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">
				
				<!-- SECTION TITLE -->
				<h2>About Super Dhobi</h2>
				
				<div class="colored-line-left">
				</div>
				
				<p>Everyone chooses best clothes to wear, then why should you settle even for second best when it comes to laundering them !!! <br>
                    We at “Superdhobi” takes proper care of your garments so that your ‘New clothes looks new forever’. <br>
                    Laundry is not an art, its a science. Superdhobi is committed to deliver quality laundry services at your convenience. Our state-of-the-art equipments combined with professional handling techniques replenishes the life of your garments, making them as fresh as brand new.
                    <br>Superdhobi do your dry cleaning and laundry at just the tap of a button. superdhobi.in is simply the best dry cleaning & laundry service in India provided by IITians.
					
	            <p class="privacy-policy"><i>*</i>Know about our <a href="#" data-toggle="modal" data-target="#ppModal">Privacy Policy</a></p>
				
			</div>
			<!-- /ENDBRIEF -->
			
			<!-- PHONES IMAGE -->
			<div class="col-md-6 wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
				<div class="phone-image">
					<img src="images/sd-abt.png" alt="">
				</div>
			</div>
			<!-- /END PHONES IMAGE -->
			
		</div>
		<!-- /END ROW -->	
	</div>
	<!-- /END CONTAINER -->
</section>