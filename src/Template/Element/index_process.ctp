<section class="services grey-bg" id="process">
	<div class="container">
		
		<!-- SECTION HEADER -->
		<div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
			
			<!-- SECTION TITLE -->
			<h2>Our Process</h2>
			
			<div class="colored-line">
			</div>
			<div class="section-description">
				Spend your valuable time with your family and friends. Let handle your laundry to our experts.
			</div>
			<div class="colored-line">
			</div>
			
		</div>
		<!-- /END SECTION HEADER -->
		
		<div class="row">
			
			<!-- SINGLE SERVICE -->
			<div class="col-md-3 single-service wow fadeInLeft animated" data-wow-delay=".5s">
				
				<!-- SERVICE ICON -->
				<div class="service-icon">
					<i aria-hidden="true" class="icon_mobile"></i>
				</div>
				
				<!-- SERVICE HEADING -->
				<h3>Order</h3>
				
				<!-- SERVICE DESCRIPTION -->
				<p>
					 Forget low quality dry cleaners and washerman. Order SuperDhobi through phone or android app.
				</p>
				
			</div>
			<!-- /END SINGLE SERVICE -->
			
			<!-- SINGLE SERVICE -->
			<div class="col-md-3 single-service wow fadeInLeft animated" data-wow-delay=".8s">
				
				<!-- SERVICE ICON -->
				<div class="service-icon">
					<i aria-hidden="true" class="icon_gift_alt"></i>
				</div>
				
				<!-- SERVICE HEADING -->
				<h3>Pick Up</h3>
				
				<!-- SERVICE DESCRIPTION -->
				<p>
					Super Dhobi armed with ecofriendly bags will arrive at your door to pick up your dry cleaning and laundry.
				</p>
				
			</div>
			<!-- /END SINGLE SERVICE -->
			
			<!-- SINGLE SERVICE -->
			<div class="col-md-3 single-service wow fadeInLeft animated" data-wow-delay="1.1s">
				
				<!-- SERVICE ICON -->
				<div class="service-icon">
					<i aria-hidden="true" class="icon_genius"></i>
				</div>
				
				<!-- SERVICE HEADING -->
				<h3>Clean</h3>
				
				<!-- SERVICE DESCRIPTION -->
				<p>
					 Our expert cleaners will ensure that your clothes come back looking fresh and beautiful every time.
				</p>
				
			</div>
			<!-- /END SINGLE SERVICE -->
			<!-- SINGLE SERVICE -->
			<div class="col-md-3 single-service wow fadeInLeft animated" data-wow-delay="1.4s">
				
				<!-- SERVICE ICON -->
				<div class="service-icon">
					<i aria-hidden="true" class="icon_lifesaver"></i>
				</div>
				
				<!-- SERVICE HEADING -->
				<h3>Deliver</h3>
				
				<!-- SERVICE DESCRIPTION -->
				<p>
					Your clothes will be delivered on time to your doorstep. Now getting your laundry done is that simple.
				</p>
				
			</div>
			<!-- /END SINGLE SERVICE -->        
	        
		</div>
		<!-- /END ROW -->	
	</div>
	<!-- /END CONTAINER -->
</section>