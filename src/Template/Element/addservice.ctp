<div class="card-reveal">
    <span class="card-title grey-text text-darken-4">
        <span class="green-text">Add New Service Details</span>
        <i class="mdi-navigation-close right"></i>
    </span>
    <div class="row">
        <form class="col s12 m8 offset-m2 right-alert" id="addservice" action="<?= ADMIN_URL . "services/add" ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-action-account-circle prefix"></i>
                    <input id="first_input2" class="validate" type="text" name="sname"/>
                    <label for="first_input2" data-error="Please enter Service name." data-success="Perfect!">Service Name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-action-perm-media prefix"></i>
                    <input id="sicon" class="validate" type="file" name="sicon" onchange="ValidateSingleInput(this);"/>
                    <label for="sicon" data-error="" data-success="Perfect!"></label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn green waves-effect waves-light right" name="action" id="usersubmit" onclick="submitDisable();">Save
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>