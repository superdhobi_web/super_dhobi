	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- SITE TITLE -->
	<title>SUPERDHOBI :: Premium</title>
	<!--FAV AND TOUCH ICONS-->
	<link rel="icon" href="<?php echo $this->request->webroot; ?>images/favicon/favicon-32x32.png">
	<!--STYLESHEETS-->
	<!--BOOTSTRAP-->
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/bootstrap.min.css">
	<!-- FONT ICONS -->
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>assets/elegant-icons/style.css">
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>assets/app-icons/styles.css">
	<!--[if lte IE 7]><script src="lte-ie7.js"></script><![endif]-->
	<!-- WEB FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic' rel='stylesheet' type='text/css'>
	<!-- CAROUSEL AND LIGHTBOX -->
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/owl.theme.css">
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/nivo-lightbox.css">
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/nivo_themes/default/default.css">
	<!-- ANIMATIONS -->
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/animate.min.css">
	<!-- CUSTOM STYLESHEETS -->
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/styles.css">
	<!-- COLORS -->
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/colors/blue.css"> <!-- DEFAULT COLOR/ CURRENTLY USING -->
	<!-- RESPONSIVE FIXES -->
	<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/responsive.css">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<!-- JQUERY -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
