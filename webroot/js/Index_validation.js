function loginvalidate() {
    var validator = $("#addadmin").validate({
        rules: {
            username: "required",
            password: "required"
        }
        ,
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}
