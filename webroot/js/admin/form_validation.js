$(function(){
    $("#usersubmit").submit(function(){
        if($("#first_input2").val() == ""){
            $("#first_input2").addClass("invalid");
            $("#first_input2").focus();
            return false;
        }else{
            return true;
        }
    });
    $("#adddepartment").submit(function(){
        if($("#department_input2").val() == ""){
            $("#department_input2").addClass("invalid");
            $("#department_input2").focus();
            return false;
        }
    });
    // $("#addadmin").submit(function(){
    //     if($("#empname").val() == ""){
    //         $("#empname").addClass("invalid");
    //         $("#empname").focus();
    //         return false;
    //     }
    //     // if($("#email_input2").val() == ""){
    //     //     $("#email_input2").addClass("invalid");
    //     //     $("#email_input2").focus();
    //     //     return false;
    //     // }
    //     if($("#mobile_input2").val() == ""){
    //         $("#mobile_input2").addClass("invalid");
    //         $("#mobile_input2").focus();
    //         return false;
    //     }
    //     if($("#department").val() == ""){
    //         $("#department").addClass("invalid");
    //         $("#department").focus();
    //         return false;
    //     }
    //     if($("#address").val() == ""){
    //         $("#address").addClass("invalid");
    //         $("#address").focus();
    //         return false;
    //     }
    //     if($("#state").val() == ""){
    //         $("#state").addClass("invalid");
    //         $("#state").focus();
    //         return false;
    //     }
    //     if($("#pincode").val() == ""){
    //         $("#pincode").addClass("invalid");
    //         $("#pincode").focus();
    //         return false;
    //     }
    // });
    $("#addfaqlist").submit(function(){
        if ($("#first_input2").val()=="") {
            $("#first_input2").addClass("invalid");
            $("#first_input2").focus();
            return false;
        }
        if ($("#first_input3").val()=="") {
            $("#first_input3").addClass("invalid");
            $("#first_input3").focus();
            return false;
        }

    });
    $("#addfaq").submit(function(){
        if ($("#categoryname").val()=="") {
            $("#categoryname").addClass("invalid");
            $("#categoryname").focus();
            return false;
        }
    });
    $("#itemprice").submit(function(){
        if ($("#price").val()=="") {
            $("#price").addClass("invalid");
            $("#price").focus();
            return false;
        }
        if ($("#washtype").val()=="") {
            $("#washtype").addClass("invalid");
            $("#washtype").focus();
            return false;
        }
        if ($("#desc").val()=="") {
            $("#desc").addClass("invalid");
            $("#desc").focus();
            return false;
        }
    });
    $("#add_address").submit(function(){
        if ($("#add_heading").val()=="") {
            $("#add_heading").addClass("invalid");
            $("#add_heading").focus();
            return false;
        }
        if ($("#address").val()=="") {
            $("#address").addClass("invalid");
            $("#address").focus();
            return false;
        }
        if ($("#landmark").val()=="") {
            $("#landmark").addClass("invalid");
            $("#landmark").focus();
            return false;
        }
        if ($("#pincode").val()=="") {
            $("#pincode").addClass("invalid");
            $("#pincode").focus();
            return false;
        }
    });
});
function showOption(getId)
{
    var id = getId.split('_');
    $("#optionTap"+id['1']).css("display","inline-block");
}
function hideOption(getId)
{
    var id = getId.split('_');
    $("#optionTap"+id['1']).css("display","none");
}
function validatepassword() {
    var validator = $("#password_change").validate({
        rules: {
            oldpassword: "required",
            newpassword:"required",
            password: {
                required:true,
                equalTo: "#newpassword"
            }
            
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}

function validatecoupon() {
    var validator = $("#addcoupon").validate({
        rules: {
            coupon_name: "required",
            coupon_code: {
                required: true,
                minlength: 6
            },
            coupon_desc: {
                required: true,
                maxlength: 400
            },
            no_of_times: {
                required: true,
                number:true
            },
            discount: {
                required: true,
                number: true
            },
            min_apply_amt: {
                required: true,
                number: true
            },
            min_discount: {
                required: true,
                number: true
            },
            max_discount: {
                required: true,
                number: true
            },
            customer_type: {
                required: true
            },
            'day[]': {
                required: true
            },
            'wtype[]': {
                required: true
            },
            start_date: "required",
            end_date: "required"
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}
function validate_couponcode(coupon_code){
    if(coupon_code){
        $.post(URL+"admin/coupons/check_couponcode",{
            "coupon_code":coupon_code
        },function(res){
            if(res!=0){
                $("#coupon").val('');
                $("#divCheckcoupon").html("This Coupon has already been created");
            }else{
                $("#divCheckcoupon").empty(); 
            }
        });
    }
}
function appendorder(order_id,status){
    if($(':radio[name=empuser]:checked').length == 1){
        if($("#testchk"+order_id).is(":checked")){
            var user=$(':radio[name=empuser]:checked').val();
            if(status==3){
                var img=URL+"images/truck_pick.png";  
            }else{
                var img=URL+"images/truck_delv.png";
            }
            $.post(URL+"admin/taskmanages/assigntask",{
                "order_id":order_id,
                "id_employee":user,
                "status":status
            },function(res){ 
                if(res!=0){
                    var html="<div class='chip teal white-text' id='rmv"+order_id+"'><img src='"+img+"' alt='pick/delivery'><span class='ord"+user+"'>"+order_id+"</span><i class='material-icons mdi-navigation-close' onclick=removeorder('"+order_id+"','"+user+"','"+res+"') ></i></div>";
                    $(".apdorder"+user).append(html);
                    $("#testchk"+order_id).prop("disabled",true);   
                }else{
                    alert("There is some error occoured,Please Try again");
                } 
            })
        }
    }else{
        alert("Select User First");
        $(".checkorder").each(function(){
            if(!$(this).is(":disabled")){
                $(this).attr("checked",false);
            }
        })
        return false;
    }
}
function removeorder(order_id,user_id,id_task){
    $.post(URL+"admin/taskmanages/removeassigntask",{
        "order_id":order_id,
        "id_employee":user_id,
        "id":id_task
    },function(res){ 
        if(res==1){
            $("#rmv"+order_id).remove();
            $("#testchk"+order_id).prop("checked",false);
            $("#testchk"+order_id).prop("disabled",false);
        }else{
            alert("There is some error occoured,Please Try again");
        }
    })
}
function printtask()
{
    var printContents = document.getElementById('blog-posts').innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}

function printusertask(userid){
    var allorder='';
    $(".ord"+userid).each(function(){
        allorder+=$(this).html()+" ";
    })
    if(allorder.trim()!=''){
        $("#senduserid").val(userid);
        $("#allorder").val(btoa(allorder));
        $("#alldataid").submit();
    }else{
        alert("Order not assigned to this user");
        return false;
    }
}
function checkreferby(){
    var ref_by=$("#ref_by").val();
    if(ref_by!=''){
        
        $.ajax(
        {
            type: 'POST',
            async: false,
            url: URL+'admin/users/checkreferby',
            data: {
                'ref_by':ref_by
            },
            success: function(res) {
                if (res==1) {
                    $("#userAdd").removeAttr("disabled");
                    return true;
                }else{
                    $("#userAdd").attr("disabled","disabled");
                    $("#ref_by").addClass("invalid");
                    $("#ref_by").focus();
                    return false;
                }
            }
        });
    }else{
        $("#userAdd").removeAttr("disabled");
    }
}
function uniquedetails(type,edituserid,cont){
    $("#userAdd").attr("disabled","disabled");
    if(type==1){
        var data=$("#email_input2").val();
    }else{
        var data=$("#mobile_input2").val();
        if((data.length)!=10 && (data.length) > 0){
            $("label[for=mobile_input2]").attr("data-error","Please enter a 10 digit mobile no.");
            $("#mobile_input2").addClass("invalid");
        }
    }
    if(data!=''){
        $.post(URL+"admin/"+cont+"/checkunique",{
            'data':data,
            'type':type,
            'edituserid':edituserid
        },function(res){
            if (res==1) {
                $("#userAdd").removeAttr("disabled");
            }else{
                if(type==1){
                    $("label[for=email_input2]").attr("data-error","Email Id already registered");
                    $("#email_input2").addClass("invalid");
                    $("#email_input2").val("");
                }else{
                    $("#mobile_input2_lable").attr("data-error","Mobile number already registered");
                    $("#mobile_input2").addClass("invalid");
                    $("#mobile_input2").val("");
                }
            }
        });
    }
}
function disableSubmit(refCode){
    if((refCode.length)>0){
        $("#userAdd").attr("disabled","disabled");
    }else{
        $("#userAdd").removeAttr("disabled");
    }
}
function validateaddress() {
    var validator = $("#add_address").validate({
        rules: {
            add_heading: "required",
            city: "required",
            circle: "required",
            location: "required",
            area: "required",
            address: "required",
            landmark: "required",
            pincode: {
                required: true,
                minlength: 6
            }
        }
    //        errorElement : 'div',
    //        errorPlacement: function(error, element) {
    //            var placement = $(element).data('error');
    //            if (placement) {
    //                $(placement).append(error)
    //            } else {
    //                error.insertAfter(element);
    //            }
    //        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}
function validateemp() {
    var validator = $("#addadmin").validate({
        rules: {
            empname: "required",
            email_input: "required",
            mobile_input:  {
                required: true,
                minlength: 10
            },
            department: "required",
            address: "required",
            state: "required",
            pincode: {
                required: true,
                minlength: 6
            }
        }
        ,
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}
function pricevalidate() {
    var validator = $("#itemprice").validate({
        rules: {
            washtype: "required",
            price: "required",
            desc: "required"
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}
function ValidateSingleInput(oInput) {
    var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
    if (oInput.type == "file") {
        var sFileName = oInput.value;
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
            
            if (!blnValid) {
                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}

function userValidate() {
    var validator = $("#usersubmit").validate({
        rules: {
            first_input2: "required",
            email_input2: "required",
            mobile_input2:  {
                required: true
               // minlength: 10
            }
        //ref_by: "required"
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    var y = checkreferby();
    if(x && y){
        $("#userdetail").attr("disabled","disabled");
        if($("#user_id").val()){
            $("#useredit").attr("disabled","disabled");
        } else {
            $("#userAdd").attr("disabled","disabled");
        }
        $("#usersubmit").submit();
    //        return true;
    }else{
        return false;
    }
}
function empvalidate() {
    var validator = $("#employeeform").validate({
        rules: {
            name: "required",
            email: "required",
            mobile_input:  {
                required: true,
                minlength: 10
            },
            department: "required",
            gender: "required"
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}
function opendatamodal(order_id,user_id,username){
    $("#replyto").html("Reply To "+username);
    $("#orderid").val(order_id);
    $("#userid").val(user_id);
     $("#modal-rep").openModal();
}
function getfeedback(order_id){
     $(".load").css("display","block");
    $.post(URL+"admin/feedback/getfeedback/"+order_id,{
        },function(res){
            $(".load").css("display","none");
           $("#email-details").html(res);
        });
}