/**
 * An easy tree view plugin for jQuery and Bootstrap
 * @Copyright yuez.me 2014
 * @Author yuez
 * @Version 0.1
 */

(function ($) {
    $.fn.EasyTree = function (options) {
        var defaults = {
            selectable: true,
            deletable: false,
            editable: false,
            addable: false,
            i18n: {
                deleteNull: 'Select a node to delete',
                deleteConfirmation: 'Delete this node?',
                confirmButtonLabel: 'Okay',
                editNull: 'Select a node to edit',
                editMultiple: 'Only one node can be edited at one time',
                addMultiple: 'Select a node to add a new node',
                collapseTip: 'collapse',
                expandTip: 'expand',
                selectTip: 'select',
                unselectTip: 'unselet',
                editTip: 'edit',
                addTip: 'add',
                deleteTip: 'delete',
                cancelButtonLabel: 'cancel'
            }
        };

        var warningAlert = $('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong></strong><span class="alert-content"></span> </div> ');
        var dangerAlert = $('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong></strong><span class="alert-content"></span> </div> ');

        var createInput = $('<div class="input-group"><input type="text" class="form-control"><span class="input-group-btn"><button type="button" class="btn green waves-effect waves-light confirm"></button> </span><span class="input-group-btn"><button type="button" class="btn grey waves-effect waves-light cancel"></button> </span> </div> ');

        options = $.extend(defaults, options);

        this.each(function () {
            var easyTree = $(this);//console.log(this);
            $.each($(easyTree).find('ul > li'), function() {
                var text;
                if($(this).is('li:has(ul)')) {
                    var children = $(this).find(' > ul');
                    $(children).remove();
                    var text_split = $(this).text().split('^/*/^');
                    text = text_split['1'];
                    item_id = text_split['0'];
                    level = text_split['2'];
                    type = text_split['3'];
                    $(this).html('<span><span class="glyphicon"></span><a href="javascript: void(0);"></a> </span>');
                    $(this).find(' > span > span').addClass('glyphicon-folder-open');
                    $(this).find(' > span > a').text(text);
                    $(this).find(' > span').attr("onclick","getId("+item_id+","+level+","+type+")");
                    $(this).find(' > span > a').attr("onclick","getId("+item_id+","+level+","+type+")");
                    $(this).append(children);
                }
                else {
                    var text_split = $(this).text().split('^/*/^');
                    text = text_split['1'];
                    item_id = text_split['0'];
                    level = text_split['2'];
                    type = text_split['3'];
                    $(this).html('<span><span class="glyphicon"></span><a href="javascript: void(0);"></a> </span>');
                    $(this).find(' > span > span').addClass('glyphicon-file');
                    $(this).find(' > span > a').text(text);
                    $(this).find(' > span').attr("onclick","getId("+item_id+","+level+","+type+")");
                    $(this).find(' > span > a').attr("onclick","getId("+item_id+","+level+","+type+")");
                }
            });

            $(easyTree).find('li:has(ul)').addClass('parent_li').find(' > span').attr('title', options.i18n.collapseTip);

            // add easy tree toolbar dom
            if (options.deletable || options.editable || options.addable) {
                $(easyTree).prepend('<div class="easy-tree-toolbar"></div> ');
            }

            // addable
            if (options.addable) {
                $(easyTree).find('.easy-tree-toolbar').append('<div class="create"><button class="btn cyan waves-effect waves-light" disabled><span class="mdi-content-add large"></span></button></div> ');
                $(easyTree).find('.easy-tree-toolbar .create > button').attr('title', options.i18n.addTip).click(function () {
                    var createBlock = $(easyTree).find('.easy-tree-toolbar .create');
                    $(createBlock).append(createInput);
                    $(createInput).find('input').focus();
                    $(createInput).find('.confirm').text(options.i18n.confirmButtonLabel);
                    $(createInput).find('.confirm').click(function () {
                        if ($(createInput).find('input').val() === '')
                            return;
                        var selected = getSelectedItems();
                        var item = $('<li class="next'+$(createInput).find('input').val().toLowerCase().toLowerCase().split(" ")[0]+'"><span><span class="glyphicon glyphicon-file"></span><a href="javascript: void(0);">' + $(createInput).find('input').val() + '</a> </span></li>');
                        $(item).find(' > span > span').attr('title', options.i18n.collapseTip);
                        $(item).find(' > span > a').attr('title', options.i18n.selectTip);
                        if (selected.length <= 0) {
                            $(easyTree).find(' > ul').append($(item));
                        } else if (selected.length > 1) {
                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').text(options.i18n.addMultiple);
                        } else {
                            if ($(selected).hasClass('parent_li')) {
                                $(selected).find(' > ul').append(item);
                            } else {
                                $(selected).addClass('parent_li').find(' > span > span').addClass('glyphicon-folder-open').removeClass('glyphicon-file');
                                $(selected).append($('<ul></ul>')).find(' > ul').append(item);
                            }
                        }
                        //$(createInput).find('input').val('');
                        if (options.selectable) {
                            $(item).find(' > span > a').attr('title', options.i18n.selectTip);
                            $(item).find(' > span > a').click(function (e) {
                                var li = $(this).parent().parent();
                                if (li.hasClass('li_selected')) {
                                    $(this).attr('title', options.i18n.selectTip);
                                    $(li).removeClass('li_selected');
                                }
                                else {
                                    $(easyTree).find('li.li_selected').removeClass('li_selected');
                                    $(this).attr('title', options.i18n.unselectTip);
                                    $(li).addClass('li_selected');
                                }

                                if (options.deletable || options.editable || options.addable) {
                                    var selected = getSelectedItems();
                                    if (options.editable) {
                                        if (selected.length <= 0 || selected.length > 1)
                                            $(easyTree).find('.easy-tree-toolbar .edit > button').addClass('disabled');
                                        else
                                            $(easyTree).find('.easy-tree-toolbar .edit > button').removeClass('disabled');
                                    }

                                    if (options.deletable) {
                                        if (selected.length <= 0 || selected.length > 1)
                                            $(easyTree).find('.easy-tree-toolbar .remove > button').addClass('disabled');
                                        else
                                            $(easyTree).find('.easy-tree-toolbar .remove > button').removeClass('disabled');
                                    }
                                }
                                e.stopPropagation();
                            });
                        }                       
                        var parentIDs = $("#getParentID").val();
                        var action = $("#get_for").val();
                        var level = $("#getClothLevel").val();
                        var newlevel = parseInt(level)+1;
                        var id=$(createInput).find('input').val().toLowerCase().split(" ")[0];
                        if(action=="location"){
                            var sendURL="admin/Locations/addLocation";
                            $.ajax(
                            {
                                type: 'POST',
                                url: URL+sendURL,
                                data:{
                                    location_name:$(createInput).find('input').val(),
                                    location_parent:parentIDs,
                                    level:level
                                },
                                 success: function(data) {
                                    $(".next"+id).find(' > span').attr("onclick","getId("+data+","+newlevel+","+2+")");
                                    $(".next"+id).find(' > span > a').attr("onclick","getId("+data+","+newlevel+","+2+")");
                                    console.log(data);
                                }
                            });
                        } else{
                            var sendURL="admin/Clothtypes/addCloth";
                            $.ajax(
                            {
                                type: 'POST',
                                url: URL+sendURL,
                                data:{
                                    cloth_name:$(createInput).find('input').val(),
                                    cloth_parent:parentIDs,
                                    level:level
                                        
                                },
                                success: function(data) {
                                    $(".next"+id).find(' > span > a').attr("onclick","getId("+data+","+newlevel+","+1+")");
                                    $(".next"+id).find(' > span').attr("onclick","getId("+data+","+newlevel+","+1+")");
                                    //console.log(data);
                                }
                            });
                        }
                        $(createInput).remove();
                        $(createInput).find('input').val('');

                    });
                    $(createInput).find('.cancel').text(options.i18n.cancelButtonLabel);
                    $(createInput).find('.cancel').click(function () {
                        $(createInput).remove();
                    });
                });
            }

            // editable
            if (options.editable) {
                $(easyTree).find('.easy-tree-toolbar').append('<div class="edit"><button class="btn blue waves-effect waves-light  disabled"><span class="mdi-editor-border-color"></span></button></div> ');
                $(easyTree).find('.easy-tree-toolbar .edit > button').attr('title', options.i18n.editTip).click(function () {
                    $(easyTree).find('input.easy-tree-editor').remove();
                    $(easyTree).find('li > span > a:hidden').show();
                    var selected = getSelectedItems();
                    if (selected.length <= 0) {
                        $(easyTree).prepend(warningAlert);
                        $(easyTree).find('.alert .alert-content').html(options.i18n.editNull);
                    }
                    else if (selected.length > 1) {
                        $(easyTree).prepend(warningAlert);
                        $(easyTree).find('.alert .alert-content').html(options.i18n.editMultiple);
                    }
                    else {
                        var value = $(selected).find(' > span > a').text();
                        $(selected).find(' > span > a').hide();
                        $(selected).find(' > span').append('<input type="text" class="easy-tree-editor">');
                        var editor = $(selected).find(' > span > input.easy-tree-editor');
                        $(editor).val(value);
                        $(editor).focus();
                        $(editor).keydown(function (e) {
                            if (e.which == 13) {
                                if ($(editor).val() !== '') {
                                    $(selected).find(' > span > a').text($(editor).val());
                                    $(editor).remove();
                                    $(selected).find(' > span > a').show();



                                    //alert($(editor).val());
                                    
                                    
                                    
                                    var parentIDs = $("#getParentID").val();
                                    var action = $("#get_for").val();
                                    if(action=="location"){
                                        var sendURL="admin/Locations/editLocation";
                                        $.ajax(
                                        {
                                            type: 'POST',
                                            url: URL+sendURL,
                                            data:{
                                                location_name:$(editor).val(),
                                                location_id:parentIDs
                                            },
                                            success: function(data) {
                                                console.log(data);
                                            }
                                        });
                                    } else{
                                        var sendURL="admin/Clothtypes/editCloth";
                                        $.ajax(
                                        {
                                            type: 'POST',
                                            url: URL+sendURL,
                                            data:{
                                                cloth_name:$(editor).val(),
                                                cloth_id:parentIDs
                                            },
                                            success: function(data) {
                                                console.log(data);
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                });
            }

            // deletable
            if (options.deletable) {
                $(easyTree).find('.easy-tree-toolbar').append('<div class="remove"><button class="btn red lighten-1 waves-effect waves-light  disabled"><span class="mdi-action-delete"></span></button></div> ');
                $(easyTree).find('.easy-tree-toolbar .remove > button').attr('title', options.i18n.deleteTip).click(function () {
                    var selected = getSelectedItems();
                    if (selected.length <= 0) {
                        $(easyTree).prepend(warningAlert);
                        $(easyTree).find('.alert .alert-content').html(options.i18n.deleteNull);
                    } else {
                        $(easyTree).prepend(dangerAlert);
                        $(easyTree).find('.alert .alert-content').html(options.i18n.deleteConfirmation)
                        .append('<a style="margin-left: 10px;" class="btn red lighten-1 waves-effect waves-light confirm"></a>')
                        .find('.confirm').html(options.i18n.confirmButtonLabel);
                        $(easyTree).find('.alert .alert-content .confirm').on('click', function () {
                            $(selected).find(' ul ').remove();
                            
                            
                            
                            if($(selected).parent('ul').find(' > li').length <= 1) {
                                $(selected).parents('li').removeClass('parent_li').find(' > span > span').removeClass('glyphicon-folder-open').addClass('glyphicon-file');
                                $(selected).parent('ul').remove();
                            }
                            $(selected).remove();
                            $(dangerAlert).remove();

                            var parentIDs = $("#getParentID").val();
                            var action = $("#get_for").val();
                            if(action=="location"){
                                var sendURL="admin/Locations/deleteLocation";
                                $.ajax(
                                {
                                    type: 'POST',
                                    url: URL+sendURL,
                                    data:{
                                        location_id:parentIDs
                                    },
                                    success: function(data) {
                                        console.log(data);
                                    }
                                });
                            } else{
                                var sendURL="admin/Clothtypes/deleteCloth";
                                $.ajax(
                                {
                                    type: 'POST',
                                    url: URL+sendURL,
                                    data:{
                                        cloth_id:parentIDs
                                    },
                                    success: function(data) {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    }
                });
            }

            // collapse or expand
            $(easyTree).delegate('li.parent_li > span', 'click', function (e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(':visible')) {
                    children.hide('fast');
                    $(this).attr('title', options.i18n.expandTip)
                    .find(' > span.glyphicon')
                    .addClass('glyphicon-folder-close')
                    .removeClass('glyphicon-folder-open');
                } else {
                    children.show('fast');
                    $(this).attr('title', options.i18n.collapseTip)
                    .find(' > span.glyphicon')
                    .addClass('glyphicon-folder-open')
                    .removeClass('glyphicon-folder-close');
                }
                e.stopPropagation();
            });

            // selectable, only single select
            if (options.selectable) {
                $(easyTree).find('li > span > a').attr('title', options.i18n.selectTip);
                $(easyTree).find('li > span > a').click(function (e) {
                    var li = $(this).parent().parent();
                    if (li.hasClass('li_selected')) {
                        $(this).attr('title', options.i18n.selectTip);
                        $(li).removeClass('li_selected');
                    }
                    else {
                        $(easyTree).find('li.li_selected').removeClass('li_selected');
                        $(this).attr('title', options.i18n.unselectTip);
                        $(li).addClass('li_selected');
                    }

                    if (options.deletable || options.editable || options.addable) {
                        var selected = getSelectedItems();
                        if (options.editable) {
                            if (selected.length <= 0 || selected.length > 1)
                                $(easyTree).find('.easy-tree-toolbar .edit > button').addClass('disabled');
                            else
                                $(easyTree).find('.easy-tree-toolbar .edit > button').removeClass('disabled');
                        }

                        if (options.deletable) {
                            if (selected.length <= 0 || selected.length > 1)
                                $(easyTree).find('.easy-tree-toolbar .remove > button').addClass('disabled');
                            else
                                $(easyTree).find('.easy-tree-toolbar .remove > button').removeClass('disabled');
                        }

                    }

                    e.stopPropagation();

                });
            }

            // Get selected items
            var getSelectedItems = function () {
                return $(easyTree).find('li.li_selected');
            };
        });
    };
})(jQuery);
