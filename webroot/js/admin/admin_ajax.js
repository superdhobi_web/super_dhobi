var URL = 'http://' + $(location).attr('hostname') + '/';
$(function(){
    $("#getUser_src").on("keyup",function(){
        var searchBy=$("#userCondition").val();
        if(searchBy=='all'){
            var inputValue=$('#getUser_src').val();
            $.post(URL + "users/SearchSugg",{
                "inputValue":inputValue
            },function(res){
                alert(res);
            });
        }
    });
    $("#from_time").blur(function(){
        var fromtime=$(this).val();
        $("#to_time").val(fromtime);
        $("#to_time").attr("min",fromtime);
    });

    $("#empname").blur(function(){
        var ename=$("empname").val();


    });

    /**
     *Default Address Show in address view and user view
     *   
     */   

    //  var user_id=$("#user_id").val();
    //  $.ajax({
    //     type:'POST',
    //     url:URL+'admin/Users/defaultAdd',
    //     data:{
    //         'user_id':user_id
    //     },
    //     success:function(id){console.log(id);
    //         $(".add_div"+id).switchClass("grey","cyan");
    //         $("#defaultAdd"+id).attr("checked",true);
    //     }
    // });
});

function saveCloth()
{
    var clothPname = $("#pClothid").val();
    if (clothPname!='') 
    {
        $.ajax(
        {
            type: 'POST',
            url: URL+'admin/Clothtypes/setParent',
            data: 'cloth_name='+clothPname,
            success: function(data) {
                window.location.reload();
            }
        });
    }
}
function getUserdata()
{
    var userCondition = $("#userCondition").val();
    var userData = $("#getUser_src").val();
    if (userData!='') {
        if (userCondition=='') 
        {
            var userCondition =  'all';
        }
        $.ajax(
        {
            type: 'POST',
            url: URL+'admin/Users/getsearch',
            data:{
                userCondition:userCondition,
                userData:userData
            },
            success: function(data) {
                alert(data);
                //window.location.reload();
            }
        });
    }
}
function addParentId(Pid){
    $("#slotId").val("");
    $("#day").val(Pid);
    $("#from_time").val("");
    $("#to_time").val("");
}
function addSlot(){
    var SlotId=$("#slotId").val();
    var from_time=$("#from_time").val();
    var to_time=$("#to_time").val();
    var day=$("#day").val();
    $.ajax(
    {
        type: 'POST',
        url: URL+'admin/TimeSlots/addSlot',
        data:{
            SlotId:SlotId,
            from_time:from_time,
            to_time:to_time,
            day:day
        },
        success: function(data) {
            alert(data);
            if (data != 0) 
            {
                if(data == 1){
                    $("#slotLine_"+SlotId).html(from_time+" - "+to_time);
                    //                    //                    $("#slotclick_"+SlotId).live("onclick",function(){
                    //                    //                        $("#modalslot").openModal();
                    //                    //                        $("#slotId").val(SlotId);
                    //                    //                        $("#from_time").val(from_time);
                    //                    //                        $("#to_time").val(to_time);
                    //                    //                        $("#day").val(day);
                    //                    //                    });
                    //                        $("#modalslot").unbind("click");
                    //                    $("#modalslot").on("onclick",function(){
                    //                        UpdateSlot(from_time,to_time,day,SlotId);
                    //                    });
                    $("#slotclick_"+SlotId).attr('onclick','UpdateSlot("'+from_time+'","'+to_time+'",'+day+','+SlotId+')');
                    $("#modalslot").live("onclick",function(){
                        $("#modalslot").openModal();
                    });
                }else{
                    $("#dayslot"+day).append(data);
                }
            }
            else
            {
                alert("The time slot could not be saved. Please, try again.");
            }
        }
    });
}
function UpdateSlot(from,to,Pid,SlotId){
    $("#modalslot").openModal();
    $("#slotId").val(SlotId);
    $("#from_time").val(from);
    $("#to_time").val(to);
    $("#day").val(Pid);
}
function deleteSlot(SlotId){
    var conf = confirm("Are You sure to delete the slot ?");
    if(conf){
        $.post(URL+"admin/timeslots/delete_slot/"+SlotId,{},function(res){
            window.location.href=URL+"admin/timeSlots/";
        });
    }
}
//$(function(){
//    CKEDITOR.replace('qn',
//    {
//        fullPage : true
//    });
//})
//function changeservicestatus(status,id){
//    $.ajax(
//    {
//        type: 'POST',
//        url: URL+'admin/Services/changestatus',
//        data:{
//            status:status,
//            id:id
//        },
//        success: function(res) {
//            alert(res);
//        }
//    });
//}
function saveLocation()
{
    var LocationPname = $("#pLocationid").val();
    if (LocationPname!='') 
    {
        $.ajax(
        {
            type: 'POST',
            url: URL+'admin/Locations/setParent',
            data: 'location_name='+LocationPname,
            success: function(data) {
                window.location.reload();
            }
        });
    }
}
function getChild(id,location){
    $("#ajaxLoding").show();
    $.ajax(
    {
        type: 'POST',
        async: false,
        url: URL+'admin/Locations/findChild',
        data: {
            location:location,
            id:id
        },
        success: function(data) {
            $("#ajaxLoding").hide();
            $("#get_"+id).html(data);
            if(id==1){
                $("#get_2").html("<option value=''>Location</option>");
                $("#get_3").html("<option value=''>Area</option>");
            } else if(id==2){
                $("#get_3").html("<option value=''>Area</option>");
            }
        }
    });
}
function editAddress(id){
    $("#ajaxLoding").show();
    $.ajax(
    {
        type: 'POST',
        async: false,
        url: URL+'admin/Users/setAddress',
        data: {
            id:id
        },
        success: function(data) {
            var d= $.parseJSON(data);
            getChild(1,d.city);
            $("#get_1").val(d.circle);
            getChild(2,d.circle);
            $("#get_2").val(d.location);
            getChild(3,d.location);
            $("#get_3").val(d.area);
            $(".teal").switchClass('teal','grey');
            $(".add_div"+id).not(".cyan").switchClass("grey","teal");
            $("#address_id").val(d.id);
            $("#add_heading").focus();
            $("#add_heading").val(d.add_heading);
            $("#city").val(d.city);
            $("#address").focus();
            $("#address").val(d.address);
            $("#landmark").focus();
            $("#landmark").val(d.landmark);
            $("#pincode").focus();
            $("#pincode").val(d.pincode);
            $("#ajaxLoding").hide();
        }
    });
}

/*
 * default address change
 */
function setdefault(address_id,user_id){

    $.ajax(
    {
        type: 'POST',
        url: URL+'admin/Users/setDefault',
        data: {
            id:address_id,
            user_id:user_id
        },
        success: function(data) {
            $(".Addcheckbox").not("#defaultAdd"+address_id).attr("checked",false);
            $(".cyan").switchClass("cyan","grey");
            $(".add_div"+address_id).switchClass("teal","cyan");
            $(".add_div"+address_id).switchClass("grey","cyan");
        }
    });
}
function getId(id,level,type)
{
    $("#getParentID").val(id);
    if(type==1 && level > 3){
        // alert("You Cann't add more than this level");
        $(".easy-tree-toolbar .create > button").attr("disabled","disabled");
        return false;
    }else if(type==2 && level > 4){
        // alert("You Cann't add more than this level");
        $(".easy-tree-toolbar .create > button").attr("disabled","disabled");
        return false;
    }else{
        $("#getClothLevel").val(level);
        $(".easy-tree-toolbar .create > button").removeAttr("disabled");
    }

}
function changeStatus(status,id,controller) {
    if (status == 1) {
        var conf = confirm("Are You sure to inactivate the "+controller+" ?");
        var status = 2;
    } else {
        var conf = confirm("Are You sure to activate the "+controller+" ?");
        var status = 1;
    }
    if (conf) {
        if (id != '') {
            $.post(URL + "admin/"+ controller +"/changestatus", {
                "id": id,
                "status": status
            }, function (res) {
                window.location.href = "";
            })
        }
    }else{
        if(status==2){
            $("#defaultAdd"+id).prop("checked","checked").attr('checked', 'checked');
        } else {
            $("#defaultAdd"+id).removeAttr("checked").removeAttr('checked');
        }
        
    }
}
function modalopen(id){
    $("#cloth_id").val(id);
    $("#modaladd_price").openModal();
}
function editmodalopen(id,price,wasid,clothid){
    $("#cloth_id_id").val(clothid);
    $("#id_price").val(id);
    $("#service_id_id").val(wasid);
    $("#price_id").val(price);
    $("#modaledit_price").openModal();
}
function editprice(id,price,desc){
    $("#pricespan_"+id).hide();
    $("#descspan_"+id).hide();
    $("#editpricediv_"+id).show();
    $("#editdescdiv_"+id).show();
    $("#price_save_"+id).click(function(){
        var price=$("#editprice_id_"+id).val();
        var desc=$("#editdesc_id_"+id).val();
        $.post(URL + "admin/price/update_price", {
            "id": id,
            "price": price,
            "desc": desc
        }, function (res) {
            $("#card-alert").hide();
            $("#priceId").attr("style","display:block");
            var result=$.parseJSON(res)
            $("#pricespan_"+id).show();
            $("#descspan_"+id).show();
            $("#pricespan_"+id).text(result.price);
            $("#descspan_"+id).text(result.desc);
            $("#editpricediv_"+id).hide();
            $("#editdescdiv_"+id).hide();
            //window.location.href = "";
        })
        
    });
}
/**
 *  Chane password for admin.....
 * @return {[type]} [description]
 */
function resetpassword(){
    var password = $('#oldpassword').val();
    var userid = $('#user_id').val();
    //alert(userid);
    $.post(URL+"admin/Dashboard/checkpassword",{
        "userid":userid,
        "password":password
    },
    function(res){
        if(res!=1){
            $('#oldpassword').addClass("invalid");
            $(".pass_error").html("*Please enter the Current and Correct Password.");
            //alert("Please enter the correct password");
            $("#oldpassword").val("");
            $("#oldpassword").focus();
        }else{
            $(".pass_error").html("");
            $('#oldpassword').removeClass("invalid");
        }
    });
}
function select_address(add_id,slug){
    if(slug=="pickupadd"){
        $("input[name^='"+slug+"']").attr("checked",false);
        $("#"+slug+add_id).attr("checked",true);
        $(".pickupadd").removeClass("cyan");
        $(".pickupadd").addClass("grey");
        $("#paddRess1_"+add_id).removeClass("grey");
        $("#paddRess1_"+add_id).addClass("cyan");
    } else{
        $("input[name^='"+slug+"']").attr("checked",false);
        $("#"+slug+add_id).attr("checked",true);
        $(".deliveryadd").removeClass("green");
        $(".deliveryadd").addClass("grey");
        $("#paddRess2_"+add_id).removeClass("grey");
        $("#paddRess2_"+add_id).addClass("green");
    }
}

// Date picker for pickup and delivery
$(function(){
    $('.datepicker_new').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year
        min: true,
        today: false,
        close: false,
        onSet: function () {
            var x;
            x = $('.datepicker_new').pickadate().val().toString();
            if(x){
                getslot(x,2);
                this.close();
            }
        }
    });
    $('.datepicker2').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year
        min: true,
        today: false,
        close: false,
        onSet: function () {
            var x,y,year,date,month;
            x = $('.datepicker2').pickadate().val().toString();
            var joindate = new Date(x);
            var numberOfDaysToAdd = 2;
            joindate.setDate(joindate.getDate() + numberOfDaysToAdd);
            var dd = joindate.getDate();
            var mm = joindate.getMonth();
            var yy = joindate.getFullYear();
            if(x){
                getslot(x,1);
                
                $('.datepicker3').pickadate({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 15, // Creates a dropdown of 15 years to control year
                    min: [yy,mm,dd],
                    clear:true,
                    today: false,
                    close: false,
                    onSet: function () {
                        var x,y,year,date,month;
                        x = $('.datepicker3').pickadate().val().toString();
                        if(x){
                            getslot(x,2);
                            this.close();
                        }
                    }
                });
                var monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                ];
                var y=dd+" "+monthNames[mm]+","+yy;
                $("#datepicker3").val(dd+" "+monthNames[mm]+","+yy);
                getslot(y,2);
                this.close();
            }
        }
    });
    var FromEndDate = new Date();
    var ToEndDate = new Date();

    ToEndDate.setDate(ToEndDate.getDate() + 365);
    $('.datepicker_from').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year
        min:true,
        today: false,
        close: false,
        onSet: function () {
            var x,y,year,date,month;
            x = $('.datepicker_from').pickadate().val().toString();
            var joindate = new Date(x);
            // var numberOfDaysToAdd = 1;
            // joindate.setDate(joindate.getDate() + numberOfDaysToAdd);
            var Y = joindate.getFullYear();
            var M = joindate.getMonth();
            var D = joindate.getDate();
            //            x = $('.datepicker_from').pickadate().val();
            //            startDate = new Date(x);
            //            var Y=startDate.getFullYear();
            //            var M=startDate.getMonth();
            //            var D=startDate.getDate();
            if(x){
                $('.datepicker_to').pickadate({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 15, // Creates a dropdown of 15 years to control year
                    min: [Y,M,D],
                    clear:true,
                    today: false,
                    close: false,
                    onSet: function () {
                        var x,y,year,date,month;
                        x = $('.datepicker_to').pickadate().val().toString();
                        y = x.split(/[ ,]+/);
                        date = y[0];
                        month = y[1];
                        year = y[2];
                        if(date && month && year){
                            $('.datepicker_from').pickadate({
                                max : true
                            });
                            this.close();
                        }
                    }
                });
                this.close();
            }
        }
    })
    $('.orderedDate').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year
        today: false,
        clear: false,
        close: false,
        onSet: function () {
            var x,y,year,date,month;
            x = $('.orderedDate').pickadate().val().toString();
            y = x.split(/[ ,]+/);
            date = y[0];
            month = y[1];
            year = y[2];
            if(date && month && year){
                getslot(x,1);
                this.close();
            }
        }
    })
});

// Set Timeslot Day wise

function getslot(date,type,selectslot){
    $.post(URL+"admin/Orders/SelectSlot",{
        "date":date
    },function(res){
        if(type==1){
            if(res==0){
                $("#SetSlot_p").html("<option value='' selected>No Time Slot Available</option>");
                $("#datepicker3").val("");
                $("#SetSlot_d").val("");
                alert("Please Choose another date");
            } else {
                $("#SetSlot_p").html(res);
                if(selectslot!=''){
                    $("#SetSlot_p").val(selectslot); 
                }
            }
        } else{
            if(res==0){
                $("#SetSlot_d").html("<option value='' selected>No Time Slot Available</option>");
                alert("Please Choose another date");
            } else {
                $("#SetSlot_d").html(res);
                if(selectslot!=''){
                    $("#SetSlot_d").val(selectslot); 
                }
            }
        }
    });
}
function getorderdetails(order_id){
    $(".load").css("display","block");
    $.post(URL+"admin/orders/getorderdetails/"+order_id+"/"+1,{
    },function(res){
        $(".load").css("display","none");
        $("#chat-out").addClass('side-nav rightside-navigation right-aligned ps-container ps-active-y');
        $("#chat-out").attr('style','width: 300px; right: 0px;height:100%');
        $(".collapsible-body").css("display","block");
        $("#chat-out").html(res);
    });
}
function getuserorderdetails(user_id){
    $(".load").css("display","block");
    $.post(URL+"admin/users/getuserorderdetails/"+user_id+"/",{
    },function(res){
        $(".load").css("display","none");
        $("#chat-out").addClass('side-nav rightside-navigation right-aligned ps-container ps-active-y');
        $("#chat-out").attr('style','width: 300px; right: 0px;height:100%');
        $(".collapsible-body").css("display","block");
        $("#chat-out").html(res);
    });
}
function getwalletdetails(user_id){
    $(".load").css("display","block");
    $.post(URL+"admin/users/getwalletdetails/"+user_id+"/",{
    },function(res){
        $(".load").css("display","none");
        $("#chat-out").addClass('side-nav rightside-navigation right-aligned ps-container ps-active-y');
        $("#chat-out").attr('style','width: 300px; right: 0px;height:100%');
        $(".collapsible-body").css("display","block");
        $("#chat-out").html(res);
    });
}
function closeview(){
    $("#chat-out").addClass('side-nav rightside-navigation right-aligned ps-container ps-active-y');
    $("#chat-out").attr('style','height:100%');
}
function orderStatusChange(status,orderid,statusLable){
    if(status==6 || status==12){
        $("#modal_reschedule").openModal();
        $("#type").val("");
        $("#orderid").val("");
        $("#type").val(status);
        $("#orderid").val(orderid);
    }
    else
    {
        $.post(URL+"admin/Orders/OrderStatusChange",{
            'status':status,
            'orderid':orderid
        },function(res){
            if (res==1) {
                $("#status"+orderid).html(statusLable);
            }
            else
            {
                alert("Please Try Again......");
            }

        });  
    }
    
}
function printorder()
{
    // var printContents = document.getElementById('printModule').innerHTML;
    // var originalContents = document.body.innerHTML;
    // document.body.innerHTML = printContents;
    window.print();
    // document.body.innerHTML = originalContents;

    // var divContents = $("#printModule").html();
    // var printWindow = window.open('', '', 'height=400,width=800');
    // printWindow.document.write('<html><head><title>DIV Contents</title>');
    // printWindow.document.write( "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" media=\"print\"/>" );
    // printWindow.document.write( "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" media=\"print\"/>" );
    // printWindow.document.write( "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" media=\"print\"/>" );
    // printWindow.document.write( "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" media=\"print\"/>" );
    // printWindow.document.write( "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" media=\"print\"/>" );
    // printWindow.document.write( "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" media=\"print\"/>" );
    // printWindow.document.write( "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" media=\"print\"/>" );
    // printWindow.document.write( "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" media=\"print\"/>" );
    // printWindow.document.write('</head><body >');
    // printWindow.document.write(divContents);
    // printWindow.document.write('</body></html>');
    // printWindow.document.close();
    // printWindow.print();
}


// $("#printModule").find('.print').on('click', function() {
//     $("#printModule").print({
//         globalStyles : false,
//         mediaPrint : false,
//         stylesheet : "http://fonts.googleapis.com/css?family=Inconsolata",
//         iframe : false,
//         noPrintSelector : ".avoid-this",
//         append : "Free jQuery Plugins<br/>",
//         prepend : "<br/>jQueryScript.net",
//         manuallyCopyFormValues: true,
//         deferred: $.Deferred(),
//         timeout: 250,
//         title: null,
//         doctype: '<!doctype html>'
//     });
// });


function editwallet(user_id){
    $("#wall"+user_id).css("display","none");
    $("#editwal"+user_id).css("display","block");
    $("#adnes"+user_id).val("");
}
function savewallet(userid,amount){
    var prevamount=$("#curupv"+userid).text();
    if(userid!='' && amount!='' && prevamount!=''){
        $.post(URL+"admin/users/savewallet",{
            'userid':userid,
            'amount':amount,
            'prevamount':prevamount
        },function(res){
            if (res!='error') {
                $("#curupv"+userid).html(res);
                $("#wall"+userid).css("display","block");
                $("#editwal"+userid).css("display","none");
            }
            else
            {
                alert("Please Try Again......");
            }

        }); 
    }else{
        alert("Please Try Again......");
    }
}
function price_edit(id){
    $("#price"+id).css("display","none");
    $("#editpri"+id).css("display","block");
}
function save_price(id,price){
    $.post(URL+"admin/offers/saveprice",{
        'id':id,
        'price':price
    },function(res){
        //alert(res);
        if (res!='error') {
            $("#price"+id).html(price);
            $("#price"+id).css("display","block");
            $("#editpri"+id).css("display","none");
        }
        else
        {
            alert("Please Try Again......");
        }

    }); 
}
function AutoSearch(userCondition,data){
    var condition='name';
    $.post(URL+"admin/orders/finduser",{
        "condition":userCondition,
        "data":data
    },function(res){

    });

}
function orderpayment(orderId, userId, price, wallet){
    $("#ordPrc").html(price);
    $("#totalPrice").val(price);
    $("#waletamnt").html(wallet);
    $("#cusTmerid").val(userId);
    $("#ORder_ID").val(orderId);
    $("#modal_payment").openModal();
}
function viewmodal(id) {
    $(".load").css("display","block");
    $.ajax({
        type:'POST',
        url:URL+'admin/Coupons/viewcoupon',
        data:{
            'id':id
        },
        success:function(res){
            $("#couponDetals").openModal();
            $("#couponDetals").html(res);
            $(".load").css("display","none");
        }
    });
    
}
function closemodal(){
    $("#couponDetals").closeModal();
}
function couponcount(total_price,discount){
    var discount=total_price*(discount/100);
    $("#discount").html(discount);
    var total_price=total_price-discount;
    $("#total_price").html(total_price);
}
function addressDelete(address_id){
    $.post(URL+"admin/users/address_delete",{
        "address_id":address_id
    },function(res){
        if(res==1){
            $("#Add"+address_id).attr("style","display:none;");
        }

    });
}
function searchUser(){
 
}
function buttonDisable(){
    //alert("buttonDisable");
    $("#searchBtn").attr("disabled","disabled");
    $("#ordersearch").submit();
    //$("#searchBtn").attr("disable","disable");
}
function submitDisable(){
    $("#addservice").submit(function(){
        if($("#first_input2").val() == ""){
            $("#first_input2").addClass("invalid");
            $("#first_input2").focus();
            return false;
        } else if($("#sicon").val() == ""){
            $("#sicon").addClass("invalid");
            $("#sicon").focus();
            return false;
        } else {
            $("#usersubmit").attr("disabled","disabled");
        }
    });
}
function addClothid(cloth_id){
    $("#clothId").val(cloth_id);
    $("#addimg").openModal();
}
function validate_emailid(email){
    if(email){
        $.post(URL+"admin/adminusers/checkemail",{
            "email":email
        },function(res){
            if(res!=0){
                $("#checkemailid").val("");
                $("#divCheckEmail").html("Email Id is already registered");
                $("#checkemailid").focus();
            }else{
                $("#divCheckEmail").empty(); 
            }
        });
    }
}
function addreadonly(id){
    $(".clsreadonly").prop("readonly","readonly");
    window.location.href=URL+"admin/adminusers/adduser/"+id;
}

$('.clsPin').on('keyup', function(event) {
     if(this.value.length==this.maxLength){
         //alert($(".clsPin").val());
         $(this).next('.clsPin').focus();
         $.tabNext();
         //$(".clsPin").next().focus();
        // $(this).trigger("keypress",[9]);
     }
});

