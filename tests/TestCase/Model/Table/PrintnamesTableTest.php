<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PrintnamesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PrintnamesTable Test Case
 */
class PrintnamesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PrintnamesTable
     */
    public $Printnames;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.printnames'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Printnames') ? [] : ['className' => 'App\Model\Table\PrintnamesTable'];
        $this->Printnames = TableRegistry::get('Printnames', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Printnames);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
