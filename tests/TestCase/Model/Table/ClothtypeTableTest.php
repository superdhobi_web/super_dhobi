<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClothtypeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClothtypeTable Test Case
 */
class ClothtypeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClothtypeTable
     */
    public $Clothtype;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.clothtype'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Clothtype') ? [] : ['className' => 'App\Model\Table\ClothtypeTable'];
        $this->Clothtype = TableRegistry::get('Clothtype', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Clothtype);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
