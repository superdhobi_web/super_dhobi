<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmployeeTasksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmployeeTasksTable Test Case
 */
class EmployeeTasksTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmployeeTasksTable
     */
    public $EmployeeTasks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.employee_tasks',
        'app.orders',
        'app.clothtypes',
        'app.services',
        'app.trackings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EmployeeTasks') ? [] : ['className' => 'App\Model\Table\EmployeeTasksTable'];
        $this->EmployeeTasks = TableRegistry::get('EmployeeTasks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmployeeTasks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
