<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NoticeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NoticeTable Test Case
 */
class NoticeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NoticeTable
     */
    public $Notice;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.notice'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Notice') ? [] : ['className' => 'App\Model\Table\NoticeTable'];
        $this->Notice = TableRegistry::get('Notice', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Notice);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
