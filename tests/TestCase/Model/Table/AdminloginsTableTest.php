<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdminloginsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdminloginsTable Test Case
 */
class AdminloginsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AdminloginsTable
     */
    public $Adminlogins;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.adminlogins'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Adminlogins') ? [] : ['className' => 'App\Model\Table\AdminloginsTable'];
        $this->Adminlogins = TableRegistry::get('Adminlogins', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Adminlogins);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
