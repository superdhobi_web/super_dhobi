<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FaqcatsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FaqcatsTable Test Case
 */
class FaqcatsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FaqcatsTable
     */
    public $Faqcats;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.faqcats'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Faqcats') ? [] : ['className' => 'App\Model\Table\FaqcatsTable'];
        $this->Faqcats = TableRegistry::get('Faqcats', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Faqcats);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
