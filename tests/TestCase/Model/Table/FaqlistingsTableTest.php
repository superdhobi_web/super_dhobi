<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FaqlistingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FaqlistingsTable Test Case
 */
class FaqlistingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FaqlistingsTable
     */
    public $Faqlistings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.faqlistings',
        'app.cats'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Faqlistings') ? [] : ['className' => 'App\Model\Table\FaqlistingsTable'];
        $this->Faqlistings = TableRegistry::get('Faqlistings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Faqlistings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
