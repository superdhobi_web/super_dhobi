<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TimeSlotsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TimeSlotsTable Test Case
 */
class TimeSlotsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TimeSlotsTable
     */
    public $TimeSlots;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.time_slots'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TimeSlots') ? [] : ['className' => 'App\Model\Table\TimeSlotsTable'];
        $this->TimeSlots = TableRegistry::get('TimeSlots', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TimeSlots);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
